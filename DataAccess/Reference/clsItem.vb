﻿Imports DataAccess.My.Resources

Namespace Reference
    Public Class clsItem
        Public oConnection As Setting.clsConnectionMain = Nothing
        Public oError As Setting.clsError = Nothing


        Public sMODUL As String = ""
        Public sREFERENCE As String = ""
        Public sSTATUS As String = ""
        Public sLASTNUMBER As Integer = 0

        Public oCounter As Setting.clsCounter = Nothing




        Public Sub New(Optional ByVal sConnection As String = "")
            If sConnection = "" Then
                oConnection = New Setting.clsConnectionMain
                oError = New Setting.clsError
                oCounter = New Setting.clsCounter
            Else
                oConnection = New Setting.clsConnectionMain("TAX")
                oError = New Setting.clsError("TAX")
                oCounter = New Setting.clsCounter("TAX")
            End If

            sMODUL = "ITEM"

        End Sub
        Protected Overrides Sub Finalize()
            MyBase.Finalize()
            oConnection = Nothing
            GC.SuppressFinalize(Me)
        End Sub
        Public Function GetStructureHeader() As M_ITEM
            If Not oConnection.GetConnection() Then
                GetStructureHeader = Nothing
            End If
            GetStructureHeader = New M_ITEM
        End Function
        Public Function GetStructureDetail_UOM() As M_ITEM_UOM
            If Not oConnection.GetConnection() Then
                GetStructureDetail_UOM = Nothing
            End If
            GetStructureDetail_UOM = New M_ITEM_UOM
        End Function
        Public Function GetStructureDetail_UOMList() As List(Of M_ITEM_UOM)
            If Not oConnection.GetConnection() Then
                GetStructureDetail_UOMList = Nothing
            End If
            GetStructureDetail_UOMList = New List(Of M_ITEM_UOM)
        End Function
        Public Function GetStructureDetail_WAREHOUSE() As M_ITEM_WAREHOUSE
            If Not oConnection.GetConnection() Then
                GetStructureDetail_WAREHOUSE = Nothing
            End If
            GetStructureDetail_WAREHOUSE = New M_ITEM_WAREHOUSE
        End Function
        Public Function GetStructureDetail_WAREHOUSEList() As List(Of M_ITEM_WAREHOUSE)
            If Not oConnection.GetConnection() Then
                GetStructureDetail_WAREHOUSEList = Nothing
            End If
            GetStructureDetail_WAREHOUSEList = New List(Of M_ITEM_WAREHOUSE)
        End Function
        Public Function GetData() As List(Of M_ITEM)
            If Not oConnection.GetConnection() Then
                GetData = Nothing
                Exit Function
            End If
            GetData = oConnection.db.M_ITEMs.OrderBy(Function(x) x.NMITEM1).ToList()
        End Function
        Public Function GetData(ByVal sKDITEM As String) As M_ITEM
            If Not oConnection.GetConnection() Then
                GetData = Nothing
                Exit Function
            End If
            GetData = oConnection.db.M_ITEMs.FirstOrDefault(Function(x) x.KDITEM = sKDITEM)
        End Function
        Public Function GetDataDetail_UOM() As List(Of M_ITEM_UOM)
            If Not oConnection.GetConnection() Then
                GetDataDetail_UOM = Nothing
                Exit Function
            End If
            GetDataDetail_UOM = oConnection.db.M_ITEM_UOMs.ToList()
        End Function
        Public Function GetDataDetail_UOM(ByVal sKDITEM As String) As List(Of M_ITEM_UOM)
            If Not oConnection.GetConnection() Then
                GetDataDetail_UOM = Nothing
                Exit Function
            End If
            GetDataDetail_UOM = oConnection.db.M_ITEM_UOMs.Where(Function(x) x.KDITEM = sKDITEM).ToList()
        End Function
        Public Function GetDataDetail_UOM(ByVal sKDITEM As String, ByVal sKDUOM As String) As M_ITEM_UOM
            If Not oConnection.GetConnection() Then
                GetDataDetail_UOM = Nothing
                Exit Function
            End If
            GetDataDetail_UOM = oConnection.db.M_ITEM_UOMs.FirstOrDefault(Function(x) x.KDITEM = sKDITEM And x.KDUOM = sKDUOM)
        End Function
        Public Function GetDataDetail_WAREHOUSE() As List(Of M_ITEM_WAREHOUSE)
            If Not oConnection.GetConnection() Then
                GetDataDetail_WAREHOUSE = Nothing
                Exit Function
            End If
            GetDataDetail_WAREHOUSE = oConnection.db.M_ITEM_WAREHOUSEs.ToList()
        End Function
        Public Function GetDataDetail_WAREHOUSE(ByVal sKDITEM As String) As List(Of M_ITEM_WAREHOUSE)
            If Not oConnection.GetConnection() Then
                GetDataDetail_WAREHOUSE = Nothing
                Exit Function
            End If
            GetDataDetail_WAREHOUSE = oConnection.db.M_ITEM_WAREHOUSEs.Where(Function(x) x.KDITEM = sKDITEM).ToList
        End Function
        Public Function GetDataDetail_WAREHOUSE(ByVal sKDITEM As String, ByVal sKDWAREHOUSE As String) As List(Of M_ITEM_WAREHOUSE)
            If Not oConnection.GetConnection() Then
                GetDataDetail_WAREHOUSE = Nothing
                Exit Function
            End If
            GetDataDetail_WAREHOUSE = oConnection.db.M_ITEM_WAREHOUSEs.Where(Function(x) x.KDITEM = sKDITEM And x.KDWAREHOUSE = sKDWAREHOUSE).ToList
        End Function
        Public Function GetDataDetail_WAREHOUSE(ByVal sKDITEM As String, ByVal sKDWAREHOUSE As String, ByVal sKDUOM As String) As M_ITEM_WAREHOUSE
            If Not oConnection.GetConnection() Then
                GetDataDetail_WAREHOUSE = Nothing
                Exit Function
            End If
            GetDataDetail_WAREHOUSE = oConnection.db.M_ITEM_WAREHOUSEs.FirstOrDefault(Function(x) x.KDITEM = sKDITEM And x.KDWAREHOUSE = sKDWAREHOUSE And x.KDUOM = sKDUOM)
        End Function
        Public Function GetDataRate(ByVal sKDITEM As String, ByVal sKDUOM As String) As Decimal
            If Not oConnection.GetConnection() Then
                GetDataRate = Nothing
                Exit Function
            End If
            GetDataRate = oConnection.db.M_ITEM_UOMs.FirstOrDefault(Function(x) x.KDITEM = sKDITEM And x.KDUOM = sKDUOM).RATE
        End Function
        Public Function IsExist(ByVal sNMITEM2 As String) As Boolean
            If Not oConnection.GetConnection() Then
                IsExist = False
                Exit Function
            End If

            Dim ds = oConnection.db.M_ITEMs.FirstOrDefault(Function(x) x.NMITEM2 = sNMITEM2)

            If ds IsNot Nothing Then
                IsExist = True
            Else
                IsExist = False
            End If
        End Function
        Public Function InsertData(ByVal entity As M_ITEM, ByVal entityDetail_UOM As List(Of M_ITEM_UOM), Optional ByVal isAuto As Boolean = True) As Boolean
            Try
                If Not oConnection.GetConnection() Then
                    InsertData = False
                    Exit Function
                End If

                sREFERENCE = entity.KDITEM
                sSTATUS = "INSERT"


                If isAuto = True Then
                    'Generate Auto Number

                    Try
                        Try
                            sLASTNUMBER = oCounter.GetLastNumberBarcoder
                        Catch ex As Exception
                            sLASTNUMBER = 0
                        End Try

                        If sLASTNUMBER = 0 Then
                            Try
                                oCounter.InsertDataBarcode()
                                sLASTNUMBER = oCounter.GetLastNumberBarcoder()
                            Catch ex As Exception
                                sLASTNUMBER = 0
                            End Try
                        End If
                        entity.NMITEM1 = AutoNumberCodeBarcode(sLASTNUMBER + 1)
                    Catch ex As Exception
                        oError.InsertData("ITEM", "INSERTDATA", ex.ToString, sREFERENCE)
                        Throw ex
                    End Try
                    Try
                        oCounter.UpdateDataBarcode(sLASTNUMBER + 1)
                    Catch ex As Exception
                        oError.InsertData("ITEM", "INSERTDATA", ex.ToString, sREFERENCE)
                        Throw ex
                    End Try
                    'End Generate
                End If

                'Generate Auto Number
                Try
                    sLASTNUMBER = CInt(oConnection.db.M_ITEMs.Where(Function(x) x.KDITEM.Contains(sMODUL)).OrderByDescending(Function(x) x.KDITEM).FirstOrDefault().KDITEM.Remove(0, (sMODUL & " _ ").Length)) + 1
                Catch ex As Exception
                    sLASTNUMBER = 1
                End Try
                'End Generate


                Try
                    entity.KDITEM = sMODUL & "_" & AutoNumberCode(sLASTNUMBER)
                    oConnection.db.M_ITEMs.InsertOnSubmit(entity)
                Catch ex As Exception
                    oError.InsertData(sMODUL, sSTATUS, ex.ToString, sREFERENCE)
                    Throw ex
                End Try
                Try
                    If entityDetail_UOM IsNot Nothing Then
                        For Each iLoop In entityDetail_UOM
                            iLoop.KDITEM = sMODUL & "_" & AutoNumberCode(sLASTNUMBER)
                        Next

                        oConnection.db.M_ITEM_UOMs.InsertAllOnSubmit(entityDetail_UOM)
                    End If
                Catch ex As Exception
                    oError.InsertData(sMODUL, sSTATUS, ex.ToString, sREFERENCE)
                    Throw ex
                End Try
                Try
                    oConnection.db.SubmitChanges()
                Catch ex As Exception
                    oError.InsertData(sMODUL, sSTATUS, ex.ToString, sREFERENCE)
                    Throw ex
                End Try

                InsertData = True
            Catch ex As Exception
                InsertData = False
                oError.InsertData(sMODUL, sSTATUS, ex.ToString, sREFERENCE)
                Throw ex
            End Try
        End Function
        Public Function UpdateData(ByVal entity As M_ITEM, ByVal entityDetail_UOM As List(Of M_ITEM_UOM)) As Boolean
            Try
                If Not oConnection.GetConnection() Then
                    UpdateData = False
                    Exit Function
                End If

                sREFERENCE = entity.KDITEM
                sSTATUS = "UPDATE"

                Dim ds = oConnection.db.M_ITEMs.FirstOrDefault(Function(x) x.KDITEM = entity.KDITEM)

                Try
                    oConnection.db.M_ITEMs.DeleteOnSubmit(ds)
                    oConnection.db.M_ITEMs.InsertOnSubmit(entity)
                Catch ex As Exception
                    oError.InsertData(sMODUL, sSTATUS, ex.ToString, sREFERENCE)
                    Throw ex
                End Try

                Dim dsDetail_UOM = oConnection.db.M_ITEM_UOMs.Where(Function(x) x.KDITEM = entity.KDITEM)

                Try
                    oConnection.db.M_ITEM_UOMs.DeleteAllOnSubmit(dsDetail_UOM)
                    oConnection.db.M_ITEM_UOMs.InsertAllOnSubmit(entityDetail_UOM)
                Catch ex As Exception
                    oError.InsertData(sMODUL, sSTATUS, ex.ToString, sREFERENCE)
                    Throw ex
                End Try
                Try
                    oConnection.db.SubmitChanges()
                Catch ex As Exception
                    oError.InsertData(sMODUL, sSTATUS, ex.ToString, sREFERENCE)
                    Throw ex
                End Try

                UpdateData = True
            Catch ex As Exception
                UpdateData = False
                oError.InsertData(sMODUL, sSTATUS, ex.ToString, sREFERENCE)
                Throw ex
            End Try
        End Function
        Public Function DeleteData(ByVal sKDITEM As String) As Boolean
            Try
                If Not oConnection.GetConnection() Then
                    DeleteData = False
                    Exit Function
                End If

                sREFERENCE = sKDITEM
                sSTATUS = "DELETE"

                Dim ds = oConnection.db.M_ITEMs.FirstOrDefault(Function(x) x.KDITEM = sKDITEM)
                Dim dsDetail_UOM = oConnection.db.M_ITEM_UOMs.Where(Function(x) x.KDITEM = sKDITEM)

                Try
                    oConnection.db.M_ITEMs.DeleteOnSubmit(ds)
                    oConnection.db.M_ITEM_UOMs.DeleteAllOnSubmit(dsDetail_UOM)
                Catch ex As Exception
                    oError.InsertData(sMODUL, sSTATUS, ex.ToString, sREFERENCE)
                    Throw ex
                End Try

                Try
                    oConnection.db.SubmitChanges()
                Catch ex As Exception
                    oError.InsertData(sMODUL, sSTATUS, ex.ToString, sREFERENCE)
                    Throw ex
                End Try

                DeleteData = True
            Catch ex As Exception
                DeleteData = False
                oError.InsertData(sMODUL, sSTATUS, ex.ToString, sREFERENCE)
                Throw ex
            End Try
        End Function
        Public Function AccountInventoryDefault() As String
            Try
                If Not oConnection.GetConnection() Then
                    AccountInventoryDefault = String.Empty
                    Exit Function
                End If

                Dim ds = oConnection.db.SET_SETTINGs.FirstOrDefault().COA_ITEM_INVENTORY
                Try
                    AccountInventoryDefault = ds
                Catch ex As Exception
                    AccountInventoryDefault = String.Empty
                End Try
            Catch ex As Exception
                AccountInventoryDefault = String.Empty
                Throw ex
            End Try
        End Function
        Public Function AccountSalesDefault() As String
            Try
                If Not oConnection.GetConnection() Then
                    AccountSalesDefault = String.Empty
                    Exit Function
                End If

                Dim ds = oConnection.db.SET_SETTINGs.FirstOrDefault().COA_ITEM_SALES
                Try
                    AccountSalesDefault = ds
                Catch ex As Exception
                    AccountSalesDefault = String.Empty
                End Try
            Catch ex As Exception
                AccountSalesDefault = String.Empty
                Throw ex
            End Try
        End Function
        Public Function AccountCostDefault() As String
            Try
                If Not oConnection.GetConnection() Then
                    AccountCostDefault = String.Empty
                    Exit Function
                End If

                Dim ds = oConnection.db.SET_SETTINGs.FirstOrDefault().COA_ITEM_HPP
                Try
                    AccountCostDefault = ds
                Catch ex As Exception
                    AccountCostDefault = String.Empty
                End Try
            Catch ex As Exception
                AccountCostDefault = String.Empty
                Throw ex
            End Try
        End Function
        Public Function DefaultItem_L1() As String
            Try
                If Not oConnection.GetConnection() Then
                    DefaultItem_L1 = String.Empty
                    Exit Function
                End If

                Dim ds = oConnection.db.M_ITEM_L1s.FirstOrDefault(Function(x) x.ISDEFAULT = True)
                Try
                    DefaultItem_L1 = ds.KDITEM_L1
                Catch ex As Exception
                    DefaultItem_L1 = String.Empty
                End Try
            Catch ex As Exception
                DefaultItem_L1 = String.Empty
                Throw ex
            End Try
        End Function
        Public Function DefaultItem_L2() As String
            Try
                If Not oConnection.GetConnection() Then
                    DefaultItem_L2 = String.Empty
                    Exit Function
                End If

                Dim ds = oConnection.db.M_ITEM_L2s.FirstOrDefault(Function(x) x.ISDEFAULT = True)
                Try
                    DefaultItem_L2 = ds.KDITEM_L2
                Catch ex As Exception
                    DefaultItem_L2 = String.Empty
                End Try
            Catch ex As Exception
                DefaultItem_L2 = String.Empty
                Throw ex
            End Try
        End Function
        Public Function DefaultItem_L3() As String
            Try
                If Not oConnection.GetConnection() Then
                    DefaultItem_L3 = String.Empty
                    Exit Function
                End If

                Dim ds = oConnection.db.M_ITEM_L3s.FirstOrDefault(Function(x) x.ISDEFAULT = True)
                Try
                    DefaultItem_L3 = ds.KDITEM_L3
                Catch ex As Exception
                    DefaultItem_L3 = String.Empty
                End Try
            Catch ex As Exception
                DefaultItem_L3 = String.Empty
                Throw ex
            End Try
        End Function
        Public Function DefaultItem_L4() As String
            Try
                If Not oConnection.GetConnection() Then
                    DefaultItem_L4 = String.Empty
                    Exit Function
                End If

                Dim ds = oConnection.db.M_ITEM_L4s.FirstOrDefault(Function(x) x.ISDEFAULT = True)
                Try
                    DefaultItem_L4 = ds.KDITEM_L4
                Catch ex As Exception
                    DefaultItem_L4 = String.Empty
                End Try
            Catch ex As Exception
                DefaultItem_L4 = String.Empty
                Throw ex
            End Try
        End Function
        Public Function DefaultItem_L5() As String
            Try
                If Not oConnection.GetConnection() Then
                    DefaultItem_L5 = String.Empty
                    Exit Function
                End If

                Dim ds = oConnection.db.M_ITEM_L5s.FirstOrDefault(Function(x) x.ISDEFAULT = True)
                Try
                    DefaultItem_L5 = ds.KDITEM_L5
                Catch ex As Exception
                    DefaultItem_L5 = String.Empty
                End Try
            Catch ex As Exception
                DefaultItem_L5 = String.Empty
                Throw ex
            End Try
        End Function
        Public Function DefaultItem_L6() As String
            Try
                If Not oConnection.GetConnection() Then
                    DefaultItem_L6 = String.Empty
                    Exit Function
                End If

                Dim ds = oConnection.db.M_ITEM_L6s.FirstOrDefault(Function(x) x.ISDEFAULT = True)
                Try
                    DefaultItem_L6 = ds.KDITEM_L6
                Catch ex As Exception
                    DefaultItem_L6 = String.Empty
                End Try
            Catch ex As Exception
                DefaultItem_L6 = String.Empty
                Throw ex
            End Try
        End Function


    End Class
End Namespace