﻿Imports System.Data
Imports System.Data.SqlClient

Namespace Accounting
    Public Class clsStockCard
        Public oConnection As Setting.clsConnectionMain = Nothing
        Public oError As Setting.clsError = Nothing
        Public oCounter As Setting.clsCounter = Nothing

        Public sMODUL As String = ""
        Public sREFERENCE As String = ""
        Public sSTATUS As String = ""
        Public sLASTNUMBER As Integer = 0

        Public Sub New(Optional ByVal sConnection As String = "")
            If sConnection = "" Then
                oConnection = New Setting.clsConnectionMain
                oError = New Setting.clsError
                oCounter = New Setting.clsCounter
            Else
                oConnection = New Setting.clsConnectionMain("TAX")
                oError = New Setting.clsError("TAX")
                oCounter = New Setting.clsCounter("TAX")
            End If

            sMODUL = "STOCKCARD"
        End Sub
        Protected Overrides Sub Finalize()
            MyBase.Finalize()
            oConnection = Nothing
            GC.SuppressFinalize(Me)
        End Sub
        Public Function GetStructureHeader() As A_STOCK_CARD
            If Not oConnection.GetConnection() Then
                GetStructureHeader = Nothing
            End If
            GetStructureHeader = New A_STOCK_CARD
        End Function
        Public Function GetData() As List(Of A_STOCK_CARD)
            If Not oConnection.GetConnection() Then
                GetData = Nothing
                Exit Function
            End If
            GetData = oConnection.db.A_STOCK_CARDs.ToList()
        End Function
        Public Function GetData(ByVal sKDITEM As String) As IQueryable(Of A_STOCK_CARD)
            If Not oConnection.GetConnection() Then
                GetData = Nothing
                Exit Function
            End If
            GetData = oConnection.db.A_STOCK_CARDs.Where(Function(x) x.KDITEM = sKDITEM)
        End Function
        Public Function GetData(ByVal sKDITEM As String, ByVal sKDUOM As String) As IQueryable(Of A_STOCK_CARD)
            If Not oConnection.GetConnection() Then
                GetData = Nothing
                Exit Function
            End If
            GetData = oConnection.db.A_STOCK_CARDs.Where(Function(x) x.KDITEM = sKDITEM And x.KDUOM = sKDUOM)
        End Function
        Public Function PostingAverage(ByVal sKDITEM As List(Of String), Optional ByVal sTotal As Decimal = 0) As Boolean
            Try
                If Not oConnection.GetConnection() Then
                    PostingAverage = False
                    Exit Function
                End If

                For Each xLoop In sKDITEM.Distinct
                    sREFERENCE = xLoop
                    sSTATUS = "POSTINGAVERAGE"

                    Dim Average As Decimal = 0
                    Dim QtyBalance As Decimal = 0
                    Dim PriceBalance As Decimal = 0
                    Dim TotalBalance As Decimal = 0
                    Dim sSeq = 1
                    Dim sItem = xLoop

                    Dim oConn As New SqlConnection
                    Dim oComm As New SqlCommand
                    Dim da As SqlDataAdapter
                    Dim ds As New DataSet
                    Dim SQL As String

                    Dim sConn As String = Decrypt(My.Computer.Registry.GetValue("HKEY_CURRENT_USER\Software\BUDIJAYAGROUP\SW\", "Database", "").ToString())

                    oConn = New SqlConnection(sConn)
                    If oConn.State = ConnectionState.Closed Then
                        oConn.Open()
                    End If

                    SQL = "SELECT * FROM ("
                    SQL &= "(SELECT "
                    SQL &= "NOID = A.KDPI "
                    SQL &= ",NOREF = A.KDPI "
                    SQL &= ",[DATE] = A.DATE "
                    SQL &= ",KDITEM = B.KDITEM "
                    SQL &= ",KDUOM = B.KDUOM "
                    SQL &= ",QTY = B.QTY * C.RATE "
                    'SQL &= ",HPP = (B.GRANDTOTAL / (B.QTY * C.RATE)) - (A.DISCOUNT / (B.QTY * C.RATE)) + (A.TAX / (B.QTY * C.RATE)) "
                    'SQL &= ",TOTAL = (B.GRANDTOTAL / (B.QTY * C.RATE)) - (A.DISCOUNT / (B.QTY * C.RATE)) + (A.TAX / (B.QTY * C.RATE)) * (B.QTY * C.RATE) "
                    SQL &= ",HPP = (B.GRANDTOTAL / (B.QTY * C.RATE)) - (A.DISCOUNT / (SELECT SUM(AA.QTY * BB.RATE) FROM P_PI_D AA INNER JOIN M_ITEM_UOM BB ON AA.KDITEM = BB.KDITEM AND AA.KDUOM = BB.KDUOM WHERE AA.KDPI = A.KDPI)) + (A.TAX / (SELECT SUM(AA.QTY * BB.RATE) FROM P_PI_D AA INNER JOIN M_ITEM_UOM BB ON AA.KDITEM = BB.KDITEM AND AA.KDUOM = BB.KDUOM WHERE AA.KDPI = A.KDPI)) "
                    SQL &= ",TOTAL = (B.GRANDTOTAL / (B.QTY * C.RATE)) - (A.DISCOUNT / (SELECT SUM(AA.QTY * BB.RATE) FROM P_PI_D AA INNER JOIN M_ITEM_UOM BB ON AA.KDITEM = BB.KDITEM AND AA.KDUOM = BB.KDUOM WHERE AA.KDPI = A.KDPI)) + (A.TAX / (SELECT SUM(AA.QTY * BB.RATE) FROM P_PI_D AA INNER JOIN M_ITEM_UOM BB ON AA.KDITEM = BB.KDITEM AND AA.KDUOM = BB.KDUOM WHERE AA.KDPI = A.KDPI)) * (B.QTY * C.RATE) "
                    SQL &= ",TIPE = '001' "
                    SQL &= ",SEQREF = B.SEQ "
                    SQL &= "FROM P_PI_H A "
                    SQL &= "INNER JOIN P_PI_D B "
                    SQL &= "ON B.KDPI = A.KDPI "
                    SQL &= "INNER JOIN M_ITEM_UOM C "
                    SQL &= "ON B.KDITEM = C.KDITEM AND B.KDUOM = C.KDUOM "
                    SQL &= "WHERE B.KDITEM = '" & xLoop & "'"
                    SQL &= ") "
                    SQL &= "UNION "
                    SQL &= "(SELECT "
                    SQL &= "NOID = A.KDPR "
                    SQL &= ",NOREF = A.KDPR "
                    SQL &= ",[DATE] = A.DATE "
                    SQL &= ",KDITEM = B.KDITEM "
                    SQL &= ",KDUOM = B.KDUOM "
                    SQL &= ",QTY = B.QTY * C.RATE "
                    SQL &= ",HPP = (B.GRANDTOTAL / (B.QTY * C.RATE)) - (A.DISCOUNT / (SELECT SUM(AA.QTY * BB.RATE) FROM P_PR_D AA INNER JOIN M_ITEM_UOM BB ON AA.KDITEM = BB.KDITEM AND AA.KDUOM = BB.KDUOM WHERE AA.KDPR = A.KDPR)) + (A.TAX / (SELECT SUM(AA.QTY * BB.RATE) FROM P_PR_D AA INNER JOIN M_ITEM_UOM BB ON AA.KDITEM = BB.KDITEM AND AA.KDUOM = BB.KDUOM WHERE AA.KDPR = A.KDPR)) "
                    SQL &= ",TOTAL = (B.GRANDTOTAL / (B.QTY * C.RATE)) - (A.DISCOUNT / (SELECT SUM(AA.QTY * BB.RATE) FROM P_PR_D AA INNER JOIN M_ITEM_UOM BB ON AA.KDITEM = BB.KDITEM AND AA.KDUOM = BB.KDUOM WHERE AA.KDPR = A.KDPR)) + (A.TAX / (SELECT SUM(AA.QTY * BB.RATE) FROM P_PR_D AA INNER JOIN M_ITEM_UOM BB ON AA.KDITEM = BB.KDITEM AND AA.KDUOM = BB.KDUOM WHERE AA.KDPR = A.KDPR)) * (B.QTY * C.RATE) "
                    SQL &= ",TIPE = '002' "
                    SQL &= ",SEQREF = B.SEQ "
                    SQL &= "FROM P_PR_H A "
                    SQL &= "INNER JOIN P_PR_D B "
                    SQL &= "ON B.KDPR = A.KDPR "
                    SQL &= "INNER JOIN M_ITEM_UOM C "
                    SQL &= "ON B.KDITEM = C.KDITEM AND B.KDUOM = C.KDUOM "
                    SQL &= "WHERE B.KDITEM = '" & xLoop & "'"
                    SQL &= ") "
                    SQL &= "UNION "
                    SQL &= "(SELECT "
                    SQL &= "NOID = A.KDSI "
                    SQL &= ",NOREF = A.KDSI "
                    SQL &= ",[DATE] = A.DATE "
                    SQL &= ",KDITEM = B.KDITEM "
                    SQL &= ",KDUOM = B.KDUOM "
                    SQL &= ",QTY = B.QTY * C.RATE "
                    SQL &= ",HPP = 0 "
                    SQL &= ",TOTAL = 0 "
                    SQL &= ",TIPE = '003' "
                    SQL &= ",SEQREF = B.SEQ "
                    SQL &= "FROM S_SI_H A "
                    SQL &= "INNER JOIN S_SI_D B "
                    SQL &= "ON B.KDSI = A.KDSI "
                    SQL &= "INNER JOIN M_ITEM_UOM C "
                    SQL &= "ON B.KDITEM = C.KDITEM AND B.KDUOM = C.KDUOM "
                    SQL &= "WHERE B.KDITEM = '" & xLoop & "'"
                    SQL &= ") "
                    SQL &= "UNION "
                    SQL &= "(SELECT "
                    SQL &= "NOID = A.KDSR "
                    SQL &= ",NOREF = A.KDSR "
                    SQL &= ",[DATE] = A.DATE "
                    SQL &= ",KDITEM = B.KDITEM "
                    SQL &= ",KDUOM = B.KDUOM "
                    SQL &= ",QTY = B.QTY * C.RATE "
                    SQL &= ",HPP = 0 "
                    SQL &= ",TOTAL = 0 "
                    SQL &= ",TIPE = '004' "
                    SQL &= ",SEQREF = B.SEQ "
                    SQL &= "FROM S_SR_H A "
                    SQL &= "INNER JOIN S_SR_D B "
                    SQL &= "ON B.KDSR = A.KDSR "
                    SQL &= "INNER JOIN M_ITEM_UOM C "
                    SQL &= "ON B.KDITEM = C.KDITEM AND B.KDUOM = C.KDUOM "
                    SQL &= "WHERE B.KDITEM = '" & xLoop & "'"
                    SQL &= ") "
                    SQL &= "UNION "
                    SQL &= "(SELECT "
                    SQL &= "NOID = A.KDOPNAME "
                    SQL &= ",NOREF = A.KDOPNAME "
                    SQL &= ",[DATE] = A.DATE "
                    SQL &= ",KDITEM = B.KDITEM "
                    SQL &= ",KDUOM = B.KDUOM "
                    SQL &= ",QTY = B.QTY * C.RATE "
                    SQL &= ",HPP = 0 "
                    SQL &= ",TOTAL = 0 "
                    SQL &= ",TIPE = '000' "
                    SQL &= ",SEQREF = B.SEQ "
                    SQL &= "FROM I_OPNAME_H A "
                    SQL &= "INNER JOIN I_OPNAME_D B "
                    SQL &= "ON B.KDOPNAME = A.KDOPNAME "
                    SQL &= "INNER JOIN M_ITEM_UOM C "
                    SQL &= "ON B.KDITEM = C.KDITEM AND B.KDUOM = C.KDUOM "
                    SQL &= "WHERE B.KDITEM = '" & xLoop & "'"
                    SQL &= ") "
                    SQL &= ") AS A "
                    SQL &= "ORDER BY CONVERT(VARCHAR(10), [DATE], 112), TIPE, SEQREF  "

                    Try
                        Dim dsClear = GetData(sItem)
                        oConnection.db.A_STOCK_CARDs.DeleteAllOnSubmit(dsClear)
                        oConnection.db.SubmitChanges()
                    Catch ex As Exception
                        oError.InsertData(sMODUL, sSTATUS, ex.ToString, sREFERENCE)
                        Throw ex
                    End Try

                    oComm.Connection = oConn
                    oComm.CommandText = SQL
                    oComm.CommandTimeout = 120
                    oComm.CommandType = CommandType.Text

                    da = New SqlDataAdapter(oComm)
                    da.Fill(ds, "ALL")

                    If ds.Tables("ALL").Rows.Count < 1 Then
                        Continue For
                    End If

                    For iLoop As Integer = 0 To ds.Tables("ALL").Rows.Count - 1
                        Dim sSQL = "INSERT INTO A_STOCK_CARD VALUES "

                        With ds.Tables("ALL")
                            Dim sTOTALIN As Decimal = 0
                            Dim sTOTALOUT As Decimal = 0

                            sSQL &= "("
                            sSQL &= "'" & .Rows(iLoop)("NOID") & "',"
                            sSQL &= "'" & CDate(.Rows(iLoop)("DATE")).ToString("yyyy-MM-dd HH:mm:ss") & "',"
                            sSQL &= "'" & .Rows(iLoop)("KDITEM") & "',"
                            sSQL &= "'" & .Rows(iLoop)("KDUOM") & "',"

                            If .Rows(iLoop)("TIPE") = "001" Then 'PI
                                sSQL &= "" & .Rows(iLoop)("QTY") & ","
                                sSQL &= "" & .Rows(iLoop)("HPP") & ","
                                sSQL &= "" & .Rows(iLoop)("QTY") * .Rows(iLoop)("HPP") & ","

                                sTOTALIN = .Rows(iLoop)("QTY") * .Rows(iLoop)("HPP")

                                sSQL &= "0,"
                                sSQL &= "0,"
                                sSQL &= "0,"

                                sTOTALOUT = 0
                            End If
                            If .Rows(iLoop)("TIPE") = "002" Then 'PR
                                sSQL &= "0,"
                                sSQL &= "0,"
                                sSQL &= "0,"

                                sTOTALIN = 0

                                sSQL &= "" & .Rows(iLoop)("QTY") & ","
                                sSQL &= "" & .Rows(iLoop)("HPP") & ","
                                sSQL &= "" & .Rows(iLoop)("QTY") * .Rows(iLoop)("HPP") & ","

                                sTOTALOUT = .Rows(iLoop)("QTY") * .Rows(iLoop)("HPP")
                            End If
                            If .Rows(iLoop)("TIPE") = "003" Then 'SI
                                sSQL &= "0,"
                                sSQL &= "0,"
                                sSQL &= "0,"

                                sTOTALIN = 0

                                sSQL &= "" & .Rows(iLoop)("QTY") & ","
                                sSQL &= "" & Average & ","
                                sSQL &= "" & .Rows(iLoop)("QTY") * Average & ","

                                sTOTALOUT = .Rows(iLoop)("QTY") * Average
                            End If
                            If .Rows(iLoop)("TIPE") = "004" Then 'SR
                                sSQL &= "" & .Rows(iLoop)("QTY") & ","
                                sSQL &= "" & Average & ","
                                sSQL &= "" & .Rows(iLoop)("QTY") * Average & ","

                                sTOTALIN = .Rows(iLoop)("QTY") * Average

                                sSQL &= "0,"
                                sSQL &= "0,"
                                sSQL &= "0,"

                                sTOTALOUT = 0
                            End If
                            If .Rows(iLoop)("TIPE") = "000" Then 'OPNAME
                                If .Rows(iLoop)("QTY") > 0 Then
                                    sSQL &= "" & .Rows(iLoop)("QTY") & ","
                                    sSQL &= "" & Average & ","
                                    sSQL &= "" & .Rows(iLoop)("QTY") * Average & ","

                                    sTOTALIN = .Rows(iLoop)("QTY") * Average

                                    sSQL &= "0,"
                                    sSQL &= "0,"
                                    sSQL &= "0,"

                                    sTOTALOUT = 0
                                Else
                                    sSQL &= "0,"
                                    sSQL &= "0,"
                                    sSQL &= "0,"

                                    sTOTALIN = 0

                                    sSQL &= "" & Math.Abs(CDec(.Rows(iLoop)("QTY"))) & ","
                                    sSQL &= "" & Average & ","
                                    sSQL &= "" & Math.Abs(CDec(.Rows(iLoop)("QTY"))) * Average & ","

                                    sTOTALOUT = Math.Abs(CDec(.Rows(iLoop)("QTY"))) * Average
                                End If
                            End If
                            If .Rows(iLoop)("TIPE") = "001" Or .Rows(iLoop)("TIPE") = "004" Or (.Rows(iLoop)("TIPE") = "000" And .Rows(iLoop)("QTY") > 0) Then
                                QtyBalance += Math.Abs(CDbl(.Rows(iLoop)("QTY")))
                                TotalBalance += sTOTALIN
                            Else
                                If .Rows(iLoop)("TIPE") = "003" And CDbl(.Rows(iLoop)("QTY")) < 1 Then
                                    QtyBalance += Math.Abs(CDbl(.Rows(iLoop)("QTY")))
                                Else

                                    QtyBalance -= Math.Abs(CDbl(.Rows(iLoop)("QTY")))
                                End If

                                TotalBalance -= sTOTALOUT
                            End If

                                If QtyBalance <= 0 Then
                                    PriceBalance = Average
                                Else
                                    PriceBalance = TotalBalance / QtyBalance
                                End If

                                sSQL &= "" & QtyBalance & ","
                                sSQL &= "" & PriceBalance & ","
                                sSQL &= "" & TotalBalance & ","
                                sSQL &= "" & sSeq & ","
                                sSQL &= "" & .Rows(iLoop)("SEQREF") & ""
                                sSQL &= ")"

                                Average = PriceBalance

                                sSeq += 1
                        End With

                        Dim oConnEx As New SqlConnection
                        Dim oCommEx As New SqlCommand
                        Dim readerEx As SqlDataReader

                        oConnEx = New SqlConnection(Decrypt(My.Computer.Registry.GetValue("HKEY_CURRENT_USER\Software\BUDIJAYAGROUP\SW\", "Database", "").ToString()))
                        If oConnEx.State = ConnectionState.Closed Then
                            oConnEx.Open()
                        End If

                        oCommEx.Connection = oConnEx
                        oCommEx.CommandText = sSQL
                        oCommEx.CommandTimeout = 120
                        oCommEx.CommandType = CommandType.Text

                        readerEx = oCommEx.ExecuteReader()

                        oConnEx.Close()
                    Next

                    oConn = Nothing
                    da = Nothing
                    ds = Nothing
                    SQL = Nothing
                Next

                PostingAverage = True
            Catch ex As Exception
                PostingAverage = False
                oError.InsertData(sMODUL, sSTATUS, ex.ToString, sREFERENCE)
                Throw ex
            Finally
                Finalize()
            End Try
        End Function
        'Public Function PostingAverage(ByVal sKDITEM As List(Of String), Optional ByVal sTotal As Decimal = 0) As Boolean
        '    Try
        '        If Not oConnection.GetConnection() Then
        '            PostingAverage = False
        '            Exit Function
        '        End If

        '        For Each xLoop In sKDITEM.Distinct
        '            sREFERENCE = xLoop
        '            sSTATUS = "POSTINGAVERAGE"

        '            Dim listAverage As New List(Of A_STOCK_CARD)
        '            Dim Average As Decimal = 0
        '            Dim QtyBalance As Decimal = 0
        '            Dim PriceBalance As Decimal = 0
        '            Dim TotalBalance As Decimal = 0
        '            Dim sSeq = 1
        '            Dim sItem = xLoop

        '            Dim oConn As New SqlConnection
        '            Dim oComm As New SqlCommand
        '            Dim da As SqlDataAdapter
        '            Dim ds As New DataSet
        '            Dim SQL As String

        '            Dim sConn As String = Decrypt(My.Computer.Registry.GetValue("HKEY_CURRENT_USER\Software\BUDIJAYAGROUP\SW\", "Database", "").ToString())

        '            oConn = New SqlConnection(sConn)
        '            If oConn.State = ConnectionState.Closed Then
        '                oConn.Open()
        '            End If

        '            SQL = "SELECT * FROM ("
        '            SQL &= "(SELECT "
        '            SQL &= "NOID = A.KDPI "
        '            SQL &= ",NOREF = A.KDPI "
        '            SQL &= ",[DATE] = A.DATE "
        '            SQL &= ",KDITEM = B.KDITEM "
        '            SQL &= ",KDUOM = B.KDUOM "
        '            SQL &= ",QTY = B.QTY * C.RATE "
        '            SQL &= ",HPP = (B.GRANDTOTAL / (B.QTY * C.RATE)) - (A.DISCOUNT / (B.QTY * C.RATE)) + (A.TAX / (B.QTY * C.RATE)) "
        '            SQL &= ",TOTAL = (B.GRANDTOTAL / (B.QTY * C.RATE)) - (A.DISCOUNT / (B.QTY * C.RATE)) + (A.TAX / (B.QTY * C.RATE)) * (B.QTY * C.RATE) "
        '            SQL &= ",TIPE = '001' "
        '            SQL &= ",SEQREF = B.SEQ "
        '            SQL &= "FROM P_PI_H A "
        '            SQL &= "INNER JOIN P_PI_D B "
        '            SQL &= "ON B.KDPI = A.KDPI "
        '            SQL &= "INNER JOIN M_ITEM_UOM C "
        '            SQL &= "ON B.KDITEM = C.KDITEM AND B.KDUOM = C.KDUOM "
        '            SQL &= "WHERE B.KDITEM = '" & xLoop & "'"
        '            SQL &= ") "
        '            SQL &= "UNION "
        '            SQL &= "(SELECT "
        '            SQL &= "NOID = A.KDPR "
        '            SQL &= ",NOREF = A.KDPR "
        '            SQL &= ",[DATE] = A.DATE "
        '            SQL &= ",KDITEM = B.KDITEM "
        '            SQL &= ",KDUOM = B.KDUOM "
        '            SQL &= ",QTY = B.QTY * C.RATE "
        '            SQL &= ",HPP = (B.GRANDTOTAL / (B.QTY * C.RATE)) - (A.DISCOUNT / (B.QTY * C.RATE)) + (A.TAX / (B.QTY * C.RATE)) "
        '            SQL &= ",TOTAL = (B.GRANDTOTAL / (B.QTY * C.RATE)) - (A.DISCOUNT / (B.QTY * C.RATE)) + (A.TAX / (B.QTY * C.RATE)) * (B.QTY * C.RATE) "
        '            SQL &= ",TIPE = '002' "
        '            SQL &= ",SEQREF = B.SEQ "
        '            SQL &= "FROM P_PR_H A "
        '            SQL &= "INNER JOIN P_PR_D B "
        '            SQL &= "ON B.KDPR = A.KDPR "
        '            SQL &= "INNER JOIN M_ITEM_UOM C "
        '            SQL &= "ON B.KDITEM = C.KDITEM AND B.KDUOM = C.KDUOM "
        '            SQL &= "WHERE B.KDITEM = '" & xLoop & "'"
        '            SQL &= ") "
        '            SQL &= "UNION "
        '            SQL &= "(SELECT "
        '            SQL &= "NOID = A.KDSI "
        '            SQL &= ",NOREF = A.KDSI "
        '            SQL &= ",[DATE] = A.DATE "
        '            SQL &= ",KDITEM = B.KDITEM "
        '            SQL &= ",KDUOM = B.KDUOM "
        '            SQL &= ",QTY = B.QTY * C.RATE "
        '            SQL &= ",HPP = (B.GRANDTOTAL / (B.QTY * C.RATE)) - (A.DISCOUNT / (B.QTY * C.RATE)) + (A.TAX / (B.QTY * C.RATE)) "
        '            SQL &= ",TOTAL = (B.GRANDTOTAL / (B.QTY * C.RATE)) - (A.DISCOUNT / (B.QTY * C.RATE)) + (A.TAX / (B.QTY * C.RATE)) * (B.QTY * C.RATE) "
        '            SQL &= ",TIPE = '003' "
        '            SQL &= ",SEQREF = B.SEQ "
        '            SQL &= "FROM S_SI_H A "
        '            SQL &= "INNER JOIN S_SI_D B "
        '            SQL &= "ON B.KDSI = A.KDSI "
        '            SQL &= "INNER JOIN M_ITEM_UOM C "
        '            SQL &= "ON B.KDITEM = C.KDITEM AND B.KDUOM = C.KDUOM "
        '            SQL &= "WHERE B.KDITEM = '" & xLoop & "'"
        '            SQL &= ") "
        '            SQL &= "UNION "
        '            SQL &= "(SELECT "
        '            SQL &= "NOID = A.KDSR "
        '            SQL &= ",NOREF = A.KDSR "
        '            SQL &= ",[DATE] = A.DATE "
        '            SQL &= ",KDITEM = B.KDITEM "
        '            SQL &= ",KDUOM = B.KDUOM "
        '            SQL &= ",QTY = B.QTY * C.RATE "
        '            SQL &= ",HPP = (B.GRANDTOTAL / (B.QTY * C.RATE)) - (A.DISCOUNT / (B.QTY * C.RATE)) + (A.TAX / (B.QTY * C.RATE)) "
        '            SQL &= ",TOTAL = (B.GRANDTOTAL / (B.QTY * C.RATE)) - (A.DISCOUNT / (B.QTY * C.RATE)) + (A.TAX / (B.QTY * C.RATE)) * (B.QTY * C.RATE) "
        '            SQL &= ",TIPE = '004' "
        '            SQL &= ",SEQREF = B.SEQ "
        '            SQL &= "FROM S_SR_H A "
        '            SQL &= "INNER JOIN S_SR_D B "
        '            SQL &= "ON B.KDSR = A.KDSR "
        '            SQL &= "INNER JOIN M_ITEM_UOM C "
        '            SQL &= "ON B.KDITEM = C.KDITEM AND B.KDUOM = C.KDUOM "
        '            SQL &= "WHERE B.KDITEM = '" & xLoop & "'"
        '            SQL &= ") "
        '            SQL &= "UNION "
        '            SQL &= "(SELECT "
        '            SQL &= "NOID = A.KDOPNAME "
        '            SQL &= ",NOREF = A.KDOPNAME "
        '            SQL &= ",[DATE] = A.DATE "
        '            SQL &= ",KDITEM = B.KDITEM "
        '            SQL &= ",KDUOM = B.KDUOM "
        '            SQL &= ",QTY = B.QTY * C.RATE "
        '            SQL &= ",HPP = 0 "
        '            SQL &= ",TOTAL = 0 "
        '            SQL &= ",TIPE = '000' "
        '            SQL &= ",SEQREF = B.SEQ "
        '            SQL &= "FROM I_OPNAME_H A "
        '            SQL &= "INNER JOIN I_OPNAME_D B "
        '            SQL &= "ON B.KDOPNAME = A.KDOPNAME "
        '            SQL &= "INNER JOIN M_ITEM_UOM C "
        '            SQL &= "ON B.KDITEM = C.KDITEM AND B.KDUOM = C.KDUOM "
        '            SQL &= "WHERE B.KDITEM = '" & xLoop & "'"
        '            SQL &= ") "
        '            SQL &= ") AS A "
        '            SQL &= "ORDER BY SEQREF, TIPE, CONVERT(VARCHAR(10), [DATE], 112) "

        '            Try
        '                Dim dsClear = GetData(sItem)
        '                oConnection.db.A_STOCK_CARDs.DeleteAllOnSubmit(dsClear)
        '                oConnection.db.SubmitChanges()
        '            Catch ex As Exception
        '                oError.InsertData(sMODUL, sSTATUS, ex.ToString, sREFERENCE)
        '                Throw ex
        '            End Try

        '            oComm.Connection = oConn
        '            oComm.CommandText = SQL
        '            oComm.CommandTimeout = 120
        '            oComm.CommandType = CommandType.Text

        '            da = New SqlDataAdapter(oComm)
        '            da.Fill(ds, "ALL")

        '            If ds.Tables("ALL").Rows.Count < 1 Then
        '                Continue For
        '            End If

        '            For iLoop As Integer = 0 To ds.Tables("ALL").Rows.Count - 1
        '                With ds.Tables("ALL")
        '                    Dim sTOTALIN As Decimal = 0
        '                    Dim sTOTALOUT As Decimal = 0

        '                    Dim sSQL = "INSERT INTO A_STOCK_CARD VALUES "
        '                    sSQL &= "("
        '                    sSQL &= "'" & .Rows(iLoop)("NOID") & "',"
        '                    sSQL &= "'" & CDate(.Rows(iLoop)("DATE")).ToString("yyyy-MM-dd HH:mm:ss") & "',"
        '                    sSQL &= "'" & .Rows(iLoop)("KDITEM") & "',"
        '                    sSQL &= "'" & .Rows(iLoop)("KDUOM") & "',"

        '                    If .Rows(iLoop)("TIPE") = "001" Then 'PI
        '                        sSQL &= "" & .Rows(iLoop)("QTY") & ","
        '                        sSQL &= "" & .Rows(iLoop)("HPP") & ","
        '                        sSQL &= "" & .Rows(iLoop)("TOTAL") & ","

        '                        sTOTALIN = .Rows(iLoop)("TOTAL")

        '                        sSQL &= "0,"
        '                        sSQL &= "0,"
        '                        sSQL &= "0,"

        '                        sTOTALOUT = 0
        '                    End If
        '                    If .Rows(iLoop)("TIPE") = "002" Then 'PR
        '                        sSQL &= "0,"
        '                        sSQL &= "0,"
        '                        sSQL &= "0,"

        '                        sTOTALIN = 0

        '                        sSQL &= "" & .Rows(iLoop)("QTY") & ","
        '                        sSQL &= "" & .Rows(iLoop)("HPP") & ","
        '                        sSQL &= "" & .Rows(iLoop)("TOTAL") & ","

        '                        sTOTALOUT = .Rows(iLoop)("TOTAL")
        '                    End If
        '                    If .Rows(iLoop)("TIPE") = "003" Then 'SI
        '                        sSQL &= "0,"
        '                        sSQL &= "0,"
        '                        sSQL &= "0,"

        '                        sTOTALIN = 0

        '                        sSQL &= "" & .Rows(iLoop)("QTY") & ","
        '                        sSQL &= "" & Average & ","
        '                        sSQL &= "" & .Rows(iLoop)("QTY") * Average & ","

        '                        sTOTALOUT = .Rows(iLoop)("QTY") * Average
        '                    End If
        '                    If .Rows(iLoop)("TIPE") = "004" Then 'SR
        '                        sSQL &= "" & .Rows(iLoop)("QTY") & ","
        '                        sSQL &= "" & Average & ","
        '                        sSQL &= "" & .Rows(iLoop)("QTY") * Average & ","

        '                        sTOTALIN = .Rows(iLoop)("QTY") * Average

        '                        sSQL &= "0,"
        '                        sSQL &= "0,"
        '                        sSQL &= "0,"

        '                        sTOTALOUT = 0
        '                    End If
        '                    If .Rows(iLoop)("TIPE") = "000" Then 'OPNAME
        '                        If .Rows(iLoop)("QTY") > 0 Then
        '                            sSQL &= "" & .Rows(iLoop)("QTY") & ","
        '                            sSQL &= "" & Average & ","
        '                            sSQL &= "" & .Rows(iLoop)("QTY") * Average & ","

        '                            sTOTALIN = .Rows(iLoop)("QTY") * Average

        '                            sSQL &= "0,"
        '                            sSQL &= "0,"
        '                            sSQL &= "0,"

        '                            sTOTALOUT = 0
        '                        Else
        '                            sSQL &= "0,"
        '                            sSQL &= "0,"
        '                            sSQL &= "0,"

        '                            sTOTALIN = 0

        '                            sSQL &= "" & Math.Abs(CDec(.Rows(iLoop)("QTY"))) & ","
        '                            sSQL &= "" & Average & ","
        '                            sSQL &= "" & Math.Abs(CDec(.Rows(iLoop)("QTY"))) * Average & ","

        '                            sTOTALOUT = Math.Abs(CDec(.Rows(iLoop)("QTY"))) * Average
        '                        End If
        '                    End If
        '                    If .Rows(iLoop)("TIPE") = "001" Or .Rows(iLoop)("TIPE") = "004" Or (.Rows(iLoop)("TIPE") = "000" And .Rows(iLoop)("QTY") > 0) Then
        '                        QtyBalance += Math.Abs(CDbl(.Rows(iLoop)("QTY")))
        '                        TotalBalance += sTOTALIN
        '                    Else
        '                        QtyBalance -= Math.Abs(CDbl(.Rows(iLoop)("QTY")))
        '                        TotalBalance -= sTOTALOUT
        '                    End If

        '                    If QtyBalance <= 0 Then
        '                        PriceBalance = Average
        '                    Else
        '                        PriceBalance = TotalBalance / QtyBalance
        '                    End If

        '                    sSQL &= "" & QtyBalance & ","
        '                    sSQL &= "" & PriceBalance & ","
        '                    sSQL &= "" & TotalBalance & ","
        '                    sSQL &= "" & sSeq & ","
        '                    sSQL &= "" & .Rows(iLoop)("SEQREF") & ""
        '                    sSQL &= ")"

        '                    Average = PriceBalance

        '                    sSeq += 1
        '                End With

        '                Dim dsAverage As New A_STOCK_CARD

        '                With ds.Tables("ALL")
        '                    dsAverage.NOREFERENCE = .Rows(iLoop)("NOID")
        '                    dsAverage.DATE = .Rows(iLoop)("DATE")
        '                    dsAverage.KDITEM = .Rows(iLoop)("KDITEM")
        '                    dsAverage.KDUOM = .Rows(iLoop)("KDUOM")

        '                    If .Rows(iLoop)("TIPE") = "001" Then 'PI
        '                        dsAverage.QTYIN = .Rows(iLoop)("QTY")
        '                        dsAverage.HPPIN = .Rows(iLoop)("HPP")
        '                        dsAverage.TOTALIN = .Rows(iLoop)("TOTAL")

        '                        dsAverage.QTYOUT = 0
        '                        dsAverage.HPPOUT = 0
        '                        dsAverage.TOTALOUT = 0
        '                    End If

        '                    If .Rows(iLoop)("TIPE") = "002" Then 'PR
        '                        dsAverage.QTYOUT = .Rows(iLoop)("QTY")
        '                        dsAverage.HPPOUT = .Rows(iLoop)("HPP")
        '                        dsAverage.TOTALOUT = .Rows(iLoop)("TOTAL")

        '                        dsAverage.QTYIN = 0
        '                        dsAverage.HPPIN = 0
        '                        dsAverage.TOTALIN = 0
        '                    End If

        '                    If .Rows(iLoop)("TIPE") = "003" Then 'SI
        '                        dsAverage.QTYOUT = .Rows(iLoop)("QTY")
        '                        dsAverage.HPPOUT = Average
        '                        dsAverage.TOTALOUT = .Rows(iLoop)("QTY") * Average

        '                        dsAverage.QTYIN = 0
        '                        dsAverage.HPPIN = 0
        '                        dsAverage.TOTALIN = 0
        '                    End If

        '                    If .Rows(iLoop)("TIPE") = "004" Then 'SR
        '                        dsAverage.QTYIN = .Rows(iLoop)("QTY")
        '                        dsAverage.HPPIN = Average
        '                        dsAverage.TOTALIN = .Rows(iLoop)("QTY") * Average

        '                        dsAverage.QTYOUT = 0
        '                        dsAverage.HPPOUT = 0
        '                        dsAverage.TOTALOUT = 0
        '                    End If

        '                    If .Rows(iLoop)("TIPE") = "000" Then 'OPNAME
        '                        If .Rows(iLoop)("QTY") > 0 Then
        '                            dsAverage.QTYIN = .Rows(iLoop)("QTY")
        '                            dsAverage.HPPIN = Average
        '                            dsAverage.TOTALIN = .Rows(iLoop)("QTY") * Average

        '                            dsAverage.QTYOUT = 0
        '                            dsAverage.HPPOUT = 0
        '                            dsAverage.TOTALOUT = 0
        '                        Else
        '                            dsAverage.QTYOUT = Math.Abs(CDbl(.Rows(iLoop)("QTY")))
        '                            dsAverage.HPPOUT = Average
        '                            dsAverage.TOTALOUT = Math.Abs(CDbl(.Rows(iLoop)("QTY"))) * Average

        '                            dsAverage.QTYIN = 0
        '                            dsAverage.HPPIN = 0
        '                            dsAverage.TOTALIN = 0
        '                        End If
        '                    End If

        '                    If .Rows(iLoop)("TIPE") = "001" Or .Rows(iLoop)("TIPE") = "004" Or (.Rows(iLoop)("TIPE") = "000" And .Rows(iLoop)("QTY") > 0) Then
        '                        QtyBalance += Math.Abs(CDbl(.Rows(iLoop)("QTY")))
        '                        TotalBalance += dsAverage.TOTALIN
        '                    Else
        '                        QtyBalance -= Math.Abs(CDbl(.Rows(iLoop)("QTY")))
        '                        TotalBalance -= dsAverage.TOTALOUT
        '                    End If

        '                    If QtyBalance <= 0 Then
        '                        PriceBalance = Average
        '                    Else
        '                        PriceBalance = TotalBalance / QtyBalance
        '                    End If

        '                    dsAverage.QTYAVERAGE = QtyBalance
        '                    dsAverage.HPPAVERAGE = PriceBalance
        '                    dsAverage.TOTALAVERAGE = TotalBalance
        '                    dsAverage.SEQ = sSeq
        '                    dsAverage.SEQREF = .Rows(iLoop)("SEQREF")

        '                    Average = PriceBalance
        '                    listAverage.Add(dsAverage)

        '                    sSeq += 1
        '                End With
        '            Next

        '            oConnection.db.A_STOCK_CARDs.InsertAllOnSubmit(listAverage)
        '            oConnection.db.SubmitChanges()
        '        Next

        '        PostingAverage = True
        '    Catch ex As Exception
        '        PostingAverage = False
        '        oError.InsertData(sMODUL, sSTATUS, ex.ToString, sREFERENCE)
        '        Throw ex
        '    End Try
        'End Function
    End Class
End Namespace