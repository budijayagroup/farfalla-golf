﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCashOut
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.layoutControl = New DevExpress.XtraLayout.LayoutControl()
        Me.txtGRANDTOTAL = New DevExpress.XtraEditors.TextEdit()
        Me.barManager = New DevExpress.XtraBars.BarManager(Me.components)
        Me.barTop = New DevExpress.XtraBars.Bar()
        Me.btnSaveNew = New DevExpress.XtraBars.BarButtonItem()
        Me.btnSaveClose = New DevExpress.XtraBars.BarButtonItem()
        Me.btnClose = New DevExpress.XtraBars.BarButtonItem()
        Me.barDockControlTop = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlBottom = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlLeft = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlRight = New DevExpress.XtraBars.BarDockControl()
        Me.progressBarSave = New DevExpress.XtraEditors.Repository.RepositoryItemMarqueeProgressBar()
        Me.progressSave = New DevExpress.XtraEditors.Repository.RepositoryItemMarqueeProgressBar()
        Me.txtROUND = New DevExpress.XtraEditors.TextEdit()
        Me.txtADMIN = New DevExpress.XtraEditors.TextEdit()
        Me.txtSUBTOTAL = New DevExpress.XtraEditors.TextEdit()
        Me.grdKDPAYMENTTYPE = New DevExpress.XtraEditors.GridLookUpEdit()
        Me.grvKDPAYMENTTYPE = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn9 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.grdKDVENDOR = New DevExpress.XtraEditors.GridLookUpEdit()
        Me.grvKDVENDOR = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn3 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.LayoutControl5 = New DevExpress.XtraLayout.LayoutControl()
        Me.LayoutControlGroup5 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.EmptySpaceItem1 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.deDATE = New DevExpress.XtraEditors.DateEdit()
        Me.LayoutControl2 = New DevExpress.XtraLayout.LayoutControl()
        Me.Root = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.tabControl = New DevExpress.XtraTab.XtraTabControl()
        Me.tab1 = New DevExpress.XtraTab.XtraTabPage()
        Me.grdDetail = New DevExpress.XtraGrid.GridControl()
        Me.mnuStrip = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.DeleteToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.bindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.grvDetail = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colNOINVOICE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.grdNOINVOICE = New DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit()
        Me.grvNOINVOICE = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn4 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colAMOUNTORIGINAL_UB = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colAMOUNTDUE_UB = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colAMOUNTPAYMENT = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colREMARKS = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.txtREMARKS = New DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit()
        Me.tab2 = New DevExpress.XtraTab.XtraTabPage()
        Me.grdDetail_R = New DevExpress.XtraGrid.GridControl()
        Me.mnuStrip_R = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.DeleteToolStripMenuItem_R = New System.Windows.Forms.ToolStripMenuItem()
        Me.bindingSource_R = New System.Windows.Forms.BindingSource(Me.components)
        Me.grvDetail_R = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colNOINVOICE_R = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.grdNOINVOICE_R = New DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit()
        Me.grvNOINVOICE_R = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn8 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colAMOUNTORIGINAL_UB_R = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colAMOUNTDUE_UB_R = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colAMOUNTPAYMENT_R = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colREMARKS_R = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.txtREMARKS_R = New DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit()
        Me.RepositoryItemGridLookUpEdit2 = New DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit()
        Me.GridView3 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn14 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn15 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn16 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.tab3 = New DevExpress.XtraTab.XtraTabPage()
        Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl()
        Me.txtMEMO = New DevExpress.XtraEditors.MemoEdit()
        Me.s = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem7 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.txtKDCASHOUT = New DevExpress.XtraEditors.TextEdit()
        Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.lKDCASH = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem5 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem4 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.lDATE = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem3 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.lKDVENDOR = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem2 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.lKDPAYMENTTYPE = New DevExpress.XtraLayout.LayoutControlItem()
        Me.lSUBTOTAL = New DevExpress.XtraLayout.LayoutControlItem()
        Me.lADMIN = New DevExpress.XtraLayout.LayoutControlItem()
        Me.lROUND = New DevExpress.XtraLayout.LayoutControlItem()
        Me.lGRANDTOTAL = New DevExpress.XtraLayout.LayoutControlItem()
        Me.GridColumn6 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn5 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn7 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn10 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn11 = New DevExpress.XtraGrid.Columns.GridColumn()
        CType(Me.layoutControl, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.layoutControl.SuspendLayout()
        CType(Me.txtGRANDTOTAL.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.barManager, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.progressBarSave, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.progressSave, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtROUND.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtADMIN.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSUBTOTAL.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grdKDPAYMENTTYPE.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvKDPAYMENTTYPE, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grdKDVENDOR.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvKDVENDOR, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControl5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deDATE.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deDATE.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Root, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tabControl, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabControl.SuspendLayout()
        Me.tab1.SuspendLayout()
        CType(Me.grdDetail, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.mnuStrip.SuspendLayout()
        CType(Me.bindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvDetail, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grdNOINVOICE, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvNOINVOICE, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtREMARKS, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tab2.SuspendLayout()
        CType(Me.grdDetail_R, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.mnuStrip_R.SuspendLayout()
        CType(Me.bindingSource_R, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvDetail_R, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grdNOINVOICE_R, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvNOINVOICE_R, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtREMARKS_R, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemGridLookUpEdit2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tab3.SuspendLayout()
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl1.SuspendLayout()
        CType(Me.txtMEMO.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.s, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtKDCASHOUT.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lKDCASH, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lDATE, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lKDVENDOR, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lKDPAYMENTTYPE, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lSUBTOTAL, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lADMIN, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lROUND, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lGRANDTOTAL, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'layoutControl
        '
        Me.layoutControl.Controls.Add(Me.txtGRANDTOTAL)
        Me.layoutControl.Controls.Add(Me.txtROUND)
        Me.layoutControl.Controls.Add(Me.txtADMIN)
        Me.layoutControl.Controls.Add(Me.txtSUBTOTAL)
        Me.layoutControl.Controls.Add(Me.grdKDPAYMENTTYPE)
        Me.layoutControl.Controls.Add(Me.grdKDVENDOR)
        Me.layoutControl.Controls.Add(Me.LayoutControl5)
        Me.layoutControl.Controls.Add(Me.deDATE)
        Me.layoutControl.Controls.Add(Me.LayoutControl2)
        Me.layoutControl.Controls.Add(Me.tabControl)
        Me.layoutControl.Controls.Add(Me.txtKDCASHOUT)
        Me.layoutControl.Dock = System.Windows.Forms.DockStyle.Fill
        Me.layoutControl.Location = New System.Drawing.Point(0, 0)
        Me.layoutControl.Name = "layoutControl"
        Me.layoutControl.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = New System.Drawing.Rectangle(652, 156, 250, 350)
        Me.layoutControl.Root = Me.LayoutControlGroup1
        Me.layoutControl.Size = New System.Drawing.Size(790, 549)
        Me.layoutControl.TabIndex = 0
        Me.layoutControl.Text = "LayoutControl1"
        '
        'txtGRANDTOTAL
        '
        Me.txtGRANDTOTAL.EnterMoveNextControl = True
        Me.txtGRANDTOTAL.Location = New System.Drawing.Point(502, 517)
        Me.txtGRANDTOTAL.MenuManager = Me.barManager
        Me.txtGRANDTOTAL.Name = "txtGRANDTOTAL"
        Me.txtGRANDTOTAL.Properties.Appearance.Options.UseTextOptions = True
        Me.txtGRANDTOTAL.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtGRANDTOTAL.Properties.Mask.EditMask = "n2"
        Me.txtGRANDTOTAL.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtGRANDTOTAL.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtGRANDTOTAL.Properties.NullText = "0.00"
        Me.txtGRANDTOTAL.Properties.ReadOnly = True
        Me.txtGRANDTOTAL.Size = New System.Drawing.Size(276, 20)
        Me.txtGRANDTOTAL.StyleController = Me.layoutControl
        Me.txtGRANDTOTAL.TabIndex = 35
        Me.txtGRANDTOTAL.TabStop = False
        '
        'barManager
        '
        Me.barManager.AllowQuickCustomization = False
        Me.barManager.Bars.AddRange(New DevExpress.XtraBars.Bar() {Me.barTop})
        Me.barManager.DockControls.Add(Me.barDockControlTop)
        Me.barManager.DockControls.Add(Me.barDockControlBottom)
        Me.barManager.DockControls.Add(Me.barDockControlLeft)
        Me.barManager.DockControls.Add(Me.barDockControlRight)
        Me.barManager.Form = Me
        Me.barManager.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.btnSaveNew, Me.btnClose, Me.btnSaveClose})
        Me.barManager.MainMenu = Me.barTop
        Me.barManager.MaxItemId = 8
        Me.barManager.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.progressBarSave, Me.progressSave})
        '
        'barTop
        '
        Me.barTop.BarName = "Main menu"
        Me.barTop.DockCol = 0
        Me.barTop.DockRow = 0
        Me.barTop.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom
        Me.barTop.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.btnSaveNew), New DevExpress.XtraBars.LinkPersistInfo(Me.btnSaveClose), New DevExpress.XtraBars.LinkPersistInfo(Me.btnClose)})
        Me.barTop.OptionsBar.DrawDragBorder = False
        Me.barTop.OptionsBar.MultiLine = True
        Me.barTop.OptionsBar.UseWholeRow = True
        Me.barTop.Text = "Main menu"
        '
        'btnSaveNew
        '
        Me.btnSaveNew.Caption = "F2 - Save && New"
        Me.btnSaveNew.Id = 2
        Me.btnSaveNew.Name = "btnSaveNew"
        '
        'btnSaveClose
        '
        Me.btnSaveClose.Caption = "F3 - Save && Close"
        Me.btnSaveClose.Id = 5
        Me.btnSaveClose.Name = "btnSaveClose"
        '
        'btnClose
        '
        Me.btnClose.Caption = "F12 - Close"
        Me.btnClose.Id = 3
        Me.btnClose.Name = "btnClose"
        '
        'barDockControlTop
        '
        Me.barDockControlTop.CausesValidation = False
        Me.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top
        Me.barDockControlTop.Location = New System.Drawing.Point(0, 0)
        Me.barDockControlTop.Size = New System.Drawing.Size(790, 0)
        '
        'barDockControlBottom
        '
        Me.barDockControlBottom.CausesValidation = False
        Me.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.barDockControlBottom.Location = New System.Drawing.Point(0, 549)
        Me.barDockControlBottom.Size = New System.Drawing.Size(790, 22)
        '
        'barDockControlLeft
        '
        Me.barDockControlLeft.CausesValidation = False
        Me.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left
        Me.barDockControlLeft.Location = New System.Drawing.Point(0, 0)
        Me.barDockControlLeft.Size = New System.Drawing.Size(0, 549)
        '
        'barDockControlRight
        '
        Me.barDockControlRight.CausesValidation = False
        Me.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right
        Me.barDockControlRight.Location = New System.Drawing.Point(790, 0)
        Me.barDockControlRight.Size = New System.Drawing.Size(0, 549)
        '
        'progressBarSave
        '
        Me.progressBarSave.Name = "progressBarSave"
        Me.progressBarSave.Stopped = True
        '
        'progressSave
        '
        Me.progressSave.Name = "progressSave"
        Me.progressSave.Paused = True
        '
        'txtROUND
        '
        Me.txtROUND.EnterMoveNextControl = True
        Me.txtROUND.Location = New System.Drawing.Point(502, 493)
        Me.txtROUND.MenuManager = Me.barManager
        Me.txtROUND.Name = "txtROUND"
        Me.txtROUND.Properties.Appearance.Options.UseTextOptions = True
        Me.txtROUND.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtROUND.Properties.Mask.EditMask = "n2"
        Me.txtROUND.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtROUND.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtROUND.Properties.NullText = "0.00"
        Me.txtROUND.Size = New System.Drawing.Size(276, 20)
        Me.txtROUND.StyleController = Me.layoutControl
        Me.txtROUND.TabIndex = 34
        '
        'txtADMIN
        '
        Me.txtADMIN.EnterMoveNextControl = True
        Me.txtADMIN.Location = New System.Drawing.Point(502, 469)
        Me.txtADMIN.MenuManager = Me.barManager
        Me.txtADMIN.Name = "txtADMIN"
        Me.txtADMIN.Properties.Appearance.Options.UseTextOptions = True
        Me.txtADMIN.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtADMIN.Properties.Mask.EditMask = "n2"
        Me.txtADMIN.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtADMIN.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtADMIN.Properties.NullText = "0.00"
        Me.txtADMIN.Size = New System.Drawing.Size(276, 20)
        Me.txtADMIN.StyleController = Me.layoutControl
        Me.txtADMIN.TabIndex = 33
        '
        'txtSUBTOTAL
        '
        Me.txtSUBTOTAL.EnterMoveNextControl = True
        Me.txtSUBTOTAL.Location = New System.Drawing.Point(502, 445)
        Me.txtSUBTOTAL.MenuManager = Me.barManager
        Me.txtSUBTOTAL.Name = "txtSUBTOTAL"
        Me.txtSUBTOTAL.Properties.Appearance.Options.UseTextOptions = True
        Me.txtSUBTOTAL.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtSUBTOTAL.Properties.Mask.EditMask = "n2"
        Me.txtSUBTOTAL.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtSUBTOTAL.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtSUBTOTAL.Properties.NullText = "0.00"
        Me.txtSUBTOTAL.Properties.ReadOnly = True
        Me.txtSUBTOTAL.Size = New System.Drawing.Size(276, 20)
        Me.txtSUBTOTAL.StyleController = Me.layoutControl
        Me.txtSUBTOTAL.TabIndex = 32
        Me.txtSUBTOTAL.TabStop = False
        '
        'grdKDPAYMENTTYPE
        '
        Me.grdKDPAYMENTTYPE.EnterMoveNextControl = True
        Me.grdKDPAYMENTTYPE.Location = New System.Drawing.Point(117, 84)
        Me.grdKDPAYMENTTYPE.MenuManager = Me.barManager
        Me.grdKDPAYMENTTYPE.Name = "grdKDPAYMENTTYPE"
        Me.grdKDPAYMENTTYPE.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.grdKDPAYMENTTYPE.Properties.NullText = ""
        Me.grdKDPAYMENTTYPE.Properties.PopupFormMinSize = New System.Drawing.Size(600, 300)
        Me.grdKDPAYMENTTYPE.Properties.View = Me.grvKDPAYMENTTYPE
        Me.grdKDPAYMENTTYPE.Size = New System.Drawing.Size(375, 20)
        Me.grdKDPAYMENTTYPE.StyleController = Me.layoutControl
        Me.grdKDPAYMENTTYPE.TabIndex = 31
        '
        'grvKDPAYMENTTYPE
        '
        Me.grvKDPAYMENTTYPE.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn9})
        Me.grvKDPAYMENTTYPE.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.grvKDPAYMENTTYPE.Name = "grvKDPAYMENTTYPE"
        Me.grvKDPAYMENTTYPE.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.grvKDPAYMENTTYPE.OptionsView.ShowAutoFilterRow = True
        Me.grvKDPAYMENTTYPE.OptionsView.ShowGroupPanel = False
        '
        'GridColumn9
        '
        Me.GridColumn9.Caption = "Display Name"
        Me.GridColumn9.FieldName = "MEMO"
        Me.GridColumn9.Name = "GridColumn9"
        Me.GridColumn9.Visible = True
        Me.GridColumn9.VisibleIndex = 0
        '
        'grdKDVENDOR
        '
        Me.grdKDVENDOR.EditValue = ""
        Me.grdKDVENDOR.EnterMoveNextControl = True
        Me.grdKDVENDOR.Location = New System.Drawing.Point(117, 60)
        Me.grdKDVENDOR.MenuManager = Me.barManager
        Me.grdKDVENDOR.Name = "grdKDVENDOR"
        Me.grdKDVENDOR.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.grdKDVENDOR.Properties.NullText = ""
        Me.grdKDVENDOR.Properties.PopupFormMinSize = New System.Drawing.Size(600, 300)
        Me.grdKDVENDOR.Properties.View = Me.grvKDVENDOR
        Me.grdKDVENDOR.Size = New System.Drawing.Size(375, 20)
        Me.grdKDVENDOR.StyleController = Me.layoutControl
        Me.grdKDVENDOR.TabIndex = 22
        '
        'grvKDVENDOR
        '
        Me.grvKDVENDOR.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn3})
        Me.grvKDVENDOR.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.grvKDVENDOR.Name = "grvKDVENDOR"
        Me.grvKDVENDOR.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.grvKDVENDOR.OptionsView.ShowAutoFilterRow = True
        Me.grvKDVENDOR.OptionsView.ShowGroupPanel = False
        '
        'GridColumn3
        '
        Me.GridColumn3.Caption = "Name Display"
        Me.GridColumn3.FieldName = "NAME_DISPLAY"
        Me.GridColumn3.Name = "GridColumn3"
        Me.GridColumn3.Visible = True
        Me.GridColumn3.VisibleIndex = 0
        '
        'LayoutControl5
        '
        Me.LayoutControl5.Location = New System.Drawing.Point(496, 12)
        Me.LayoutControl5.Name = "LayoutControl5"
        Me.LayoutControl5.Root = Me.LayoutControlGroup5
        Me.LayoutControl5.Size = New System.Drawing.Size(282, 116)
        Me.LayoutControl5.TabIndex = 21
        Me.LayoutControl5.Text = "LayoutControl5"
        '
        'LayoutControlGroup5
        '
        Me.LayoutControlGroup5.CustomizationFormText = "LayoutControlGroup5"
        Me.LayoutControlGroup5.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup5.GroupBordersVisible = False
        Me.LayoutControlGroup5.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.EmptySpaceItem1})
        Me.LayoutControlGroup5.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup5.Name = "LayoutControlGroup5"
        Me.LayoutControlGroup5.Size = New System.Drawing.Size(282, 116)
        Me.LayoutControlGroup5.Text = "LayoutControlGroup5"
        Me.LayoutControlGroup5.TextVisible = False
        '
        'EmptySpaceItem1
        '
        Me.EmptySpaceItem1.AllowHotTrack = False
        Me.EmptySpaceItem1.CustomizationFormText = "EmptySpaceItem1"
        Me.EmptySpaceItem1.Location = New System.Drawing.Point(0, 0)
        Me.EmptySpaceItem1.Name = "EmptySpaceItem1"
        Me.EmptySpaceItem1.Size = New System.Drawing.Size(262, 96)
        Me.EmptySpaceItem1.Text = "EmptySpaceItem1"
        Me.EmptySpaceItem1.TextSize = New System.Drawing.Size(0, 0)
        '
        'deDATE
        '
        Me.deDATE.EditValue = Nothing
        Me.deDATE.EnterMoveNextControl = True
        Me.deDATE.Location = New System.Drawing.Point(117, 36)
        Me.deDATE.MenuManager = Me.barManager
        Me.deDATE.Name = "deDATE"
        Me.deDATE.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.deDATE.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.deDATE.Properties.Mask.EditMask = "dd/MM/yyyy"
        Me.deDATE.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.deDATE.Size = New System.Drawing.Size(375, 20)
        Me.deDATE.StyleController = Me.layoutControl
        Me.deDATE.TabIndex = 20
        '
        'LayoutControl2
        '
        Me.LayoutControl2.Location = New System.Drawing.Point(12, 108)
        Me.LayoutControl2.Name = "LayoutControl2"
        Me.LayoutControl2.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = New System.Drawing.Rectangle(336, 280, 250, 350)
        Me.LayoutControl2.Root = Me.Root
        Me.LayoutControl2.Size = New System.Drawing.Size(480, 20)
        Me.LayoutControl2.TabIndex = 19
        Me.LayoutControl2.Text = "LayoutControl2"
        '
        'Root
        '
        Me.Root.CustomizationFormText = "Root"
        Me.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.Root.GroupBordersVisible = False
        Me.Root.Location = New System.Drawing.Point(0, 0)
        Me.Root.Name = "Root"
        Me.Root.Size = New System.Drawing.Size(480, 20)
        Me.Root.Text = "Root"
        Me.Root.TextVisible = False
        '
        'tabControl
        '
        Me.tabControl.Location = New System.Drawing.Point(12, 132)
        Me.tabControl.Name = "tabControl"
        Me.tabControl.SelectedTabPage = Me.tab1
        Me.tabControl.Size = New System.Drawing.Size(766, 309)
        Me.tabControl.TabIndex = 18
        Me.tabControl.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.tab1, Me.tab2, Me.tab3})
        '
        'tab1
        '
        Me.tab1.Controls.Add(Me.grdDetail)
        Me.tab1.Name = "tab1"
        Me.tab1.Size = New System.Drawing.Size(760, 281)
        Me.tab1.Text = "Detail Information"
        '
        'grdDetail
        '
        Me.grdDetail.ContextMenuStrip = Me.mnuStrip
        Me.grdDetail.DataSource = Me.bindingSource
        Me.grdDetail.Dock = System.Windows.Forms.DockStyle.Fill
        Me.grdDetail.Location = New System.Drawing.Point(0, 0)
        Me.grdDetail.MainView = Me.grvDetail
        Me.grdDetail.MenuManager = Me.barManager
        Me.grdDetail.Name = "grdDetail"
        Me.grdDetail.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.grdNOINVOICE, Me.txtREMARKS})
        Me.grdDetail.Size = New System.Drawing.Size(760, 281)
        Me.grdDetail.TabIndex = 18
        Me.grdDetail.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.grvDetail})
        '
        'mnuStrip
        '
        Me.mnuStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.DeleteToolStripMenuItem})
        Me.mnuStrip.Name = "mnuStrip"
        Me.mnuStrip.Size = New System.Drawing.Size(108, 26)
        '
        'DeleteToolStripMenuItem
        '
        Me.DeleteToolStripMenuItem.Name = "DeleteToolStripMenuItem"
        Me.DeleteToolStripMenuItem.Size = New System.Drawing.Size(107, 22)
        Me.DeleteToolStripMenuItem.Text = "Delete"
        '
        'bindingSource
        '
        Me.bindingSource.DataSource = GetType(DataAccess.F_CASHOUT_D)
        '
        'grvDetail
        '
        Me.grvDetail.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colNOINVOICE, Me.colAMOUNTORIGINAL_UB, Me.colAMOUNTDUE_UB, Me.colAMOUNTPAYMENT, Me.colREMARKS})
        Me.grvDetail.GridControl = Me.grdDetail
        Me.grvDetail.Name = "grvDetail"
        Me.grvDetail.OptionsCustomization.AllowColumnMoving = False
        Me.grvDetail.OptionsCustomization.AllowFilter = False
        Me.grvDetail.OptionsCustomization.AllowGroup = False
        Me.grvDetail.OptionsCustomization.AllowQuickHideColumns = False
        Me.grvDetail.OptionsCustomization.AllowSort = False
        Me.grvDetail.OptionsDetail.EnableMasterViewMode = False
        Me.grvDetail.OptionsFind.AllowFindPanel = False
        Me.grvDetail.OptionsLayout.StoreAllOptions = True
        Me.grvDetail.OptionsLayout.StoreAppearance = True
        Me.grvDetail.OptionsMenu.EnableColumnMenu = False
        Me.grvDetail.OptionsNavigation.AutoFocusNewRow = True
        Me.grvDetail.OptionsNavigation.EnterMoveNextColumn = True
        Me.grvDetail.OptionsView.EnableAppearanceEvenRow = True
        Me.grvDetail.OptionsView.EnableAppearanceOddRow = True
        Me.grvDetail.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Bottom
        Me.grvDetail.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never
        Me.grvDetail.OptionsView.ShowFooter = True
        Me.grvDetail.OptionsView.ShowGroupPanel = False
        '
        'colNOINVOICE
        '
        Me.colNOINVOICE.Caption = "Invoice No."
        Me.colNOINVOICE.ColumnEdit = Me.grdNOINVOICE
        Me.colNOINVOICE.FieldName = "NOINVOICE"
        Me.colNOINVOICE.Name = "colNOINVOICE"
        Me.colNOINVOICE.Visible = True
        Me.colNOINVOICE.VisibleIndex = 0
        '
        'grdNOINVOICE
        '
        Me.grdNOINVOICE.AutoHeight = False
        Me.grdNOINVOICE.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.grdNOINVOICE.Name = "grdNOINVOICE"
        Me.grdNOINVOICE.NullText = ""
        Me.grdNOINVOICE.PopupFormMinSize = New System.Drawing.Size(600, 300)
        Me.grdNOINVOICE.View = Me.grvNOINVOICE
        '
        'grvNOINVOICE
        '
        Me.grvNOINVOICE.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn4, Me.GridColumn1, Me.GridColumn2, Me.GridColumn5})
        Me.grvNOINVOICE.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.grvNOINVOICE.Name = "grvNOINVOICE"
        Me.grvNOINVOICE.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.grvNOINVOICE.OptionsView.ShowAutoFilterRow = True
        Me.grvNOINVOICE.OptionsView.ShowGroupPanel = False
        '
        'GridColumn4
        '
        Me.GridColumn4.Caption = "Number"
        Me.GridColumn4.FieldName = "NOINVOICE"
        Me.GridColumn4.Name = "GridColumn4"
        Me.GridColumn4.Visible = True
        Me.GridColumn4.VisibleIndex = 0
        '
        'colAMOUNTORIGINAL_UB
        '
        Me.colAMOUNTORIGINAL_UB.Caption = "Amount Original"
        Me.colAMOUNTORIGINAL_UB.DisplayFormat.FormatString = "{0:n2}"
        Me.colAMOUNTORIGINAL_UB.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.colAMOUNTORIGINAL_UB.FieldName = "AMOUNTORIGINAL_UB"
        Me.colAMOUNTORIGINAL_UB.Name = "colAMOUNTORIGINAL_UB"
        Me.colAMOUNTORIGINAL_UB.OptionsColumn.AllowEdit = False
        Me.colAMOUNTORIGINAL_UB.OptionsColumn.AllowFocus = False
        Me.colAMOUNTORIGINAL_UB.OptionsColumn.ReadOnly = True
        Me.colAMOUNTORIGINAL_UB.OptionsColumn.TabStop = False
        Me.colAMOUNTORIGINAL_UB.UnboundType = DevExpress.Data.UnboundColumnType.[Decimal]
        Me.colAMOUNTORIGINAL_UB.Visible = True
        Me.colAMOUNTORIGINAL_UB.VisibleIndex = 1
        '
        'colAMOUNTDUE_UB
        '
        Me.colAMOUNTDUE_UB.Caption = "Amount Due"
        Me.colAMOUNTDUE_UB.DisplayFormat.FormatString = "{0:n2}"
        Me.colAMOUNTDUE_UB.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.colAMOUNTDUE_UB.FieldName = "AMOUNTDUE_UB"
        Me.colAMOUNTDUE_UB.Name = "colAMOUNTDUE_UB"
        Me.colAMOUNTDUE_UB.OptionsColumn.AllowEdit = False
        Me.colAMOUNTDUE_UB.OptionsColumn.AllowFocus = False
        Me.colAMOUNTDUE_UB.OptionsColumn.ReadOnly = True
        Me.colAMOUNTDUE_UB.OptionsColumn.TabStop = False
        Me.colAMOUNTDUE_UB.UnboundType = DevExpress.Data.UnboundColumnType.[Decimal]
        Me.colAMOUNTDUE_UB.Visible = True
        Me.colAMOUNTDUE_UB.VisibleIndex = 2
        '
        'colAMOUNTPAYMENT
        '
        Me.colAMOUNTPAYMENT.AppearanceCell.Options.UseTextOptions = True
        Me.colAMOUNTPAYMENT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.colAMOUNTPAYMENT.Caption = "Amount Payment"
        Me.colAMOUNTPAYMENT.DisplayFormat.FormatString = "{0:n2}"
        Me.colAMOUNTPAYMENT.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.colAMOUNTPAYMENT.FieldName = "AMOUNTPAYMENT"
        Me.colAMOUNTPAYMENT.Name = "colAMOUNTPAYMENT"
        Me.colAMOUNTPAYMENT.Visible = True
        Me.colAMOUNTPAYMENT.VisibleIndex = 3
        '
        'colREMARKS
        '
        Me.colREMARKS.AppearanceCell.Options.UseTextOptions = True
        Me.colREMARKS.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.colREMARKS.Caption = "Remarks"
        Me.colREMARKS.ColumnEdit = Me.txtREMARKS
        Me.colREMARKS.FieldName = "REMARKS"
        Me.colREMARKS.Name = "colREMARKS"
        Me.colREMARKS.Visible = True
        Me.colREMARKS.VisibleIndex = 4
        '
        'txtREMARKS
        '
        Me.txtREMARKS.AutoHeight = False
        Me.txtREMARKS.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtREMARKS.Name = "txtREMARKS"
        '
        'tab2
        '
        Me.tab2.Controls.Add(Me.grdDetail_R)
        Me.tab2.Name = "tab2"
        Me.tab2.Size = New System.Drawing.Size(760, 281)
        Me.tab2.Text = "Detail Return Information"
        '
        'grdDetail_R
        '
        Me.grdDetail_R.ContextMenuStrip = Me.mnuStrip_R
        Me.grdDetail_R.DataSource = Me.bindingSource_R
        Me.grdDetail_R.Dock = System.Windows.Forms.DockStyle.Fill
        Me.grdDetail_R.Location = New System.Drawing.Point(0, 0)
        Me.grdDetail_R.MainView = Me.grvDetail_R
        Me.grdDetail_R.MenuManager = Me.barManager
        Me.grdDetail_R.Name = "grdDetail_R"
        Me.grdDetail_R.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemGridLookUpEdit2, Me.grdNOINVOICE_R, Me.txtREMARKS_R})
        Me.grdDetail_R.Size = New System.Drawing.Size(760, 281)
        Me.grdDetail_R.TabIndex = 19
        Me.grdDetail_R.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.grvDetail_R})
        '
        'mnuStrip_R
        '
        Me.mnuStrip_R.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.DeleteToolStripMenuItem_R})
        Me.mnuStrip_R.Name = "mnuStrip"
        Me.mnuStrip_R.Size = New System.Drawing.Size(108, 26)
        '
        'DeleteToolStripMenuItem_R
        '
        Me.DeleteToolStripMenuItem_R.Name = "DeleteToolStripMenuItem_R"
        Me.DeleteToolStripMenuItem_R.Size = New System.Drawing.Size(107, 22)
        Me.DeleteToolStripMenuItem_R.Text = "Delete"
        '
        'bindingSource_R
        '
        Me.bindingSource_R.DataSource = GetType(DataAccess.F_CASHIN_D)
        '
        'grvDetail_R
        '
        Me.grvDetail_R.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colNOINVOICE_R, Me.colAMOUNTORIGINAL_UB_R, Me.colAMOUNTDUE_UB_R, Me.colAMOUNTPAYMENT_R, Me.colREMARKS_R})
        Me.grvDetail_R.GridControl = Me.grdDetail_R
        Me.grvDetail_R.Name = "grvDetail_R"
        Me.grvDetail_R.OptionsCustomization.AllowColumnMoving = False
        Me.grvDetail_R.OptionsCustomization.AllowFilter = False
        Me.grvDetail_R.OptionsCustomization.AllowGroup = False
        Me.grvDetail_R.OptionsCustomization.AllowQuickHideColumns = False
        Me.grvDetail_R.OptionsCustomization.AllowSort = False
        Me.grvDetail_R.OptionsDetail.EnableMasterViewMode = False
        Me.grvDetail_R.OptionsFind.AllowFindPanel = False
        Me.grvDetail_R.OptionsLayout.StoreAllOptions = True
        Me.grvDetail_R.OptionsLayout.StoreAppearance = True
        Me.grvDetail_R.OptionsMenu.EnableColumnMenu = False
        Me.grvDetail_R.OptionsNavigation.AutoFocusNewRow = True
        Me.grvDetail_R.OptionsNavigation.EnterMoveNextColumn = True
        Me.grvDetail_R.OptionsView.EnableAppearanceEvenRow = True
        Me.grvDetail_R.OptionsView.EnableAppearanceOddRow = True
        Me.grvDetail_R.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Bottom
        Me.grvDetail_R.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never
        Me.grvDetail_R.OptionsView.ShowFooter = True
        Me.grvDetail_R.OptionsView.ShowGroupPanel = False
        '
        'colNOINVOICE_R
        '
        Me.colNOINVOICE_R.Caption = "Invoice No."
        Me.colNOINVOICE_R.ColumnEdit = Me.grdNOINVOICE_R
        Me.colNOINVOICE_R.FieldName = "NOINVOICE"
        Me.colNOINVOICE_R.Name = "colNOINVOICE_R"
        Me.colNOINVOICE_R.OptionsColumn.TabStop = False
        Me.colNOINVOICE_R.Visible = True
        Me.colNOINVOICE_R.VisibleIndex = 0
        '
        'grdNOINVOICE_R
        '
        Me.grdNOINVOICE_R.AutoHeight = False
        Me.grdNOINVOICE_R.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.grdNOINVOICE_R.Name = "grdNOINVOICE_R"
        Me.grdNOINVOICE_R.NullText = ""
        Me.grdNOINVOICE_R.PopupFormMinSize = New System.Drawing.Size(600, 300)
        Me.grdNOINVOICE_R.View = Me.grvNOINVOICE_R
        '
        'grvNOINVOICE_R
        '
        Me.grvNOINVOICE_R.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn8, Me.GridColumn7, Me.GridColumn10, Me.GridColumn11})
        Me.grvNOINVOICE_R.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.grvNOINVOICE_R.Name = "grvNOINVOICE_R"
        Me.grvNOINVOICE_R.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.grvNOINVOICE_R.OptionsView.ShowAutoFilterRow = True
        Me.grvNOINVOICE_R.OptionsView.ShowGroupPanel = False
        '
        'GridColumn8
        '
        Me.GridColumn8.Caption = "Number"
        Me.GridColumn8.FieldName = "NOINVOICE"
        Me.GridColumn8.Name = "GridColumn8"
        Me.GridColumn8.Visible = True
        Me.GridColumn8.VisibleIndex = 0
        '
        'colAMOUNTORIGINAL_UB_R
        '
        Me.colAMOUNTORIGINAL_UB_R.Caption = "Amount Original"
        Me.colAMOUNTORIGINAL_UB_R.DisplayFormat.FormatString = "{0:n2}"
        Me.colAMOUNTORIGINAL_UB_R.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.colAMOUNTORIGINAL_UB_R.FieldName = "AMOUNTORIGINAL_UB"
        Me.colAMOUNTORIGINAL_UB_R.Name = "colAMOUNTORIGINAL_UB_R"
        Me.colAMOUNTORIGINAL_UB_R.OptionsColumn.AllowEdit = False
        Me.colAMOUNTORIGINAL_UB_R.OptionsColumn.AllowFocus = False
        Me.colAMOUNTORIGINAL_UB_R.OptionsColumn.ReadOnly = True
        Me.colAMOUNTORIGINAL_UB_R.OptionsColumn.TabStop = False
        Me.colAMOUNTORIGINAL_UB_R.UnboundType = DevExpress.Data.UnboundColumnType.[Decimal]
        Me.colAMOUNTORIGINAL_UB_R.Visible = True
        Me.colAMOUNTORIGINAL_UB_R.VisibleIndex = 1
        '
        'colAMOUNTDUE_UB_R
        '
        Me.colAMOUNTDUE_UB_R.Caption = "Amount Due"
        Me.colAMOUNTDUE_UB_R.DisplayFormat.FormatString = "{0:n2}"
        Me.colAMOUNTDUE_UB_R.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.colAMOUNTDUE_UB_R.FieldName = "AMOUNTDUE_UB"
        Me.colAMOUNTDUE_UB_R.Name = "colAMOUNTDUE_UB_R"
        Me.colAMOUNTDUE_UB_R.OptionsColumn.AllowEdit = False
        Me.colAMOUNTDUE_UB_R.OptionsColumn.AllowFocus = False
        Me.colAMOUNTDUE_UB_R.OptionsColumn.ReadOnly = True
        Me.colAMOUNTDUE_UB_R.OptionsColumn.TabStop = False
        Me.colAMOUNTDUE_UB_R.UnboundType = DevExpress.Data.UnboundColumnType.[Decimal]
        Me.colAMOUNTDUE_UB_R.Visible = True
        Me.colAMOUNTDUE_UB_R.VisibleIndex = 2
        '
        'colAMOUNTPAYMENT_R
        '
        Me.colAMOUNTPAYMENT_R.AppearanceCell.Options.UseTextOptions = True
        Me.colAMOUNTPAYMENT_R.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.colAMOUNTPAYMENT_R.Caption = "Amount Payment"
        Me.colAMOUNTPAYMENT_R.DisplayFormat.FormatString = "{0:n2}"
        Me.colAMOUNTPAYMENT_R.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.colAMOUNTPAYMENT_R.FieldName = "AMOUNTPAYMENT"
        Me.colAMOUNTPAYMENT_R.Name = "colAMOUNTPAYMENT_R"
        Me.colAMOUNTPAYMENT_R.Visible = True
        Me.colAMOUNTPAYMENT_R.VisibleIndex = 3
        '
        'colREMARKS_R
        '
        Me.colREMARKS_R.AppearanceCell.Options.UseTextOptions = True
        Me.colREMARKS_R.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.colREMARKS_R.Caption = "Remarks"
        Me.colREMARKS_R.ColumnEdit = Me.txtREMARKS_R
        Me.colREMARKS_R.FieldName = "REMARKS"
        Me.colREMARKS_R.Name = "colREMARKS_R"
        Me.colREMARKS_R.Visible = True
        Me.colREMARKS_R.VisibleIndex = 4
        '
        'txtREMARKS_R
        '
        Me.txtREMARKS_R.AutoHeight = False
        Me.txtREMARKS_R.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtREMARKS_R.Name = "txtREMARKS_R"
        '
        'RepositoryItemGridLookUpEdit2
        '
        Me.RepositoryItemGridLookUpEdit2.AutoHeight = False
        Me.RepositoryItemGridLookUpEdit2.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemGridLookUpEdit2.Name = "RepositoryItemGridLookUpEdit2"
        Me.RepositoryItemGridLookUpEdit2.NullText = ""
        Me.RepositoryItemGridLookUpEdit2.PopupFormMinSize = New System.Drawing.Size(600, 300)
        Me.RepositoryItemGridLookUpEdit2.View = Me.GridView3
        '
        'GridView3
        '
        Me.GridView3.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn14, Me.GridColumn15, Me.GridColumn16})
        Me.GridView3.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridView3.Name = "GridView3"
        Me.GridView3.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView3.OptionsView.ShowAutoFilterRow = True
        Me.GridView3.OptionsView.ShowGroupPanel = False
        '
        'GridColumn14
        '
        Me.GridColumn14.Caption = "Item Name #1"
        Me.GridColumn14.FieldName = "NMITEM1"
        Me.GridColumn14.Name = "GridColumn14"
        Me.GridColumn14.Visible = True
        Me.GridColumn14.VisibleIndex = 0
        '
        'GridColumn15
        '
        Me.GridColumn15.Caption = "Item Name #2"
        Me.GridColumn15.FieldName = "NMITEM2"
        Me.GridColumn15.Name = "GridColumn15"
        Me.GridColumn15.Visible = True
        Me.GridColumn15.VisibleIndex = 1
        '
        'GridColumn16
        '
        Me.GridColumn16.Caption = "Item Name #3"
        Me.GridColumn16.FieldName = "NMITEM3"
        Me.GridColumn16.Name = "GridColumn16"
        Me.GridColumn16.Visible = True
        Me.GridColumn16.VisibleIndex = 2
        '
        'tab3
        '
        Me.tab3.Controls.Add(Me.LayoutControl1)
        Me.tab3.Name = "tab3"
        Me.tab3.Size = New System.Drawing.Size(760, 281)
        Me.tab3.Text = "Memo Information"
        '
        'LayoutControl1
        '
        Me.LayoutControl1.Controls.Add(Me.txtMEMO)
        Me.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControl1.Name = "LayoutControl1"
        Me.LayoutControl1.Root = Me.s
        Me.LayoutControl1.Size = New System.Drawing.Size(760, 281)
        Me.LayoutControl1.TabIndex = 0
        Me.LayoutControl1.Text = "LayoutControl1"
        '
        'txtMEMO
        '
        Me.txtMEMO.EnterMoveNextControl = True
        Me.txtMEMO.Location = New System.Drawing.Point(12, 12)
        Me.txtMEMO.MenuManager = Me.barManager
        Me.txtMEMO.Name = "txtMEMO"
        Me.txtMEMO.Size = New System.Drawing.Size(736, 257)
        Me.txtMEMO.StyleController = Me.LayoutControl1
        Me.txtMEMO.TabIndex = 4
        Me.txtMEMO.UseOptimizedRendering = True
        '
        's
        '
        Me.s.CustomizationFormText = "s"
        Me.s.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.s.GroupBordersVisible = False
        Me.s.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem7})
        Me.s.Location = New System.Drawing.Point(0, 0)
        Me.s.Name = "s"
        Me.s.Size = New System.Drawing.Size(760, 281)
        Me.s.Text = "s"
        Me.s.TextVisible = False
        '
        'LayoutControlItem7
        '
        Me.LayoutControlItem7.Control = Me.txtMEMO
        Me.LayoutControlItem7.CustomizationFormText = "LayoutControlItem7"
        Me.LayoutControlItem7.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem7.Name = "LayoutControlItem7"
        Me.LayoutControlItem7.Size = New System.Drawing.Size(740, 261)
        Me.LayoutControlItem7.Text = "LayoutControlItem7"
        Me.LayoutControlItem7.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem7.TextToControlDistance = 0
        Me.LayoutControlItem7.TextVisible = False
        '
        'txtKDCASHOUT
        '
        Me.txtKDCASHOUT.EditValue = ""
        Me.txtKDCASHOUT.EnterMoveNextControl = True
        Me.txtKDCASHOUT.Location = New System.Drawing.Point(117, 12)
        Me.txtKDCASHOUT.Name = "txtKDCASHOUT"
        Me.txtKDCASHOUT.Properties.ReadOnly = True
        Me.txtKDCASHOUT.Size = New System.Drawing.Size(375, 20)
        Me.txtKDCASHOUT.StyleController = Me.layoutControl
        Me.txtKDCASHOUT.TabIndex = 9
        Me.txtKDCASHOUT.TabStop = False
        '
        'LayoutControlGroup1
        '
        Me.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1"
        Me.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup1.GroupBordersVisible = False
        Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.lKDCASH, Me.LayoutControlItem5, Me.LayoutControlItem4, Me.lDATE, Me.LayoutControlItem3, Me.lKDVENDOR, Me.EmptySpaceItem2, Me.lKDPAYMENTTYPE, Me.lSUBTOTAL, Me.lADMIN, Me.lROUND, Me.lGRANDTOTAL})
        Me.LayoutControlGroup1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup1.Name = "Root"
        Me.LayoutControlGroup1.Size = New System.Drawing.Size(790, 549)
        Me.LayoutControlGroup1.Text = "Root"
        Me.LayoutControlGroup1.TextVisible = False
        '
        'lKDCASH
        '
        Me.lKDCASH.AppearanceItemCaption.Options.UseTextOptions = True
        Me.lKDCASH.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.lKDCASH.Control = Me.txtKDCASHOUT
        Me.lKDCASH.CustomizationFormText = "Display Name * :"
        Me.lKDCASH.Location = New System.Drawing.Point(0, 0)
        Me.lKDCASH.Name = "lKDCASH"
        Me.lKDCASH.Size = New System.Drawing.Size(484, 24)
        Me.lKDCASH.Text = "Number * :"
        Me.lKDCASH.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.lKDCASH.TextSize = New System.Drawing.Size(100, 20)
        Me.lKDCASH.TextToControlDistance = 5
        '
        'LayoutControlItem5
        '
        Me.LayoutControlItem5.Control = Me.tabControl
        Me.LayoutControlItem5.CustomizationFormText = "LayoutControlItem5"
        Me.LayoutControlItem5.Location = New System.Drawing.Point(0, 120)
        Me.LayoutControlItem5.Name = "LayoutControlItem5"
        Me.LayoutControlItem5.Size = New System.Drawing.Size(770, 313)
        Me.LayoutControlItem5.Text = "LayoutControlItem5"
        Me.LayoutControlItem5.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem5.TextToControlDistance = 0
        Me.LayoutControlItem5.TextVisible = False
        '
        'LayoutControlItem4
        '
        Me.LayoutControlItem4.Control = Me.LayoutControl2
        Me.LayoutControlItem4.CustomizationFormText = "LayoutControlItem4"
        Me.LayoutControlItem4.Location = New System.Drawing.Point(0, 96)
        Me.LayoutControlItem4.Name = "LayoutControlItem4"
        Me.LayoutControlItem4.Size = New System.Drawing.Size(484, 24)
        Me.LayoutControlItem4.Text = "LayoutControlItem4"
        Me.LayoutControlItem4.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem4.TextToControlDistance = 0
        Me.LayoutControlItem4.TextVisible = False
        '
        'lDATE
        '
        Me.lDATE.AppearanceItemCaption.Options.UseTextOptions = True
        Me.lDATE.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.lDATE.Control = Me.deDATE
        Me.lDATE.CustomizationFormText = "Date :"
        Me.lDATE.Location = New System.Drawing.Point(0, 24)
        Me.lDATE.Name = "lDATE"
        Me.lDATE.Size = New System.Drawing.Size(484, 24)
        Me.lDATE.Text = "Date :"
        Me.lDATE.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.lDATE.TextSize = New System.Drawing.Size(100, 20)
        Me.lDATE.TextToControlDistance = 5
        '
        'LayoutControlItem3
        '
        Me.LayoutControlItem3.Control = Me.LayoutControl5
        Me.LayoutControlItem3.CustomizationFormText = "LayoutControlItem3"
        Me.LayoutControlItem3.Location = New System.Drawing.Point(484, 0)
        Me.LayoutControlItem3.Name = "LayoutControlItem3"
        Me.LayoutControlItem3.Size = New System.Drawing.Size(286, 120)
        Me.LayoutControlItem3.Text = "LayoutControlItem3"
        Me.LayoutControlItem3.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem3.TextToControlDistance = 0
        Me.LayoutControlItem3.TextVisible = False
        '
        'lKDVENDOR
        '
        Me.lKDVENDOR.AppearanceItemCaption.Options.UseTextOptions = True
        Me.lKDVENDOR.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.lKDVENDOR.Control = Me.grdKDVENDOR
        Me.lKDVENDOR.CustomizationFormText = "Vendor * :"
        Me.lKDVENDOR.Location = New System.Drawing.Point(0, 48)
        Me.lKDVENDOR.Name = "lKDVENDOR"
        Me.lKDVENDOR.Size = New System.Drawing.Size(484, 24)
        Me.lKDVENDOR.Text = "Vendor * :"
        Me.lKDVENDOR.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.lKDVENDOR.TextSize = New System.Drawing.Size(100, 20)
        Me.lKDVENDOR.TextToControlDistance = 5
        '
        'EmptySpaceItem2
        '
        Me.EmptySpaceItem2.AllowHotTrack = False
        Me.EmptySpaceItem2.CustomizationFormText = "EmptySpaceItem2"
        Me.EmptySpaceItem2.Location = New System.Drawing.Point(0, 433)
        Me.EmptySpaceItem2.Name = "EmptySpaceItem2"
        Me.EmptySpaceItem2.Size = New System.Drawing.Size(385, 96)
        Me.EmptySpaceItem2.Text = "EmptySpaceItem2"
        Me.EmptySpaceItem2.TextSize = New System.Drawing.Size(0, 0)
        '
        'lKDPAYMENTTYPE
        '
        Me.lKDPAYMENTTYPE.AppearanceItemCaption.Options.UseTextOptions = True
        Me.lKDPAYMENTTYPE.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.lKDPAYMENTTYPE.Control = Me.grdKDPAYMENTTYPE
        Me.lKDPAYMENTTYPE.CustomizationFormText = "Warehouse * :"
        Me.lKDPAYMENTTYPE.Location = New System.Drawing.Point(0, 72)
        Me.lKDPAYMENTTYPE.Name = "lKDPAYMENTTYPE"
        Me.lKDPAYMENTTYPE.Size = New System.Drawing.Size(484, 24)
        Me.lKDPAYMENTTYPE.Text = "Payment Type * :"
        Me.lKDPAYMENTTYPE.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.lKDPAYMENTTYPE.TextSize = New System.Drawing.Size(100, 20)
        Me.lKDPAYMENTTYPE.TextToControlDistance = 5
        '
        'lSUBTOTAL
        '
        Me.lSUBTOTAL.AppearanceItemCaption.Options.UseTextOptions = True
        Me.lSUBTOTAL.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.lSUBTOTAL.Control = Me.txtSUBTOTAL
        Me.lSUBTOTAL.CustomizationFormText = "Sub Total :"
        Me.lSUBTOTAL.Location = New System.Drawing.Point(385, 433)
        Me.lSUBTOTAL.Name = "lSUBTOTAL"
        Me.lSUBTOTAL.Size = New System.Drawing.Size(385, 24)
        Me.lSUBTOTAL.Text = "Sub Total :"
        Me.lSUBTOTAL.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.lSUBTOTAL.TextSize = New System.Drawing.Size(100, 20)
        Me.lSUBTOTAL.TextToControlDistance = 5
        '
        'lADMIN
        '
        Me.lADMIN.AppearanceItemCaption.Options.UseTextOptions = True
        Me.lADMIN.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.lADMIN.Control = Me.txtADMIN
        Me.lADMIN.CustomizationFormText = "Admin :"
        Me.lADMIN.Location = New System.Drawing.Point(385, 457)
        Me.lADMIN.Name = "lADMIN"
        Me.lADMIN.Size = New System.Drawing.Size(385, 24)
        Me.lADMIN.Text = "Admin :"
        Me.lADMIN.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.lADMIN.TextSize = New System.Drawing.Size(100, 20)
        Me.lADMIN.TextToControlDistance = 5
        '
        'lROUND
        '
        Me.lROUND.AppearanceItemCaption.Options.UseTextOptions = True
        Me.lROUND.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.lROUND.Control = Me.txtROUND
        Me.lROUND.CustomizationFormText = "Round :"
        Me.lROUND.Location = New System.Drawing.Point(385, 481)
        Me.lROUND.Name = "lROUND"
        Me.lROUND.Size = New System.Drawing.Size(385, 24)
        Me.lROUND.Text = "Round :"
        Me.lROUND.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.lROUND.TextSize = New System.Drawing.Size(100, 20)
        Me.lROUND.TextToControlDistance = 5
        '
        'lGRANDTOTAL
        '
        Me.lGRANDTOTAL.AppearanceItemCaption.Options.UseTextOptions = True
        Me.lGRANDTOTAL.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.lGRANDTOTAL.Control = Me.txtGRANDTOTAL
        Me.lGRANDTOTAL.CustomizationFormText = "Grand Total :"
        Me.lGRANDTOTAL.Location = New System.Drawing.Point(385, 505)
        Me.lGRANDTOTAL.Name = "lGRANDTOTAL"
        Me.lGRANDTOTAL.Size = New System.Drawing.Size(385, 24)
        Me.lGRANDTOTAL.Text = "Grand Total :"
        Me.lGRANDTOTAL.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.lGRANDTOTAL.TextSize = New System.Drawing.Size(100, 20)
        Me.lGRANDTOTAL.TextToControlDistance = 5
        '
        'GridColumn6
        '
        Me.GridColumn6.Caption = "Number"
        Me.GridColumn6.FieldName = "KDSO"
        Me.GridColumn6.Name = "GridColumn6"
        Me.GridColumn6.Visible = True
        Me.GridColumn6.VisibleIndex = 0
        '
        'GridColumn1
        '
        Me.GridColumn1.Caption = "Tanggal"
        Me.GridColumn1.DisplayFormat.FormatString = "dd/MM/yyyy"
        Me.GridColumn1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.GridColumn1.FieldName = "DATE"
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.Visible = True
        Me.GridColumn1.VisibleIndex = 1
        '
        'GridColumn2
        '
        Me.GridColumn2.AppearanceCell.Options.UseTextOptions = True
        Me.GridColumn2.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.GridColumn2.Caption = "Total"
        Me.GridColumn2.DisplayFormat.FormatString = "{0:n2}"
        Me.GridColumn2.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.GridColumn2.FieldName = "GRANDTOTAL"
        Me.GridColumn2.Name = "GridColumn2"
        Me.GridColumn2.Visible = True
        Me.GridColumn2.VisibleIndex = 2
        '
        'GridColumn5
        '
        Me.GridColumn5.AppearanceCell.Options.UseTextOptions = True
        Me.GridColumn5.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.GridColumn5.Caption = "Telah Dibayar"
        Me.GridColumn5.DisplayFormat.FormatString = "{0:n2}"
        Me.GridColumn5.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.GridColumn5.FieldName = "PAYAMOUNT"
        Me.GridColumn5.Name = "GridColumn5"
        Me.GridColumn5.Visible = True
        Me.GridColumn5.VisibleIndex = 3
        '
        'GridColumn7
        '
        Me.GridColumn7.Caption = "Tanggal"
        Me.GridColumn7.DisplayFormat.FormatString = "dd/MM/yyyy"
        Me.GridColumn7.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.GridColumn7.FieldName = "DATE"
        Me.GridColumn7.Name = "GridColumn7"
        Me.GridColumn7.Visible = True
        Me.GridColumn7.VisibleIndex = 1
        '
        'GridColumn10
        '
        Me.GridColumn10.AppearanceCell.Options.UseTextOptions = True
        Me.GridColumn10.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.GridColumn10.Caption = "Total"
        Me.GridColumn10.DisplayFormat.FormatString = "{0:n2}"
        Me.GridColumn10.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.GridColumn10.FieldName = "GRANDTOTAL"
        Me.GridColumn10.Name = "GridColumn10"
        Me.GridColumn10.Visible = True
        Me.GridColumn10.VisibleIndex = 2
        '
        'GridColumn11
        '
        Me.GridColumn11.AppearanceCell.Options.UseTextOptions = True
        Me.GridColumn11.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.GridColumn11.Caption = "Telah Dibayar"
        Me.GridColumn11.DisplayFormat.FormatString = "{0:n2}"
        Me.GridColumn11.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.GridColumn11.FieldName = "PAYAMOUNT"
        Me.GridColumn11.Name = "GridColumn11"
        Me.GridColumn11.Visible = True
        Me.GridColumn11.VisibleIndex = 3
        '
        'frmCashOut
        '
        Me.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(235, Byte), Integer), CType(CType(236, Byte), Integer), CType(CType(239, Byte), Integer))
        Me.Appearance.Options.UseBackColor = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(790, 571)
        Me.Controls.Add(Me.layoutControl)
        Me.Controls.Add(Me.barDockControlLeft)
        Me.Controls.Add(Me.barDockControlRight)
        Me.Controls.Add(Me.barDockControlBottom)
        Me.Controls.Add(Me.barDockControlTop)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.KeyPreview = True
        Me.Name = "frmCashOut"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        CType(Me.layoutControl, System.ComponentModel.ISupportInitialize).EndInit()
        Me.layoutControl.ResumeLayout(False)
        CType(Me.txtGRANDTOTAL.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.barManager, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.progressBarSave, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.progressSave, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtROUND.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtADMIN.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSUBTOTAL.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grdKDPAYMENTTYPE.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvKDPAYMENTTYPE, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grdKDVENDOR.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvKDVENDOR, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControl5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deDATE.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deDATE.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControl2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Root, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tabControl, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabControl.ResumeLayout(False)
        Me.tab1.ResumeLayout(False)
        CType(Me.grdDetail, System.ComponentModel.ISupportInitialize).EndInit()
        Me.mnuStrip.ResumeLayout(False)
        CType(Me.bindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvDetail, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grdNOINVOICE, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvNOINVOICE, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtREMARKS, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tab2.ResumeLayout(False)
        CType(Me.grdDetail_R, System.ComponentModel.ISupportInitialize).EndInit()
        Me.mnuStrip_R.ResumeLayout(False)
        CType(Me.bindingSource_R, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvDetail_R, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grdNOINVOICE_R, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvNOINVOICE_R, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtREMARKS_R, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemGridLookUpEdit2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tab3.ResumeLayout(False)
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl1.ResumeLayout(False)
        CType(Me.txtMEMO.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.s, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtKDCASHOUT.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lKDCASH, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lDATE, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lKDVENDOR, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lKDPAYMENTTYPE, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lSUBTOTAL, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lADMIN, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lROUND, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lGRANDTOTAL, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents layoutControl As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents lKDCASH As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents barManager As DevExpress.XtraBars.BarManager
    Friend WithEvents barTop As DevExpress.XtraBars.Bar
    Friend WithEvents barDockControlTop As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlBottom As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlLeft As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlRight As DevExpress.XtraBars.BarDockControl
    Friend WithEvents btnSaveNew As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnClose As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnSaveClose As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents progressBarSave As DevExpress.XtraEditors.Repository.RepositoryItemMarqueeProgressBar
    Friend WithEvents progressSave As DevExpress.XtraEditors.Repository.RepositoryItemMarqueeProgressBar
    Friend WithEvents txtKDCASHOUT As DevExpress.XtraEditors.TextEdit
    Friend WithEvents bindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents tabControl As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents tab1 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents LayoutControlItem5 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents grdDetail As DevExpress.XtraGrid.GridControl
    Friend WithEvents grvDetail As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents LayoutControl2 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents Root As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem4 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents deDATE As DevExpress.XtraEditors.DateEdit
    Friend WithEvents lDATE As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControl5 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents LayoutControlGroup5 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem3 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents tab3 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents LayoutControl1 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents s As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents txtMEMO As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents LayoutControlItem7 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents colAMOUNTPAYMENT As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colREMARKS As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grdNOINVOICE As DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit
    Friend WithEvents grvNOINVOICE As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn4 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents mnuStrip As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents DeleteToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EmptySpaceItem1 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents colNOINVOICE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents txtREMARKS As DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit
    Friend WithEvents grdKDVENDOR As DevExpress.XtraEditors.GridLookUpEdit
    Friend WithEvents grvKDVENDOR As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents lKDVENDOR As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents GridColumn3 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn6 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents EmptySpaceItem2 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents grdKDPAYMENTTYPE As DevExpress.XtraEditors.GridLookUpEdit
    Friend WithEvents grvKDPAYMENTTYPE As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn9 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents lKDPAYMENTTYPE As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents colAMOUNTORIGINAL_UB As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colAMOUNTDUE_UB As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents txtGRANDTOTAL As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtROUND As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtADMIN As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtSUBTOTAL As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lSUBTOTAL As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents lADMIN As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents lROUND As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents lGRANDTOTAL As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents tab2 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents grdDetail_R As DevExpress.XtraGrid.GridControl
    Friend WithEvents grvDetail_R As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colNOINVOICE_R As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grdNOINVOICE_R As DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit
    Friend WithEvents grvNOINVOICE_R As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn8 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colAMOUNTORIGINAL_UB_R As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colAMOUNTDUE_UB_R As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colAMOUNTPAYMENT_R As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colREMARKS_R As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents txtREMARKS_R As DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit
    Friend WithEvents RepositoryItemGridLookUpEdit2 As DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit
    Friend WithEvents GridView3 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn14 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn15 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn16 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents mnuStrip_R As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents DeleteToolStripMenuItem_R As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bindingSource_R As System.Windows.Forms.BindingSource
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn5 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn7 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn10 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn11 As DevExpress.XtraGrid.Columns.GridColumn
End Class
