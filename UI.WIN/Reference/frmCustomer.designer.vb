﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCustomer
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.layoutControl = New DevExpress.XtraLayout.LayoutControl()
        Me.txtLIMIT = New DevExpress.XtraEditors.TextEdit()
        Me.barManager = New DevExpress.XtraBars.BarManager(Me.components)
        Me.barTop = New DevExpress.XtraBars.Bar()
        Me.btnSaveNew = New DevExpress.XtraBars.BarButtonItem()
        Me.btnSaveClose = New DevExpress.XtraBars.BarButtonItem()
        Me.btnClose = New DevExpress.XtraBars.BarButtonItem()
        Me.barDockControlTop = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlBottom = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlLeft = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlRight = New DevExpress.XtraBars.BarDockControl()
        Me.progressBarSave = New DevExpress.XtraEditors.Repository.RepositoryItemMarqueeProgressBar()
        Me.progressSave = New DevExpress.XtraEditors.Repository.RepositoryItemMarqueeProgressBar()
        Me.txtDUE = New DevExpress.XtraEditors.TextEdit()
        Me.chkISACTIVE = New DevExpress.XtraEditors.CheckEdit()
        Me.txtNothing = New DevExpress.XtraEditors.TextEdit()
        Me.tabControl = New DevExpress.XtraTab.XtraTabControl()
        Me.tab1 = New DevExpress.XtraTab.XtraTabPage()
        Me.LayoutControl2 = New DevExpress.XtraLayout.LayoutControl()
        Me.txtWEBSITE = New DevExpress.XtraEditors.TextEdit()
        Me.txtOTHER = New DevExpress.XtraEditors.TextEdit()
        Me.txtEMAIL = New DevExpress.XtraEditors.TextEdit()
        Me.txtMOBILE = New DevExpress.XtraEditors.TextEdit()
        Me.txtFAX = New DevExpress.XtraEditors.TextEdit()
        Me.txtPHONE = New DevExpress.XtraEditors.TextEdit()
        Me.LayoutControlGroup2 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.lPHONE = New DevExpress.XtraLayout.LayoutControlItem()
        Me.lFAX = New DevExpress.XtraLayout.LayoutControlItem()
        Me.lMOBILE = New DevExpress.XtraLayout.LayoutControlItem()
        Me.lEMAIL = New DevExpress.XtraLayout.LayoutControlItem()
        Me.lOTHER = New DevExpress.XtraLayout.LayoutControlItem()
        Me.lWEBSITE = New DevExpress.XtraLayout.LayoutControlItem()
        Me.tab2 = New DevExpress.XtraTab.XtraTabPage()
        Me.LayoutControl3 = New DevExpress.XtraLayout.LayoutControl()
        Me.txtBILL_CITY = New DevExpress.XtraEditors.TextEdit()
        Me.txtBILL_STREET = New DevExpress.XtraEditors.MemoEdit()
        Me.txtBILL_STATE = New DevExpress.XtraEditors.TextEdit()
        Me.txtBILL_COUNTRY = New DevExpress.XtraEditors.TextEdit()
        Me.txtBILL_ZIP = New DevExpress.XtraEditors.TextEdit()
        Me.LayoutControlGroup3 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.lBILL_CITY = New DevExpress.XtraLayout.LayoutControlItem()
        Me.lBILL_STREET = New DevExpress.XtraLayout.LayoutControlItem()
        Me.lBILL_STATE = New DevExpress.XtraLayout.LayoutControlItem()
        Me.lBILL_ZIP = New DevExpress.XtraLayout.LayoutControlItem()
        Me.lBILL_COUNTRY = New DevExpress.XtraLayout.LayoutControlItem()
        Me.tab3 = New DevExpress.XtraTab.XtraTabPage()
        Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl()
        Me.grdKDCOA = New DevExpress.XtraEditors.GridLookUpEdit()
        Me.grvKDCOA = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.LayoutControlGroup4 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.lKDCOA = New DevExpress.XtraLayout.LayoutControlItem()
        Me.tab4 = New DevExpress.XtraTab.XtraTabPage()
        Me.LayoutControl8 = New DevExpress.XtraLayout.LayoutControl()
        Me.txtMEMO = New DevExpress.XtraEditors.MemoEdit()
        Me.LayoutControlGroup8 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem33 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.txtNAME_DISPLAY = New DevExpress.XtraEditors.TextEdit()
        Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.lNAME_DISPLAY = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem9 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem34 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.lDUE = New DevExpress.XtraLayout.LayoutControlItem()
        Me.lLIMIT = New DevExpress.XtraLayout.LayoutControlItem()
        CType(Me.layoutControl, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.layoutControl.SuspendLayout()
        CType(Me.txtLIMIT.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.barManager, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.progressBarSave, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.progressSave, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDUE.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkISACTIVE.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNothing.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tabControl, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabControl.SuspendLayout()
        Me.tab1.SuspendLayout()
        CType(Me.LayoutControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl2.SuspendLayout()
        CType(Me.txtWEBSITE.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtOTHER.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtEMAIL.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtMOBILE.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtFAX.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtPHONE.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lPHONE, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lFAX, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lMOBILE, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lEMAIL, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lOTHER, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lWEBSITE, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tab2.SuspendLayout()
        CType(Me.LayoutControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl3.SuspendLayout()
        CType(Me.txtBILL_CITY.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtBILL_STREET.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtBILL_STATE.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtBILL_COUNTRY.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtBILL_ZIP.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lBILL_CITY, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lBILL_STREET, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lBILL_STATE, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lBILL_ZIP, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lBILL_COUNTRY, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tab3.SuspendLayout()
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl1.SuspendLayout()
        CType(Me.grdKDCOA.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvKDCOA, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lKDCOA, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tab4.SuspendLayout()
        CType(Me.LayoutControl8, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl8.SuspendLayout()
        CType(Me.txtMEMO.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem33, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNAME_DISPLAY.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lNAME_DISPLAY, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem34, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lDUE, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lLIMIT, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'layoutControl
        '
        Me.layoutControl.Controls.Add(Me.txtLIMIT)
        Me.layoutControl.Controls.Add(Me.txtDUE)
        Me.layoutControl.Controls.Add(Me.chkISACTIVE)
        Me.layoutControl.Controls.Add(Me.txtNothing)
        Me.layoutControl.Controls.Add(Me.tabControl)
        Me.layoutControl.Controls.Add(Me.txtNAME_DISPLAY)
        Me.layoutControl.Dock = System.Windows.Forms.DockStyle.Fill
        Me.layoutControl.Location = New System.Drawing.Point(0, 0)
        Me.layoutControl.Name = "layoutControl"
        Me.layoutControl.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = New System.Drawing.Rectangle(774, 238, 250, 350)
        Me.layoutControl.Root = Me.LayoutControlGroup1
        Me.layoutControl.Size = New System.Drawing.Size(590, 401)
        Me.layoutControl.TabIndex = 0
        Me.layoutControl.Text = "LayoutControl1"
        '
        'txtLIMIT
        '
        Me.txtLIMIT.EnterMoveNextControl = True
        Me.txtLIMIT.Location = New System.Drawing.Point(117, 60)
        Me.txtLIMIT.MenuManager = Me.barManager
        Me.txtLIMIT.Name = "txtLIMIT"
        Me.txtLIMIT.Properties.Appearance.Options.UseTextOptions = True
        Me.txtLIMIT.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtLIMIT.Properties.Mask.EditMask = "n0"
        Me.txtLIMIT.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtLIMIT.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtLIMIT.Properties.NullText = "0"
        Me.txtLIMIT.Size = New System.Drawing.Size(284, 20)
        Me.txtLIMIT.StyleController = Me.layoutControl
        Me.txtLIMIT.TabIndex = 16
        '
        'barManager
        '
        Me.barManager.AllowQuickCustomization = False
        Me.barManager.Bars.AddRange(New DevExpress.XtraBars.Bar() {Me.barTop})
        Me.barManager.DockControls.Add(Me.barDockControlTop)
        Me.barManager.DockControls.Add(Me.barDockControlBottom)
        Me.barManager.DockControls.Add(Me.barDockControlLeft)
        Me.barManager.DockControls.Add(Me.barDockControlRight)
        Me.barManager.Form = Me
        Me.barManager.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.btnSaveNew, Me.btnClose, Me.btnSaveClose})
        Me.barManager.MainMenu = Me.barTop
        Me.barManager.MaxItemId = 8
        Me.barManager.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.progressBarSave, Me.progressSave})
        '
        'barTop
        '
        Me.barTop.BarName = "Main menu"
        Me.barTop.DockCol = 0
        Me.barTop.DockRow = 0
        Me.barTop.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom
        Me.barTop.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.btnSaveNew), New DevExpress.XtraBars.LinkPersistInfo(Me.btnSaveClose), New DevExpress.XtraBars.LinkPersistInfo(Me.btnClose)})
        Me.barTop.OptionsBar.DrawDragBorder = False
        Me.barTop.OptionsBar.MultiLine = True
        Me.barTop.OptionsBar.UseWholeRow = True
        Me.barTop.Text = "Main menu"
        '
        'btnSaveNew
        '
        Me.btnSaveNew.Caption = "F2 - Save && New"
        Me.btnSaveNew.Id = 2
        Me.btnSaveNew.Name = "btnSaveNew"
        '
        'btnSaveClose
        '
        Me.btnSaveClose.Caption = "F3 - Save && Close"
        Me.btnSaveClose.Id = 5
        Me.btnSaveClose.Name = "btnSaveClose"
        '
        'btnClose
        '
        Me.btnClose.Caption = "F12 - Close"
        Me.btnClose.Id = 3
        Me.btnClose.Name = "btnClose"
        '
        'barDockControlTop
        '
        Me.barDockControlTop.CausesValidation = False
        Me.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top
        Me.barDockControlTop.Location = New System.Drawing.Point(0, 0)
        Me.barDockControlTop.Size = New System.Drawing.Size(590, 0)
        '
        'barDockControlBottom
        '
        Me.barDockControlBottom.CausesValidation = False
        Me.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.barDockControlBottom.Location = New System.Drawing.Point(0, 401)
        Me.barDockControlBottom.Size = New System.Drawing.Size(590, 22)
        '
        'barDockControlLeft
        '
        Me.barDockControlLeft.CausesValidation = False
        Me.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left
        Me.barDockControlLeft.Location = New System.Drawing.Point(0, 0)
        Me.barDockControlLeft.Size = New System.Drawing.Size(0, 401)
        '
        'barDockControlRight
        '
        Me.barDockControlRight.CausesValidation = False
        Me.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right
        Me.barDockControlRight.Location = New System.Drawing.Point(590, 0)
        Me.barDockControlRight.Size = New System.Drawing.Size(0, 401)
        '
        'progressBarSave
        '
        Me.progressBarSave.Name = "progressBarSave"
        Me.progressBarSave.Stopped = True
        '
        'progressSave
        '
        Me.progressSave.Name = "progressSave"
        Me.progressSave.Paused = True
        '
        'txtDUE
        '
        Me.txtDUE.EnterMoveNextControl = True
        Me.txtDUE.Location = New System.Drawing.Point(117, 36)
        Me.txtDUE.MenuManager = Me.barManager
        Me.txtDUE.Name = "txtDUE"
        Me.txtDUE.Properties.Appearance.Options.UseTextOptions = True
        Me.txtDUE.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtDUE.Properties.Mask.EditMask = "n0"
        Me.txtDUE.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtDUE.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtDUE.Properties.NullText = "0"
        Me.txtDUE.Size = New System.Drawing.Size(284, 20)
        Me.txtDUE.StyleController = Me.layoutControl
        Me.txtDUE.TabIndex = 15
        '
        'chkISACTIVE
        '
        Me.chkISACTIVE.Location = New System.Drawing.Point(405, 12)
        Me.chkISACTIVE.MenuManager = Me.barManager
        Me.chkISACTIVE.Name = "chkISACTIVE"
        Me.chkISACTIVE.Properties.Caption = "Active?"
        Me.chkISACTIVE.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.chkISACTIVE.Size = New System.Drawing.Size(173, 19)
        Me.chkISACTIVE.StyleController = Me.layoutControl
        Me.chkISACTIVE.TabIndex = 14
        Me.chkISACTIVE.TabStop = False
        '
        'txtNothing
        '
        Me.txtNothing.Location = New System.Drawing.Point(118, 84)
        Me.txtNothing.Name = "txtNothing"
        Me.txtNothing.Size = New System.Drawing.Size(460, 20)
        Me.txtNothing.StyleController = Me.layoutControl
        Me.txtNothing.TabIndex = 13
        '
        'tabControl
        '
        Me.tabControl.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(235, Byte), Integer), CType(CType(236, Byte), Integer), CType(CType(239, Byte), Integer))
        Me.tabControl.Appearance.Options.UseBackColor = True
        Me.tabControl.Location = New System.Drawing.Point(12, 108)
        Me.tabControl.Name = "tabControl"
        Me.tabControl.SelectedTabPage = Me.tab1
        Me.tabControl.Size = New System.Drawing.Size(566, 281)
        Me.tabControl.TabIndex = 12
        Me.tabControl.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.tab1, Me.tab2, Me.tab3, Me.tab4})
        '
        'tab1
        '
        Me.tab1.Controls.Add(Me.LayoutControl2)
        Me.tab1.Name = "tab1"
        Me.tab1.Size = New System.Drawing.Size(560, 253)
        Me.tab1.Text = "Contact Information"
        '
        'LayoutControl2
        '
        Me.LayoutControl2.Controls.Add(Me.txtWEBSITE)
        Me.LayoutControl2.Controls.Add(Me.txtOTHER)
        Me.LayoutControl2.Controls.Add(Me.txtEMAIL)
        Me.LayoutControl2.Controls.Add(Me.txtMOBILE)
        Me.LayoutControl2.Controls.Add(Me.txtFAX)
        Me.LayoutControl2.Controls.Add(Me.txtPHONE)
        Me.LayoutControl2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl2.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControl2.Name = "LayoutControl2"
        Me.LayoutControl2.Root = Me.LayoutControlGroup2
        Me.LayoutControl2.Size = New System.Drawing.Size(560, 253)
        Me.LayoutControl2.TabIndex = 0
        Me.LayoutControl2.Text = "LayoutControl2"
        '
        'txtWEBSITE
        '
        Me.txtWEBSITE.EnterMoveNextControl = True
        Me.txtWEBSITE.Location = New System.Drawing.Point(117, 132)
        Me.txtWEBSITE.Name = "txtWEBSITE"
        Me.txtWEBSITE.Size = New System.Drawing.Size(431, 20)
        Me.txtWEBSITE.StyleController = Me.LayoutControl2
        Me.txtWEBSITE.TabIndex = 9
        '
        'txtOTHER
        '
        Me.txtOTHER.EnterMoveNextControl = True
        Me.txtOTHER.Location = New System.Drawing.Point(117, 108)
        Me.txtOTHER.Name = "txtOTHER"
        Me.txtOTHER.Size = New System.Drawing.Size(431, 20)
        Me.txtOTHER.StyleController = Me.LayoutControl2
        Me.txtOTHER.TabIndex = 8
        '
        'txtEMAIL
        '
        Me.txtEMAIL.EnterMoveNextControl = True
        Me.txtEMAIL.Location = New System.Drawing.Point(117, 84)
        Me.txtEMAIL.Name = "txtEMAIL"
        Me.txtEMAIL.Size = New System.Drawing.Size(431, 20)
        Me.txtEMAIL.StyleController = Me.LayoutControl2
        Me.txtEMAIL.TabIndex = 7
        '
        'txtMOBILE
        '
        Me.txtMOBILE.EnterMoveNextControl = True
        Me.txtMOBILE.Location = New System.Drawing.Point(117, 60)
        Me.txtMOBILE.Name = "txtMOBILE"
        Me.txtMOBILE.Size = New System.Drawing.Size(431, 20)
        Me.txtMOBILE.StyleController = Me.LayoutControl2
        Me.txtMOBILE.TabIndex = 6
        '
        'txtFAX
        '
        Me.txtFAX.EnterMoveNextControl = True
        Me.txtFAX.Location = New System.Drawing.Point(117, 36)
        Me.txtFAX.Name = "txtFAX"
        Me.txtFAX.Size = New System.Drawing.Size(431, 20)
        Me.txtFAX.StyleController = Me.LayoutControl2
        Me.txtFAX.TabIndex = 5
        '
        'txtPHONE
        '
        Me.txtPHONE.EnterMoveNextControl = True
        Me.txtPHONE.Location = New System.Drawing.Point(117, 12)
        Me.txtPHONE.Name = "txtPHONE"
        Me.txtPHONE.Size = New System.Drawing.Size(431, 20)
        Me.txtPHONE.StyleController = Me.LayoutControl2
        Me.txtPHONE.TabIndex = 4
        '
        'LayoutControlGroup2
        '
        Me.LayoutControlGroup2.CustomizationFormText = "LayoutControlGroup2"
        Me.LayoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup2.GroupBordersVisible = False
        Me.LayoutControlGroup2.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.lPHONE, Me.lFAX, Me.lMOBILE, Me.lEMAIL, Me.lOTHER, Me.lWEBSITE})
        Me.LayoutControlGroup2.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup2.Name = "LayoutControlGroup2"
        Me.LayoutControlGroup2.Size = New System.Drawing.Size(560, 253)
        Me.LayoutControlGroup2.Text = "LayoutControlGroup2"
        Me.LayoutControlGroup2.TextVisible = False
        '
        'lPHONE
        '
        Me.lPHONE.AppearanceItemCaption.Options.UseTextOptions = True
        Me.lPHONE.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.lPHONE.Control = Me.txtPHONE
        Me.lPHONE.CustomizationFormText = "Phone :"
        Me.lPHONE.Location = New System.Drawing.Point(0, 0)
        Me.lPHONE.Name = "lPHONE"
        Me.lPHONE.Size = New System.Drawing.Size(540, 24)
        Me.lPHONE.Text = "Phone :"
        Me.lPHONE.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.lPHONE.TextSize = New System.Drawing.Size(100, 20)
        Me.lPHONE.TextToControlDistance = 5
        '
        'lFAX
        '
        Me.lFAX.AppearanceItemCaption.Options.UseTextOptions = True
        Me.lFAX.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.lFAX.Control = Me.txtFAX
        Me.lFAX.CustomizationFormText = "Fax :"
        Me.lFAX.Location = New System.Drawing.Point(0, 24)
        Me.lFAX.Name = "lFAX"
        Me.lFAX.Size = New System.Drawing.Size(540, 24)
        Me.lFAX.Text = "Fax :"
        Me.lFAX.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.lFAX.TextSize = New System.Drawing.Size(100, 20)
        Me.lFAX.TextToControlDistance = 5
        '
        'lMOBILE
        '
        Me.lMOBILE.AppearanceItemCaption.Options.UseTextOptions = True
        Me.lMOBILE.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.lMOBILE.Control = Me.txtMOBILE
        Me.lMOBILE.CustomizationFormText = "Mobile :"
        Me.lMOBILE.Location = New System.Drawing.Point(0, 48)
        Me.lMOBILE.Name = "lMOBILE"
        Me.lMOBILE.Size = New System.Drawing.Size(540, 24)
        Me.lMOBILE.Text = "Mobile :"
        Me.lMOBILE.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.lMOBILE.TextSize = New System.Drawing.Size(100, 20)
        Me.lMOBILE.TextToControlDistance = 5
        '
        'lEMAIL
        '
        Me.lEMAIL.AppearanceItemCaption.Options.UseTextOptions = True
        Me.lEMAIL.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.lEMAIL.Control = Me.txtEMAIL
        Me.lEMAIL.CustomizationFormText = "Email :"
        Me.lEMAIL.Location = New System.Drawing.Point(0, 72)
        Me.lEMAIL.Name = "lEMAIL"
        Me.lEMAIL.Size = New System.Drawing.Size(540, 24)
        Me.lEMAIL.Text = "Email :"
        Me.lEMAIL.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.lEMAIL.TextSize = New System.Drawing.Size(100, 20)
        Me.lEMAIL.TextToControlDistance = 5
        '
        'lOTHER
        '
        Me.lOTHER.AppearanceItemCaption.Options.UseTextOptions = True
        Me.lOTHER.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.lOTHER.Control = Me.txtOTHER
        Me.lOTHER.CustomizationFormText = "Other :"
        Me.lOTHER.Location = New System.Drawing.Point(0, 96)
        Me.lOTHER.Name = "lOTHER"
        Me.lOTHER.Size = New System.Drawing.Size(540, 24)
        Me.lOTHER.Text = "Other :"
        Me.lOTHER.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.lOTHER.TextSize = New System.Drawing.Size(100, 20)
        Me.lOTHER.TextToControlDistance = 5
        '
        'lWEBSITE
        '
        Me.lWEBSITE.AppearanceItemCaption.Options.UseTextOptions = True
        Me.lWEBSITE.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.lWEBSITE.Control = Me.txtWEBSITE
        Me.lWEBSITE.CustomizationFormText = "Website :"
        Me.lWEBSITE.Location = New System.Drawing.Point(0, 120)
        Me.lWEBSITE.Name = "lWEBSITE"
        Me.lWEBSITE.Size = New System.Drawing.Size(540, 113)
        Me.lWEBSITE.Text = "Website :"
        Me.lWEBSITE.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.lWEBSITE.TextSize = New System.Drawing.Size(100, 20)
        Me.lWEBSITE.TextToControlDistance = 5
        '
        'tab2
        '
        Me.tab2.Controls.Add(Me.LayoutControl3)
        Me.tab2.Name = "tab2"
        Me.tab2.Size = New System.Drawing.Size(560, 253)
        Me.tab2.Text = "Address Information"
        '
        'LayoutControl3
        '
        Me.LayoutControl3.Controls.Add(Me.txtBILL_CITY)
        Me.LayoutControl3.Controls.Add(Me.txtBILL_STREET)
        Me.LayoutControl3.Controls.Add(Me.txtBILL_STATE)
        Me.LayoutControl3.Controls.Add(Me.txtBILL_COUNTRY)
        Me.LayoutControl3.Controls.Add(Me.txtBILL_ZIP)
        Me.LayoutControl3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl3.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControl3.Name = "LayoutControl3"
        Me.LayoutControl3.Root = Me.LayoutControlGroup3
        Me.LayoutControl3.Size = New System.Drawing.Size(560, 253)
        Me.LayoutControl3.TabIndex = 0
        Me.LayoutControl3.Text = "LayoutControl3"
        '
        'txtBILL_CITY
        '
        Me.txtBILL_CITY.EnterMoveNextControl = True
        Me.txtBILL_CITY.Location = New System.Drawing.Point(117, 149)
        Me.txtBILL_CITY.Name = "txtBILL_CITY"
        Me.txtBILL_CITY.Size = New System.Drawing.Size(431, 20)
        Me.txtBILL_CITY.StyleController = Me.LayoutControl3
        Me.txtBILL_CITY.TabIndex = 5
        '
        'txtBILL_STREET
        '
        Me.txtBILL_STREET.EnterMoveNextControl = True
        Me.txtBILL_STREET.Location = New System.Drawing.Point(117, 12)
        Me.txtBILL_STREET.Name = "txtBILL_STREET"
        Me.txtBILL_STREET.Size = New System.Drawing.Size(431, 133)
        Me.txtBILL_STREET.StyleController = Me.LayoutControl3
        Me.txtBILL_STREET.TabIndex = 4
        '
        'txtBILL_STATE
        '
        Me.txtBILL_STATE.EnterMoveNextControl = True
        Me.txtBILL_STATE.Location = New System.Drawing.Point(117, 173)
        Me.txtBILL_STATE.Name = "txtBILL_STATE"
        Me.txtBILL_STATE.Size = New System.Drawing.Size(431, 20)
        Me.txtBILL_STATE.StyleController = Me.LayoutControl3
        Me.txtBILL_STATE.TabIndex = 6
        '
        'txtBILL_COUNTRY
        '
        Me.txtBILL_COUNTRY.EnterMoveNextControl = True
        Me.txtBILL_COUNTRY.Location = New System.Drawing.Point(117, 221)
        Me.txtBILL_COUNTRY.Name = "txtBILL_COUNTRY"
        Me.txtBILL_COUNTRY.Size = New System.Drawing.Size(431, 20)
        Me.txtBILL_COUNTRY.StyleController = Me.LayoutControl3
        Me.txtBILL_COUNTRY.TabIndex = 8
        '
        'txtBILL_ZIP
        '
        Me.txtBILL_ZIP.EnterMoveNextControl = True
        Me.txtBILL_ZIP.Location = New System.Drawing.Point(117, 197)
        Me.txtBILL_ZIP.Name = "txtBILL_ZIP"
        Me.txtBILL_ZIP.Size = New System.Drawing.Size(431, 20)
        Me.txtBILL_ZIP.StyleController = Me.LayoutControl3
        Me.txtBILL_ZIP.TabIndex = 7
        '
        'LayoutControlGroup3
        '
        Me.LayoutControlGroup3.CustomizationFormText = "LayoutControlGroup3"
        Me.LayoutControlGroup3.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup3.GroupBordersVisible = False
        Me.LayoutControlGroup3.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.lBILL_CITY, Me.lBILL_STREET, Me.lBILL_STATE, Me.lBILL_ZIP, Me.lBILL_COUNTRY})
        Me.LayoutControlGroup3.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup3.Name = "LayoutControlGroup3"
        Me.LayoutControlGroup3.Size = New System.Drawing.Size(560, 253)
        Me.LayoutControlGroup3.Text = "LayoutControlGroup3"
        Me.LayoutControlGroup3.TextVisible = False
        '
        'lBILL_CITY
        '
        Me.lBILL_CITY.AppearanceItemCaption.Options.UseTextOptions = True
        Me.lBILL_CITY.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.lBILL_CITY.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap
        Me.lBILL_CITY.Control = Me.txtBILL_CITY
        Me.lBILL_CITY.CustomizationFormText = "City :"
        Me.lBILL_CITY.Location = New System.Drawing.Point(0, 137)
        Me.lBILL_CITY.Name = "lBILL_CITY"
        Me.lBILL_CITY.Size = New System.Drawing.Size(540, 24)
        Me.lBILL_CITY.Text = "City :"
        Me.lBILL_CITY.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.lBILL_CITY.TextSize = New System.Drawing.Size(100, 13)
        Me.lBILL_CITY.TextToControlDistance = 5
        '
        'lBILL_STREET
        '
        Me.lBILL_STREET.AppearanceItemCaption.Options.UseTextOptions = True
        Me.lBILL_STREET.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.lBILL_STREET.AppearanceItemCaption.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top
        Me.lBILL_STREET.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap
        Me.lBILL_STREET.Control = Me.txtBILL_STREET
        Me.lBILL_STREET.CustomizationFormText = "Street :"
        Me.lBILL_STREET.Location = New System.Drawing.Point(0, 0)
        Me.lBILL_STREET.Name = "lBILL_STREET"
        Me.lBILL_STREET.Size = New System.Drawing.Size(540, 137)
        Me.lBILL_STREET.Text = "Street :"
        Me.lBILL_STREET.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.lBILL_STREET.TextSize = New System.Drawing.Size(100, 13)
        Me.lBILL_STREET.TextToControlDistance = 5
        '
        'lBILL_STATE
        '
        Me.lBILL_STATE.AppearanceItemCaption.Options.UseTextOptions = True
        Me.lBILL_STATE.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.lBILL_STATE.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap
        Me.lBILL_STATE.Control = Me.txtBILL_STATE
        Me.lBILL_STATE.CustomizationFormText = "State :"
        Me.lBILL_STATE.Location = New System.Drawing.Point(0, 161)
        Me.lBILL_STATE.Name = "lBILL_STATE"
        Me.lBILL_STATE.Size = New System.Drawing.Size(540, 24)
        Me.lBILL_STATE.Text = "State :"
        Me.lBILL_STATE.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.lBILL_STATE.TextSize = New System.Drawing.Size(100, 13)
        Me.lBILL_STATE.TextToControlDistance = 5
        '
        'lBILL_ZIP
        '
        Me.lBILL_ZIP.AppearanceItemCaption.Options.UseTextOptions = True
        Me.lBILL_ZIP.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.lBILL_ZIP.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap
        Me.lBILL_ZIP.Control = Me.txtBILL_ZIP
        Me.lBILL_ZIP.CustomizationFormText = "Zip Code :"
        Me.lBILL_ZIP.Location = New System.Drawing.Point(0, 185)
        Me.lBILL_ZIP.Name = "lBILL_ZIP"
        Me.lBILL_ZIP.Size = New System.Drawing.Size(540, 24)
        Me.lBILL_ZIP.Text = "Zip Code :"
        Me.lBILL_ZIP.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.lBILL_ZIP.TextSize = New System.Drawing.Size(100, 13)
        Me.lBILL_ZIP.TextToControlDistance = 5
        '
        'lBILL_COUNTRY
        '
        Me.lBILL_COUNTRY.AppearanceItemCaption.Options.UseTextOptions = True
        Me.lBILL_COUNTRY.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.lBILL_COUNTRY.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap
        Me.lBILL_COUNTRY.Control = Me.txtBILL_COUNTRY
        Me.lBILL_COUNTRY.CustomizationFormText = "LayoutControlItem21"
        Me.lBILL_COUNTRY.Location = New System.Drawing.Point(0, 209)
        Me.lBILL_COUNTRY.Name = "lBILL_COUNTRY"
        Me.lBILL_COUNTRY.Size = New System.Drawing.Size(540, 24)
        Me.lBILL_COUNTRY.Text = "Country :"
        Me.lBILL_COUNTRY.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.lBILL_COUNTRY.TextSize = New System.Drawing.Size(100, 13)
        Me.lBILL_COUNTRY.TextToControlDistance = 5
        '
        'tab3
        '
        Me.tab3.Controls.Add(Me.LayoutControl1)
        Me.tab3.Name = "tab3"
        Me.tab3.Size = New System.Drawing.Size(560, 253)
        Me.tab3.Text = "Account Information"
        '
        'LayoutControl1
        '
        Me.LayoutControl1.Controls.Add(Me.grdKDCOA)
        Me.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControl1.Name = "LayoutControl1"
        Me.LayoutControl1.Root = Me.LayoutControlGroup4
        Me.LayoutControl1.Size = New System.Drawing.Size(560, 253)
        Me.LayoutControl1.TabIndex = 0
        Me.LayoutControl1.Text = "LayoutControl1"
        '
        'grdKDCOA
        '
        Me.grdKDCOA.EditValue = ""
        Me.grdKDCOA.EnterMoveNextControl = True
        Me.grdKDCOA.Location = New System.Drawing.Point(117, 12)
        Me.grdKDCOA.MenuManager = Me.barManager
        Me.grdKDCOA.Name = "grdKDCOA"
        Me.grdKDCOA.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.grdKDCOA.Properties.NullText = ""
        Me.grdKDCOA.Properties.PopupFormMinSize = New System.Drawing.Size(600, 300)
        Me.grdKDCOA.Properties.View = Me.grvKDCOA
        Me.grdKDCOA.Size = New System.Drawing.Size(431, 20)
        Me.grdKDCOA.StyleController = Me.LayoutControl1
        Me.grdKDCOA.TabIndex = 4
        '
        'grvKDCOA
        '
        Me.grvKDCOA.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn1, Me.GridColumn2})
        Me.grvKDCOA.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.grvKDCOA.Name = "grvKDCOA"
        Me.grvKDCOA.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.grvKDCOA.OptionsView.ShowAutoFilterRow = True
        Me.grvKDCOA.OptionsView.ShowGroupPanel = False
        '
        'GridColumn1
        '
        Me.GridColumn1.Caption = "Account Code"
        Me.GridColumn1.FieldName = "KDCOA"
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.Visible = True
        Me.GridColumn1.VisibleIndex = 0
        '
        'GridColumn2
        '
        Me.GridColumn2.Caption = "Account Name"
        Me.GridColumn2.FieldName = "NMCOA"
        Me.GridColumn2.Name = "GridColumn2"
        Me.GridColumn2.Visible = True
        Me.GridColumn2.VisibleIndex = 1
        '
        'LayoutControlGroup4
        '
        Me.LayoutControlGroup4.CustomizationFormText = "LayoutControlGroup4"
        Me.LayoutControlGroup4.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup4.GroupBordersVisible = False
        Me.LayoutControlGroup4.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.lKDCOA})
        Me.LayoutControlGroup4.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup4.Name = "LayoutControlGroup4"
        Me.LayoutControlGroup4.Size = New System.Drawing.Size(560, 253)
        Me.LayoutControlGroup4.Text = "LayoutControlGroup4"
        Me.LayoutControlGroup4.TextVisible = False
        '
        'lKDCOA
        '
        Me.lKDCOA.AppearanceItemCaption.Options.UseTextOptions = True
        Me.lKDCOA.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.lKDCOA.Control = Me.grdKDCOA
        Me.lKDCOA.CustomizationFormText = "LayoutControlItem2"
        Me.lKDCOA.Location = New System.Drawing.Point(0, 0)
        Me.lKDCOA.Name = "lKDCOA"
        Me.lKDCOA.Size = New System.Drawing.Size(540, 233)
        Me.lKDCOA.Text = "Account * :"
        Me.lKDCOA.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.lKDCOA.TextSize = New System.Drawing.Size(100, 13)
        Me.lKDCOA.TextToControlDistance = 5
        '
        'tab4
        '
        Me.tab4.Controls.Add(Me.LayoutControl8)
        Me.tab4.Name = "tab4"
        Me.tab4.Size = New System.Drawing.Size(560, 253)
        Me.tab4.Text = "Other Information"
        '
        'LayoutControl8
        '
        Me.LayoutControl8.Controls.Add(Me.txtMEMO)
        Me.LayoutControl8.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl8.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControl8.Name = "LayoutControl8"
        Me.LayoutControl8.Root = Me.LayoutControlGroup8
        Me.LayoutControl8.Size = New System.Drawing.Size(560, 253)
        Me.LayoutControl8.TabIndex = 0
        Me.LayoutControl8.Text = "LayoutControl8"
        '
        'txtMEMO
        '
        Me.txtMEMO.EnterMoveNextControl = True
        Me.txtMEMO.Location = New System.Drawing.Point(12, 12)
        Me.txtMEMO.Name = "txtMEMO"
        Me.txtMEMO.Size = New System.Drawing.Size(536, 229)
        Me.txtMEMO.StyleController = Me.LayoutControl8
        Me.txtMEMO.TabIndex = 4
        '
        'LayoutControlGroup8
        '
        Me.LayoutControlGroup8.CustomizationFormText = "LayoutControlGroup8"
        Me.LayoutControlGroup8.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup8.GroupBordersVisible = False
        Me.LayoutControlGroup8.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem33})
        Me.LayoutControlGroup8.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup8.Name = "LayoutControlGroup8"
        Me.LayoutControlGroup8.Size = New System.Drawing.Size(560, 253)
        Me.LayoutControlGroup8.Text = "LayoutControlGroup8"
        Me.LayoutControlGroup8.TextVisible = False
        '
        'LayoutControlItem33
        '
        Me.LayoutControlItem33.AppearanceItemCaption.Options.UseTextOptions = True
        Me.LayoutControlItem33.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.LayoutControlItem33.AppearanceItemCaption.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top
        Me.LayoutControlItem33.Control = Me.txtMEMO
        Me.LayoutControlItem33.CustomizationFormText = "Catatan :"
        Me.LayoutControlItem33.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem33.Name = "LayoutControlItem33"
        Me.LayoutControlItem33.Size = New System.Drawing.Size(540, 233)
        Me.LayoutControlItem33.Text = "Catatan :"
        Me.LayoutControlItem33.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem33.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem33.TextToControlDistance = 0
        Me.LayoutControlItem33.TextVisible = False
        '
        'txtNAME_DISPLAY
        '
        Me.txtNAME_DISPLAY.EditValue = ""
        Me.txtNAME_DISPLAY.EnterMoveNextControl = True
        Me.txtNAME_DISPLAY.Location = New System.Drawing.Point(117, 12)
        Me.txtNAME_DISPLAY.Name = "txtNAME_DISPLAY"
        Me.txtNAME_DISPLAY.Size = New System.Drawing.Size(284, 20)
        Me.txtNAME_DISPLAY.StyleController = Me.layoutControl
        Me.txtNAME_DISPLAY.TabIndex = 9
        '
        'LayoutControlGroup1
        '
        Me.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1"
        Me.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup1.GroupBordersVisible = False
        Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.lNAME_DISPLAY, Me.LayoutControlItem9, Me.LayoutControlItem1, Me.LayoutControlItem34, Me.lDUE, Me.lLIMIT})
        Me.LayoutControlGroup1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup1.Name = "LayoutControlGroup1"
        Me.LayoutControlGroup1.Size = New System.Drawing.Size(590, 401)
        Me.LayoutControlGroup1.Text = "LayoutControlGroup1"
        Me.LayoutControlGroup1.TextVisible = False
        '
        'lNAME_DISPLAY
        '
        Me.lNAME_DISPLAY.AppearanceItemCaption.Options.UseTextOptions = True
        Me.lNAME_DISPLAY.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.lNAME_DISPLAY.Control = Me.txtNAME_DISPLAY
        Me.lNAME_DISPLAY.CustomizationFormText = "Display Name * :"
        Me.lNAME_DISPLAY.Location = New System.Drawing.Point(0, 0)
        Me.lNAME_DISPLAY.Name = "lNAME_DISPLAY"
        Me.lNAME_DISPLAY.Size = New System.Drawing.Size(393, 24)
        Me.lNAME_DISPLAY.Text = "Display Name * :"
        Me.lNAME_DISPLAY.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.lNAME_DISPLAY.TextSize = New System.Drawing.Size(100, 20)
        Me.lNAME_DISPLAY.TextToControlDistance = 5
        '
        'LayoutControlItem9
        '
        Me.LayoutControlItem9.Control = Me.tabControl
        Me.LayoutControlItem9.CustomizationFormText = "LayoutControlItem9"
        Me.LayoutControlItem9.Location = New System.Drawing.Point(0, 96)
        Me.LayoutControlItem9.Name = "LayoutControlItem9"
        Me.LayoutControlItem9.Size = New System.Drawing.Size(570, 285)
        Me.LayoutControlItem9.Text = "LayoutControlItem9"
        Me.LayoutControlItem9.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem9.TextToControlDistance = 0
        Me.LayoutControlItem9.TextVisible = False
        '
        'LayoutControlItem1
        '
        Me.LayoutControlItem1.Control = Me.chkISACTIVE
        Me.LayoutControlItem1.CustomizationFormText = "LayoutControlItem1"
        Me.LayoutControlItem1.Location = New System.Drawing.Point(393, 0)
        Me.LayoutControlItem1.Name = "LayoutControlItem1"
        Me.LayoutControlItem1.Size = New System.Drawing.Size(177, 72)
        Me.LayoutControlItem1.Text = "LayoutControlItem1"
        Me.LayoutControlItem1.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem1.TextToControlDistance = 0
        Me.LayoutControlItem1.TextVisible = False
        '
        'LayoutControlItem34
        '
        Me.LayoutControlItem34.ContentVisible = False
        Me.LayoutControlItem34.Control = Me.txtNothing
        Me.LayoutControlItem34.CustomizationFormText = "LayoutControlItem34"
        Me.LayoutControlItem34.Location = New System.Drawing.Point(0, 72)
        Me.LayoutControlItem34.Name = "LayoutControlItem34"
        Me.LayoutControlItem34.Size = New System.Drawing.Size(570, 24)
        Me.LayoutControlItem34.Text = "LayoutControlItem34"
        Me.LayoutControlItem34.TextSize = New System.Drawing.Size(102, 13)
        '
        'lDUE
        '
        Me.lDUE.AppearanceItemCaption.Options.UseTextOptions = True
        Me.lDUE.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.lDUE.Control = Me.txtDUE
        Me.lDUE.CustomizationFormText = "Due :"
        Me.lDUE.Location = New System.Drawing.Point(0, 24)
        Me.lDUE.Name = "lDUE"
        Me.lDUE.Size = New System.Drawing.Size(393, 24)
        Me.lDUE.Text = "Due :"
        Me.lDUE.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.lDUE.TextSize = New System.Drawing.Size(100, 20)
        Me.lDUE.TextToControlDistance = 5
        '
        'lLIMIT
        '
        Me.lLIMIT.AppearanceItemCaption.Options.UseTextOptions = True
        Me.lLIMIT.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.lLIMIT.Control = Me.txtLIMIT
        Me.lLIMIT.CustomizationFormText = "Limit :"
        Me.lLIMIT.Location = New System.Drawing.Point(0, 48)
        Me.lLIMIT.Name = "lLIMIT"
        Me.lLIMIT.Size = New System.Drawing.Size(393, 24)
        Me.lLIMIT.Text = "Limit :"
        Me.lLIMIT.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.lLIMIT.TextSize = New System.Drawing.Size(100, 20)
        Me.lLIMIT.TextToControlDistance = 5
        '
        'frmCustomer
        '
        Me.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(235, Byte), Integer), CType(CType(236, Byte), Integer), CType(CType(239, Byte), Integer))
        Me.Appearance.Options.UseBackColor = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(590, 423)
        Me.Controls.Add(Me.layoutControl)
        Me.Controls.Add(Me.barDockControlLeft)
        Me.Controls.Add(Me.barDockControlRight)
        Me.Controls.Add(Me.barDockControlBottom)
        Me.Controls.Add(Me.barDockControlTop)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.KeyPreview = True
        Me.Name = "frmCustomer"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        CType(Me.layoutControl, System.ComponentModel.ISupportInitialize).EndInit()
        Me.layoutControl.ResumeLayout(False)
        CType(Me.txtLIMIT.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.barManager, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.progressBarSave, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.progressSave, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDUE.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkISACTIVE.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNothing.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tabControl, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabControl.ResumeLayout(False)
        Me.tab1.ResumeLayout(False)
        CType(Me.LayoutControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl2.ResumeLayout(False)
        CType(Me.txtWEBSITE.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtOTHER.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtEMAIL.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtMOBILE.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtFAX.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtPHONE.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lPHONE, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lFAX, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lMOBILE, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lEMAIL, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lOTHER, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lWEBSITE, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tab2.ResumeLayout(False)
        CType(Me.LayoutControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl3.ResumeLayout(False)
        CType(Me.txtBILL_CITY.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtBILL_STREET.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtBILL_STATE.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtBILL_COUNTRY.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtBILL_ZIP.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lBILL_CITY, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lBILL_STREET, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lBILL_STATE, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lBILL_ZIP, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lBILL_COUNTRY, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tab3.ResumeLayout(False)
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl1.ResumeLayout(False)
        CType(Me.grdKDCOA.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvKDCOA, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lKDCOA, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tab4.ResumeLayout(False)
        CType(Me.LayoutControl8, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl8.ResumeLayout(False)
        CType(Me.txtMEMO.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem33, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNAME_DISPLAY.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lNAME_DISPLAY, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem34, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lDUE, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lLIMIT, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents layoutControl As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents tabControl As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents tab1 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents tab2 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents lNAME_DISPLAY As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem9 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControl2 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents txtWEBSITE As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtOTHER As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtEMAIL As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtMOBILE As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtFAX As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtPHONE As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlGroup2 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents lPHONE As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents lFAX As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents lMOBILE As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents lEMAIL As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents lOTHER As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents lWEBSITE As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControl3 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents LayoutControlGroup3 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents txtBILL_STREET As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents txtBILL_CITY As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtBILL_STATE As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtBILL_COUNTRY As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtBILL_ZIP As DevExpress.XtraEditors.TextEdit
    Friend WithEvents tab4 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents LayoutControl8 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents LayoutControlGroup8 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents txtMEMO As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents LayoutControlItem33 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents txtNothing As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem34 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents barManager As DevExpress.XtraBars.BarManager
    Friend WithEvents barTop As DevExpress.XtraBars.Bar
    Friend WithEvents barDockControlTop As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlBottom As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlLeft As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlRight As DevExpress.XtraBars.BarDockControl
    Friend WithEvents btnSaveNew As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnClose As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnSaveClose As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents progressBarSave As DevExpress.XtraEditors.Repository.RepositoryItemMarqueeProgressBar
    Friend WithEvents progressSave As DevExpress.XtraEditors.Repository.RepositoryItemMarqueeProgressBar
    Friend WithEvents lBILL_CITY As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents lBILL_STREET As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents lBILL_STATE As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents lBILL_ZIP As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents lBILL_COUNTRY As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents chkISACTIVE As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents txtNAME_DISPLAY As DevExpress.XtraEditors.TextEdit
    Friend WithEvents tab3 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents LayoutControl1 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents grdKDCOA As DevExpress.XtraEditors.GridLookUpEdit
    Friend WithEvents grvKDCOA As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents LayoutControlGroup4 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents lKDCOA As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents txtLIMIT As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtDUE As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lDUE As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents lLIMIT As DevExpress.XtraLayout.LayoutControlItem
End Class
