﻿Imports DataAccess
Imports UI.WIN.MAIN.My.Resources
Imports System.Linq

Imports DevExpress.XtraSplashScreen
Imports System.Threading
Imports DevExpress.XtraWaitForm

Public Class frmMain
#Region "Declaration"
    Private isRestart As Boolean = False
#End Region
#Region "Function"
    Private Sub frmMain_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        frmDashboard.MdiParent = Me
        frmDashboard.Show()

        fn_LoadLanguage("id")
        fn_LoadLogin()
    End Sub
    Private Sub frmMain_FormClosing(sender As Object, e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If (MessageBox.Show(Statement.ExitApplication, Caption.Title, MessageBoxButtons.YesNo, MessageBoxIcon.Question)) = Windows.Forms.DialogResult.Yes Then
                For Each iLoop In Me.MdiChildren
                    iLoop.Close()
            Next

            Environment.Exit(1)
        Else
            e.Cancel = True
        End If
    End Sub
    Private Sub fn_LoadLogin()
        frmLogin.ShowDialog()

        statusKDUSER.Caption = Caption.User & " : " & sUserID
        statusDATE.Caption = Caption.Tanggal & " : " & Now.ToString("dd/MM/yyyy")
    End Sub
    Private Sub fn_LoadLanguage(ByVal sCulture As String)
        Try
            If sCulture = "id" Then
                'System.Threading.Thread.CurrentThread.CurrentUICulture = System.Globalization.CultureInfo.GetCultureInfo("id-ID")
                System.Threading.Thread.CurrentThread.CurrentUICulture = New System.Globalization.CultureInfo("id-ID")

                statusLANGUAGE.Caption = "Bahasa : Indonesia"
            Else
                System.Threading.Thread.CurrentThread.CurrentUICulture = System.Globalization.CultureInfo.InstalledUICulture

                statusLANGUAGE.Caption = "Language : English"
            End If

            My.Settings.Save()

            For Each iLoop In Me.MdiChildren
                Dim child As ILanguage = TryCast(iLoop, ILanguage)

                If child IsNot Nothing Then
                    child.fn_LoadLanguage()
                End If
            Next
        Catch oErr As Exception

        End Try

        'File
        mnuFileLanguage.Caption = Caption.FileLanguage
        mnuFileLanguageIndonesian.Caption = Caption.FileLanguageIndonesian
        mnuFileLanguageEnglish.Caption = Caption.FileLanguageEnglish
        mnuFileExit.Caption = Caption.FileExit

        'Reference
        mnuReference.Caption = Caption.Reference
        mnuReferenceVendor.Caption = Caption.ReferenceVendor
        mnuReferenceCustomer.Caption = Caption.ReferenceCustomer
        mnuReferenceWarehouse.Caption = Caption.ReferenceWarehouse
        mnuReferenceCOA.Caption = Caption.ReferenceCOA
        mnuReferenceUOM.Caption = Caption.ReferenceUOM
        mnuReferenceItem_L1.Caption = Caption.ReferenceItem_L1
        mnuReferenceItem_L2.Caption = Caption.ReferenceItem_L2
        mnuReferenceItem_L3.Caption = Caption.ReferenceItem_L3
        mnuReferenceItem_L4.Caption = Caption.ReferenceItem_L4
        mnuReferenceItem_L5.Caption = Caption.ReferenceItem_L5
        mnuReferenceItem_L6.Caption = Caption.ReferenceItem_L6
        mnuReferenceItem.Caption = Caption.ReferenceItem
        mnuReferencePaymentType.Caption = Caption.ReferencePaymentType

        'Purchasing
        mnuPurchasing.Caption = Caption.Purchasing
        mnuPurchasingPurchaseOrder.Caption = Caption.PurchasingPurchaseOrder
        mnuPurchasingPurchaseInvoice.Caption = Caption.PurchasingPurchaseInvoice
        mnuPurchasingPurchaseReturn.Caption = Caption.PurchasingPurchaseReturn
        mnuPurchasingReport.Caption = Caption.PurchasingReport
        mnuPurchasingReportPurchaseOrder.Caption = Caption.PurchasingPurchaseOrder
        mnuPurchasingReportPurchaseInvoice.Caption = Caption.PurchasingPurchaseInvoice
        mnuPurchasingReportPurchaseReturn.Caption = Caption.PurchasingPurchaseReturn

        'Sales
        mnuSales.Caption = Caption.Sales
        mnuSalesSalesOrder.Caption = Caption.SalesSalesOrder
        mnuSalesSalesInvoice.Caption = Caption.SalesSalesInvoice
        mnuSalesSalesReturn.Caption = Caption.SalesSalesReturn
        mnuSalesReport.Caption = Caption.SalesReport
        mnuSalesReportSalesOrder.Caption = Caption.SalesSalesOrder
        mnuSalesReportSalesInvoice.Caption = Caption.SalesSalesInvoice
        mnuSalesReportSalesReturn.Caption = Caption.SalesSalesReturn

        'Inventory
        mnuInventory.Caption = Caption.Inventory
        mnuInventoryMutation.Caption = Caption.InventoryMutation
        mnuInventoryOpname.Caption = Caption.InventoryOpname
        mnuInventoryReport.Caption = Caption.InventoryReport
        mnuInventoryReportMutation.Caption = Caption.InventoryMutation
        mnuInventoryReportOpname.Caption = Caption.InventoryOpname
        mnuInventoryReportMonitoringItem.Caption = Caption.InventoryReportMonitoringItem

        'Finance
        mnuFinance.Caption = Caption.Finance
        mnuFinanceCashIn.Caption = Caption.FinanceCashIn
        mnuFinanceCashOut.Caption = Caption.FinanceCashOut
        mnuFinanceReport.Caption = Caption.FinanceReport
        mnuFinanceReportCashIn.Caption = Caption.FinanceCashIn
        mnuFinanceReportCashOut.Caption = Caption.FinanceCashOut
        mnuFinanceReportReceiveables.Caption = Caption.FinanceReportReceiveables
        mnuFinanceReportPayables.Caption = Caption.FinanceReportPayables

        'Accounting
        mnuAccounting.Caption = Caption.Accounting
        mnuAccountingJournal.Caption = Caption.AccountingJournal
        mnuAccountingReport.Caption = Caption.AccountingReport
        mnuAccountingReportJournal.Caption = Caption.AccountingJournal
        mnuAccountingReportLedger.Caption = Caption.AccountingReportLedger
        mnuAccountingReportBalanceSheet.Caption = Caption.AccountingReportBalanceSheet
        mnuAccountingReportProfitLossStatement.Caption = Caption.AccountingReportProfitLossStatement

        'Setting
        mnuSetting.Caption = Caption.Setting
        mnuSettingDatabase.Caption = Caption.SettingDatabase
        mnuSettingDatabaseBackup.Caption = Caption.SettingDatabaseBackup
        mnuSettingDatabaseRestore.Caption = Caption.SettingDatabaseRestore
        mnuSettingDatabaseConnection.Caption = Caption.SettingDatabaseConnection
        mnuSettingUser.Caption = Caption.SettingUser
        mnuSettingUserUser.Caption = Caption.SettingUserUser
        mnuSettingUserOtority.Caption = Caption.SettingUserOtority
        mnuSettingDatabaseConnectionUser.Caption = Caption.SettingDatabaseConnectionUser
    End Sub
    Private Sub mnuFileLogOut_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuFileLogOut.ItemClick
        For Each iLoop In Me.MdiChildren
            iLoop.Close()
        Next

        frmDashboard.MdiParent = Me
        frmDashboard.Show()

        fn_LoadLogin()
    End Sub
#End Region
#Region "File"
    Private Sub mnuFileLanguageIndonesian_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuFileLanguageIndonesian.ItemClick
        fn_LoadLanguage("id")
    End Sub
    Private Sub mnuFileLanguageEnglish_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuFileLanguageEnglish.ItemClick
        fn_LoadLanguage("en")
    End Sub
    Private Sub mnuFileExit_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuFileExit.ItemClick
        isRestart = False
        Application.Exit()
    End Sub
#End Region
#Region "Reference"
    Private Sub mnuReferenceVendor_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuReferenceVendor.ItemClick
        For Each iLoop In Me.MdiChildren
            If iLoop.Name = frmVendorList.Name Then
                iLoop.Activate()
                Exit Sub
            End If
        Next

        frmVendorList.MdiParent = Me
        frmVendorList.Show()
    End Sub
    Private Sub mnuReferenceCustomer_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuReferenceCustomer.ItemClick
        For Each iLoop In Me.MdiChildren
            If iLoop.Name = frmCustomerList.Name Then
                iLoop.Activate()
                Exit Sub
            End If
        Next

        frmCustomerList.MdiParent = Me
        frmCustomerList.Show()
    End Sub
    Private Sub mnuReferenceSalesman_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuReferenceSalesman.ItemClick
        For Each iLoop In Me.MdiChildren
            If iLoop.Name = frmSalesmanList.Name Then
                iLoop.Activate()
                Exit Sub
            End If
        Next

        frmSalesmanList.MdiParent = Me
        frmSalesmanList.Show()
    End Sub
    Private Sub mnuReferenceWarehouse_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuReferenceWarehouse.ItemClick
        For Each iLoop In Me.MdiChildren
            If iLoop.Name = frmWarehouseList.Name Then
                iLoop.Activate()
                Exit Sub
            End If
        Next

        frmWarehouseList.MdiParent = Me
        frmWarehouseList.Show()
    End Sub
    Private Sub mnuReferenceCOA_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuReferenceCOA.ItemClick
        For Each iLoop In Me.MdiChildren
            If iLoop.Name = frmCOAList.Name Then
                iLoop.Activate()
                Exit Sub
            End If
        Next

        frmCOAList.MdiParent = Me
        frmCOAList.Show()
    End Sub
    Private Sub mnuReferenceUOM_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuReferenceUOM.ItemClick
        For Each iLoop In Me.MdiChildren
            If iLoop.Name = frmUOMList.Name Then
                iLoop.Activate()
                Exit Sub
            End If
        Next

        frmUOMList.MdiParent = Me
        frmUOMList.Show()
    End Sub
    Private Sub mnuReferenceItem_L1_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuReferenceItem_L1.ItemClick
        For Each iLoop In Me.MdiChildren
            If iLoop.Name = frmItem_L1List.Name Then
                iLoop.Activate()
                Exit Sub
            End If
        Next

        frmItem_L1List.MdiParent = Me
        frmItem_L1List.Show()
    End Sub
    Private Sub mnuReferenceItem_L2_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuReferenceItem_L2.ItemClick
        For Each iLoop In Me.MdiChildren
            If iLoop.Name = frmItem_L2List.Name Then
                iLoop.Activate()
                Exit Sub
            End If
        Next

        frmItem_L2List.MdiParent = Me
        frmItem_L2List.Show()
    End Sub
    Private Sub mnuReferenceItem_L3_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuReferenceItem_L3.ItemClick
        For Each iLoop In Me.MdiChildren
            If iLoop.Name = frmItem_L3List.Name Then
                iLoop.Activate()
                Exit Sub
            End If
        Next

        frmItem_L3List.MdiParent = Me
        frmItem_L3List.Show()
    End Sub
    Private Sub mnuReferenceItem_L4_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuReferenceItem_L4.ItemClick
        For Each iLoop In Me.MdiChildren
            If iLoop.Name = frmItem_L4List.Name Then
                iLoop.Activate()
                Exit Sub
            End If
        Next

        frmItem_L4List.MdiParent = Me
        frmItem_L4List.Show()
    End Sub
    Private Sub mnuReferenceItem_L5_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuReferenceItem_L5.ItemClick
        For Each iLoop In Me.MdiChildren
            If iLoop.Name = frmItem_L5List.Name Then
                iLoop.Activate()
                Exit Sub
            End If
        Next

        frmItem_L5List.MdiParent = Me
        frmItem_L5List.Show()
    End Sub
    Private Sub mnuReferenceItem_L6_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuReferenceItem_L6.ItemClick
        For Each iLoop In Me.MdiChildren
            If iLoop.Name = frmItem_L6List.Name Then
                iLoop.Activate()
                Exit Sub
            End If
        Next

        frmItem_L6List.MdiParent = Me
        frmItem_L6List.Show()
    End Sub
    Private Sub mnuReferenceItem_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuReferenceItem.ItemClick
        For Each iLoop In Me.MdiChildren
            If iLoop.Name = frmItemList.Name Then
                iLoop.Activate()
                Exit Sub
            End If
        Next

        frmItemList.MdiParent = Me
        frmItemList.Show()
    End Sub
    Private Sub mnuReferencePaymentType_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuReferencePaymentType.ItemClick
        For Each iLoop In Me.MdiChildren
            If iLoop.Name = frmPaymentTypeList.Name Then
                iLoop.Activate()
                Exit Sub
            End If
        Next

        frmPaymentTypeList.MdiParent = Me
        frmPaymentTypeList.Show()
    End Sub
#End Region
#Region "Purchasing"
    Private Sub mnuPurchasingPurchaseOrder_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuPurchasingPurchaseOrder.ItemClick
        For Each iLoop In Me.MdiChildren
            If iLoop.Name = frmPurchaseOrderList.Name Then
                iLoop.Activate()
                Exit Sub
            End If
        Next

        frmPurchaseOrderList.MdiParent = Me
        frmPurchaseOrderList.Show()
    End Sub
    Private Sub mnuPurchasingPurchaseInvoice_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuPurchasingPurchaseInvoice.ItemClick
        For Each iLoop In Me.MdiChildren
            If iLoop.Name = frmPurchaseInvoiceList.Name Then
                iLoop.Activate()
                Exit Sub
            End If
        Next

        frmPurchaseInvoiceList.MdiParent = Me
        frmPurchaseInvoiceList.Show()
    End Sub
    Private Sub mnuPurchasingPurchaseReturn_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuPurchasingPurchaseReturn.ItemClick
        For Each iLoop In Me.MdiChildren
            If iLoop.Name = frmPurchaseReturnList.Name Then
                iLoop.Activate()
                Exit Sub
            End If
        Next

        frmPurchaseReturnList.MdiParent = Me
        frmPurchaseReturnList.Show()
    End Sub
#End Region
#Region "Report Purchasing"
    Private Sub mnuPurchasingReportPurchaseOrder_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuPurchasingReportPurchaseOrder.ItemClick
        For Each iLoop In Me.MdiChildren
            If iLoop.Name = frmReportPurchaseOrder.Name Then
                iLoop.Activate()
                Exit Sub
            End If
        Next

        frmReportPurchaseOrder.MdiParent = Me
        frmReportPurchaseOrder.Show()
    End Sub
    Private Sub mnuPurchasingReportPurchaseInvoice_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuPurchasingReportPurchaseInvoice.ItemClick
        For Each iLoop In Me.MdiChildren
            If iLoop.Name = frmReportPurchaseInvoice.Name Then
                iLoop.Activate()
                Exit Sub
            End If
        Next

        frmReportPurchaseInvoice.MdiParent = Me
        frmReportPurchaseInvoice.Show()
    End Sub
    Private Sub mnuPurchasingReportPurchaseReturn_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuPurchasingReportPurchaseReturn.ItemClick
        For Each iLoop In Me.MdiChildren
            If iLoop.Name = frmReportPurchaseReturn.Name Then
                iLoop.Activate()
                Exit Sub
            End If
        Next

        frmReportPurchaseReturn.MdiParent = Me
        frmReportPurchaseReturn.Show()
    End Sub
#End Region
#Region "Sales"
    Private Sub mnuSalesDirectSales_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuSalesDirectSales.ItemClick
        For Each iLoop In Me.MdiChildren
            If iLoop.Name = frmDirectSales.Name Then
                iLoop.Activate()
                Exit Sub
            End If
        Next

        frmDirectSales.MdiParent = Me
        frmDirectSales.Show()
    End Sub
    Private Sub mnuSalesSalesOrder_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuSalesSalesOrder.ItemClick
        For Each iLoop In Me.MdiChildren
            If iLoop.Name = frmSalesOrderList.Name Then
                iLoop.Activate()
                Exit Sub
            End If
        Next

        frmSalesOrderList.MdiParent = Me
        frmSalesOrderList.Show()
    End Sub
    Private Sub mnuSalesSalesInvoice_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuSalesSalesInvoice.ItemClick
        For Each iLoop In Me.MdiChildren
            If iLoop.Name = frmSalesInvoiceList.Name Then
                iLoop.Activate()
                Exit Sub
            End If
        Next

        frmSalesInvoiceList.MdiParent = Me
        frmSalesInvoiceList.Show()
    End Sub
    Private Sub mnuSalesSalesReturn_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuSalesSalesReturn.ItemClick
        For Each iLoop In Me.MdiChildren
            If iLoop.Name = frmSalesReturnList.Name Then
                iLoop.Activate()
                Exit Sub
            End If
        Next

        frmSalesReturnList.MdiParent = Me
        frmSalesReturnList.Show()
    End Sub
#End Region
#Region "Report Sales"
    Private Sub mnuSalesReportCetakLaporanHarian_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuSalesReportCetakLaporanHarian.ItemClick
        For Each iLoop In Me.MdiChildren
            If iLoop.Name = frmReportDailyIncome.Name Then
                iLoop.Activate()
                Exit Sub
            End If
        Next

        frmReportDailyIncome.MdiParent = Me
        frmReportDailyIncome.Show()
    End Sub
    Private Sub mnuSalesReportSalesOrder_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuSalesReportSalesOrder.ItemClick
        For Each iLoop In Me.MdiChildren
            If iLoop.Name = frmReportSalesOrder.Name Then
                iLoop.Activate()
                Exit Sub
            End If
        Next

        frmReportSalesOrder.MdiParent = Me
        frmReportSalesOrder.Show()
    End Sub
    Private Sub mnuSalesReportSalesInvoice_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuSalesReportSalesInvoice.ItemClick
        For Each iLoop In Me.MdiChildren
            If iLoop.Name = frmReportSalesInvoice.Name Then
                iLoop.Activate()
                Exit Sub
            End If
        Next

        frmReportSalesInvoice.MdiParent = Me
        frmReportSalesInvoice.Show()
    End Sub
    Private Sub mnuSalesReportSalesReturn_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuSalesReportSalesReturn.ItemClick
        For Each iLoop In Me.MdiChildren
            If iLoop.Name = frmReportSalesReturn.Name Then
                iLoop.Activate()
                Exit Sub
            End If
        Next

        frmReportSalesReturn.MdiParent = Me
        frmReportSalesReturn.Show()
    End Sub


#End Region
#Region "Inventory"
    Private Sub mnuInventoryMutation_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuInventoryMutation.ItemClick
        For Each iLoop In Me.MdiChildren
            If iLoop.Name = frmMutationList.Name Then
                iLoop.Activate()
                Exit Sub
            End If
        Next

        frmMutationList.MdiParent = Me
        frmMutationList.Show()
    End Sub
    Private Sub mnuInventoryOpname_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuInventoryOpname.ItemClick
        For Each iLoop In Me.MdiChildren
            If iLoop.Name = frmOpnameList.Name Then
                iLoop.Activate()
                Exit Sub
            End If
        Next

        frmOpnameList.MdiParent = Me
        frmOpnameList.Show()
    End Sub
#End Region
#Region "Report Inventory"
    Private Sub mnuInventoryReportMutation_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuInventoryReportMutation.ItemClick
        For Each iLoop In Me.MdiChildren
            If iLoop.Name = frmReportMutation.Name Then
                iLoop.Activate()
                Exit Sub
            End If
        Next

        frmReportMutation.MdiParent = Me
        frmReportMutation.Show()
    End Sub
    Private Sub mnuInventoryReportOpname_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuInventoryReportOpname.ItemClick
        For Each iLoop In Me.MdiChildren
            If iLoop.Name = frmReportOpname.Name Then
                iLoop.Activate()
                Exit Sub
            End If
        Next

        frmReportOpname.MdiParent = Me
        frmReportOpname.Show()
    End Sub
    Private Sub mnuInventoryReportMonitoringItem_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuInventoryReportMonitoringItem.ItemClick
        For Each iLoop In Me.MdiChildren
            If iLoop.Name = frmReportMonitoringItem.Name Then
                iLoop.Activate()
                Exit Sub
            End If
        Next

        frmReportMonitoringItem.MdiParent = Me
        frmReportMonitoringItem.Show()
    End Sub
#End Region
#Region "Finance"
    Private Sub mnuFinanceCashBank_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuFinanceCashBank.ItemClick
        For Each iLoop In Me.MdiChildren
            If iLoop.Name = frmCashList.Name Then
                iLoop.Activate()
                Exit Sub
            End If
        Next

        frmCashList.MdiParent = Me
        frmCashList.Show()
    End Sub
    Private Sub mnuFinanceCashIn_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuFinanceCashIn.ItemClick
        For Each iLoop In Me.MdiChildren
            If iLoop.Name = frmCashInList.Name Then
                iLoop.Activate()
                Exit Sub
            End If
        Next

        frmCashInList.MdiParent = Me
        frmCashInList.Show()
    End Sub
    Private Sub mnuFinanceCashOut_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuFinanceCashOut.ItemClick
        For Each iLoop In Me.MdiChildren
            If iLoop.Name = frmCashOutList.Name Then
                iLoop.Activate()
                Exit Sub
            End If
        Next

        frmCashOutList.MdiParent = Me
        frmCashOutList.Show()
    End Sub
#End Region
#Region "Report Finance"
    Private Sub mnuFinanceReportCashBank_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuFinanceReportCashBank.ItemClick
        For Each iLoop In Me.MdiChildren
            If iLoop.Name = frmReportCash.Name Then
                iLoop.Activate()
                Exit Sub
            End If
        Next

        frmReportCash.MdiParent = Me
        frmReportCash.Show()
    End Sub
    Private Sub mnuFinanceReportCashIn_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuFinanceReportCashIn.ItemClick
        For Each iLoop In Me.MdiChildren
            If iLoop.Name = frmReportCashIn.Name Then
                iLoop.Activate()
                Exit Sub
            End If
        Next

        frmReportCashIn.MdiParent = Me
        frmReportCashIn.Show()
    End Sub
    Private Sub mnuFinanceReportCashOut_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuFinanceReportCashOut.ItemClick
        For Each iLoop In Me.MdiChildren
            If iLoop.Name = frmReportCashOut.Name Then
                iLoop.Activate()
                Exit Sub
            End If
        Next

        frmReportCashOut.MdiParent = Me
        frmReportCashOut.Show()
    End Sub
    Private Sub mnuFinanceReportReceiveables_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuFinanceReportReceiveables.ItemClick
        For Each iLoop In Me.MdiChildren
            If iLoop.Name = frmReportReceiveables.Name Then
                iLoop.Activate()
                Exit Sub
            End If
        Next

        frmReportReceiveables.MdiParent = Me
        frmReportReceiveables.Show()
    End Sub
    Private Sub mnuFinanceReportPayables_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuFinanceReportPayables.ItemClick
        For Each iLoop In Me.MdiChildren
            If iLoop.Name = frmReportPayables.Name Then
                iLoop.Activate()
                Exit Sub
            End If
        Next

        frmReportPayables.MdiParent = Me
        frmReportPayables.Show()
    End Sub
#End Region
#Region "Accounting"
    Private Sub mnuAccountingJournal_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuAccountingJournal.ItemClick
        For Each iLoop In Me.MdiChildren
            If iLoop.Name = frmJournalList.Name Then
                iLoop.Activate()
                Exit Sub
            End If
        Next

        frmJournalList.MdiParent = Me
        frmJournalList.Show()
    End Sub
#End Region
#Region "Report Accounting"
    Private Sub mnuAccountingReportLedger_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuAccountingReportLedger.ItemClick
        For Each iLoop In Me.MdiChildren
            If iLoop.Name = frmReportLedger.Name Then
                iLoop.Activate()
                Exit Sub
            End If
        Next

        frmReportLedger.MdiParent = Me
        frmReportLedger.Show()
    End Sub
    Private Sub mnuAccountingReportBalanceSheet_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuAccountingReportBalanceSheet.ItemClick
        For Each iLoop In Me.MdiChildren
            If iLoop.Name = frmReportBalanceSheet.Name Then
                iLoop.Activate()
                Exit Sub
            End If
        Next

        frmReportBalanceSheet.MdiParent = Me
        frmReportBalanceSheet.Show()
    End Sub
    Private Sub mnuAccountingReportProfitLossStatement_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuAccountingReportProfitLossStatement.ItemClick
        For Each iLoop In Me.MdiChildren
            If iLoop.Name = frmReportProfitLossStatement.Name Then
                iLoop.Activate()
                Exit Sub
            End If
        Next

        frmReportProfitLossStatement.MdiParent = Me
        frmReportProfitLossStatement.Show()
    End Sub
#End Region
#Region "Setting"
    Private Sub mnuSettingDatabaseBackup_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuSettingDatabaseBackup.ItemClick
        Dim oConnection As New Setting.clsConnectionMain

        Try
            Dim arrMain() As String = Decrypt(My.Computer.Registry.GetValue("HKEY_CURRENT_USER\Software\BUDIJAYAGROUP\SW\", "Database", "").ToString()).Split(";")

            For iLoop As Integer = 0 To arrMain.Length - 1
                Dim arrMainResult() As String = arrMain(iLoop).Split("=")

                For xLoop As Integer = 0 To arrMainResult.Length - 1
                    If arrMainResult(xLoop) = "Initial Catalog" Then
                        If fileDialogSave.ShowDialog() = Windows.Forms.DialogResult.Cancel Then

                        Else
                            If fileDialogSave.FileName <> String.Empty Then
                                Try
                                    If oConnection.BackupData(arrMainResult(xLoop + 1), fileDialogSave.FileName) = True Then
                                        MsgBox(Statement.BackupSuccess, MsgBoxStyle.Information, Caption.Title)
                                    Else
                                        MsgBox(Statement.BackupFail, MsgBoxStyle.Exclamation, Caption.Title)
                                    End If
                                Catch oErr As Exception
                                    MsgBox(Statement.BackupFail, MsgBoxStyle.Exclamation, Caption.Title)
                                End Try
                            End If
                        End If
                    End If
                Next
            Next
        Catch oErr As Exception
            MsgBox(Statement.BackupFail, MsgBoxStyle.Exclamation, Caption.Title)
        End Try
    End Sub
    Private Sub mnuSettingDatabaseRestore_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuSettingDatabaseRestore.ItemClick
        Dim oConnection As New Setting.clsConnectionMain

        Try
            Dim arrMain() As String = Decrypt(My.Computer.Registry.GetValue("HKEY_CURRENT_USER\Software\BUDIJAYAGROUP\SW\", "Database", "").ToString()).Split(";")

            For iLoop As Integer = 0 To arrMain.Length - 1
                Dim arrMainResult() As String = arrMain(iLoop).Split("=")

                For xLoop As Integer = 0 To arrMainResult.Length - 1
                    If arrMainResult(xLoop) = "Initial Catalog" Then
                        If fileDialogOpen.ShowDialog() = Windows.Forms.DialogResult.Cancel Then

                        Else
                            If fileDialogOpen.FileName <> String.Empty Then
                                Try
                                    If oConnection.RestoreData(arrMainResult(xLoop + 1), fileDialogOpen.FileName) = True Then
                                        MsgBox(Statement.RestoreSuccess, MsgBoxStyle.Information, Caption.Title)

                                        Application.Exit()
                                    Else
                                        MsgBox(Statement.RestoreFail, MsgBoxStyle.Exclamation, Caption.Title)
                                    End If
                                Catch oErr As Exception
                                    MsgBox(Statement.RestoreFail, MsgBoxStyle.Exclamation, Caption.Title)
                                End Try
                            End If
                        End If
                    End If
                Next
            Next
        Catch oErr As Exception
            MsgBox(Statement.RestoreFail, MsgBoxStyle.Exclamation, Caption.Title)
        End Try
    End Sub
    Private Sub mnuSettingDatabaseConnectionUser_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuSettingDatabaseConnectionUser.ItemClick
        frmDatabaseUser.ShowDialog(Me)
    End Sub
    Private Sub mnuSettingUserOtority_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuSettingUserOtority.ItemClick
        For Each iLoop In Me.MdiChildren
            If iLoop.Name = frmOtorityList.Name Then
                iLoop.Activate()
                Exit Sub
            End If
        Next

        frmOtorityList.MdiParent = Me
        frmOtorityList.Show()
    End Sub
    Private Sub mnuSettingUserUser_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuSettingUserUser.ItemClick
        For Each iLoop In Me.MdiChildren
            If iLoop.Name = frmUserList.Name Then
                iLoop.Activate()
                Exit Sub
            End If
        Next

        frmUserList.MdiParent = Me
        frmUserList.Show()
    End Sub
    Private Sub mnuAccountingRecalculate_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuAccountingRecalculate.ItemClick
        Dim oPurchaseInvoice As New Purchasing.clsPurchaseInvoice
        Dim oPurchaseReturn As New Purchasing.clsPurchaseReturn

        Dim oSalesInvoice As New Sales.clsSalesInvoice
        Dim oSalesReturn As New Sales.clsSalesReturn

        Dim oCashIn As New Finance.clsCashIn
        Dim oCashOut As New Finance.clsCashOut

        Dim oCash As New Finance.clsCash

        Dim oOpname As New Inventory.clsOpname
        Dim oItem As New Reference.clsItem

        Dim oJournal As New Accounting.clsJournal

        If MessageBox.Show("Recalculate?", Me.Text, MessageBoxButtons.YesNo, MessageBoxIcon.Information) = DialogResult.Yes Then

            Try
                SplashScreenManager.ShowForm(Me, GetType(frmLoadingPanel), True, True, False)

                Dim sKDITEM As New List(Of String)

                Try
                    For Each xLoop In oItem.GetData
                        sKDITEM.Add(xLoop.KDITEM)
                    Next
                    Dim oAverage As New Accounting.clsStockCard
                    oAverage.PostingAverage(sKDITEM)
                Catch ex As Exception
                    MsgBox(ex)
                End Try

                Try
                    oPurchaseInvoice.UpdateDataFix(False)
                Catch ex As Exception
                    MsgBox(ex)
                End Try
                Try
                    oPurchaseReturn.UpdateDataFix(False)
                Catch ex As Exception
                    MsgBox(ex)
                End Try
                Try
                    oSalesInvoice.UpdateDataFix(False)
                Catch ex As Exception
                    MsgBox(ex)
                End Try
                Try
                    oSalesReturn.UpdateDataFix(False)
                Catch ex As Exception
                    MsgBox(ex)
                End Try
                Try
                    oCashIn.UpdateDataFix(False)
                Catch ex As Exception
                    MsgBox(ex)
                End Try
                Try
                    oCashOut.UpdateDataFix(False)
                Catch ex As Exception
                    MsgBox(ex)
                End Try
                Try
                    oCash.UpdateDataFix(False)
                Catch ex As Exception
                    MsgBox(ex)
                End Try
                Try
                    oOpname.UpdateDataFix(False)
                Catch ex As Exception
                    MsgBox(ex)
                End Try
                Try
                    oJournal.UpdateDataFix(False)
                Catch ex As Exception
                    MsgBox(ex)
                End Try
            Catch ex As Exception

            Finally
                Thread.Sleep(1000)
                SplashScreenManager.CloseForm(False)

                MsgBox("Recalculate Done!")
            End Try
        End If
    End Sub
#End Region

   
    Private Sub xtraTab_PageRemoved(sender As Object, e As DevExpress.XtraTabbedMdi.MdiTabPageEventArgs) Handles xtraTab.PageRemoved
        If sCode = "Direct Sales" Then
            Dim frmDirectSales As New frmDirectSales

            frmDirectSales.MdiParent = Me
            frmDirectSales.Show()

            sCode = ""
        End If
    End Sub
End Class