﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.barManager = New DevExpress.XtraBars.BarManager(Me.components)
        Me.Bar2 = New DevExpress.XtraBars.Bar()
        Me.mnuFile = New DevExpress.XtraBars.BarSubItem()
        Me.mnuFileLanguage = New DevExpress.XtraBars.BarSubItem()
        Me.mnuFileLanguageIndonesian = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuFileLanguageEnglish = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuFileLogOut = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuFileExit = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuReference = New DevExpress.XtraBars.BarSubItem()
        Me.mnuReferenceVendor = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuReferenceCustomer = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuReferenceSalesman = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuReferenceWarehouse = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuReferenceCOA = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuReferenceUOM = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuReferenceItem_L1 = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuReferenceItem_L2 = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuReferenceItem_L3 = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuReferenceItem_L4 = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuReferenceItem_L5 = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuReferenceItem_L6 = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuReferenceItem = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuReferencePaymentType = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuPurchasing = New DevExpress.XtraBars.BarSubItem()
        Me.mnuPurchasingPurchaseOrder = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuPurchasingPurchaseInvoice = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuPurchasingPurchaseReturn = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuPurchasingReport = New DevExpress.XtraBars.BarSubItem()
        Me.mnuPurchasingReportPurchaseOrder = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuPurchasingReportPurchaseInvoice = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuPurchasingReportPurchaseReturn = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuSales = New DevExpress.XtraBars.BarSubItem()
        Me.mnuSalesDirectSales = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuSalesSalesOrder = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuSalesSalesInvoice = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuSalesSalesReturn = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuSalesReport = New DevExpress.XtraBars.BarSubItem()
        Me.mnuSalesReportCetakLaporanHarian = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuSalesReportSalesOrder = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuSalesReportSalesInvoice = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuSalesReportSalesReturn = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuSalesReportTagihanCustomer = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuInventory = New DevExpress.XtraBars.BarSubItem()
        Me.mnuInventoryReportMonitoringItem = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuInventoryMutation = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuInventoryOpname = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuInventoryReport = New DevExpress.XtraBars.BarSubItem()
        Me.mnuInventoryReportMutation = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuInventoryReportOpname = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuFinance = New DevExpress.XtraBars.BarSubItem()
        Me.mnuFinanceCashBank = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuFinanceCashIn = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuFinanceCashOut = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuFinanceReport = New DevExpress.XtraBars.BarSubItem()
        Me.mnuFinanceReportCashBank = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuFinanceReportCashIn = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuFinanceReportCashOut = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuFinanceReportReceiveables = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuFinanceReportPayables = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuAccounting = New DevExpress.XtraBars.BarSubItem()
        Me.mnuAccountingJournal = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuAccountingRecalculate = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuAccountingReport = New DevExpress.XtraBars.BarSubItem()
        Me.mnuAccountingReportLedger = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuAccountingReportBalanceSheet = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuAccountingReportProfitLossStatement = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuSetting = New DevExpress.XtraBars.BarSubItem()
        Me.mnuSettingDatabase = New DevExpress.XtraBars.BarSubItem()
        Me.mnuSettingDatabaseBackup = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuSettingDatabaseRestore = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuSettingDatabaseConnection = New DevExpress.XtraBars.BarSubItem()
        Me.mnuSettingDatabaseConnectionUser = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuSettingUser = New DevExpress.XtraBars.BarSubItem()
        Me.mnuSettingUserUser = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuSettingUserOtority = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem5 = New DevExpress.XtraBars.BarButtonItem()
        Me.Bar3 = New DevExpress.XtraBars.Bar()
        Me.statusKDUSER = New DevExpress.XtraBars.BarStaticItem()
        Me.statusDATE = New DevExpress.XtraBars.BarStaticItem()
        Me.statusLANGUAGE = New DevExpress.XtraBars.BarStaticItem()
        Me.barDockControlTop = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlBottom = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlLeft = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlRight = New DevExpress.XtraBars.BarDockControl()
        Me.BarButtonItem1 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem4 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem2 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem3 = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuAccountingReportJournal = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuInventoryLabel = New DevExpress.XtraBars.BarButtonItem()
        Me.xtraTab = New DevExpress.XtraTabbedMdi.XtraTabbedMdiManager(Me.components)
        Me.fileDialogSave = New System.Windows.Forms.SaveFileDialog()
        Me.fileDialogOpen = New System.Windows.Forms.OpenFileDialog()
        CType(Me.barManager, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.xtraTab, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'barManager
        '
        Me.barManager.Bars.AddRange(New DevExpress.XtraBars.Bar() {Me.Bar2, Me.Bar3})
        Me.barManager.DockControls.Add(Me.barDockControlTop)
        Me.barManager.DockControls.Add(Me.barDockControlBottom)
        Me.barManager.DockControls.Add(Me.barDockControlLeft)
        Me.barManager.DockControls.Add(Me.barDockControlRight)
        Me.barManager.Form = Me
        Me.barManager.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.mnuFile, Me.mnuFileExit, Me.mnuReference, Me.mnuReferenceCustomer, Me.mnuFileLanguage, Me.mnuFileLanguageIndonesian, Me.mnuFileLanguageEnglish, Me.mnuReferenceCOA, Me.mnuSetting, Me.BarButtonItem1, Me.mnuSettingDatabase, Me.mnuSettingDatabaseBackup, Me.mnuSettingDatabaseRestore, Me.BarButtonItem4, Me.mnuReferenceVendor, Me.mnuReferenceWarehouse, Me.mnuReferenceUOM, Me.mnuReferenceItem_L1, Me.mnuReferenceItem_L2, Me.mnuReferenceItem_L3, Me.mnuReferenceItem_L4, Me.mnuReferenceItem_L5, Me.mnuReferenceItem_L6, Me.mnuReferencePaymentType, Me.mnuReferenceItem, Me.mnuPurchasing, Me.mnuPurchasingPurchaseOrder, Me.mnuPurchasingReport, Me.mnuPurchasingReportPurchaseOrder, Me.mnuSales, Me.mnuSalesSalesOrder, Me.mnuAccounting, Me.mnuAccountingJournal, Me.mnuPurchasingPurchaseInvoice, Me.mnuPurchasingPurchaseReturn, Me.mnuPurchasingReportPurchaseInvoice, Me.mnuPurchasingReportPurchaseReturn, Me.mnuSalesSalesInvoice, Me.mnuSalesSalesReturn, Me.mnuSalesReport, Me.mnuSalesReportSalesOrder, Me.mnuSalesReportSalesInvoice, Me.mnuSalesReportSalesReturn, Me.mnuInventory, Me.mnuInventoryMutation, Me.mnuInventoryOpname, Me.mnuInventoryReport, Me.mnuInventoryReportMutation, Me.mnuInventoryReportOpname, Me.mnuFinance, Me.mnuFinanceCashIn, Me.mnuFinanceCashOut, Me.mnuFinanceReport, Me.mnuFinanceReportCashIn, Me.mnuFinanceReportCashOut, Me.mnuSettingUser, Me.mnuSettingUserUser, Me.mnuSettingUserOtority, Me.BarButtonItem2, Me.BarButtonItem3, Me.mnuSettingDatabaseConnection, Me.mnuSettingDatabaseConnectionUser, Me.mnuAccountingReport, Me.mnuAccountingReportJournal, Me.mnuAccountingReportLedger, Me.mnuAccountingReportBalanceSheet, Me.mnuAccountingReportProfitLossStatement, Me.mnuInventoryReportMonitoringItem, Me.mnuFileLogOut, Me.mnuFinanceReportReceiveables, Me.mnuFinanceReportPayables, Me.statusKDUSER, Me.statusDATE, Me.statusLANGUAGE, Me.mnuAccountingRecalculate, Me.BarButtonItem5, Me.mnuFinanceCashBank, Me.mnuFinanceReportCashBank, Me.mnuSalesDirectSales, Me.mnuInventoryLabel, Me.mnuSalesReportCetakLaporanHarian, Me.mnuReferenceSalesman, Me.mnuSalesReportTagihanCustomer})
        Me.barManager.MainMenu = Me.Bar2
        Me.barManager.MaxItemId = 100
        Me.barManager.StatusBar = Me.Bar3
        '
        'Bar2
        '
        Me.Bar2.BarName = "Main menu"
        Me.Bar2.DockCol = 0
        Me.Bar2.DockRow = 0
        Me.Bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top
        Me.Bar2.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuFile), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuReference), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuPurchasing), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuSales), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuInventory), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuFinance), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuAccounting), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuSetting)})
        Me.Bar2.OptionsBar.AllowQuickCustomization = False
        Me.Bar2.OptionsBar.DrawDragBorder = False
        Me.Bar2.OptionsBar.MultiLine = True
        Me.Bar2.OptionsBar.UseWholeRow = True
        Me.Bar2.Text = "Main menu"
        '
        'mnuFile
        '
        Me.mnuFile.Caption = "File"
        Me.mnuFile.Id = 0
        Me.mnuFile.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuFileLanguage), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuFileLogOut, True), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuFileExit)})
        Me.mnuFile.Name = "mnuFile"
        '
        'mnuFileLanguage
        '
        Me.mnuFileLanguage.Caption = "Language"
        Me.mnuFileLanguage.Id = 4
        Me.mnuFileLanguage.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuFileLanguageIndonesian), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuFileLanguageEnglish)})
        Me.mnuFileLanguage.Name = "mnuFileLanguage"
        '
        'mnuFileLanguageIndonesian
        '
        Me.mnuFileLanguageIndonesian.Caption = "Indonesian"
        Me.mnuFileLanguageIndonesian.Id = 5
        Me.mnuFileLanguageIndonesian.Name = "mnuFileLanguageIndonesian"
        '
        'mnuFileLanguageEnglish
        '
        Me.mnuFileLanguageEnglish.Caption = "English"
        Me.mnuFileLanguageEnglish.Id = 6
        Me.mnuFileLanguageEnglish.Name = "mnuFileLanguageEnglish"
        '
        'mnuFileLogOut
        '
        Me.mnuFileLogOut.Caption = "Log Out"
        Me.mnuFileLogOut.Id = 76
        Me.mnuFileLogOut.Name = "mnuFileLogOut"
        '
        'mnuFileExit
        '
        Me.mnuFileExit.Caption = "Exit"
        Me.mnuFileExit.Id = 1
        Me.mnuFileExit.Name = "mnuFileExit"
        '
        'mnuReference
        '
        Me.mnuReference.Caption = "Reference"
        Me.mnuReference.Id = 2
        Me.mnuReference.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuReferenceVendor), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuReferenceCustomer, True), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuReferenceSalesman), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuReferenceWarehouse, True), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuReferenceCOA, True), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuReferenceUOM, True), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuReferenceItem_L1), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuReferenceItem_L2), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuReferenceItem_L3), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuReferenceItem_L4), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuReferenceItem_L5), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuReferenceItem_L6), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuReferenceItem), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuReferencePaymentType, True)})
        Me.mnuReference.Name = "mnuReference"
        '
        'mnuReferenceVendor
        '
        Me.mnuReferenceVendor.Caption = "Vendor"
        Me.mnuReferenceVendor.Id = 16
        Me.mnuReferenceVendor.Name = "mnuReferenceVendor"
        '
        'mnuReferenceCustomer
        '
        Me.mnuReferenceCustomer.Caption = "Customer"
        Me.mnuReferenceCustomer.Id = 3
        Me.mnuReferenceCustomer.Name = "mnuReferenceCustomer"
        '
        'mnuReferenceSalesman
        '
        Me.mnuReferenceSalesman.Caption = "Salesman"
        Me.mnuReferenceSalesman.Id = 98
        Me.mnuReferenceSalesman.Name = "mnuReferenceSalesman"
        '
        'mnuReferenceWarehouse
        '
        Me.mnuReferenceWarehouse.Caption = "Warehouse"
        Me.mnuReferenceWarehouse.Id = 17
        Me.mnuReferenceWarehouse.Name = "mnuReferenceWarehouse"
        '
        'mnuReferenceCOA
        '
        Me.mnuReferenceCOA.Caption = "Chart of Account"
        Me.mnuReferenceCOA.Id = 7
        Me.mnuReferenceCOA.Name = "mnuReferenceCOA"
        '
        'mnuReferenceUOM
        '
        Me.mnuReferenceUOM.Caption = "Unit of Meassure"
        Me.mnuReferenceUOM.Id = 18
        Me.mnuReferenceUOM.Name = "mnuReferenceUOM"
        '
        'mnuReferenceItem_L1
        '
        Me.mnuReferenceItem_L1.Caption = "Item_L1"
        Me.mnuReferenceItem_L1.Id = 19
        Me.mnuReferenceItem_L1.Name = "mnuReferenceItem_L1"
        '
        'mnuReferenceItem_L2
        '
        Me.mnuReferenceItem_L2.Caption = "Item_L2"
        Me.mnuReferenceItem_L2.Id = 20
        Me.mnuReferenceItem_L2.Name = "mnuReferenceItem_L2"
        '
        'mnuReferenceItem_L3
        '
        Me.mnuReferenceItem_L3.Caption = "Item_L3"
        Me.mnuReferenceItem_L3.Id = 21
        Me.mnuReferenceItem_L3.Name = "mnuReferenceItem_L3"
        '
        'mnuReferenceItem_L4
        '
        Me.mnuReferenceItem_L4.Caption = "Item_L4"
        Me.mnuReferenceItem_L4.Id = 22
        Me.mnuReferenceItem_L4.Name = "mnuReferenceItem_L4"
        '
        'mnuReferenceItem_L5
        '
        Me.mnuReferenceItem_L5.Caption = "Item_L5"
        Me.mnuReferenceItem_L5.Id = 23
        Me.mnuReferenceItem_L5.Name = "mnuReferenceItem_L5"
        '
        'mnuReferenceItem_L6
        '
        Me.mnuReferenceItem_L6.Caption = "Item_L6"
        Me.mnuReferenceItem_L6.Id = 24
        Me.mnuReferenceItem_L6.Name = "mnuReferenceItem_L6"
        '
        'mnuReferenceItem
        '
        Me.mnuReferenceItem.Caption = "Item"
        Me.mnuReferenceItem.Id = 26
        Me.mnuReferenceItem.Name = "mnuReferenceItem"
        '
        'mnuReferencePaymentType
        '
        Me.mnuReferencePaymentType.Caption = "Payment Type"
        Me.mnuReferencePaymentType.Id = 25
        Me.mnuReferencePaymentType.Name = "mnuReferencePaymentType"
        '
        'mnuPurchasing
        '
        Me.mnuPurchasing.Caption = "Purchasing"
        Me.mnuPurchasing.Id = 29
        Me.mnuPurchasing.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuPurchasingPurchaseOrder), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuPurchasingPurchaseInvoice), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuPurchasingPurchaseReturn), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuPurchasingReport, True)})
        Me.mnuPurchasing.Name = "mnuPurchasing"
        '
        'mnuPurchasingPurchaseOrder
        '
        Me.mnuPurchasingPurchaseOrder.Caption = "Purchase Order"
        Me.mnuPurchasingPurchaseOrder.Id = 30
        Me.mnuPurchasingPurchaseOrder.Name = "mnuPurchasingPurchaseOrder"
        '
        'mnuPurchasingPurchaseInvoice
        '
        Me.mnuPurchasingPurchaseInvoice.Caption = "Purchase Invoice"
        Me.mnuPurchasingPurchaseInvoice.Id = 37
        Me.mnuPurchasingPurchaseInvoice.Name = "mnuPurchasingPurchaseInvoice"
        '
        'mnuPurchasingPurchaseReturn
        '
        Me.mnuPurchasingPurchaseReturn.Caption = "Purchase Return"
        Me.mnuPurchasingPurchaseReturn.Id = 38
        Me.mnuPurchasingPurchaseReturn.Name = "mnuPurchasingPurchaseReturn"
        '
        'mnuPurchasingReport
        '
        Me.mnuPurchasingReport.Caption = "Report"
        Me.mnuPurchasingReport.Id = 31
        Me.mnuPurchasingReport.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuPurchasingReportPurchaseOrder), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuPurchasingReportPurchaseInvoice), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuPurchasingReportPurchaseReturn)})
        Me.mnuPurchasingReport.Name = "mnuPurchasingReport"
        '
        'mnuPurchasingReportPurchaseOrder
        '
        Me.mnuPurchasingReportPurchaseOrder.Caption = "Purchase Order"
        Me.mnuPurchasingReportPurchaseOrder.Id = 32
        Me.mnuPurchasingReportPurchaseOrder.Name = "mnuPurchasingReportPurchaseOrder"
        '
        'mnuPurchasingReportPurchaseInvoice
        '
        Me.mnuPurchasingReportPurchaseInvoice.Caption = "Purchase Invoice"
        Me.mnuPurchasingReportPurchaseInvoice.Id = 39
        Me.mnuPurchasingReportPurchaseInvoice.Name = "mnuPurchasingReportPurchaseInvoice"
        '
        'mnuPurchasingReportPurchaseReturn
        '
        Me.mnuPurchasingReportPurchaseReturn.Caption = "Purchase Return"
        Me.mnuPurchasingReportPurchaseReturn.Id = 40
        Me.mnuPurchasingReportPurchaseReturn.Name = "mnuPurchasingReportPurchaseReturn"
        '
        'mnuSales
        '
        Me.mnuSales.Caption = "Sales"
        Me.mnuSales.Id = 33
        Me.mnuSales.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuSalesDirectSales), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuSalesSalesOrder, True), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuSalesSalesInvoice), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuSalesSalesReturn), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuSalesReport, True)})
        Me.mnuSales.Name = "mnuSales"
        '
        'mnuSalesDirectSales
        '
        Me.mnuSalesDirectSales.Caption = "Direct Sales"
        Me.mnuSalesDirectSales.Id = 95
        Me.mnuSalesDirectSales.Name = "mnuSalesDirectSales"
        '
        'mnuSalesSalesOrder
        '
        Me.mnuSalesSalesOrder.Caption = "Sales Order"
        Me.mnuSalesSalesOrder.Id = 34
        Me.mnuSalesSalesOrder.Name = "mnuSalesSalesOrder"
        '
        'mnuSalesSalesInvoice
        '
        Me.mnuSalesSalesInvoice.Caption = "Sales Invoice"
        Me.mnuSalesSalesInvoice.Id = 41
        Me.mnuSalesSalesInvoice.Name = "mnuSalesSalesInvoice"
        '
        'mnuSalesSalesReturn
        '
        Me.mnuSalesSalesReturn.Caption = "Sales Return"
        Me.mnuSalesSalesReturn.Id = 42
        Me.mnuSalesSalesReturn.Name = "mnuSalesSalesReturn"
        '
        'mnuSalesReport
        '
        Me.mnuSalesReport.Caption = "Report"
        Me.mnuSalesReport.Id = 43
        Me.mnuSalesReport.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuSalesReportCetakLaporanHarian), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuSalesReportSalesOrder, True), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuSalesReportSalesInvoice), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuSalesReportSalesReturn)})
        Me.mnuSalesReport.Name = "mnuSalesReport"
        '
        'mnuSalesReportCetakLaporanHarian
        '
        Me.mnuSalesReportCetakLaporanHarian.Caption = "Cetak Laporan Harian"
        Me.mnuSalesReportCetakLaporanHarian.Id = 97
        Me.mnuSalesReportCetakLaporanHarian.Name = "mnuSalesReportCetakLaporanHarian"
        '
        'mnuSalesReportSalesOrder
        '
        Me.mnuSalesReportSalesOrder.Caption = "Sales Order"
        Me.mnuSalesReportSalesOrder.Id = 44
        Me.mnuSalesReportSalesOrder.Name = "mnuSalesReportSalesOrder"
        '
        'mnuSalesReportSalesInvoice
        '
        Me.mnuSalesReportSalesInvoice.Caption = "Sales Invoice"
        Me.mnuSalesReportSalesInvoice.Id = 45
        Me.mnuSalesReportSalesInvoice.Name = "mnuSalesReportSalesInvoice"
        '
        'mnuSalesReportSalesReturn
        '
        Me.mnuSalesReportSalesReturn.Caption = "Sales Return"
        Me.mnuSalesReportSalesReturn.Id = 46
        Me.mnuSalesReportSalesReturn.Name = "mnuSalesReportSalesReturn"
        '
        'mnuSalesReportTagihanCustomer
        '
        Me.mnuSalesReportTagihanCustomer.Caption = "Tagihan Customer"
        Me.mnuSalesReportTagihanCustomer.Id = 99
        Me.mnuSalesReportTagihanCustomer.Name = "mnuSalesReportTagihanCustomer"
        '
        'mnuInventory
        '
        Me.mnuInventory.Caption = "Inventory"
        Me.mnuInventory.Id = 47
        Me.mnuInventory.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuInventoryReportMonitoringItem), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuInventoryMutation), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuInventoryOpname, True), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuInventoryReport, True)})
        Me.mnuInventory.Name = "mnuInventory"
        '
        'mnuInventoryReportMonitoringItem
        '
        Me.mnuInventoryReportMonitoringItem.Caption = "Monitoring Item"
        Me.mnuInventoryReportMonitoringItem.Id = 75
        Me.mnuInventoryReportMonitoringItem.Name = "mnuInventoryReportMonitoringItem"
        '
        'mnuInventoryMutation
        '
        Me.mnuInventoryMutation.Caption = "Mutation"
        Me.mnuInventoryMutation.Id = 48
        Me.mnuInventoryMutation.Name = "mnuInventoryMutation"
        '
        'mnuInventoryOpname
        '
        Me.mnuInventoryOpname.Caption = "Opname"
        Me.mnuInventoryOpname.Id = 49
        Me.mnuInventoryOpname.Name = "mnuInventoryOpname"
        '
        'mnuInventoryReport
        '
        Me.mnuInventoryReport.Caption = "Report"
        Me.mnuInventoryReport.Id = 50
        Me.mnuInventoryReport.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuInventoryReportMutation), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuInventoryReportOpname)})
        Me.mnuInventoryReport.Name = "mnuInventoryReport"
        '
        'mnuInventoryReportMutation
        '
        Me.mnuInventoryReportMutation.Caption = "Mutation"
        Me.mnuInventoryReportMutation.Id = 51
        Me.mnuInventoryReportMutation.Name = "mnuInventoryReportMutation"
        '
        'mnuInventoryReportOpname
        '
        Me.mnuInventoryReportOpname.Caption = "Opname"
        Me.mnuInventoryReportOpname.Id = 52
        Me.mnuInventoryReportOpname.Name = "mnuInventoryReportOpname"
        '
        'mnuFinance
        '
        Me.mnuFinance.Caption = "Finance"
        Me.mnuFinance.Id = 53
        Me.mnuFinance.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuFinanceCashBank, True), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuFinanceCashIn, True), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuFinanceCashOut), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuFinanceReport, True)})
        Me.mnuFinance.Name = "mnuFinance"
        '
        'mnuFinanceCashBank
        '
        Me.mnuFinanceCashBank.Caption = "Cash / Bank"
        Me.mnuFinanceCashBank.Id = 93
        Me.mnuFinanceCashBank.Name = "mnuFinanceCashBank"
        '
        'mnuFinanceCashIn
        '
        Me.mnuFinanceCashIn.Caption = "Cash In"
        Me.mnuFinanceCashIn.Id = 54
        Me.mnuFinanceCashIn.Name = "mnuFinanceCashIn"
        '
        'mnuFinanceCashOut
        '
        Me.mnuFinanceCashOut.Caption = "Cash Out"
        Me.mnuFinanceCashOut.Id = 55
        Me.mnuFinanceCashOut.Name = "mnuFinanceCashOut"
        '
        'mnuFinanceReport
        '
        Me.mnuFinanceReport.Caption = "Report"
        Me.mnuFinanceReport.Id = 56
        Me.mnuFinanceReport.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuFinanceReportCashBank, True), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuFinanceReportCashIn, True), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuFinanceReportCashOut), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuFinanceReportReceiveables, True), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuFinanceReportPayables)})
        Me.mnuFinanceReport.Name = "mnuFinanceReport"
        '
        'mnuFinanceReportCashBank
        '
        Me.mnuFinanceReportCashBank.Caption = "Cash / Bank"
        Me.mnuFinanceReportCashBank.Id = 94
        Me.mnuFinanceReportCashBank.Name = "mnuFinanceReportCashBank"
        '
        'mnuFinanceReportCashIn
        '
        Me.mnuFinanceReportCashIn.Caption = "Cash In"
        Me.mnuFinanceReportCashIn.Id = 57
        Me.mnuFinanceReportCashIn.Name = "mnuFinanceReportCashIn"
        '
        'mnuFinanceReportCashOut
        '
        Me.mnuFinanceReportCashOut.Caption = "Cash Out"
        Me.mnuFinanceReportCashOut.Id = 58
        Me.mnuFinanceReportCashOut.Name = "mnuFinanceReportCashOut"
        '
        'mnuFinanceReportReceiveables
        '
        Me.mnuFinanceReportReceiveables.Caption = "Receiveables"
        Me.mnuFinanceReportReceiveables.Id = 78
        Me.mnuFinanceReportReceiveables.Name = "mnuFinanceReportReceiveables"
        '
        'mnuFinanceReportPayables
        '
        Me.mnuFinanceReportPayables.Caption = "Payables"
        Me.mnuFinanceReportPayables.Id = 79
        Me.mnuFinanceReportPayables.Name = "mnuFinanceReportPayables"
        '
        'mnuAccounting
        '
        Me.mnuAccounting.Caption = "Accounting"
        Me.mnuAccounting.Id = 35
        Me.mnuAccounting.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuAccountingJournal), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuAccountingRecalculate, True), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuAccountingReport, True)})
        Me.mnuAccounting.Name = "mnuAccounting"
        '
        'mnuAccountingJournal
        '
        Me.mnuAccountingJournal.Caption = "Journal"
        Me.mnuAccountingJournal.Id = 36
        Me.mnuAccountingJournal.Name = "mnuAccountingJournal"
        '
        'mnuAccountingRecalculate
        '
        Me.mnuAccountingRecalculate.Caption = "Recalculate"
        Me.mnuAccountingRecalculate.Id = 90
        Me.mnuAccountingRecalculate.Name = "mnuAccountingRecalculate"
        '
        'mnuAccountingReport
        '
        Me.mnuAccountingReport.Caption = "Report"
        Me.mnuAccountingReport.Id = 70
        Me.mnuAccountingReport.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuAccountingReportLedger, True), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuAccountingReportBalanceSheet), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuAccountingReportProfitLossStatement)})
        Me.mnuAccountingReport.Name = "mnuAccountingReport"
        '
        'mnuAccountingReportLedger
        '
        Me.mnuAccountingReportLedger.Caption = "Ledger"
        Me.mnuAccountingReportLedger.Id = 72
        Me.mnuAccountingReportLedger.Name = "mnuAccountingReportLedger"
        '
        'mnuAccountingReportBalanceSheet
        '
        Me.mnuAccountingReportBalanceSheet.Caption = "Balance Sheet"
        Me.mnuAccountingReportBalanceSheet.Id = 73
        Me.mnuAccountingReportBalanceSheet.Name = "mnuAccountingReportBalanceSheet"
        '
        'mnuAccountingReportProfitLossStatement
        '
        Me.mnuAccountingReportProfitLossStatement.Caption = "Profit Loss Statement"
        Me.mnuAccountingReportProfitLossStatement.Id = 74
        Me.mnuAccountingReportProfitLossStatement.Name = "mnuAccountingReportProfitLossStatement"
        '
        'mnuSetting
        '
        Me.mnuSetting.Caption = "Setting"
        Me.mnuSetting.Id = 8
        Me.mnuSetting.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuSettingDatabase), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuSettingUser), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.None, False, Me.BarButtonItem5, False)})
        Me.mnuSetting.Name = "mnuSetting"
        '
        'mnuSettingDatabase
        '
        Me.mnuSettingDatabase.Caption = "Database"
        Me.mnuSettingDatabase.Id = 10
        Me.mnuSettingDatabase.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuSettingDatabaseBackup), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuSettingDatabaseRestore), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuSettingDatabaseConnection)})
        Me.mnuSettingDatabase.Name = "mnuSettingDatabase"
        '
        'mnuSettingDatabaseBackup
        '
        Me.mnuSettingDatabaseBackup.Caption = "Backup"
        Me.mnuSettingDatabaseBackup.Id = 11
        Me.mnuSettingDatabaseBackup.Name = "mnuSettingDatabaseBackup"
        '
        'mnuSettingDatabaseRestore
        '
        Me.mnuSettingDatabaseRestore.Caption = "Restore"
        Me.mnuSettingDatabaseRestore.Id = 12
        Me.mnuSettingDatabaseRestore.Name = "mnuSettingDatabaseRestore"
        '
        'mnuSettingDatabaseConnection
        '
        Me.mnuSettingDatabaseConnection.Caption = "Connection"
        Me.mnuSettingDatabaseConnection.Id = 66
        Me.mnuSettingDatabaseConnection.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuSettingDatabaseConnectionUser)})
        Me.mnuSettingDatabaseConnection.Name = "mnuSettingDatabaseConnection"
        '
        'mnuSettingDatabaseConnectionUser
        '
        Me.mnuSettingDatabaseConnectionUser.Caption = "User"
        Me.mnuSettingDatabaseConnectionUser.Id = 68
        Me.mnuSettingDatabaseConnectionUser.Name = "mnuSettingDatabaseConnectionUser"
        '
        'mnuSettingUser
        '
        Me.mnuSettingUser.Caption = "User"
        Me.mnuSettingUser.Id = 59
        Me.mnuSettingUser.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuSettingUserUser), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuSettingUserOtority)})
        Me.mnuSettingUser.Name = "mnuSettingUser"
        '
        'mnuSettingUserUser
        '
        Me.mnuSettingUserUser.Caption = "User"
        Me.mnuSettingUserUser.Id = 60
        Me.mnuSettingUserUser.Name = "mnuSettingUserUser"
        '
        'mnuSettingUserOtority
        '
        Me.mnuSettingUserOtority.Caption = "Otority"
        Me.mnuSettingUserOtority.Id = 61
        Me.mnuSettingUserOtority.Name = "mnuSettingUserOtority"
        '
        'BarButtonItem5
        '
        Me.BarButtonItem5.Caption = "Open Cash Drawer"
        Me.BarButtonItem5.Id = 92
        Me.BarButtonItem5.Name = "BarButtonItem5"
        '
        'Bar3
        '
        Me.Bar3.BarName = "Status bar"
        Me.Bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom
        Me.Bar3.DockCol = 0
        Me.Bar3.DockRow = 0
        Me.Bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom
        Me.Bar3.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.statusKDUSER), New DevExpress.XtraBars.LinkPersistInfo(Me.statusDATE), New DevExpress.XtraBars.LinkPersistInfo(Me.statusLANGUAGE)})
        Me.Bar3.OptionsBar.AllowQuickCustomization = False
        Me.Bar3.OptionsBar.DrawDragBorder = False
        Me.Bar3.OptionsBar.UseWholeRow = True
        Me.Bar3.Text = "Status bar"
        '
        'statusKDUSER
        '
        Me.statusKDUSER.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right
        Me.statusKDUSER.Border = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.statusKDUSER.Id = 80
        Me.statusKDUSER.Name = "statusKDUSER"
        Me.statusKDUSER.TextAlignment = System.Drawing.StringAlignment.Near
        '
        'statusDATE
        '
        Me.statusDATE.Border = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.statusDATE.Id = 81
        Me.statusDATE.Name = "statusDATE"
        Me.statusDATE.TextAlignment = System.Drawing.StringAlignment.Near
        '
        'statusLANGUAGE
        '
        Me.statusLANGUAGE.Border = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.statusLANGUAGE.Id = 86
        Me.statusLANGUAGE.Name = "statusLANGUAGE"
        Me.statusLANGUAGE.TextAlignment = System.Drawing.StringAlignment.Near
        '
        'barDockControlTop
        '
        Me.barDockControlTop.CausesValidation = False
        Me.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top
        Me.barDockControlTop.Location = New System.Drawing.Point(0, 0)
        Me.barDockControlTop.Size = New System.Drawing.Size(792, 22)
        '
        'barDockControlBottom
        '
        Me.barDockControlBottom.CausesValidation = False
        Me.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.barDockControlBottom.Location = New System.Drawing.Point(0, 548)
        Me.barDockControlBottom.Size = New System.Drawing.Size(792, 25)
        '
        'barDockControlLeft
        '
        Me.barDockControlLeft.CausesValidation = False
        Me.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left
        Me.barDockControlLeft.Location = New System.Drawing.Point(0, 22)
        Me.barDockControlLeft.Size = New System.Drawing.Size(0, 526)
        '
        'barDockControlRight
        '
        Me.barDockControlRight.CausesValidation = False
        Me.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right
        Me.barDockControlRight.Location = New System.Drawing.Point(792, 22)
        Me.barDockControlRight.Size = New System.Drawing.Size(0, 526)
        '
        'BarButtonItem1
        '
        Me.BarButtonItem1.Caption = "Syncron"
        Me.BarButtonItem1.Id = 9
        Me.BarButtonItem1.Name = "BarButtonItem1"
        '
        'BarButtonItem4
        '
        Me.BarButtonItem4.Caption = "Update Cloud"
        Me.BarButtonItem4.Id = 13
        Me.BarButtonItem4.Name = "BarButtonItem4"
        '
        'BarButtonItem2
        '
        Me.BarButtonItem2.Caption = "Export"
        Me.BarButtonItem2.Id = 63
        Me.BarButtonItem2.Name = "BarButtonItem2"
        '
        'BarButtonItem3
        '
        Me.BarButtonItem3.Caption = "User Connection"
        Me.BarButtonItem3.Id = 65
        Me.BarButtonItem3.Name = "BarButtonItem3"
        '
        'mnuAccountingReportJournal
        '
        Me.mnuAccountingReportJournal.Caption = "Journal"
        Me.mnuAccountingReportJournal.Id = 71
        Me.mnuAccountingReportJournal.Name = "mnuAccountingReportJournal"
        '
        'mnuInventoryLabel
        '
        Me.mnuInventoryLabel.Caption = "Label"
        Me.mnuInventoryLabel.Id = 96
        Me.mnuInventoryLabel.Name = "mnuInventoryLabel"
        '
        'xtraTab
        '
        Me.xtraTab.ClosePageButtonShowMode = DevExpress.XtraTab.ClosePageButtonShowMode.InAllTabPageHeaders
        Me.xtraTab.HeaderLocation = DevExpress.XtraTab.TabHeaderLocation.Bottom
        Me.xtraTab.MdiParent = Me
        '
        'fileDialogSave
        '
        Me.fileDialogSave.Filter = "BAK Files|*.BAK|All Files|*.*"
        '
        'fileDialogOpen
        '
        Me.fileDialogOpen.Filter = "BAK Files|*.BAK|All files|*.*"
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(792, 573)
        Me.Controls.Add(Me.barDockControlLeft)
        Me.Controls.Add(Me.barDockControlRight)
        Me.Controls.Add(Me.barDockControlBottom)
        Me.Controls.Add(Me.barDockControlTop)
        Me.IsMdiContainer = True
        Me.KeyPreview = True
        Me.Name = "frmMain"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        CType(Me.barManager, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.xtraTab, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents barManager As DevExpress.XtraBars.BarManager
    Friend WithEvents Bar2 As DevExpress.XtraBars.Bar
    Friend WithEvents Bar3 As DevExpress.XtraBars.Bar
    Friend WithEvents barDockControlTop As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlBottom As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlLeft As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlRight As DevExpress.XtraBars.BarDockControl
    Friend WithEvents mnuFile As DevExpress.XtraBars.BarSubItem
    Friend WithEvents mnuFileExit As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuReference As DevExpress.XtraBars.BarSubItem
    Friend WithEvents mnuReferenceCustomer As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuFileLanguage As DevExpress.XtraBars.BarSubItem
    Friend WithEvents mnuFileLanguageIndonesian As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuFileLanguageEnglish As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents xtraTab As DevExpress.XtraTabbedMdi.XtraTabbedMdiManager
    Friend WithEvents mnuReferenceCOA As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuSetting As DevExpress.XtraBars.BarSubItem
    Friend WithEvents mnuSettingDatabase As DevExpress.XtraBars.BarSubItem
    Friend WithEvents BarButtonItem1 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuSettingDatabaseBackup As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuSettingDatabaseRestore As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem4 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents fileDialogSave As System.Windows.Forms.SaveFileDialog
    Friend WithEvents fileDialogOpen As System.Windows.Forms.OpenFileDialog
    Friend WithEvents mnuReferenceVendor As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuReferenceWarehouse As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuReferenceUOM As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuReferenceItem_L1 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuReferenceItem_L2 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuReferenceItem_L3 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuReferenceItem_L4 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuReferenceItem_L5 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuReferenceItem_L6 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuReferencePaymentType As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuReferenceItem As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuPurchasing As DevExpress.XtraBars.BarSubItem
    Friend WithEvents mnuPurchasingPurchaseOrder As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuPurchasingReport As DevExpress.XtraBars.BarSubItem
    Friend WithEvents mnuPurchasingReportPurchaseOrder As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuSales As DevExpress.XtraBars.BarSubItem
    Friend WithEvents mnuSalesSalesOrder As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuAccounting As DevExpress.XtraBars.BarSubItem
    Friend WithEvents mnuAccountingJournal As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuPurchasingPurchaseInvoice As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuPurchasingPurchaseReturn As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuPurchasingReportPurchaseInvoice As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuPurchasingReportPurchaseReturn As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuSalesSalesInvoice As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuSalesSalesReturn As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuSalesReport As DevExpress.XtraBars.BarSubItem
    Friend WithEvents mnuSalesReportSalesOrder As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuSalesReportSalesInvoice As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuSalesReportSalesReturn As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuInventory As DevExpress.XtraBars.BarSubItem
    Friend WithEvents mnuInventoryMutation As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuInventoryOpname As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuInventoryReport As DevExpress.XtraBars.BarSubItem
    Friend WithEvents mnuInventoryReportMutation As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuInventoryReportOpname As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuFinance As DevExpress.XtraBars.BarSubItem
    Friend WithEvents mnuFinanceCashIn As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuFinanceCashOut As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuFinanceReport As DevExpress.XtraBars.BarSubItem
    Friend WithEvents mnuFinanceReportCashIn As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuFinanceReportCashOut As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuSettingUser As DevExpress.XtraBars.BarSubItem
    Friend WithEvents mnuSettingUserUser As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuSettingUserOtority As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem2 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem3 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuSettingDatabaseConnection As DevExpress.XtraBars.BarSubItem
    Friend WithEvents mnuSettingDatabaseConnectionUser As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuAccountingReport As DevExpress.XtraBars.BarSubItem
    Friend WithEvents mnuAccountingReportJournal As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuAccountingReportLedger As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuAccountingReportBalanceSheet As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuAccountingReportProfitLossStatement As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuInventoryReportMonitoringItem As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuFileLogOut As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuFinanceReportReceiveables As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuFinanceReportPayables As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents statusKDUSER As DevExpress.XtraBars.BarStaticItem
    Friend WithEvents statusDATE As DevExpress.XtraBars.BarStaticItem
    Friend WithEvents statusLANGUAGE As DevExpress.XtraBars.BarStaticItem
    Friend WithEvents mnuAccountingRecalculate As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem5 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuFinanceCashBank As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuFinanceReportCashBank As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuSalesDirectSales As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuInventoryLabel As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuSalesReportCetakLaporanHarian As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuReferenceSalesman As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuSalesReportTagihanCustomer As DevExpress.XtraBars.BarButtonItem
End Class
