Imports DataAccess
Imports UI.WIN.MAIN.My.Resources
Imports System.Linq

Imports DevExpress.XtraPrinting

Imports System.Data
Imports System.Data.SqlClient


Public Class frmReportMonitoringItem
    Implements ILanguage

#Region "Function"
    Private Sub Me_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Text = Item.TITLE_REPORT

        lTYPE.Text = Report.FILTER_TYPE
        lDATEFROM.Text = Report.FILTER_DATEFROM
        lDATETO.Text = Report.FILTER_DATETO

        cboTYPE.Properties.Items.Clear()
        cboTYPE.Properties.Items.Add(Report.FILTER_ITEM_TYPE_01)
        cboTYPE.Properties.Items.Add(Report.FILTER_ITEM_TYPE_02)
        cboTYPE.Properties.Items.Add("Serial Number")
        cboTYPE.SelectedIndex = 0

        deDATEFrom.DateTime = Now.AddDays((-Now.Day) + 1)
        deDATETo.DateTime = Now
    End Sub
    Private Sub Me_FormClosed(sender As System.Object, e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        Try
            grv.SaveLayoutToRegistry("HKEY_CURRENT_USER\Software\BUDIJAYAGROUP\SW\" & sUserID & "\" & Me.Text & "\" & cboTYPE.Text)
        Catch oErr As Exception

        End Try
    End Sub
    Public Sub fn_LoadLanguage() Implements ILanguage.fn_LoadLanguage
        Try
            Me.Text = Item.TITLE_REPORT

            lTYPE.Text = Report.FILTER_TYPE

            If cboTYPE.SelectedIndex = 1 Then
                lDATEFROM.Text = Report.FILTER_DATEFROM
            Else
                lDATEFROM.Text = Report.FILTER_DATE
            End If

            lDATETO.Text = Report.FILTER_DATETO

            cboTYPE.Properties.Items.Clear()
            cboTYPE.Properties.Items.Add(Report.FILTER_ITEM_TYPE_01)
            cboTYPE.Properties.Items.Add(Report.FILTER_ITEM_TYPE_02)
            cboTYPE.Properties.Items.Add("Serial Number")

            If cboTYPE.SelectedIndex = 0 Then
                fn_LoadLanguageCurrent()
            ElseIf cboTYPE.SelectedIndex = 1 Then
                fn_LoadLanguageCard()
            Else
                fn_LoadLanguageSerial()
            End If
        Catch oErr As Exception
            MsgBox(Statement.ErrorStatement & vbCrLf & oErr.Message, MsgBoxStyle.Exclamation, Me.Text)
        End Try
    End Sub
    Private Sub fn_LoadSecurity()
        Try
            Dim oOtority As New Setting.clsOtority
            Dim oUser As New Setting.clsUser

            Dim ds = (From x In oOtority.GetDataDetail _
                     Join y In oUser.GetData _
                     On x.KDOTORITY Equals y.KDOTORITY _
                     Where x.MODUL = "ITEM_R" _
                     And y.KDUSER = sUserID _
                     Select x.ISADD, x.ISDELETE, x.ISUPDATE, x.ISPRINT, x.ISVIEW).FirstOrDefault

            Try
                picPrint.Enabled = ds.ISPRINT
                picRefresh.Enabled = ds.ISVIEW

                If ds.ISVIEW = True Then
                    fn_Preview()
                    fn_LoadLanguage()
                End If
            Catch oErr As Exception
                MsgBox(Statement.SecurityNotInstalled, MsgBoxStyle.Exclamation, Me.Text)

                picPrint.Enabled = False
                picRefresh.Enabled = False
            End Try
        Catch oErr As Exception
            MsgBox(Statement.SecurityNotInstalled, MsgBoxStyle.Exclamation, Me.Text)
        End Try
    End Sub
    Private Sub fn_Print()
        Try
            printableComponentLink.Landscape = True
            printableComponentLink.PaperKind = Printing.PaperKind.A4

            Dim phf As PageHeaderFooter = _
        TryCast(printableComponentLink.PageHeaderFooter, PageHeaderFooter)
            phf.Header.Content.Clear()
            phf.Header.Font = New Font("Times New Roman", 14, FontStyle.Bold)
            phf.Header.LineAlignment = BrickAlignment.Center
            phf.Footer.Font = New Font("Times New Roman", 9.75)
            phf.Footer.LineAlignment = BrickAlignment.Far
            phf.Footer.Content.AddRange(New String() _
        {sWATERMARK, "", Report.REPORT_PAGE & " : [Page # of Pages #]"})

            If cboTYPE.SelectedIndex = 0 Then
                phf.Header.Content.AddRange(New String() _
{"", Item.TITLE_REPORT, ""})
            ElseIf cboTYPE.SelectedIndex = 1 Then
                phf.Header.Content.AddRange(New String() _
{"", Item.TITLE_REPORT & vbCrLf & deDATEFrom.DateTime.ToString("dd/MM/yyyy") & " - " & deDATETo.DateTime.ToString("dd/MM/yyyy"), ""})
            Else
                phf.Header.Content.AddRange(New String() _
{"", Item.TITLE_REPORT, ""})
            End If

            printableComponentLink.Component = grd
            printableComponentLink.CreateDocument()
            printableComponentLink.ShowPreviewDialog(Me)
        Catch oErr As Exception
            MsgBox(Statement.ErrorStatement & vbCrLf & oErr.Message, MsgBoxStyle.Exclamation, Me.Text)
        End Try
    End Sub
    Private Sub fn_Preview()
        Try
            grv.Columns.Clear()
            grd.DataSource = Nothing
            grv.GroupSummary.Clear()
            grv.OptionsView.GroupFooterShowMode = DevExpress.XtraGrid.Views.Grid.GroupFooterShowMode.VisibleAlways
            grv1.OptionsView.GroupFooterShowMode = DevExpress.XtraGrid.Views.Grid.GroupFooterShowMode.VisibleAlways

            If cboTYPE.SelectedIndex = 0 Then
                fn_LoadDataCurrent()
            ElseIf cboTYPE.SelectedIndex = 1 Then
                fn_LoadDataCard()
            Else
                fn_LoadDataSerial()
            End If
        Catch oErr As Exception
            MsgBox(Statement.ErrorStatement & vbCrLf & oErr.Message, MsgBoxStyle.Exclamation, Me.Text)
        End Try
    End Sub

#Region "Current"
    Private Sub fn_LoadDataCurrent()
        Try
            'Dim oConn As New SqlConnection
            'Dim oComm As New SqlCommand
            'Dim da As SqlDataAdapter
            'Dim ds As New DataSet
            'Dim SQL As String

            'Dim sConn As String = Decrypt(My.Computer.Registry.GetValue("HKEY_CURRENT_USER\Software\BUDIJAYAGROUP\SW\", "Database", "").ToString())

            'oConn = New SqlConnection(sConn)
            'If oConn.State = ConnectionState.Closed Then
            '    oConn.Open()
            'End If

            'SQL = "SELECT NMITEM1, NMITEM2, AMOUNT, AVERAGE, TOTAL = AMOUNT * AVERAGE "
            'SQL &= ",Catatan = '' "
            'SQL &= "FROM ( "
            'SQL &= "SELECT "
            'SQL &= "B.NMITEM1 "
            'SQL &= ",B.NMITEM2 "
            'SQL &= ",AMOUNT = A.AMOUNT * K.RATE "
            'SQL &= ",AVERAGE = ISNULL((SELECT TOP 1 HPPAVERAGE FROM A_STOCK_CARD AS AA WHERE AA.KDITEM = B.KDITEM ORDER BY SEQ DESC), 0) "
            'SQL &= ",TOTAL = (A.AMOUNT * K.RATE) * ISNULL((SELECT TOP 1 HPPAVERAGE FROM A_STOCK_CARD AS AA WHERE AA.KDITEM = B.KDITEM ORDER BY SEQ DESC), 0) "
            'SQL &= "FROM M_ITEM_WAREHOUSE A "
            'SQL &= "INNER JOIN M_ITEM B "
            'SQL &= "ON A.KDITEM = B.KDITEM "
            'SQL &= "INNER JOIN M_UOM C "
            'SQL &= "ON A.KDUOM = C.KDUOM "
            'SQL &= "INNER JOIN M_WAREHOUSE D "
            'SQL &= "ON A.KDWAREHOUSE = D.KDWAREHOUSE "
            'SQL &= "INNER JOIN M_ITEM_UOM K "
            'SQL &= "ON A.KDITEM = K.KDITEM AND A.KDUOM = K.KDUOM ) AS A "
            'SQL &= "GROUP BY NMITEM1, NMITEM2, AVERAGE, TOTAL "
            'SQL &= "ORDER BY NMITEM2 DESC"

            'oComm.Connection = oConn
            'oComm.CommandText = SQL
            'oComm.CommandTimeout = 120
            'oComm.CommandType = CommandType.Text

            'da = New SqlDataAdapter(oComm)
            'da.Fill(ds, "ITEM")


            'grd.MainView = grv
            'grd.DataSource = ds.Tables("ITEM")
            'grd.ForceInitialize()

            Dim oItem As New Reference.clsItem
            Dim oAverage As New Accounting.clsStockCard

            'Dim ds = From x In oItem.GetData _
            '        Join y In oItem.GetDataWarehouse _
            '       On x.KdItem Equals y.KdItem _
            '      Join z In oAverage.GetData _
            '     On x.KdItem Equals z.KdItem _
            '    Select Code = x.KdItem, Name = x.NmItem, Warehouse = y.M_Warehouse.NmWarehouse, Qty = y.Amount _
            '   Order By Code Ascending

            Dim dsSeq = From x In oAverage.GetData _
                     Group x By x.KDITEM Into Max(x.SEQ) _
                     Select KDITEM, Max

            'Dim ds = From x In oAverage.GetData _
            '         Join y In oItem.GetData _
            '         On x.KdItem Equals y.KdItem _
            '         Join z In dsSeq _
            '         On x.KdItem Equals z.KdItem And x.Seq Equals z.Max _
            '         Select Code = x.KdItem, Name = y.NmItem, Qty = x.QtyAverage, Average = x.HPPAverage, Total = x.TotalAverage _
            '         Order By Code Ascending

            Dim ds = From x In oItem.GetData
                     Join y In oItem.GetDataDetail_WAREHOUSE
                     On x.KDITEM Equals y.KDITEM
                     Join z In oAverage.GetData
                     On x.KDITEM Equals z.KDITEM
                     Join a In dsSeq
                     On x.KDITEM Equals a.KDITEM And z.SEQ Equals a.Max
                     Select GUDANG = y.M_WAREHOUSE.NAME_DISPLAY, NMITEM1 = x.NMITEM1, NMITEM2 = x.NMITEM2, NMITEM3 = x.NMITEM3, y.AMOUNT, HARGA = y.M_ITEM_UOM.PRICESALESSTANDARD, AVERAGE = z.HPPAVERAGE, TOTAL = CDec(y.AMOUNT * z.HPPAVERAGE)
                     Order By NMITEM2 Ascending

            grd.DataSource = ds.ToList()
            fn_LoadFormatDataCurrent()
        Catch oErr As Exception
            MsgBox(Statement.ErrorStatement & vbCrLf & oErr.Message, MsgBoxStyle.Exclamation, Me.Text)
        End Try
    End Sub
    Private Sub fn_LoadFormatDataCurrent()
        For iLoop As Integer = 0 To grv.Columns.Count - 1
            If grv.Columns(iLoop).ColumnType.Name = "Decimal" Then
                grv.Columns(iLoop).DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
                grv.Columns(iLoop).DisplayFormat.FormatString = "{0:n2}"
                grv.Columns(iLoop).AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            ElseIf grv.Columns(iLoop).ColumnType.Name = "DateTime" Then
                grv.Columns(iLoop).DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
                grv.Columns(iLoop).DisplayFormat.FormatString = "{0:dd/MM/yyyy}"
            End If
        Next
    End Sub
    Public Sub fn_LoadLanguageCurrent()
        Try
            grv.Columns("NMITEM1").Caption = Item.NMITEM1
            grv.Columns("NMITEM2").Caption = Item.NMITEM2
            grv.Columns("NMITEM3").Caption = Item.NMITEM3
            grv.Columns("HARGA").Caption = "Harga"
            grv.Columns("GUDANG").Caption = "Gudang"
            grv.Columns("AMOUNT").Caption = Item.AMOUNT
            grv.Columns("AVERAGE").Caption = "Average"
            grv.Columns("TOTAL").Caption = "Total"

            MasterColumnChooserToolStripMenuItem.Text = Caption.ColumnChooserMaster
        Catch oErr As Exception

        End Try
    End Sub
#End Region
#Region "Card"
    Private Sub fn_LoadDataCard()
        Try
            Dim oConn As New SqlConnection
            Dim oComm As New SqlCommand
            Dim da As SqlDataAdapter
            Dim ds As New DataSet
            Dim SQL As String

            Dim sConn As String = Decrypt(My.Computer.Registry.GetValue("HKEY_CURRENT_USER\Software\BUDIJAYAGROUP\SW\", "Database", "").ToString())

            oConn = New SqlConnection(sConn)
            If oConn.State = ConnectionState.Closed Then
                oConn.Open()
            End If

            SQL = "SELECT * FROM ("
            SQL &= "(SELECT "
            SQL &= "A.NOREFERENCE "
            SQL &= ",A.DATE "
            SQL &= ",B.NMITEM1 "
            SQL &= ",B.NMITEM2 "
            SQL &= ",A.QTYIN "
            SQL &= ",A.HPPIN "
            SQL &= ",A.TOTALIN "
            SQL &= ",A.QTYOUT "
            SQL &= ",A.HPPOUT "
            SQL &= ",A.TOTALOUT "
            SQL &= ",A.QTYAVERAGE "
            SQL &= ",A.HPPAVERAGE "
            SQL &= ",A.TOTALAVERAGE "
            SQL &= ",A.SEQ "
            SQL &= "FROM M_ITEM B "
            SQL &= "INNER JOIN A_STOCK_CARD A "
            SQL &= "ON B.KDITEM = A.KDITEM "
            SQL &= "INNER JOIN M_UOM C "
            SQL &= "ON A.KDUOM = C.KDUOM "
            SQL &= "WHERE CONVERT(VARCHAR(8), A.DATE, 112) >= '" & deDATEFrom.DateTime.ToString("yyyyMMdd") & "' AND CONVERT(VARCHAR(8), A.DATE, 112) <= '" & deDATETo.DateTime.ToString("yyyyMMdd") & "' "
            SQL &= ") "
            SQL &= "UNION "
            SQL &= "(SELECT * "
            SQL &= "FROM "
            SQL &= "(SELECT "
            SQL &= "NOREFERENCE = 'SALDO AWAL' "
            SQL &= ",DATE = '" & deDATEFrom.DateTime.ToString("yyyy-MM-dd") & "' "
            SQL &= ",B.NMITEM1 "
            SQL &= ",B.NMITEM2 "
            SQL &= ",QTYIN = ISNULL((SELECT TOP 1 AA.QTYAVERAGE FROM A_STOCK_CARD AS AA WHERE CONVERT(VARCHAR(8), AA.DATE, 112) < '" & deDATEFrom.DateTime.ToString("yyyyMMdd") & "' AND AA.KDITEM = B.KDITEM ORDER BY SEQ DESC), 0) "
            SQL &= ",HPPIN = ISNULL((SELECT TOP 1 AA.HPPAVERAGE FROM A_STOCK_CARD AS AA WHERE CONVERT(VARCHAR(8), AA.DATE, 112) < '" & deDATEFrom.DateTime.ToString("yyyyMMdd") & "' AND AA.KDITEM = B.KDITEM ORDER BY SEQ DESC), 0) "
            SQL &= ",TOTALIN = ISNULL((SELECT TOP 1 AA.TOTALAVERAGE FROM A_STOCK_CARD AS AA WHERE CONVERT(VARCHAR(8), AA.DATE, 112) < '" & deDATEFrom.DateTime.ToString("yyyyMMdd") & "' AND AA.KDITEM = B.KDITEM ORDER BY SEQ DESC), 0) "
            SQL &= ",QTYOUT = 0 "
            SQL &= ",HPPOUT = 0 "
            SQL &= ",TOTALOUT = 0 "
            SQL &= ",QTYAVERAGE = ISNULL((SELECT TOP 1 AA.QTYAVERAGE FROM A_STOCK_CARD AS AA WHERE CONVERT(VARCHAR(8), AA.DATE, 112) < '" & deDATEFrom.DateTime.ToString("yyyyMMdd") & "' AND AA.KDITEM = B.KDITEM ORDER BY SEQ DESC), 0) "
            SQL &= ",HPPAVERAGE = ISNULL((SELECT TOP 1 AA.HPPAVERAGE FROM A_STOCK_CARD AS AA WHERE CONVERT(VARCHAR(8), AA.DATE, 112) < '" & deDATEFrom.DateTime.ToString("yyyyMMdd") & "' AND AA.KDITEM = B.KDITEM ORDER BY SEQ DESC), 0) "
            SQL &= ",TOTALAVERAGE = ISNULL((SELECT TOP 1 AA.TOTALAVERAGE FROM A_STOCK_CARD AS AA WHERE CONVERT(VARCHAR(8), AA.DATE, 112) < '" & deDATEFrom.DateTime.ToString("yyyyMMdd") & "' AND AA.KDITEM = B.KDITEM ORDER BY SEQ DESC), 0) "
            SQL &= ",SEQ =ISNULL((SELECT TOP 1 AA.SEQ FROM A_STOCK_CARD AS AA WHERE CONVERT(VARCHAR(8), AA.DATE, 112) < '" & deDATEFrom.DateTime.ToString("yyyyMMdd") & "' AND AA.KDITEM = B.KDITEM ORDER BY SEQ DESC), 0) "
            SQL &= "FROM M_ITEM B "
            SQL &= ") AS A "
            SQL &= ") "
            SQL &= ") AS A "
            SQL &= "ORDER BY NMITEM1, SEQ ASC "


            oComm.Connection = oConn
            oComm.CommandText = SQL
            oComm.CommandTimeout = 120
            oComm.CommandType = CommandType.Text

            da = New SqlDataAdapter(oComm)
            da.Fill(ds, "ITEM")

            grd.MainView = grv
            grd.DataSource = ds.Tables("ITEM")
            grd.ForceInitialize()

            fn_LoadFormatDataCard()
        Catch oErr As Exception
            MsgBox(Statement.ErrorStatement & vbCrLf & oErr.Message, MsgBoxStyle.Exclamation, Me.Text)
        End Try
    End Sub
    Private Sub fn_LoadFormatDataCard()
        For iLoop As Integer = 0 To grv.Columns.Count - 1
            If grv.Columns(iLoop).ColumnType.Name = "Decimal" Then
                grv.Columns(iLoop).DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
                grv.Columns(iLoop).DisplayFormat.FormatString = "{0:n2}"
                grv.Columns(iLoop).AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            ElseIf grv.Columns(iLoop).ColumnType.Name = "DateTime" Then
                grv.Columns(iLoop).DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
                grv.Columns(iLoop).DisplayFormat.FormatString = "{0:dd/MM/yyyy}"
            End If
        Next

        grv.Columns("SEQ").Visible = False
        grv.Columns("SEQ").OptionsColumn.ShowInCustomizationForm = False

        grv.Columns("NMITEM1").Group()
        grv.ExpandAllGroups()
    End Sub
    Public Sub fn_LoadLanguageCard()
        Try
            grv.Columns("NOREFERENCE").Caption = Item.CARD_NOREFERENCE
            grv.Columns("DATE").Caption = Item.CARD_DATE
            grv.Columns("NMITEM1").Caption = Item.NMITEM1
            grv.Columns("NMITEM2").Caption = Item.NMITEM2

            grv.Columns("QTYIN").Caption = Item.CARD_QTYIN
            grv.Columns("HPPIN").Caption = Item.CARD_HPPIN
            grv.Columns("TOTALIN").Caption = Item.CARD_TOTALIN

            grv.Columns("QTYOUT").Caption = Item.CARD_QTYOUT
            grv.Columns("HPPOUT").Caption = Item.CARD_HPPOUT
            grv.Columns("TOTALOUT").Caption = Item.CARD_TOTALOUT

            grv.Columns("QTYAVERAGE").Caption = Item.CARD_QTYAVERAGE
            grv.Columns("HPPAVERAGE").Caption = Item.CARD_HPPAVERAGE
            grv.Columns("TOTALAVERAGE").Caption = Item.CARD_TOTALAVERAGE

            MasterColumnChooserToolStripMenuItem.Text = Caption.ColumnChooserMaster
        Catch oErr As Exception

        End Try
    End Sub
#End Region
#Region "Serial Number"
    Private Sub fn_LoadDataSerial()
        'Try
        '    Dim oConn As New SqlConnection
        '    Dim oComm As New SqlCommand
        '    Dim da As SqlDataAdapter
        '    Dim ds As New DataSet
        '    Dim SQL As String

        '    Dim sConn As String = Decrypt(My.Computer.Registry.GetValue("HKEY_CURRENT_USER\Software\BUDIJAYAGROUP\SW\", "Database", "").ToString())

        '    oConn = New SqlConnection(sConn)
        '    If oConn.State = ConnectionState.Closed Then
        '        oConn.Open()
        '    End If

        '    SQL = "(SELECT NMITEM2, REMARKS, AMOUNT = SUM(AMOUNT) "
        '    SQL &= "FROM ( "
        '    SQL &= "SELECT "
        '    SQL &= "B.NMITEM1 "
        '    SQL &= ",B.NMITEM2 "
        '    SQL &= ",AMOUNT = A.QTY * D.RATE "
        '    SQL &= ",A.REMARKS "
        '    SQL &= "FROM P_PI_D A "
        '    SQL &= "INNER JOIN M_ITEM B "
        '    SQL &= "ON A.KDITEM = B.KDITEM "
        '    SQL &= "INNER JOIN M_UOM C "
        '    SQL &= "ON A.KDUOM = C.KDUOM "
        '    SQL &= "INNER JOIN M_ITEM_UOM D "
        '    SQL &= "ON A.KDITEM = D.KDITEM AND A.KDUOM = D.KDUOM "
        '    SQL &= "WHERE A.REMARKS <> '-' ) AS A "
        '    SQL &= "GROUP BY NMITEM2, REMARKS) "

        '    SQL &= " UNION "

        '    SQL &= "(SELECT NMITEM2, REMARKS, AMOUNT = SUM(AMOUNT) "
        '    SQL &= "FROM ( "
        '    SQL &= "SELECT "
        '    SQL &= "B.NMITEM1 "
        '    SQL &= ",B.NMITEM2 "
        '    SQL &= ",AMOUNT = A.QTY * D.RATE "
        '    SQL &= ",A.REMARKS "
        '    SQL &= "FROM S_SI_D A "
        '    SQL &= "INNER JOIN M_ITEM B "
        '    SQL &= "ON A.KDITEM = B.KDITEM "
        '    SQL &= "INNER JOIN M_UOM C "
        '    SQL &= "ON A.KDUOM = C.KDUOM "
        '    SQL &= "INNER JOIN M_ITEM_UOM D "
        '    SQL &= "ON A.KDITEM = D.KDITEM AND A.KDUOM = D.KDUOM "
        '    SQL &= "WHERE A.REMARKS <> '-' ) AS A "
        '    SQL &= "GROUP BY NMITEM2, REMARKS ) "

        '    oComm.Connection = oConn
        '    oComm.CommandText = SQL
        '    oComm.CommandTimeout = 120
        '    oComm.CommandType = CommandType.Text

        '    da = New SqlDataAdapter(oComm)
        '    da.Fill(ds, "ITEM")


        '    grd.MainView = grv
        '    grd.DataSource = ds.Tables("ITEM")
        '    grd.ForceInitialize()

        '    fn_LoadFormatDataSerial()
        'Catch oErr As Exception
        '    MsgBox(Statement.ErrorStatement & vbCrLf & oErr.Message, MsgBoxStyle.Exclamation, Me.Text)
        'End Try

        Dim oPI As New Purchasing.clsPurchaseInvoice
        Dim oPR As New Purchasing.clsPurchaseReturn
        Dim oSI As New Sales.clsSalesInvoice
        Dim oSR As New Sales.clsSalesReturn
        Dim oItem As New Reference.clsItem

        Try
            Try
                Dim dsItem = From x In oItem.GetData _
                             Where x.M_ITEM_L1.MEMO = "HANDPHONE" _
                             Select x

                Dim multiList As New Dictionary(Of String, String)
                Dim sCount = 0

                For Each iLoop In dsItem
                    Dim sKDITEM = iLoop.KDITEM

                    Dim ds1 = (From x In oPI.GetDataDetail _
                         Where x.M_ITEM.ISACTIVE = True And x.KDITEM = sKDITEM _
                         Select Name = x.M_ITEM.NMITEM2, SERIALNUMBER = x.REMARKS.Split(Environment.NewLine, 50, StringSplitOptions.RemoveEmptyEntries)).SelectMany(Function(x) x.SERIALNUMBER).ToList()

                    Dim ds2 = (From x In oSI.GetDataDetail _
                             Where x.M_ITEM.ISACTIVE = True And x.KDITEM = sKDITEM _
                             Select x.M_ITEM.NMITEM2, SERIALNUMBER = x.REMARKS.Split(Environment.NewLine, 50, StringSplitOptions.RemoveEmptyEntries)).SelectMany(Function(x) x.SERIALNUMBER).ToList()

                    Dim ds3 = (From x In oPR.GetDataDetail _
                             Where x.M_ITEM.ISACTIVE = True And x.KDITEM = sKDITEM _
                             Select x.M_ITEM.NMITEM2, SERIALNUMBER = x.REMARKS.Split(Environment.NewLine, 50, StringSplitOptions.RemoveEmptyEntries)).SelectMany(Function(x) x.SERIALNUMBER).ToList()

                    Dim ds4 = (From x In oSR.GetDataDetail _
                             Where x.M_ITEM.ISACTIVE = True And x.KDITEM = sKDITEM _
                             Select x.M_ITEM.NMITEM2, SERIALNUMBER = x.REMARKS.Split(Environment.NewLine, 50, StringSplitOptions.RemoveEmptyEntries)).SelectMany(Function(x) x.SERIALNUMBER).ToList()

                    Dim dsUnion = ds1.Except(ds2).Except(ds3).Union(ds4)

                    For Each xLoop In dsUnion
                        Try
                            multiList.Add(iLoop.NMITEM2 & "/" & xLoop, xLoop)
                        Catch ex As Exception

                        End Try
                    Next
                Next

                Dim ds = From x In multiList _
                         Select NMITEM2 = x.Key.Split("/").FirstOrDefault(), SERIALNUMBER = x.Value

                grd.DataSource = ds.ToList()
                grd.RefreshDataSource()

                fn_LoadFormatDataSerial()
            Catch ex As Exception
                MsgBox("Load Data : " & vbCrLf & ex.Message, MsgBoxStyle.Exclamation, Me.Text)
        End Try
        Catch oErr As Exception
            MsgBox("Load Data : " & vbCrLf & oErr.Message, MsgBoxStyle.Exclamation, Me.Text)
        End Try
    End Sub
    Private Sub fn_LoadFormatDataSerial()
        For iLoop As Integer = 0 To grv.Columns.Count - 1
            If grv.Columns(iLoop).ColumnType.Name = "Decimal" Then
                grv.Columns(iLoop).DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
                grv.Columns(iLoop).DisplayFormat.FormatString = "{0:n2}"
                grv.Columns(iLoop).AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            ElseIf grv.Columns(iLoop).ColumnType.Name = "DateTime" Then
                grv.Columns(iLoop).DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
                grv.Columns(iLoop).DisplayFormat.FormatString = "{0:dd/MM/yyyy}"
            End If
        Next

        grv.Columns("NMITEM2").Group()
        grv.ExpandAllGroups()
    End Sub
    Public Sub fn_LoadLanguageSerial()
        Try
            grv.Columns("NMITEM2").Caption = Item.NMITEM2
            grv.Columns("SERIALNUMBER").Caption = "Serial Number"

            MasterColumnChooserToolStripMenuItem.Text = Caption.ColumnChooserMaster
        Catch oErr As Exception

        End Try
    End Sub
#End Region

    Private Sub MasterColumnChooserToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles MasterColumnChooserToolStripMenuItem.Click
        grv.ShowCustomization()
    End Sub
    Private Sub cboTYPE_SelectedIndexChanged() Handles cboTYPE.SelectedIndexChanged
        If cboTYPE.SelectedIndex = 0 Then
            lDATEFROM.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never
            lDATETO.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never
        ElseIf cboTYPE.SelectedIndex = 1 Then
            lDATEFROM.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always
            lDATEFROM.Text = Report.FILTER_DATEFROM
            lDATETO.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always
        Else
            lDATEFROM.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always
            lDATEFROM.Text = Report.FILTER_DATE
            lDATETO.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never
        End If

        fn_LoadSecurity()

        'Try
        '    grv.RestoreLayoutFromRegistry("HKEY_CURRENT_USER\Software\BUDIJAYAGROUP\SW\" & sUserID & "\" & Me.Text & "\" & cboTYPE.Text)
        'Catch ex As Exception

        'End Try
    End Sub
#End Region
#Region "Command Button"
    Private Sub frmMember_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Select Case e.KeyCode
            Case Keys.Escape
                Me.Close()
            Case Keys.P
                If e.Alt = True And picPrint.Enabled = True Then
                    picPrint_Click()
                End If
            Case Keys.R
                If e.Alt = True And picRefresh.Enabled = True Then
                    picRefresh_Click()
                End If
        End Select
    End Sub
    Private Sub picPrint_Click() Handles picPrint.Click
        Try
            fn_Print()
        Catch oErr As Exception
            MsgBox(Statement.ErrorStatement & vbCrLf & oErr.Message, MsgBoxStyle.Exclamation, Me.Text)
        End Try
    End Sub
    Private Sub picRefresh_Click() Handles picRefresh.Click
        fn_LoadSecurity()

        'Try
        '    grv.RestoreLayoutFromRegistry("HKEY_CURRENT_USER\Software\BUDIJAYAGROUP\SW\" & sUserID & "\" & Me.Text & "\" & cboTYPE.Text)
        'Catch ex As Exception

        'End Try
    End Sub
#End Region
End Class