﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class xtraItem
    Inherits DevExpress.XtraReports.UI.XtraReport

    'XtraReport overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Designer
    'It can be modified using the Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Detail = New DevExpress.XtraReports.UI.DetailBand()
        Me.picATTACHMENT = New DevExpress.XtraReports.UI.XRPictureBox()
        Me.chkISACTIVE = New DevExpress.XtraReports.UI.XRCheckBox()
        Me.XrLabel9 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel8 = New DevExpress.XtraReports.UI.XRLabel()
        Me.lKDCOA_SALES = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel7 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel6 = New DevExpress.XtraReports.UI.XRLabel()
        Me.lKDCOA_INVENTORY = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel()
        Me.lNMITEM3 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.lNMITEM2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel43 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel40 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel39 = New DevExpress.XtraReports.UI.XRLabel()
        Me.lKDCOA_COST = New DevExpress.XtraReports.UI.XRLabel()
        Me.lINFORMATION_ACCOUNT = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel31 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel30 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel29 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel28 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel27 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel26 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel25 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel19 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel18 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel17 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel16 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel15 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel14 = New DevExpress.XtraReports.UI.XRLabel()
        Me.lKDITEM_L6 = New DevExpress.XtraReports.UI.XRLabel()
        Me.lKDITEM_L5 = New DevExpress.XtraReports.UI.XRLabel()
        Me.lKDITEM_L4 = New DevExpress.XtraReports.UI.XRLabel()
        Me.lKDITEM_L3 = New DevExpress.XtraReports.UI.XRLabel()
        Me.lKDITEM_L2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.lINFORMATION_SPECIFICATION = New DevExpress.XtraReports.UI.XRLabel()
        Me.lKDITEM_L1 = New DevExpress.XtraReports.UI.XRLabel()
        Me.lNMITEM1 = New DevExpress.XtraReports.UI.XRLabel()
        Me.TopMargin = New DevExpress.XtraReports.UI.TopMarginBand()
        Me.lTITLE = New DevExpress.XtraReports.UI.XRLabel()
        Me.BottomMargin = New DevExpress.XtraReports.UI.BottomMarginBand()
        Me.bindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DetailReport = New DevExpress.XtraReports.UI.DetailReportBand()
        Me.Detail1 = New DevExpress.XtraReports.UI.DetailBand()
        Me.XrLabel13 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel12 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel11 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel10 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
        Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.lDETAILUOM_MARGIN = New DevExpress.XtraReports.UI.XRLabel()
        Me.lDETAILUOM_PRICESALESSTANDARD = New DevExpress.XtraReports.UI.XRLabel()
        Me.lDETAILUOM_PRICEPURCHASESTANDARD = New DevExpress.XtraReports.UI.XRLabel()
        Me.lDETAILUOM_RATE = New DevExpress.XtraReports.UI.XRLabel()
        Me.lDETAILUOM_KDUOM = New DevExpress.XtraReports.UI.XRLabel()
        Me.lINFORMATION_UNIT = New DevExpress.XtraReports.UI.XRLabel()
        Me.GroupFooter1 = New DevExpress.XtraReports.UI.GroupFooterBand()
        CType(Me.bindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'Detail
        '
        Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.picATTACHMENT, Me.chkISACTIVE, Me.XrLabel9, Me.XrLabel8, Me.lKDCOA_SALES, Me.XrLabel7, Me.XrLabel6, Me.lKDCOA_INVENTORY, Me.XrLabel5, Me.XrLabel4, Me.lNMITEM3, Me.XrLabel3, Me.XrLabel2, Me.lNMITEM2, Me.XrLabel43, Me.XrLabel40, Me.XrLabel39, Me.lKDCOA_COST, Me.lINFORMATION_ACCOUNT, Me.XrLabel31, Me.XrLabel30, Me.XrLabel29, Me.XrLabel28, Me.XrLabel27, Me.XrLabel26, Me.XrLabel25, Me.XrLabel19, Me.XrLabel18, Me.XrLabel17, Me.XrLabel16, Me.XrLabel15, Me.XrLabel14, Me.lKDITEM_L6, Me.lKDITEM_L5, Me.lKDITEM_L4, Me.lKDITEM_L3, Me.lKDITEM_L2, Me.lINFORMATION_SPECIFICATION, Me.lKDITEM_L1, Me.lNMITEM1})
        Me.Detail.HeightF = 298.9583!
        Me.Detail.KeepTogether = True
        Me.Detail.Name = "Detail"
        Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'picATTACHMENT
        '
        Me.picATTACHMENT.LocationFloat = New DevExpress.Utils.PointFloat(445.1251!, 37.20834!)
        Me.picATTACHMENT.Name = "picATTACHMENT"
        Me.picATTACHMENT.SizeF = New System.Drawing.SizeF(171.8749!, 164.0419!)
        Me.picATTACHMENT.Sizing = DevExpress.XtraPrinting.ImageSizeMode.StretchImage
        '
        'chkISACTIVE
        '
        Me.chkISACTIVE.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("CheckState", Nothing, "ISACTIVE")})
        Me.chkISACTIVE.LocationFloat = New DevExpress.Utils.PointFloat(445.1251!, 0.0!)
        Me.chkISACTIVE.Name = "chkISACTIVE"
        Me.chkISACTIVE.SizeF = New System.Drawing.SizeF(147.9167!, 18.00001!)
        Me.chkISACTIVE.Text = "ISACTIVE"
        '
        'XrLabel9
        '
        Me.XrLabel9.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "A_COA_SALES.NMCOA")})
        Me.XrLabel9.LocationFloat = New DevExpress.Utils.PointFloat(158.9584!, 272.6667!)
        Me.XrLabel9.Name = "XrLabel9"
        Me.XrLabel9.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel9.SizeF = New System.Drawing.SizeF(274.7082!, 18.0!)
        Me.XrLabel9.Text = "XrLabel9"
        '
        'XrLabel8
        '
        Me.XrLabel8.LocationFloat = New DevExpress.Utils.PointFloat(148.5418!, 272.6667!)
        Me.XrLabel8.Name = "XrLabel8"
        Me.XrLabel8.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel8.SizeF = New System.Drawing.SizeF(10.41666!, 18.0!)
        Me.XrLabel8.Text = ":"
        '
        'lKDCOA_SALES
        '
        Me.lKDCOA_SALES.LocationFloat = New DevExpress.Utils.PointFloat(10.00001!, 272.6667!)
        Me.lKDCOA_SALES.Name = "lKDCOA_SALES"
        Me.lKDCOA_SALES.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.lKDCOA_SALES.SizeF = New System.Drawing.SizeF(138.5416!, 18.0!)
        Me.lKDCOA_SALES.Text = "Account Sales"
        '
        'XrLabel7
        '
        Me.XrLabel7.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "A_COA_INVENTORY.NMCOA")})
        Me.XrLabel7.LocationFloat = New DevExpress.Utils.PointFloat(158.9584!, 254.6667!)
        Me.XrLabel7.Name = "XrLabel7"
        Me.XrLabel7.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel7.SizeF = New System.Drawing.SizeF(274.7082!, 18.0!)
        Me.XrLabel7.Text = "XrLabel7"
        '
        'XrLabel6
        '
        Me.XrLabel6.LocationFloat = New DevExpress.Utils.PointFloat(148.5416!, 254.6667!)
        Me.XrLabel6.Name = "XrLabel6"
        Me.XrLabel6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel6.SizeF = New System.Drawing.SizeF(10.41666!, 18.0!)
        Me.XrLabel6.Text = ":"
        '
        'lKDCOA_INVENTORY
        '
        Me.lKDCOA_INVENTORY.LocationFloat = New DevExpress.Utils.PointFloat(9.999974!, 254.6667!)
        Me.lKDCOA_INVENTORY.Name = "lKDCOA_INVENTORY"
        Me.lKDCOA_INVENTORY.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.lKDCOA_INVENTORY.SizeF = New System.Drawing.SizeF(138.5416!, 18.0!)
        Me.lKDCOA_INVENTORY.Text = "Account Inventory"
        '
        'XrLabel5
        '
        Me.XrLabel5.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "NMITEM3")})
        Me.XrLabel5.Font = New System.Drawing.Font("Times New Roman", 9.75!)
        Me.XrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(158.9583!, 37.20834!)
        Me.XrLabel5.Name = "XrLabel5"
        Me.XrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel5.SizeF = New System.Drawing.SizeF(274.7082!, 18.0!)
        Me.XrLabel5.StylePriority.UseFont = False
        Me.XrLabel5.Text = "XrLabel5"
        '
        'XrLabel4
        '
        Me.XrLabel4.Font = New System.Drawing.Font("Times New Roman", 9.75!)
        Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(151.6667!, 37.20834!)
        Me.XrLabel4.Name = "XrLabel4"
        Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel4.SizeF = New System.Drawing.SizeF(7.291656!, 18.0!)
        Me.XrLabel4.StylePriority.UseFont = False
        Me.XrLabel4.Text = ":"
        '
        'lNMITEM3
        '
        Me.lNMITEM3.Font = New System.Drawing.Font("Times New Roman", 9.75!)
        Me.lNMITEM3.LocationFloat = New DevExpress.Utils.PointFloat(10.00001!, 37.20834!)
        Me.lNMITEM3.Name = "lNMITEM3"
        Me.lNMITEM3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.lNMITEM3.SizeF = New System.Drawing.SizeF(141.6667!, 18.0!)
        Me.lNMITEM3.StylePriority.UseFont = False
        Me.lNMITEM3.Text = "Item Name #3"
        '
        'XrLabel3
        '
        Me.XrLabel3.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "NMITEM2")})
        Me.XrLabel3.Font = New System.Drawing.Font("Times New Roman", 9.75!)
        Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(158.9583!, 18.0!)
        Me.XrLabel3.Name = "XrLabel3"
        Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel3.SizeF = New System.Drawing.SizeF(274.7082!, 18.0!)
        Me.XrLabel3.StylePriority.UseFont = False
        Me.XrLabel3.Text = "XrLabel3"
        '
        'XrLabel2
        '
        Me.XrLabel2.Font = New System.Drawing.Font("Times New Roman", 9.75!)
        Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(151.6667!, 18.0!)
        Me.XrLabel2.Name = "XrLabel2"
        Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel2.SizeF = New System.Drawing.SizeF(7.291656!, 18.0!)
        Me.XrLabel2.StylePriority.UseFont = False
        Me.XrLabel2.Text = ":"
        '
        'lNMITEM2
        '
        Me.lNMITEM2.Font = New System.Drawing.Font("Times New Roman", 9.75!)
        Me.lNMITEM2.LocationFloat = New DevExpress.Utils.PointFloat(10.00001!, 18.0!)
        Me.lNMITEM2.Name = "lNMITEM2"
        Me.lNMITEM2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.lNMITEM2.SizeF = New System.Drawing.SizeF(141.6667!, 18.0!)
        Me.lNMITEM2.StylePriority.UseFont = False
        Me.lNMITEM2.Text = "Item Name #2"
        '
        'XrLabel43
        '
        Me.XrLabel43.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "NMITEM1")})
        Me.XrLabel43.Font = New System.Drawing.Font("Times New Roman", 9.75!)
        Me.XrLabel43.LocationFloat = New DevExpress.Utils.PointFloat(158.9583!, 0.0!)
        Me.XrLabel43.Name = "XrLabel43"
        Me.XrLabel43.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel43.SizeF = New System.Drawing.SizeF(274.7082!, 18.0!)
        Me.XrLabel43.StylePriority.UseFont = False
        Me.XrLabel43.Text = "XrLabel43"
        '
        'XrLabel40
        '
        Me.XrLabel40.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "A_COA_COST.NMCOA")})
        Me.XrLabel40.LocationFloat = New DevExpress.Utils.PointFloat(158.9583!, 236.6667!)
        Me.XrLabel40.Name = "XrLabel40"
        Me.XrLabel40.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel40.SizeF = New System.Drawing.SizeF(274.7082!, 18.0!)
        Me.XrLabel40.Text = "XrLabel40"
        '
        'XrLabel39
        '
        Me.XrLabel39.LocationFloat = New DevExpress.Utils.PointFloat(148.5416!, 236.6667!)
        Me.XrLabel39.Name = "XrLabel39"
        Me.XrLabel39.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel39.SizeF = New System.Drawing.SizeF(10.41666!, 18.0!)
        Me.XrLabel39.Text = ":"
        '
        'lKDCOA_COST
        '
        Me.lKDCOA_COST.LocationFloat = New DevExpress.Utils.PointFloat(9.999974!, 236.6667!)
        Me.lKDCOA_COST.Name = "lKDCOA_COST"
        Me.lKDCOA_COST.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.lKDCOA_COST.SizeF = New System.Drawing.SizeF(138.5416!, 18.0!)
        Me.lKDCOA_COST.Text = "Account Cost"
        '
        'lINFORMATION_ACCOUNT
        '
        Me.lINFORMATION_ACCOUNT.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
        Me.lINFORMATION_ACCOUNT.LocationFloat = New DevExpress.Utils.PointFloat(9.999974!, 211.6667!)
        Me.lINFORMATION_ACCOUNT.Name = "lINFORMATION_ACCOUNT"
        Me.lINFORMATION_ACCOUNT.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.lINFORMATION_ACCOUNT.SizeF = New System.Drawing.SizeF(423.6664!, 25.0!)
        Me.lINFORMATION_ACCOUNT.StylePriority.UseFont = False
        Me.lINFORMATION_ACCOUNT.Text = "ACCOUNT INFORMATION"
        '
        'XrLabel31
        '
        Me.XrLabel31.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "KDITEM_L6")})
        Me.XrLabel31.LocationFloat = New DevExpress.Utils.PointFloat(158.9583!, 183.25!)
        Me.XrLabel31.Name = "XrLabel31"
        Me.XrLabel31.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel31.SizeF = New System.Drawing.SizeF(274.7082!, 18.0!)
        Me.XrLabel31.Text = "XrLabel31"
        '
        'XrLabel30
        '
        Me.XrLabel30.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "KDITEM_L5")})
        Me.XrLabel30.LocationFloat = New DevExpress.Utils.PointFloat(158.9583!, 165.2501!)
        Me.XrLabel30.Name = "XrLabel30"
        Me.XrLabel30.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel30.SizeF = New System.Drawing.SizeF(274.7083!, 18.0!)
        Me.XrLabel30.Text = "XrLabel30"
        '
        'XrLabel29
        '
        Me.XrLabel29.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "KDITEM_L4")})
        Me.XrLabel29.LocationFloat = New DevExpress.Utils.PointFloat(158.9583!, 147.2502!)
        Me.XrLabel29.Name = "XrLabel29"
        Me.XrLabel29.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel29.SizeF = New System.Drawing.SizeF(274.7082!, 18.0!)
        Me.XrLabel29.Text = "XrLabel29"
        '
        'XrLabel28
        '
        Me.XrLabel28.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "KDITEM_L3")})
        Me.XrLabel28.LocationFloat = New DevExpress.Utils.PointFloat(158.9583!, 129.2502!)
        Me.XrLabel28.Name = "XrLabel28"
        Me.XrLabel28.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel28.SizeF = New System.Drawing.SizeF(274.7082!, 17.99998!)
        Me.XrLabel28.Text = "XrLabel28"
        '
        'XrLabel27
        '
        Me.XrLabel27.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "KDITEM_L2")})
        Me.XrLabel27.LocationFloat = New DevExpress.Utils.PointFloat(158.9583!, 111.25!)
        Me.XrLabel27.Name = "XrLabel27"
        Me.XrLabel27.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel27.SizeF = New System.Drawing.SizeF(274.7083!, 18.0!)
        Me.XrLabel27.Text = "XrLabel27"
        '
        'XrLabel26
        '
        Me.XrLabel26.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "KDITEM_L1")})
        Me.XrLabel26.LocationFloat = New DevExpress.Utils.PointFloat(158.9583!, 93.24995!)
        Me.XrLabel26.Name = "XrLabel26"
        Me.XrLabel26.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel26.SizeF = New System.Drawing.SizeF(274.7083!, 18.0!)
        Me.XrLabel26.Text = "XrLabel26"
        '
        'XrLabel25
        '
        Me.XrLabel25.Font = New System.Drawing.Font("Times New Roman", 9.75!)
        Me.XrLabel25.LocationFloat = New DevExpress.Utils.PointFloat(151.6667!, 0.0!)
        Me.XrLabel25.Name = "XrLabel25"
        Me.XrLabel25.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel25.SizeF = New System.Drawing.SizeF(7.291656!, 18.0!)
        Me.XrLabel25.StylePriority.UseFont = False
        Me.XrLabel25.Text = ":"
        '
        'XrLabel19
        '
        Me.XrLabel19.LocationFloat = New DevExpress.Utils.PointFloat(148.5416!, 183.25!)
        Me.XrLabel19.Name = "XrLabel19"
        Me.XrLabel19.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel19.SizeF = New System.Drawing.SizeF(10.41666!, 18.0!)
        Me.XrLabel19.Text = ":"
        '
        'XrLabel18
        '
        Me.XrLabel18.LocationFloat = New DevExpress.Utils.PointFloat(148.5416!, 165.2501!)
        Me.XrLabel18.Name = "XrLabel18"
        Me.XrLabel18.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel18.SizeF = New System.Drawing.SizeF(10.41666!, 18.0!)
        Me.XrLabel18.Text = ":"
        '
        'XrLabel17
        '
        Me.XrLabel17.LocationFloat = New DevExpress.Utils.PointFloat(148.5416!, 147.2502!)
        Me.XrLabel17.Name = "XrLabel17"
        Me.XrLabel17.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel17.SizeF = New System.Drawing.SizeF(10.41666!, 18.0!)
        Me.XrLabel17.Text = ":"
        '
        'XrLabel16
        '
        Me.XrLabel16.LocationFloat = New DevExpress.Utils.PointFloat(148.5416!, 129.2501!)
        Me.XrLabel16.Name = "XrLabel16"
        Me.XrLabel16.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel16.SizeF = New System.Drawing.SizeF(10.41666!, 18.0!)
        Me.XrLabel16.Text = ":"
        '
        'XrLabel15
        '
        Me.XrLabel15.LocationFloat = New DevExpress.Utils.PointFloat(148.5416!, 111.25!)
        Me.XrLabel15.Name = "XrLabel15"
        Me.XrLabel15.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel15.SizeF = New System.Drawing.SizeF(10.41666!, 18.0!)
        Me.XrLabel15.Text = ":"
        '
        'XrLabel14
        '
        Me.XrLabel14.LocationFloat = New DevExpress.Utils.PointFloat(148.5416!, 93.24995!)
        Me.XrLabel14.Name = "XrLabel14"
        Me.XrLabel14.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel14.SizeF = New System.Drawing.SizeF(10.41666!, 18.0!)
        Me.XrLabel14.Text = ":"
        '
        'lKDITEM_L6
        '
        Me.lKDITEM_L6.LocationFloat = New DevExpress.Utils.PointFloat(9.999974!, 183.2502!)
        Me.lKDITEM_L6.Name = "lKDITEM_L6"
        Me.lKDITEM_L6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.lKDITEM_L6.SizeF = New System.Drawing.SizeF(138.5416!, 18.0!)
        Me.lKDITEM_L6.Text = "KDITEM_L6"
        '
        'lKDITEM_L5
        '
        Me.lKDITEM_L5.LocationFloat = New DevExpress.Utils.PointFloat(9.999974!, 165.2501!)
        Me.lKDITEM_L5.Name = "lKDITEM_L5"
        Me.lKDITEM_L5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.lKDITEM_L5.SizeF = New System.Drawing.SizeF(138.5416!, 18.0!)
        Me.lKDITEM_L5.Text = "KDITEM_L5"
        '
        'lKDITEM_L4
        '
        Me.lKDITEM_L4.LocationFloat = New DevExpress.Utils.PointFloat(9.999974!, 147.25!)
        Me.lKDITEM_L4.Name = "lKDITEM_L4"
        Me.lKDITEM_L4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.lKDITEM_L4.SizeF = New System.Drawing.SizeF(138.5416!, 18.0!)
        Me.lKDITEM_L4.Text = "KDITEM_L4"
        '
        'lKDITEM_L3
        '
        Me.lKDITEM_L3.LocationFloat = New DevExpress.Utils.PointFloat(9.999974!, 129.25!)
        Me.lKDITEM_L3.Name = "lKDITEM_L3"
        Me.lKDITEM_L3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.lKDITEM_L3.SizeF = New System.Drawing.SizeF(138.5416!, 18.0!)
        Me.lKDITEM_L3.Text = "KDITEM_L3"
        '
        'lKDITEM_L2
        '
        Me.lKDITEM_L2.LocationFloat = New DevExpress.Utils.PointFloat(9.999974!, 111.25!)
        Me.lKDITEM_L2.Name = "lKDITEM_L2"
        Me.lKDITEM_L2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.lKDITEM_L2.SizeF = New System.Drawing.SizeF(138.5416!, 18.0!)
        Me.lKDITEM_L2.Text = "KDITEM_L2"
        '
        'lINFORMATION_SPECIFICATION
        '
        Me.lINFORMATION_SPECIFICATION.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
        Me.lINFORMATION_SPECIFICATION.LocationFloat = New DevExpress.Utils.PointFloat(9.999974!, 68.24995!)
        Me.lINFORMATION_SPECIFICATION.Name = "lINFORMATION_SPECIFICATION"
        Me.lINFORMATION_SPECIFICATION.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.lINFORMATION_SPECIFICATION.SizeF = New System.Drawing.SizeF(423.6665!, 25.0!)
        Me.lINFORMATION_SPECIFICATION.StylePriority.UseFont = False
        Me.lINFORMATION_SPECIFICATION.Text = "SPECIFICATION INFORMATION"
        '
        'lKDITEM_L1
        '
        Me.lKDITEM_L1.LocationFloat = New DevExpress.Utils.PointFloat(9.999974!, 93.24995!)
        Me.lKDITEM_L1.Name = "lKDITEM_L1"
        Me.lKDITEM_L1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.lKDITEM_L1.SizeF = New System.Drawing.SizeF(138.5416!, 18.0!)
        Me.lKDITEM_L1.Text = "KDITEM_L1"
        '
        'lNMITEM1
        '
        Me.lNMITEM1.Font = New System.Drawing.Font("Times New Roman", 9.75!)
        Me.lNMITEM1.LocationFloat = New DevExpress.Utils.PointFloat(10.00001!, 0.0!)
        Me.lNMITEM1.Name = "lNMITEM1"
        Me.lNMITEM1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.lNMITEM1.SizeF = New System.Drawing.SizeF(141.6667!, 18.0!)
        Me.lNMITEM1.StylePriority.UseFont = False
        Me.lNMITEM1.Text = "Item Name #1"
        '
        'TopMargin
        '
        Me.TopMargin.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.lTITLE})
        Me.TopMargin.Name = "TopMargin"
        Me.TopMargin.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'lTITLE
        '
        Me.lTITLE.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Bold)
        Me.lTITLE.LocationFloat = New DevExpress.Utils.PointFloat(10.00001!, 75.0!)
        Me.lTITLE.Name = "lTITLE"
        Me.lTITLE.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.lTITLE.SizeF = New System.Drawing.SizeF(607.0001!, 18.0!)
        Me.lTITLE.StylePriority.UseBorders = False
        Me.lTITLE.StylePriority.UseFont = False
        Me.lTITLE.StylePriority.UseTextAlignment = False
        Me.lTITLE.Text = "ITEM"
        Me.lTITLE.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'BottomMargin
        '
        Me.BottomMargin.Name = "BottomMargin"
        Me.BottomMargin.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'bindingSource
        '
        Me.bindingSource.DataSource = GetType(DataAccess.M_ITEM)
        '
        'DetailReport
        '
        Me.DetailReport.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail1, Me.GroupHeader1, Me.GroupFooter1})
        Me.DetailReport.DataMember = "M_ITEM_UOMs"
        Me.DetailReport.DataSource = Me.bindingSource
        Me.DetailReport.Level = 0
        Me.DetailReport.Name = "DetailReport"
        '
        'Detail1
        '
        Me.Detail1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel13, Me.XrLabel12, Me.XrLabel11, Me.XrLabel10, Me.XrLabel1})
        Me.Detail1.HeightF = 18.0!
        Me.Detail1.Name = "Detail1"
        '
        'XrLabel13
        '
        Me.XrLabel13.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "M_ITEM_UOMs.MARGIN", "{0:n2}")})
        Me.XrLabel13.LocationFloat = New DevExpress.Utils.PointFloat(455.8332!, 0.0!)
        Me.XrLabel13.Name = "XrLabel13"
        Me.XrLabel13.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel13.SizeF = New System.Drawing.SizeF(128.125!, 18.0!)
        Me.XrLabel13.StylePriority.UseTextAlignment = False
        Me.XrLabel13.Text = "XrLabel13"
        Me.XrLabel13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'XrLabel12
        '
        Me.XrLabel12.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "M_ITEM_UOMs.PRICESALESSTANDARD", "{0:n2}")})
        Me.XrLabel12.LocationFloat = New DevExpress.Utils.PointFloat(327.7082!, 0.0!)
        Me.XrLabel12.Name = "XrLabel12"
        Me.XrLabel12.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel12.SizeF = New System.Drawing.SizeF(128.125!, 18.0!)
        Me.XrLabel12.StylePriority.UseTextAlignment = False
        Me.XrLabel12.Text = "XrLabel12"
        Me.XrLabel12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'XrLabel11
        '
        Me.XrLabel11.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "M_ITEM_UOMs.PRICEPURCHASESTANDARD", "{0:n2}")})
        Me.XrLabel11.LocationFloat = New DevExpress.Utils.PointFloat(199.5832!, 0.0!)
        Me.XrLabel11.Name = "XrLabel11"
        Me.XrLabel11.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel11.SizeF = New System.Drawing.SizeF(128.125!, 18.0!)
        Me.XrLabel11.StylePriority.UseTextAlignment = False
        Me.XrLabel11.Text = "XrLabel11"
        Me.XrLabel11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'XrLabel10
        '
        Me.XrLabel10.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "M_ITEM_UOMs.RATE", "{0:n2}")})
        Me.XrLabel10.LocationFloat = New DevExpress.Utils.PointFloat(104.79!, 0.0!)
        Me.XrLabel10.Name = "XrLabel10"
        Me.XrLabel10.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel10.SizeF = New System.Drawing.SizeF(94.79315!, 18.0!)
        Me.XrLabel10.StylePriority.UseTextAlignment = False
        Me.XrLabel10.Text = "XrLabel10"
        Me.XrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'XrLabel1
        '
        Me.XrLabel1.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "M_ITEM_UOMs.M_UOM.MEMO")})
        Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(10.00001!, 0.0!)
        Me.XrLabel1.Name = "XrLabel1"
        Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel1.SizeF = New System.Drawing.SizeF(94.79!, 18.0!)
        Me.XrLabel1.Text = "XrLabel1"
        '
        'GroupHeader1
        '
        Me.GroupHeader1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.lDETAILUOM_MARGIN, Me.lDETAILUOM_PRICESALESSTANDARD, Me.lDETAILUOM_PRICEPURCHASESTANDARD, Me.lDETAILUOM_RATE, Me.lDETAILUOM_KDUOM, Me.lINFORMATION_UNIT})
        Me.GroupHeader1.HeightF = 43.00003!
        Me.GroupHeader1.Name = "GroupHeader1"
        '
        'lDETAILUOM_MARGIN
        '
        Me.lDETAILUOM_MARGIN.LocationFloat = New DevExpress.Utils.PointFloat(455.8331!, 25.00003!)
        Me.lDETAILUOM_MARGIN.Name = "lDETAILUOM_MARGIN"
        Me.lDETAILUOM_MARGIN.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.lDETAILUOM_MARGIN.SizeF = New System.Drawing.SizeF(128.1249!, 18.0!)
        Me.lDETAILUOM_MARGIN.Text = "Margin (%)"
        '
        'lDETAILUOM_PRICESALESSTANDARD
        '
        Me.lDETAILUOM_PRICESALESSTANDARD.LocationFloat = New DevExpress.Utils.PointFloat(327.7081!, 25.00003!)
        Me.lDETAILUOM_PRICESALESSTANDARD.Name = "lDETAILUOM_PRICESALESSTANDARD"
        Me.lDETAILUOM_PRICESALESSTANDARD.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.lDETAILUOM_PRICESALESSTANDARD.SizeF = New System.Drawing.SizeF(128.125!, 18.0!)
        Me.lDETAILUOM_PRICESALESSTANDARD.Text = "Sales Price"
        '
        'lDETAILUOM_PRICEPURCHASESTANDARD
        '
        Me.lDETAILUOM_PRICEPURCHASESTANDARD.LocationFloat = New DevExpress.Utils.PointFloat(199.5831!, 25.00003!)
        Me.lDETAILUOM_PRICEPURCHASESTANDARD.Name = "lDETAILUOM_PRICEPURCHASESTANDARD"
        Me.lDETAILUOM_PRICEPURCHASESTANDARD.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.lDETAILUOM_PRICEPURCHASESTANDARD.SizeF = New System.Drawing.SizeF(128.125!, 18.0!)
        Me.lDETAILUOM_PRICEPURCHASESTANDARD.Text = "Purchase Price"
        '
        'lDETAILUOM_RATE
        '
        Me.lDETAILUOM_RATE.LocationFloat = New DevExpress.Utils.PointFloat(104.7915!, 25.00003!)
        Me.lDETAILUOM_RATE.Name = "lDETAILUOM_RATE"
        Me.lDETAILUOM_RATE.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.lDETAILUOM_RATE.SizeF = New System.Drawing.SizeF(94.79163!, 18.0!)
        Me.lDETAILUOM_RATE.Text = "Rate"
        '
        'lDETAILUOM_KDUOM
        '
        Me.lDETAILUOM_KDUOM.LocationFloat = New DevExpress.Utils.PointFloat(9.999847!, 25.00003!)
        Me.lDETAILUOM_KDUOM.Name = "lDETAILUOM_KDUOM"
        Me.lDETAILUOM_KDUOM.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.lDETAILUOM_KDUOM.SizeF = New System.Drawing.SizeF(94.79164!, 18.0!)
        Me.lDETAILUOM_KDUOM.Text = "Unit"
        '
        'lINFORMATION_UNIT
        '
        Me.lINFORMATION_UNIT.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
        Me.lINFORMATION_UNIT.LocationFloat = New DevExpress.Utils.PointFloat(10.00007!, 0.0!)
        Me.lINFORMATION_UNIT.Name = "lINFORMATION_UNIT"
        Me.lINFORMATION_UNIT.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.lINFORMATION_UNIT.SizeF = New System.Drawing.SizeF(423.6664!, 25.0!)
        Me.lINFORMATION_UNIT.StylePriority.UseFont = False
        Me.lINFORMATION_UNIT.Text = "UNIT INFORMATION"
        '
        'GroupFooter1
        '
        Me.GroupFooter1.HeightF = 8.333333!
        Me.GroupFooter1.Name = "GroupFooter1"
        '
        'xtraItem
        '
        Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.TopMargin, Me.BottomMargin, Me.DetailReport})
        Me.DataSource = Me.bindingSource
        Me.PageHeight = 1169
        Me.PageWidth = 827
        Me.PaperKind = System.Drawing.Printing.PaperKind.A4
        Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
        Me.Version = "12.1"
        CType(Me.bindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Friend WithEvents Detail As DevExpress.XtraReports.UI.DetailBand
    Friend WithEvents TopMargin As DevExpress.XtraReports.UI.TopMarginBand
    Friend WithEvents BottomMargin As DevExpress.XtraReports.UI.BottomMarginBand
    Friend WithEvents bindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents lTITLE As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents lNMITEM1 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents lKDITEM_L1 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents lINFORMATION_SPECIFICATION As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents lKDITEM_L5 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents lKDITEM_L4 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents lKDITEM_L3 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents lKDITEM_L2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents lKDITEM_L6 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel14 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel19 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel18 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel17 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel16 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel15 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel25 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel26 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel31 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel30 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel29 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel28 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel27 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents lINFORMATION_ACCOUNT As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents lKDCOA_COST As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel40 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel39 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel43 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents lNMITEM3 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents lNMITEM2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents DetailReport As DevExpress.XtraReports.UI.DetailReportBand
    Friend WithEvents Detail1 As DevExpress.XtraReports.UI.DetailBand
    Friend WithEvents XrLabel9 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel8 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents lKDCOA_SALES As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel7 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel6 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents lKDCOA_INVENTORY As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents chkISACTIVE As DevExpress.XtraReports.UI.XRCheckBox
    Friend WithEvents GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
    Friend WithEvents lDETAILUOM_MARGIN As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents lDETAILUOM_PRICESALESSTANDARD As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents lDETAILUOM_PRICEPURCHASESTANDARD As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents lDETAILUOM_RATE As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents lDETAILUOM_KDUOM As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel13 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel12 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel11 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel10 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents GroupFooter1 As DevExpress.XtraReports.UI.GroupFooterBand
    Friend WithEvents lINFORMATION_UNIT As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents picATTACHMENT As DevExpress.XtraReports.UI.XRPictureBox
End Class
