﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDirectSales
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmDirectSales))
        Me.txtGrandTotal = New DevExpress.XtraEditors.TextEdit()
        Me.picF12 = New DevExpress.XtraEditors.PictureEdit()
        Me.lblF12 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.picF1 = New DevExpress.XtraEditors.PictureEdit()
        Me.picF2 = New DevExpress.XtraEditors.PictureEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.picF11 = New DevExpress.XtraEditors.PictureEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.grd = New DevExpress.XtraGrid.GridControl()
        Me.bindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.grv = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colKDITEM = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.grdKDITEM = New DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit()
        Me.grvKDITEM = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.NMITEM2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colKDUOM = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.grdKDUOM = New DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit()
        Me.grvKDUOM = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colQTY = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colPRICE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colSUBTOTAL = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colDISCOUNT = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colGRANDTOTAL = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colREMARKS = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.btnSERIALNUMBER = New DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.lblCashier = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl14 = New DevExpress.XtraEditors.LabelControl()
        Me.lblPhone = New DevExpress.XtraEditors.LabelControl()
        Me.lblAddress = New DevExpress.XtraEditors.LabelControl()
        Me.lblName = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.picF3 = New DevExpress.XtraEditors.PictureEdit()
        Me.picF5 = New DevExpress.XtraEditors.PictureEdit()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.picF10 = New DevExpress.XtraEditors.PictureEdit()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.picF9 = New DevExpress.XtraEditors.PictureEdit()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.picF6 = New DevExpress.XtraEditors.PictureEdit()
        Me.picF7 = New DevExpress.XtraEditors.PictureEdit()
        Me.picF8 = New DevExpress.XtraEditors.PictureEdit()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl13 = New DevExpress.XtraEditors.LabelControl()
        Me.txtITEM = New DevExpress.XtraEditors.ButtonEdit()
        Me.txtItemName = New DevExpress.XtraEditors.TextEdit()
        Me.grdKDCUSTOMER = New DevExpress.XtraEditors.GridLookUpEdit()
        Me.grvKDCUSTOMER = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn3 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn4 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.txtEMAIL = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl15 = New DevExpress.XtraEditors.LabelControl()
        Me.grdKDSALESMAN = New DevExpress.XtraEditors.GridLookUpEdit()
        Me.grvKDSALESMAN = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn5 = New DevExpress.XtraGrid.Columns.GridColumn()
        CType(Me.txtGrandTotal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picF12.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picF1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picF2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picF11.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grd, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grv, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grdKDITEM, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvKDITEM, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grdKDUOM, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvKDUOM, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnSERIALNUMBER, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.picF3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picF5.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picF10.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picF9.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picF6.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picF7.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picF8.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtITEM.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtItemName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grdKDCUSTOMER.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvKDCUSTOMER, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtEMAIL.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grdKDSALESMAN.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvKDSALESMAN, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'txtGrandTotal
        '
        Me.txtGrandTotal.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtGrandTotal.Location = New System.Drawing.Point(415, 14)
        Me.txtGrandTotal.Name = "txtGrandTotal"
        Me.txtGrandTotal.Properties.Appearance.BackColor = System.Drawing.Color.Black
        Me.txtGrandTotal.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 58.0!, System.Drawing.FontStyle.Bold)
        Me.txtGrandTotal.Properties.Appearance.ForeColor = System.Drawing.Color.Lime
        Me.txtGrandTotal.Properties.Appearance.Options.UseBackColor = True
        Me.txtGrandTotal.Properties.Appearance.Options.UseFont = True
        Me.txtGrandTotal.Properties.Appearance.Options.UseForeColor = True
        Me.txtGrandTotal.Properties.Appearance.Options.UseTextOptions = True
        Me.txtGrandTotal.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtGrandTotal.Properties.Mask.EditMask = "n0"
        Me.txtGrandTotal.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtGrandTotal.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtGrandTotal.Properties.NullText = "0"
        Me.txtGrandTotal.Properties.ReadOnly = True
        Me.txtGrandTotal.Size = New System.Drawing.Size(598, 100)
        Me.txtGrandTotal.TabIndex = 0
        Me.txtGrandTotal.TabStop = False
        '
        'picF12
        '
        Me.picF12.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.picF12.EditValue = CType(resources.GetObject("picF12.EditValue"), Object)
        Me.picF12.Location = New System.Drawing.Point(872, 647)
        Me.picF12.Name = "picF12"
        Me.picF12.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.picF12.Properties.Appearance.Options.UseBackColor = True
        Me.picF12.Properties.ShowMenu = False
        Me.picF12.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom
        Me.picF12.Size = New System.Drawing.Size(80, 80)
        Me.picF12.TabIndex = 1
        Me.picF12.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Hand
        '
        'lblF12
        '
        Me.lblF12.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblF12.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.lblF12.Location = New System.Drawing.Point(880, 733)
        Me.lblF12.Name = "lblF12"
        Me.lblF12.Size = New System.Drawing.Size(65, 16)
        Me.lblF12.TabIndex = 2
        Me.lblF12.Text = "F12 - Close"
        '
        'LabelControl1
        '
        Me.LabelControl1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.LabelControl1.Location = New System.Drawing.Point(26, 733)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(52, 16)
        Me.LabelControl1.TabIndex = 4
        Me.LabelControl1.Text = "F1 - Help"
        '
        'picF1
        '
        Me.picF1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.picF1.EditValue = CType(resources.GetObject("picF1.EditValue"), Object)
        Me.picF1.Location = New System.Drawing.Point(12, 647)
        Me.picF1.Name = "picF1"
        Me.picF1.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.picF1.Properties.Appearance.Options.UseBackColor = True
        Me.picF1.Properties.ShowMenu = False
        Me.picF1.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom
        Me.picF1.Size = New System.Drawing.Size(80, 80)
        Me.picF1.TabIndex = 3
        Me.picF1.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Hand
        '
        'picF2
        '
        Me.picF2.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.picF2.EditValue = CType(resources.GetObject("picF2.EditValue"), Object)
        Me.picF2.Location = New System.Drawing.Point(98, 647)
        Me.picF2.Name = "picF2"
        Me.picF2.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.picF2.Properties.Appearance.Options.UseBackColor = True
        Me.picF2.Properties.ShowMenu = False
        Me.picF2.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom
        Me.picF2.Size = New System.Drawing.Size(80, 80)
        Me.picF2.TabIndex = 5
        Me.picF2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Hand
        '
        'LabelControl2
        '
        Me.LabelControl2.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.LabelControl2.Location = New System.Drawing.Point(102, 733)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(72, 16)
        Me.LabelControl2.TabIndex = 4
        Me.LabelControl2.Text = "F2 - Look Up"
        '
        'picF11
        '
        Me.picF11.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.picF11.EditValue = CType(resources.GetObject("picF11.EditValue"), Object)
        Me.picF11.Location = New System.Drawing.Point(786, 647)
        Me.picF11.Name = "picF11"
        Me.picF11.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.picF11.Properties.Appearance.Options.UseBackColor = True
        Me.picF11.Properties.ShowMenu = False
        Me.picF11.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom
        Me.picF11.Size = New System.Drawing.Size(80, 80)
        Me.picF11.TabIndex = 1
        Me.picF11.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Hand
        '
        'LabelControl3
        '
        Me.LabelControl3.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.LabelControl3.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.LabelControl3.Location = New System.Drawing.Point(785, 733)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(83, 16)
        Me.LabelControl3.TabIndex = 2
        Me.LabelControl3.Text = "F11 - Payment"
        '
        'grd
        '
        Me.grd.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grd.DataSource = Me.bindingSource
        Me.grd.Location = New System.Drawing.Point(12, 264)
        Me.grd.MainView = Me.grv
        Me.grd.Name = "grd"
        Me.grd.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.grdKDITEM, Me.grdKDUOM, Me.btnSERIALNUMBER})
        Me.grd.Size = New System.Drawing.Size(1001, 377)
        Me.grd.TabIndex = 3
        Me.grd.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.grv})
        '
        'bindingSource
        '
        Me.bindingSource.DataSource = GetType(DataAccess.S_SI_D)
        '
        'grv
        '
        Me.grv.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.Silver
        Me.grv.Appearance.ColumnFilterButton.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(212, Byte), Integer), CType(CType(212, Byte), Integer), CType(CType(212, Byte), Integer))
        Me.grv.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.Silver
        Me.grv.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Gray
        Me.grv.Appearance.ColumnFilterButton.Options.UseBackColor = True
        Me.grv.Appearance.ColumnFilterButton.Options.UseBorderColor = True
        Me.grv.Appearance.ColumnFilterButton.Options.UseForeColor = True
        Me.grv.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(CType(CType(212, Byte), Integer), CType(CType(212, Byte), Integer), CType(CType(212, Byte), Integer))
        Me.grv.Appearance.ColumnFilterButtonActive.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(223, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.grv.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(CType(CType(212, Byte), Integer), CType(CType(212, Byte), Integer), CType(CType(212, Byte), Integer))
        Me.grv.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Blue
        Me.grv.Appearance.ColumnFilterButtonActive.Options.UseBackColor = True
        Me.grv.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = True
        Me.grv.Appearance.ColumnFilterButtonActive.Options.UseForeColor = True
        Me.grv.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(CType(CType(243, Byte), Integer), CType(CType(243, Byte), Integer), CType(CType(243, Byte), Integer))
        Me.grv.Appearance.Empty.Options.UseBackColor = True
        Me.grv.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(223, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.grv.Appearance.EvenRow.BackColor2 = System.Drawing.Color.GhostWhite
        Me.grv.Appearance.EvenRow.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.grv.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black
        Me.grv.Appearance.EvenRow.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal
        Me.grv.Appearance.EvenRow.Options.UseBackColor = True
        Me.grv.Appearance.EvenRow.Options.UseFont = True
        Me.grv.Appearance.EvenRow.Options.UseForeColor = True
        Me.grv.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(212, Byte), Integer), CType(CType(208, Byte), Integer), CType(CType(200, Byte), Integer))
        Me.grv.Appearance.FilterCloseButton.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(118, Byte), Integer), CType(CType(170, Byte), Integer), CType(CType(225, Byte), Integer))
        Me.grv.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(212, Byte), Integer), CType(CType(208, Byte), Integer), CType(CType(200, Byte), Integer))
        Me.grv.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black
        Me.grv.Appearance.FilterCloseButton.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal
        Me.grv.Appearance.FilterCloseButton.Options.UseBackColor = True
        Me.grv.Appearance.FilterCloseButton.Options.UseBorderColor = True
        Me.grv.Appearance.FilterCloseButton.Options.UseForeColor = True
        Me.grv.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(80, Byte), Integer), CType(CType(135, Byte), Integer))
        Me.grv.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(212, Byte), Integer), CType(CType(208, Byte), Integer), CType(CType(200, Byte), Integer))
        Me.grv.Appearance.FilterPanel.ForeColor = System.Drawing.Color.White
        Me.grv.Appearance.FilterPanel.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal
        Me.grv.Appearance.FilterPanel.Options.UseBackColor = True
        Me.grv.Appearance.FilterPanel.Options.UseForeColor = True
        Me.grv.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(58, Byte), Integer), CType(CType(58, Byte), Integer), CType(CType(58, Byte), Integer))
        Me.grv.Appearance.FixedLine.Options.UseBackColor = True
        Me.grv.Appearance.FocusedCell.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(225, Byte), Integer))
        Me.grv.Appearance.FocusedCell.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.grv.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black
        Me.grv.Appearance.FocusedCell.Options.UseBackColor = True
        Me.grv.Appearance.FocusedCell.Options.UseFont = True
        Me.grv.Appearance.FocusedCell.Options.UseForeColor = True
        Me.grv.Appearance.FocusedRow.BackColor = System.Drawing.Color.Navy
        Me.grv.Appearance.FocusedRow.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(178, Byte), Integer))
        Me.grv.Appearance.FocusedRow.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.grv.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White
        Me.grv.Appearance.FocusedRow.Options.UseBackColor = True
        Me.grv.Appearance.FocusedRow.Options.UseFont = True
        Me.grv.Appearance.FocusedRow.Options.UseForeColor = True
        Me.grv.Appearance.FooterPanel.BackColor = System.Drawing.Color.Silver
        Me.grv.Appearance.FooterPanel.BorderColor = System.Drawing.Color.Silver
        Me.grv.Appearance.FooterPanel.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.grv.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black
        Me.grv.Appearance.FooterPanel.Options.UseBackColor = True
        Me.grv.Appearance.FooterPanel.Options.UseBorderColor = True
        Me.grv.Appearance.FooterPanel.Options.UseFont = True
        Me.grv.Appearance.FooterPanel.Options.UseForeColor = True
        Me.grv.Appearance.GroupButton.BackColor = System.Drawing.Color.Silver
        Me.grv.Appearance.GroupButton.BorderColor = System.Drawing.Color.Silver
        Me.grv.Appearance.GroupButton.ForeColor = System.Drawing.Color.Black
        Me.grv.Appearance.GroupButton.Options.UseBackColor = True
        Me.grv.Appearance.GroupButton.Options.UseBorderColor = True
        Me.grv.Appearance.GroupButton.Options.UseForeColor = True
        Me.grv.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(CType(CType(202, Byte), Integer), CType(CType(202, Byte), Integer), CType(CType(202, Byte), Integer))
        Me.grv.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(CType(CType(202, Byte), Integer), CType(CType(202, Byte), Integer), CType(CType(202, Byte), Integer))
        Me.grv.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black
        Me.grv.Appearance.GroupFooter.Options.UseBackColor = True
        Me.grv.Appearance.GroupFooter.Options.UseBorderColor = True
        Me.grv.Appearance.GroupFooter.Options.UseForeColor = True
        Me.grv.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(58, Byte), Integer), CType(CType(110, Byte), Integer), CType(CType(165, Byte), Integer))
        Me.grv.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White
        Me.grv.Appearance.GroupPanel.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.grv.Appearance.GroupPanel.ForeColor = System.Drawing.Color.White
        Me.grv.Appearance.GroupPanel.Options.UseBackColor = True
        Me.grv.Appearance.GroupPanel.Options.UseFont = True
        Me.grv.Appearance.GroupPanel.Options.UseForeColor = True
        Me.grv.Appearance.GroupRow.BackColor = System.Drawing.Color.Gray
        Me.grv.Appearance.GroupRow.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.grv.Appearance.GroupRow.ForeColor = System.Drawing.Color.Silver
        Me.grv.Appearance.GroupRow.Options.UseBackColor = True
        Me.grv.Appearance.GroupRow.Options.UseFont = True
        Me.grv.Appearance.GroupRow.Options.UseForeColor = True
        Me.grv.Appearance.HeaderPanel.BackColor = System.Drawing.Color.Silver
        Me.grv.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.Silver
        Me.grv.Appearance.HeaderPanel.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.grv.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black
        Me.grv.Appearance.HeaderPanel.Options.UseBackColor = True
        Me.grv.Appearance.HeaderPanel.Options.UseBorderColor = True
        Me.grv.Appearance.HeaderPanel.Options.UseFont = True
        Me.grv.Appearance.HeaderPanel.Options.UseForeColor = True
        Me.grv.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.Gray
        Me.grv.Appearance.HideSelectionRow.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.grv.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(CType(CType(212, Byte), Integer), CType(CType(208, Byte), Integer), CType(CType(200, Byte), Integer))
        Me.grv.Appearance.HideSelectionRow.Options.UseBackColor = True
        Me.grv.Appearance.HideSelectionRow.Options.UseFont = True
        Me.grv.Appearance.HideSelectionRow.Options.UseForeColor = True
        Me.grv.Appearance.HorzLine.BackColor = System.Drawing.Color.Silver
        Me.grv.Appearance.HorzLine.Options.UseBackColor = True
        Me.grv.Appearance.OddRow.BackColor = System.Drawing.Color.White
        Me.grv.Appearance.OddRow.BackColor2 = System.Drawing.Color.White
        Me.grv.Appearance.OddRow.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.grv.Appearance.OddRow.ForeColor = System.Drawing.Color.Black
        Me.grv.Appearance.OddRow.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal
        Me.grv.Appearance.OddRow.Options.UseBackColor = True
        Me.grv.Appearance.OddRow.Options.UseFont = True
        Me.grv.Appearance.OddRow.Options.UseForeColor = True
        Me.grv.Appearance.Preview.BackColor = System.Drawing.Color.White
        Me.grv.Appearance.Preview.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.grv.Appearance.Preview.ForeColor = System.Drawing.Color.Navy
        Me.grv.Appearance.Preview.Options.UseBackColor = True
        Me.grv.Appearance.Preview.Options.UseFont = True
        Me.grv.Appearance.Preview.Options.UseForeColor = True
        Me.grv.Appearance.Row.BackColor = System.Drawing.Color.White
        Me.grv.Appearance.Row.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.grv.Appearance.Row.ForeColor = System.Drawing.Color.Black
        Me.grv.Appearance.Row.Options.UseBackColor = True
        Me.grv.Appearance.Row.Options.UseFont = True
        Me.grv.Appearance.Row.Options.UseForeColor = True
        Me.grv.Appearance.RowSeparator.BackColor = System.Drawing.Color.White
        Me.grv.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(243, Byte), Integer), CType(CType(243, Byte), Integer), CType(CType(243, Byte), Integer))
        Me.grv.Appearance.RowSeparator.Options.UseBackColor = True
        Me.grv.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(10, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.grv.Appearance.SelectedRow.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.grv.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White
        Me.grv.Appearance.SelectedRow.Options.UseBackColor = True
        Me.grv.Appearance.SelectedRow.Options.UseFont = True
        Me.grv.Appearance.SelectedRow.Options.UseForeColor = True
        Me.grv.Appearance.TopNewRow.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.grv.Appearance.TopNewRow.Options.UseFont = True
        Me.grv.Appearance.VertLine.BackColor = System.Drawing.Color.Silver
        Me.grv.Appearance.VertLine.Options.UseBackColor = True
        Me.grv.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colKDITEM, Me.colKDUOM, Me.colQTY, Me.colPRICE, Me.colSUBTOTAL, Me.colDISCOUNT, Me.colGRANDTOTAL, Me.colREMARKS})
        Me.grv.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.grv.GridControl = Me.grd
        Me.grv.Name = "grv"
        Me.grv.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.grv.OptionsView.EnableAppearanceEvenRow = True
        Me.grv.OptionsView.EnableAppearanceOddRow = True
        Me.grv.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Bottom
        Me.grv.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never
        Me.grv.OptionsView.ShowGroupPanel = False
        Me.grv.PaintStyleName = "WindowsXP"
        '
        'colKDITEM
        '
        Me.colKDITEM.Caption = "Name"
        Me.colKDITEM.ColumnEdit = Me.grdKDITEM
        Me.colKDITEM.FieldName = "KDITEM"
        Me.colKDITEM.Name = "colKDITEM"
        Me.colKDITEM.OptionsColumn.AllowEdit = False
        Me.colKDITEM.OptionsColumn.AllowFocus = False
        Me.colKDITEM.OptionsColumn.AllowMove = False
        Me.colKDITEM.OptionsColumn.AllowShowHide = False
        Me.colKDITEM.OptionsColumn.AllowSize = False
        Me.colKDITEM.OptionsColumn.FixedWidth = True
        Me.colKDITEM.OptionsColumn.ReadOnly = True
        Me.colKDITEM.OptionsColumn.TabStop = False
        Me.colKDITEM.Visible = True
        Me.colKDITEM.VisibleIndex = 0
        Me.colKDITEM.Width = 206
        '
        'grdKDITEM
        '
        Me.grdKDITEM.AutoHeight = False
        Me.grdKDITEM.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.grdKDITEM.Name = "grdKDITEM"
        Me.grdKDITEM.NullText = ""
        Me.grdKDITEM.View = Me.grvKDITEM
        '
        'grvKDITEM
        '
        Me.grvKDITEM.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn1, Me.NMITEM2})
        Me.grvKDITEM.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.grvKDITEM.Name = "grvKDITEM"
        Me.grvKDITEM.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.grvKDITEM.OptionsView.ShowGroupPanel = False
        '
        'GridColumn1
        '
        Me.GridColumn1.Caption = "Barcode"
        Me.GridColumn1.FieldName = "NMITEM1"
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.Visible = True
        Me.GridColumn1.VisibleIndex = 0
        '
        'NMITEM2
        '
        Me.NMITEM2.Caption = "Name"
        Me.NMITEM2.FieldName = "NMITEM2"
        Me.NMITEM2.Name = "NMITEM2"
        Me.NMITEM2.Visible = True
        Me.NMITEM2.VisibleIndex = 1
        '
        'colKDUOM
        '
        Me.colKDUOM.Caption = "Unit"
        Me.colKDUOM.ColumnEdit = Me.grdKDUOM
        Me.colKDUOM.FieldName = "KDUOM"
        Me.colKDUOM.Name = "colKDUOM"
        Me.colKDUOM.OptionsColumn.AllowEdit = False
        Me.colKDUOM.OptionsColumn.AllowFocus = False
        Me.colKDUOM.OptionsColumn.AllowMove = False
        Me.colKDUOM.OptionsColumn.AllowShowHide = False
        Me.colKDUOM.OptionsColumn.AllowSize = False
        Me.colKDUOM.OptionsColumn.FixedWidth = True
        Me.colKDUOM.OptionsColumn.ReadOnly = True
        Me.colKDUOM.OptionsColumn.TabStop = False
        Me.colKDUOM.Visible = True
        Me.colKDUOM.VisibleIndex = 1
        '
        'grdKDUOM
        '
        Me.grdKDUOM.AutoHeight = False
        Me.grdKDUOM.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.grdKDUOM.Name = "grdKDUOM"
        Me.grdKDUOM.NullText = ""
        Me.grdKDUOM.PopupFormMinSize = New System.Drawing.Size(600, 300)
        Me.grdKDUOM.View = Me.grvKDUOM
        '
        'grvKDUOM
        '
        Me.grvKDUOM.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn2})
        Me.grvKDUOM.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.grvKDUOM.Name = "grvKDUOM"
        Me.grvKDUOM.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.grvKDUOM.OptionsView.ShowGroupPanel = False
        '
        'GridColumn2
        '
        Me.GridColumn2.Caption = "Memo"
        Me.GridColumn2.FieldName = "MEMO"
        Me.GridColumn2.Name = "GridColumn2"
        Me.GridColumn2.Visible = True
        Me.GridColumn2.VisibleIndex = 0
        '
        'colQTY
        '
        Me.colQTY.Caption = "Quantity"
        Me.colQTY.DisplayFormat.FormatString = "{0:n0}"
        Me.colQTY.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.colQTY.FieldName = "QTY"
        Me.colQTY.Name = "colQTY"
        Me.colQTY.OptionsColumn.AllowEdit = False
        Me.colQTY.OptionsColumn.AllowFocus = False
        Me.colQTY.OptionsColumn.AllowMove = False
        Me.colQTY.OptionsColumn.AllowShowHide = False
        Me.colQTY.OptionsColumn.AllowSize = False
        Me.colQTY.OptionsColumn.FixedWidth = True
        Me.colQTY.OptionsColumn.ReadOnly = True
        Me.colQTY.OptionsColumn.TabStop = False
        Me.colQTY.Visible = True
        Me.colQTY.VisibleIndex = 2
        '
        'colPRICE
        '
        Me.colPRICE.Caption = "Price"
        Me.colPRICE.DisplayFormat.FormatString = "{0:n0}"
        Me.colPRICE.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.colPRICE.FieldName = "PRICE"
        Me.colPRICE.Name = "colPRICE"
        Me.colPRICE.OptionsColumn.AllowEdit = False
        Me.colPRICE.OptionsColumn.AllowFocus = False
        Me.colPRICE.OptionsColumn.AllowMove = False
        Me.colPRICE.OptionsColumn.AllowShowHide = False
        Me.colPRICE.OptionsColumn.AllowSize = False
        Me.colPRICE.OptionsColumn.FixedWidth = True
        Me.colPRICE.OptionsColumn.ReadOnly = True
        Me.colPRICE.OptionsColumn.TabStop = False
        Me.colPRICE.Visible = True
        Me.colPRICE.VisibleIndex = 3
        '
        'colSUBTOTAL
        '
        Me.colSUBTOTAL.Caption = "Sub Total"
        Me.colSUBTOTAL.DisplayFormat.FormatString = "{0:n0}"
        Me.colSUBTOTAL.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.colSUBTOTAL.FieldName = "SUBTOTAL"
        Me.colSUBTOTAL.Name = "colSUBTOTAL"
        Me.colSUBTOTAL.OptionsColumn.AllowEdit = False
        Me.colSUBTOTAL.OptionsColumn.AllowFocus = False
        Me.colSUBTOTAL.OptionsColumn.AllowMove = False
        Me.colSUBTOTAL.OptionsColumn.AllowShowHide = False
        Me.colSUBTOTAL.OptionsColumn.AllowSize = False
        Me.colSUBTOTAL.OptionsColumn.FixedWidth = True
        Me.colSUBTOTAL.OptionsColumn.ReadOnly = True
        Me.colSUBTOTAL.OptionsColumn.TabStop = False
        Me.colSUBTOTAL.Visible = True
        Me.colSUBTOTAL.VisibleIndex = 4
        '
        'colDISCOUNT
        '
        Me.colDISCOUNT.Caption = "Discount (%)"
        Me.colDISCOUNT.DisplayFormat.FormatString = "{0:n0}"
        Me.colDISCOUNT.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.colDISCOUNT.FieldName = "DISCOUNT"
        Me.colDISCOUNT.Name = "colDISCOUNT"
        Me.colDISCOUNT.OptionsColumn.AllowMove = False
        Me.colDISCOUNT.OptionsColumn.AllowShowHide = False
        Me.colDISCOUNT.OptionsColumn.AllowSize = False
        Me.colDISCOUNT.OptionsColumn.FixedWidth = True
        Me.colDISCOUNT.Visible = True
        Me.colDISCOUNT.VisibleIndex = 5
        Me.colDISCOUNT.Width = 129
        '
        'colGRANDTOTAL
        '
        Me.colGRANDTOTAL.Caption = "Total"
        Me.colGRANDTOTAL.DisplayFormat.FormatString = "{0:n0}"
        Me.colGRANDTOTAL.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.colGRANDTOTAL.FieldName = "GRANDTOTAL"
        Me.colGRANDTOTAL.Name = "colGRANDTOTAL"
        Me.colGRANDTOTAL.OptionsColumn.AllowEdit = False
        Me.colGRANDTOTAL.OptionsColumn.AllowFocus = False
        Me.colGRANDTOTAL.OptionsColumn.AllowMove = False
        Me.colGRANDTOTAL.OptionsColumn.AllowShowHide = False
        Me.colGRANDTOTAL.OptionsColumn.AllowSize = False
        Me.colGRANDTOTAL.OptionsColumn.FixedWidth = True
        Me.colGRANDTOTAL.OptionsColumn.ReadOnly = True
        Me.colGRANDTOTAL.OptionsColumn.TabStop = False
        Me.colGRANDTOTAL.Visible = True
        Me.colGRANDTOTAL.VisibleIndex = 6
        Me.colGRANDTOTAL.Width = 174
        '
        'colREMARKS
        '
        Me.colREMARKS.Caption = "Remarks"
        Me.colREMARKS.FieldName = "REMARKS"
        Me.colREMARKS.Name = "colREMARKS"
        Me.colREMARKS.OptionsColumn.AllowEdit = False
        Me.colREMARKS.OptionsColumn.AllowFocus = False
        Me.colREMARKS.OptionsColumn.ReadOnly = True
        Me.colREMARKS.OptionsColumn.TabStop = False
        Me.colREMARKS.Visible = True
        Me.colREMARKS.VisibleIndex = 7
        Me.colREMARKS.Width = 173
        '
        'btnSERIALNUMBER
        '
        Me.btnSERIALNUMBER.AutoHeight = False
        Me.btnSERIALNUMBER.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.btnSERIALNUMBER.Name = "btnSERIALNUMBER"
        Me.btnSERIALNUMBER.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        '
        'LabelControl5
        '
        Me.LabelControl5.Appearance.Font = New System.Drawing.Font("Tahoma", 14.0!)
        Me.LabelControl5.Location = New System.Drawing.Point(12, 159)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(184, 23)
        Me.LabelControl5.TabIndex = 7
        Me.LabelControl5.Text = "Alt + F1 - Customer : "
        '
        'PanelControl1
        '
        Me.PanelControl1.Controls.Add(Me.lblCashier)
        Me.PanelControl1.Controls.Add(Me.LabelControl14)
        Me.PanelControl1.Controls.Add(Me.lblPhone)
        Me.PanelControl1.Controls.Add(Me.lblAddress)
        Me.PanelControl1.Controls.Add(Me.lblName)
        Me.PanelControl1.Location = New System.Drawing.Point(12, 12)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(397, 102)
        Me.PanelControl1.TabIndex = 10
        '
        'lblCashier
        '
        Me.lblCashier.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.lblCashier.Location = New System.Drawing.Point(74, 79)
        Me.lblCashier.Name = "lblCashier"
        Me.lblCashier.Size = New System.Drawing.Size(6, 19)
        Me.lblCashier.TabIndex = 0
        Me.lblCashier.Text = "-"
        '
        'LabelControl14
        '
        Me.LabelControl14.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.LabelControl14.Location = New System.Drawing.Point(5, 79)
        Me.LabelControl14.Name = "LabelControl14"
        Me.LabelControl14.Size = New System.Drawing.Size(63, 19)
        Me.LabelControl14.TabIndex = 0
        Me.LabelControl14.Text = "Cashier :"
        '
        'lblPhone
        '
        Me.lblPhone.Appearance.Font = New System.Drawing.Font("Tahoma", 8.0!)
        Me.lblPhone.Location = New System.Drawing.Point(5, 47)
        Me.lblPhone.Name = "lblPhone"
        Me.lblPhone.Size = New System.Drawing.Size(30, 13)
        Me.lblPhone.TabIndex = 0
        Me.lblPhone.Text = "Phone"
        '
        'lblAddress
        '
        Me.lblAddress.Appearance.Font = New System.Drawing.Font("Tahoma", 8.0!)
        Me.lblAddress.Location = New System.Drawing.Point(5, 28)
        Me.lblAddress.Name = "lblAddress"
        Me.lblAddress.Size = New System.Drawing.Size(39, 13)
        Me.lblAddress.TabIndex = 0
        Me.lblAddress.Text = "Address"
        '
        'lblName
        '
        Me.lblName.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.lblName.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.lblName.Location = New System.Drawing.Point(5, 3)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(41, 19)
        Me.lblName.TabIndex = 0
        Me.lblName.Text = "Name"
        '
        'LabelControl6
        '
        Me.LabelControl6.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.LabelControl6.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.LabelControl6.Location = New System.Drawing.Point(187, 733)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(74, 16)
        Me.LabelControl6.TabIndex = 4
        Me.LabelControl6.Text = "F3 - Member"
        '
        'picF3
        '
        Me.picF3.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.picF3.EditValue = CType(resources.GetObject("picF3.EditValue"), Object)
        Me.picF3.Location = New System.Drawing.Point(184, 647)
        Me.picF3.Name = "picF3"
        Me.picF3.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.picF3.Properties.Appearance.Options.UseBackColor = True
        Me.picF3.Properties.ShowMenu = False
        Me.picF3.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom
        Me.picF3.Size = New System.Drawing.Size(80, 80)
        Me.picF3.TabIndex = 5
        Me.picF3.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Hand
        '
        'picF5
        '
        Me.picF5.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.picF5.EditValue = CType(resources.GetObject("picF5.EditValue"), Object)
        Me.picF5.Location = New System.Drawing.Point(270, 647)
        Me.picF5.Name = "picF5"
        Me.picF5.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.picF5.Properties.Appearance.Options.UseBackColor = True
        Me.picF5.Properties.ShowMenu = False
        Me.picF5.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom
        Me.picF5.Size = New System.Drawing.Size(80, 80)
        Me.picF5.TabIndex = 5
        Me.picF5.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Hand
        '
        'LabelControl7
        '
        Me.LabelControl7.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.LabelControl7.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.LabelControl7.Location = New System.Drawing.Point(271, 733)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(79, 16)
        Me.LabelControl7.TabIndex = 4
        Me.LabelControl7.Text = "F5 - Cash Out"
        '
        'picF10
        '
        Me.picF10.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.picF10.EditValue = CType(resources.GetObject("picF10.EditValue"), Object)
        Me.picF10.Location = New System.Drawing.Point(700, 647)
        Me.picF10.Name = "picF10"
        Me.picF10.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.picF10.Properties.Appearance.Options.UseBackColor = True
        Me.picF10.Properties.ShowMenu = False
        Me.picF10.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom
        Me.picF10.Size = New System.Drawing.Size(80, 80)
        Me.picF10.TabIndex = 1
        Me.picF10.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Hand
        '
        'LabelControl8
        '
        Me.LabelControl8.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.LabelControl8.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.LabelControl8.Location = New System.Drawing.Point(708, 733)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(64, 16)
        Me.LabelControl8.TabIndex = 2
        Me.LabelControl8.Text = "F10 - Clear"
        '
        'picF9
        '
        Me.picF9.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.picF9.EditValue = CType(resources.GetObject("picF9.EditValue"), Object)
        Me.picF9.Location = New System.Drawing.Point(614, 647)
        Me.picF9.Name = "picF9"
        Me.picF9.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.picF9.Properties.Appearance.Options.UseBackColor = True
        Me.picF9.Properties.ShowMenu = False
        Me.picF9.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom
        Me.picF9.Size = New System.Drawing.Size(80, 80)
        Me.picF9.TabIndex = 1
        Me.picF9.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Hand
        '
        'LabelControl9
        '
        Me.LabelControl9.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.LabelControl9.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.LabelControl9.Location = New System.Drawing.Point(618, 733)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(72, 16)
        Me.LabelControl9.TabIndex = 2
        Me.LabelControl9.Text = "F9 - Pending"
        '
        'picF6
        '
        Me.picF6.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.picF6.EditValue = CType(resources.GetObject("picF6.EditValue"), Object)
        Me.picF6.Location = New System.Drawing.Point(356, 647)
        Me.picF6.Name = "picF6"
        Me.picF6.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.picF6.Properties.Appearance.Options.UseBackColor = True
        Me.picF6.Properties.ShowMenu = False
        Me.picF6.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom
        Me.picF6.Size = New System.Drawing.Size(80, 80)
        Me.picF6.TabIndex = 1
        Me.picF6.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Hand
        '
        'picF7
        '
        Me.picF7.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.picF7.EditValue = CType(resources.GetObject("picF7.EditValue"), Object)
        Me.picF7.Location = New System.Drawing.Point(442, 647)
        Me.picF7.Name = "picF7"
        Me.picF7.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.picF7.Properties.Appearance.Options.UseBackColor = True
        Me.picF7.Properties.ShowMenu = False
        Me.picF7.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom
        Me.picF7.Size = New System.Drawing.Size(80, 80)
        Me.picF7.TabIndex = 1
        Me.picF7.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Hand
        '
        'picF8
        '
        Me.picF8.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.picF8.EditValue = CType(resources.GetObject("picF8.EditValue"), Object)
        Me.picF8.Location = New System.Drawing.Point(528, 647)
        Me.picF8.Name = "picF8"
        Me.picF8.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.picF8.Properties.Appearance.Options.UseBackColor = True
        Me.picF8.Properties.ShowMenu = False
        Me.picF8.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom
        Me.picF8.Size = New System.Drawing.Size(80, 80)
        Me.picF8.TabIndex = 1
        Me.picF8.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Hand
        '
        'LabelControl10
        '
        Me.LabelControl10.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.LabelControl10.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.LabelControl10.Location = New System.Drawing.Point(373, 733)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(46, 16)
        Me.LabelControl10.TabIndex = 4
        Me.LabelControl10.Text = "F6 - Qty"
        '
        'LabelControl11
        '
        Me.LabelControl11.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.LabelControl11.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.LabelControl11.Location = New System.Drawing.Point(535, 733)
        Me.LabelControl11.Name = "LabelControl11"
        Me.LabelControl11.Size = New System.Drawing.Size(60, 16)
        Me.LabelControl11.TabIndex = 2
        Me.LabelControl11.Text = "F8 - Serial"
        '
        'LabelControl12
        '
        Me.LabelControl12.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.LabelControl12.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.LabelControl12.Location = New System.Drawing.Point(455, 733)
        Me.LabelControl12.Name = "LabelControl12"
        Me.LabelControl12.Size = New System.Drawing.Size(55, 16)
        Me.LabelControl12.TabIndex = 4
        Me.LabelControl12.Text = "F7 - Price"
        '
        'LabelControl13
        '
        Me.LabelControl13.Appearance.Font = New System.Drawing.Font("Tahoma", 14.0!)
        Me.LabelControl13.Location = New System.Drawing.Point(46, 195)
        Me.LabelControl13.Name = "LabelControl13"
        Me.LabelControl13.Size = New System.Drawing.Size(150, 23)
        Me.LabelControl13.TabIndex = 7
        Me.LabelControl13.Text = "Alt + F2 - Email : "
        '
        'txtITEM
        '
        Me.txtITEM.EnterMoveNextControl = True
        Me.txtITEM.Location = New System.Drawing.Point(214, 228)
        Me.txtITEM.Name = "txtITEM"
        Me.txtITEM.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.0!)
        Me.txtITEM.Properties.Appearance.Options.UseFont = True
        Me.txtITEM.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.txtITEM.Size = New System.Drawing.Size(221, 30)
        Me.txtITEM.TabIndex = 0
        '
        'txtItemName
        '
        Me.txtItemName.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtItemName.Enabled = False
        Me.txtItemName.Location = New System.Drawing.Point(441, 228)
        Me.txtItemName.Name = "txtItemName"
        Me.txtItemName.Properties.Appearance.BackColor = System.Drawing.Color.LightGray
        Me.txtItemName.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.0!)
        Me.txtItemName.Properties.Appearance.Options.UseBackColor = True
        Me.txtItemName.Properties.Appearance.Options.UseFont = True
        Me.txtItemName.Properties.ReadOnly = True
        Me.txtItemName.Size = New System.Drawing.Size(571, 30)
        Me.txtItemName.TabIndex = 9
        Me.txtItemName.TabStop = False
        '
        'grdKDCUSTOMER
        '
        Me.grdKDCUSTOMER.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grdKDCUSTOMER.EnterMoveNextControl = True
        Me.grdKDCUSTOMER.Location = New System.Drawing.Point(214, 156)
        Me.grdKDCUSTOMER.Name = "grdKDCUSTOMER"
        Me.grdKDCUSTOMER.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.0!)
        Me.grdKDCUSTOMER.Properties.Appearance.Options.UseFont = True
        Me.grdKDCUSTOMER.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.grdKDCUSTOMER.Properties.NullText = ""
        Me.grdKDCUSTOMER.Properties.PopupFormMinSize = New System.Drawing.Size(600, 300)
        Me.grdKDCUSTOMER.Properties.PopupSizeable = False
        Me.grdKDCUSTOMER.Properties.View = Me.grvKDCUSTOMER
        Me.grdKDCUSTOMER.Size = New System.Drawing.Size(798, 30)
        Me.grdKDCUSTOMER.TabIndex = 2
        '
        'grvKDCUSTOMER
        '
        Me.grvKDCUSTOMER.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn3, Me.GridColumn4})
        Me.grvKDCUSTOMER.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.grvKDCUSTOMER.Name = "grvKDCUSTOMER"
        Me.grvKDCUSTOMER.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.grvKDCUSTOMER.OptionsView.ShowAutoFilterRow = True
        Me.grvKDCUSTOMER.OptionsView.ShowGroupPanel = False
        '
        'GridColumn3
        '
        Me.GridColumn3.Caption = "Display Name"
        Me.GridColumn3.FieldName = "NAME_DISPLAY"
        Me.GridColumn3.Name = "GridColumn3"
        Me.GridColumn3.Visible = True
        Me.GridColumn3.VisibleIndex = 0
        '
        'GridColumn4
        '
        Me.GridColumn4.Caption = "Address"
        Me.GridColumn4.FieldName = "BILL_STREET"
        Me.GridColumn4.Name = "GridColumn4"
        Me.GridColumn4.Visible = True
        Me.GridColumn4.VisibleIndex = 1
        '
        'LabelControl4
        '
        Me.LabelControl4.Appearance.Font = New System.Drawing.Font("Tahoma", 14.0!)
        Me.LabelControl4.Location = New System.Drawing.Point(52, 231)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(144, 23)
        Me.LabelControl4.TabIndex = 7
        Me.LabelControl4.Text = "Alt + F3 - Item : "
        '
        'txtEMAIL
        '
        Me.txtEMAIL.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtEMAIL.EnterMoveNextControl = True
        Me.txtEMAIL.Location = New System.Drawing.Point(214, 192)
        Me.txtEMAIL.Name = "txtEMAIL"
        Me.txtEMAIL.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.0!)
        Me.txtEMAIL.Properties.Appearance.Options.UseFont = True
        Me.txtEMAIL.Size = New System.Drawing.Size(798, 30)
        Me.txtEMAIL.TabIndex = 1
        '
        'LabelControl15
        '
        Me.LabelControl15.Appearance.Font = New System.Drawing.Font("Tahoma", 14.0!)
        Me.LabelControl15.Location = New System.Drawing.Point(91, 123)
        Me.LabelControl15.Name = "LabelControl15"
        Me.LabelControl15.Size = New System.Drawing.Size(105, 23)
        Me.LabelControl15.TabIndex = 14
        Me.LabelControl15.Text = " Salesman : "
        '
        'grdKDSALESMAN
        '
        Me.grdKDSALESMAN.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grdKDSALESMAN.EnterMoveNextControl = True
        Me.grdKDSALESMAN.Location = New System.Drawing.Point(214, 120)
        Me.grdKDSALESMAN.Name = "grdKDSALESMAN"
        Me.grdKDSALESMAN.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.0!)
        Me.grdKDSALESMAN.Properties.Appearance.Options.UseFont = True
        Me.grdKDSALESMAN.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.grdKDSALESMAN.Properties.NullText = ""
        Me.grdKDSALESMAN.Properties.PopupFormMinSize = New System.Drawing.Size(600, 300)
        Me.grdKDSALESMAN.Properties.PopupSizeable = False
        Me.grdKDSALESMAN.Properties.View = Me.grvKDSALESMAN
        Me.grdKDSALESMAN.Size = New System.Drawing.Size(798, 30)
        Me.grdKDSALESMAN.TabIndex = 3
        Me.grdKDSALESMAN.TabStop = False
        '
        'grvKDSALESMAN
        '
        Me.grvKDSALESMAN.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn5})
        Me.grvKDSALESMAN.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.grvKDSALESMAN.Name = "grvKDSALESMAN"
        Me.grvKDSALESMAN.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.grvKDSALESMAN.OptionsView.ShowAutoFilterRow = True
        Me.grvKDSALESMAN.OptionsView.ShowGroupPanel = False
        '
        'GridColumn5
        '
        Me.GridColumn5.Caption = "Display Name"
        Me.GridColumn5.FieldName = "NAME_DISPLAY"
        Me.GridColumn5.Name = "GridColumn5"
        Me.GridColumn5.Visible = True
        Me.GridColumn5.VisibleIndex = 0
        '
        'frmDirectSales
        '
        Me.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.Appearance.Options.UseBackColor = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1024, 768)
        Me.Controls.Add(Me.LabelControl15)
        Me.Controls.Add(Me.grdKDSALESMAN)
        Me.Controls.Add(Me.txtITEM)
        Me.Controls.Add(Me.PanelControl1)
        Me.Controls.Add(Me.txtItemName)
        Me.Controls.Add(Me.LabelControl4)
        Me.Controls.Add(Me.LabelControl13)
        Me.Controls.Add(Me.LabelControl5)
        Me.Controls.Add(Me.grd)
        Me.Controls.Add(Me.picF5)
        Me.Controls.Add(Me.picF3)
        Me.Controls.Add(Me.LabelControl12)
        Me.Controls.Add(Me.LabelControl10)
        Me.Controls.Add(Me.LabelControl7)
        Me.Controls.Add(Me.LabelControl6)
        Me.Controls.Add(Me.picF2)
        Me.Controls.Add(Me.LabelControl2)
        Me.Controls.Add(Me.LabelControl1)
        Me.Controls.Add(Me.picF1)
        Me.Controls.Add(Me.LabelControl11)
        Me.Controls.Add(Me.LabelControl9)
        Me.Controls.Add(Me.LabelControl8)
        Me.Controls.Add(Me.LabelControl3)
        Me.Controls.Add(Me.lblF12)
        Me.Controls.Add(Me.picF8)
        Me.Controls.Add(Me.picF7)
        Me.Controls.Add(Me.picF6)
        Me.Controls.Add(Me.picF9)
        Me.Controls.Add(Me.picF10)
        Me.Controls.Add(Me.picF11)
        Me.Controls.Add(Me.picF12)
        Me.Controls.Add(Me.txtGrandTotal)
        Me.Controls.Add(Me.grdKDCUSTOMER)
        Me.Controls.Add(Me.txtEMAIL)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.KeyPreview = True
        Me.Name = "frmDirectSales"
        Me.ShowIcon = False
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.txtGrandTotal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picF12.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picF1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picF2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picF11.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grd, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grv, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grdKDITEM, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvKDITEM, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grdKDUOM, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvKDUOM, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnSERIALNUMBER, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        Me.PanelControl1.PerformLayout()
        CType(Me.picF3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picF5.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picF10.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picF9.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picF6.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picF7.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picF8.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtITEM.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtItemName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grdKDCUSTOMER.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvKDCUSTOMER, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtEMAIL.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grdKDSALESMAN.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvKDSALESMAN, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtGrandTotal As DevExpress.XtraEditors.TextEdit
    Friend WithEvents picF12 As DevExpress.XtraEditors.PictureEdit
    Friend WithEvents lblF12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents picF1 As DevExpress.XtraEditors.PictureEdit
    Friend WithEvents picF2 As DevExpress.XtraEditors.PictureEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents picF11 As DevExpress.XtraEditors.PictureEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents grd As DevExpress.XtraGrid.GridControl
    Friend WithEvents grv As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents picF3 As DevExpress.XtraEditors.PictureEdit
    Friend WithEvents colKDITEM As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colKDUOM As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colQTY As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colPRICE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDISCOUNT As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colGRANDTOTAL As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents picF5 As DevExpress.XtraEditors.PictureEdit
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents picF10 As DevExpress.XtraEditors.PictureEdit
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents picF9 As DevExpress.XtraEditors.PictureEdit
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents picF6 As DevExpress.XtraEditors.PictureEdit
    Friend WithEvents picF7 As DevExpress.XtraEditors.PictureEdit
    Friend WithEvents picF8 As DevExpress.XtraEditors.PictureEdit
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl13 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtITEM As DevExpress.XtraEditors.ButtonEdit
    Friend WithEvents txtItemName As DevExpress.XtraEditors.TextEdit
    Friend WithEvents bindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents colSUBTOTAL As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents lblAddress As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblName As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl14 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblCashier As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblPhone As DevExpress.XtraEditors.LabelControl
    Friend WithEvents grdKDCUSTOMER As DevExpress.XtraEditors.GridLookUpEdit
    Friend WithEvents grvKDCUSTOMER As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents grdKDITEM As DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit
    Friend WithEvents grvKDITEM As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents NMITEM2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grdKDUOM As DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit
    Friend WithEvents grvKDUOM As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn3 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn4 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colREMARKS As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents btnSERIALNUMBER As DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtEMAIL As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl15 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents grdKDSALESMAN As DevExpress.XtraEditors.GridLookUpEdit
    Friend WithEvents grvKDSALESMAN As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn5 As DevExpress.XtraGrid.Columns.GridColumn
End Class
