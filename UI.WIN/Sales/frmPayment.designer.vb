<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPayment
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl()
        Me.txtNAME5 = New DevExpress.XtraEditors.TextEdit()
        Me.mnuManager = New DevExpress.XtraBars.BarManager(Me.components)
        Me.Bar3 = New DevExpress.XtraBars.Bar()
        Me.btnSave = New DevExpress.XtraBars.BarButtonItem()
        Me.btnClose = New DevExpress.XtraBars.BarButtonItem()
        Me.barDockControlTop = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlBottom = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlLeft = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlRight = New DevExpress.XtraBars.BarDockControl()
        Me.BarStaticItem1 = New DevExpress.XtraBars.BarStaticItem()
        Me.lblNoTransaction = New DevExpress.XtraBars.BarStaticItem()
        Me.txtNAME4 = New DevExpress.XtraEditors.TextEdit()
        Me.txtNAME3 = New DevExpress.XtraEditors.TextEdit()
        Me.txtNAME2 = New DevExpress.XtraEditors.TextEdit()
        Me.txtNAME1 = New DevExpress.XtraEditors.TextEdit()
        Me.LayoutControl2 = New DevExpress.XtraLayout.LayoutControl()
        Me.txtBalance = New DevExpress.XtraEditors.TextEdit()
        Me.txtKDPAYMENTTYPE = New DevExpress.XtraEditors.TextEdit()
        Me.txtTotal = New DevExpress.XtraEditors.TextEdit()
        Me.LayoutControlGroup2 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.lGRANDTOTAL = New DevExpress.XtraLayout.LayoutControlItem()
        Me.lCHANGE = New DevExpress.XtraLayout.LayoutControlItem()
        Me.lPAY = New DevExpress.XtraLayout.LayoutControlItem()
        Me.txtMEMO5 = New DevExpress.XtraEditors.TextEdit()
        Me.txtMEMO4 = New DevExpress.XtraEditors.TextEdit()
        Me.txtMEMO3 = New DevExpress.XtraEditors.TextEdit()
        Me.txtMEMO2 = New DevExpress.XtraEditors.TextEdit()
        Me.txtGRANDTOTAL5 = New DevExpress.XtraEditors.TextEdit()
        Me.txtGRANDTOTAL4 = New DevExpress.XtraEditors.TextEdit()
        Me.txtGRANDTOTAL3 = New DevExpress.XtraEditors.TextEdit()
        Me.txtGRANDTOTAL2 = New DevExpress.XtraEditors.TextEdit()
        Me.txtKDPAYMENTTYPE5 = New DevExpress.XtraEditors.ButtonEdit()
        Me.txtKDPAYMENTTYPE4 = New DevExpress.XtraEditors.ButtonEdit()
        Me.txtKDPAYMENTTYPE3 = New DevExpress.XtraEditors.ButtonEdit()
        Me.txtKDPAYMENTTYPE2 = New DevExpress.XtraEditors.ButtonEdit()
        Me.txtMEMO1 = New DevExpress.XtraEditors.TextEdit()
        Me.txtGRANDTOTAL1 = New DevExpress.XtraEditors.TextEdit()
        Me.txtKDPAYMENTTYPE1 = New DevExpress.XtraEditors.ButtonEdit()
        Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.lPayment1 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.lGRANDTOTAL1 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.lMEMO1 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.lPayment2 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.lPayment3 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.lPayment4 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.lPayment5 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.lGRANDTOTAL2 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.lGRANDTOTAL3 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.lGRANDTOTAL4 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.lGRANDTOTAL5 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.lMEMO2 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.lMEMO3 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.lMEMO4 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.lMEMO5 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem2 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem3 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem4 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem5 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem6 = New DevExpress.XtraLayout.LayoutControlItem()
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl1.SuspendLayout()
        CType(Me.txtNAME5.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.mnuManager, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNAME4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNAME3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNAME2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNAME1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl2.SuspendLayout()
        CType(Me.txtBalance.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtKDPAYMENTTYPE.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTotal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lGRANDTOTAL, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lCHANGE, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lPAY, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtMEMO5.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtMEMO4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtMEMO3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtMEMO2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtGRANDTOTAL5.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtGRANDTOTAL4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtGRANDTOTAL3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtGRANDTOTAL2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtKDPAYMENTTYPE5.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtKDPAYMENTTYPE4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtKDPAYMENTTYPE3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtKDPAYMENTTYPE2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtMEMO1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtGRANDTOTAL1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtKDPAYMENTTYPE1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lPayment1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lGRANDTOTAL1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lMEMO1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lPayment2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lPayment3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lPayment4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lPayment5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lGRANDTOTAL2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lGRANDTOTAL3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lGRANDTOTAL4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lGRANDTOTAL5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lMEMO2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lMEMO3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lMEMO4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lMEMO5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'LayoutControl1
        '
        Me.LayoutControl1.Controls.Add(Me.txtNAME5)
        Me.LayoutControl1.Controls.Add(Me.txtNAME4)
        Me.LayoutControl1.Controls.Add(Me.txtNAME3)
        Me.LayoutControl1.Controls.Add(Me.txtNAME2)
        Me.LayoutControl1.Controls.Add(Me.txtNAME1)
        Me.LayoutControl1.Controls.Add(Me.LayoutControl2)
        Me.LayoutControl1.Controls.Add(Me.txtMEMO5)
        Me.LayoutControl1.Controls.Add(Me.txtMEMO4)
        Me.LayoutControl1.Controls.Add(Me.txtMEMO3)
        Me.LayoutControl1.Controls.Add(Me.txtMEMO2)
        Me.LayoutControl1.Controls.Add(Me.txtGRANDTOTAL5)
        Me.LayoutControl1.Controls.Add(Me.txtGRANDTOTAL4)
        Me.LayoutControl1.Controls.Add(Me.txtGRANDTOTAL3)
        Me.LayoutControl1.Controls.Add(Me.txtGRANDTOTAL2)
        Me.LayoutControl1.Controls.Add(Me.txtKDPAYMENTTYPE5)
        Me.LayoutControl1.Controls.Add(Me.txtKDPAYMENTTYPE4)
        Me.LayoutControl1.Controls.Add(Me.txtKDPAYMENTTYPE3)
        Me.LayoutControl1.Controls.Add(Me.txtKDPAYMENTTYPE2)
        Me.LayoutControl1.Controls.Add(Me.txtMEMO1)
        Me.LayoutControl1.Controls.Add(Me.txtGRANDTOTAL1)
        Me.LayoutControl1.Controls.Add(Me.txtKDPAYMENTTYPE1)
        Me.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControl1.Name = "LayoutControl1"
        Me.LayoutControl1.Root = Me.LayoutControlGroup1
        Me.LayoutControl1.Size = New System.Drawing.Size(907, 365)
        Me.LayoutControl1.TabIndex = 0
        Me.LayoutControl1.Text = "LayoutControl1"
        '
        'txtNAME5
        '
        Me.txtNAME5.Location = New System.Drawing.Point(190, 327)
        Me.txtNAME5.MenuManager = Me.mnuManager
        Me.txtNAME5.Name = "txtNAME5"
        Me.txtNAME5.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.txtNAME5.Properties.Appearance.Options.UseFont = True
        Me.txtNAME5.Properties.ReadOnly = True
        Me.txtNAME5.Size = New System.Drawing.Size(202, 26)
        Me.txtNAME5.StyleController = Me.LayoutControl1
        Me.txtNAME5.TabIndex = 20
        Me.txtNAME5.TabStop = False
        '
        'mnuManager
        '
        Me.mnuManager.Bars.AddRange(New DevExpress.XtraBars.Bar() {Me.Bar3})
        Me.mnuManager.DockControls.Add(Me.barDockControlTop)
        Me.mnuManager.DockControls.Add(Me.barDockControlBottom)
        Me.mnuManager.DockControls.Add(Me.barDockControlLeft)
        Me.mnuManager.DockControls.Add(Me.barDockControlRight)
        Me.mnuManager.Form = Me
        Me.mnuManager.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.BarStaticItem1, Me.lblNoTransaction, Me.btnSave, Me.btnClose})
        Me.mnuManager.MaxItemId = 7
        Me.mnuManager.StatusBar = Me.Bar3
        '
        'Bar3
        '
        Me.Bar3.BarName = "Status bar"
        Me.Bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom
        Me.Bar3.DockCol = 0
        Me.Bar3.DockRow = 0
        Me.Bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom
        Me.Bar3.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.btnSave, True), New DevExpress.XtraBars.LinkPersistInfo(Me.btnClose, True)})
        Me.Bar3.OptionsBar.AllowQuickCustomization = False
        Me.Bar3.OptionsBar.DrawDragBorder = False
        Me.Bar3.OptionsBar.UseWholeRow = True
        Me.Bar3.Text = "Status bar"
        '
        'btnSave
        '
        Me.btnSave.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right
        Me.btnSave.Caption = "F3 - Save && Close"
        Me.btnSave.Id = 4
        Me.btnSave.ItemShortcut = New DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F3)
        Me.btnSave.Name = "btnSave"
        '
        'btnClose
        '
        Me.btnClose.Caption = "F12 - Close"
        Me.btnClose.Id = 5
        Me.btnClose.ItemShortcut = New DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F12)
        Me.btnClose.Name = "btnClose"
        '
        'barDockControlTop
        '
        Me.barDockControlTop.CausesValidation = False
        Me.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top
        Me.barDockControlTop.Location = New System.Drawing.Point(0, 0)
        Me.barDockControlTop.Size = New System.Drawing.Size(907, 0)
        '
        'barDockControlBottom
        '
        Me.barDockControlBottom.CausesValidation = False
        Me.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.barDockControlBottom.Location = New System.Drawing.Point(0, 365)
        Me.barDockControlBottom.Size = New System.Drawing.Size(907, 23)
        '
        'barDockControlLeft
        '
        Me.barDockControlLeft.CausesValidation = False
        Me.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left
        Me.barDockControlLeft.Location = New System.Drawing.Point(0, 0)
        Me.barDockControlLeft.Size = New System.Drawing.Size(0, 365)
        '
        'barDockControlRight
        '
        Me.barDockControlRight.CausesValidation = False
        Me.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right
        Me.barDockControlRight.Location = New System.Drawing.Point(907, 0)
        Me.barDockControlRight.Size = New System.Drawing.Size(0, 365)
        '
        'BarStaticItem1
        '
        Me.BarStaticItem1.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Left
        Me.BarStaticItem1.Border = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.BarStaticItem1.Caption = "No Transaction :"
        Me.BarStaticItem1.Id = 1
        Me.BarStaticItem1.Name = "BarStaticItem1"
        Me.BarStaticItem1.TextAlignment = System.Drawing.StringAlignment.Near
        '
        'lblNoTransaction
        '
        Me.lblNoTransaction.Border = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.lblNoTransaction.Caption = "-"
        Me.lblNoTransaction.Id = 3
        Me.lblNoTransaction.Name = "lblNoTransaction"
        Me.lblNoTransaction.TextAlignment = System.Drawing.StringAlignment.Near
        '
        'txtNAME4
        '
        Me.txtNAME4.Location = New System.Drawing.Point(190, 297)
        Me.txtNAME4.MenuManager = Me.mnuManager
        Me.txtNAME4.Name = "txtNAME4"
        Me.txtNAME4.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.txtNAME4.Properties.Appearance.Options.UseFont = True
        Me.txtNAME4.Properties.ReadOnly = True
        Me.txtNAME4.Size = New System.Drawing.Size(202, 26)
        Me.txtNAME4.StyleController = Me.LayoutControl1
        Me.txtNAME4.TabIndex = 19
        Me.txtNAME4.TabStop = False
        '
        'txtNAME3
        '
        Me.txtNAME3.Location = New System.Drawing.Point(190, 267)
        Me.txtNAME3.MenuManager = Me.mnuManager
        Me.txtNAME3.Name = "txtNAME3"
        Me.txtNAME3.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.txtNAME3.Properties.Appearance.Options.UseFont = True
        Me.txtNAME3.Properties.ReadOnly = True
        Me.txtNAME3.Size = New System.Drawing.Size(202, 26)
        Me.txtNAME3.StyleController = Me.LayoutControl1
        Me.txtNAME3.TabIndex = 18
        Me.txtNAME3.TabStop = False
        '
        'txtNAME2
        '
        Me.txtNAME2.Location = New System.Drawing.Point(190, 237)
        Me.txtNAME2.MenuManager = Me.mnuManager
        Me.txtNAME2.Name = "txtNAME2"
        Me.txtNAME2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.txtNAME2.Properties.Appearance.Options.UseFont = True
        Me.txtNAME2.Properties.ReadOnly = True
        Me.txtNAME2.Size = New System.Drawing.Size(202, 26)
        Me.txtNAME2.StyleController = Me.LayoutControl1
        Me.txtNAME2.TabIndex = 17
        Me.txtNAME2.TabStop = False
        '
        'txtNAME1
        '
        Me.txtNAME1.EditValue = ""
        Me.txtNAME1.Location = New System.Drawing.Point(190, 207)
        Me.txtNAME1.MenuManager = Me.mnuManager
        Me.txtNAME1.Name = "txtNAME1"
        Me.txtNAME1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.txtNAME1.Properties.Appearance.Options.UseFont = True
        Me.txtNAME1.Properties.ReadOnly = True
        Me.txtNAME1.Size = New System.Drawing.Size(202, 26)
        Me.txtNAME1.StyleController = Me.LayoutControl1
        Me.txtNAME1.TabIndex = 16
        Me.txtNAME1.TabStop = False
        '
        'LayoutControl2
        '
        Me.LayoutControl2.Controls.Add(Me.txtBalance)
        Me.LayoutControl2.Controls.Add(Me.txtKDPAYMENTTYPE)
        Me.LayoutControl2.Controls.Add(Me.txtTotal)
        Me.LayoutControl2.Location = New System.Drawing.Point(12, 12)
        Me.LayoutControl2.Name = "LayoutControl2"
        Me.LayoutControl2.Root = Me.LayoutControlGroup2
        Me.LayoutControl2.Size = New System.Drawing.Size(883, 191)
        Me.LayoutControl2.TabIndex = 15
        Me.LayoutControl2.TabStop = False
        Me.LayoutControl2.Text = "LayoutControl2"
        '
        'txtBalance
        '
        Me.txtBalance.EditValue = "0.00"
        Me.txtBalance.Location = New System.Drawing.Point(564, 120)
        Me.txtBalance.Name = "txtBalance"
        Me.txtBalance.Properties.Appearance.BackColor = System.Drawing.Color.Black
        Me.txtBalance.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 32.0!, System.Drawing.FontStyle.Bold)
        Me.txtBalance.Properties.Appearance.ForeColor = System.Drawing.Color.Red
        Me.txtBalance.Properties.Appearance.Options.UseBackColor = True
        Me.txtBalance.Properties.Appearance.Options.UseFont = True
        Me.txtBalance.Properties.Appearance.Options.UseForeColor = True
        Me.txtBalance.Properties.Appearance.Options.UseTextOptions = True
        Me.txtBalance.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtBalance.Properties.Mask.EditMask = "n2"
        Me.txtBalance.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtBalance.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtBalance.Properties.NullText = "0"
        Me.txtBalance.Properties.ReadOnly = True
        Me.txtBalance.Size = New System.Drawing.Size(307, 58)
        Me.txtBalance.StyleController = Me.LayoutControl2
        Me.txtBalance.TabIndex = 6
        Me.txtBalance.TabStop = False
        '
        'txtKDPAYMENTTYPE
        '
        Me.txtKDPAYMENTTYPE.EditValue = "0.00"
        Me.txtKDPAYMENTTYPE.Location = New System.Drawing.Point(117, 120)
        Me.txtKDPAYMENTTYPE.Name = "txtKDPAYMENTTYPE"
        Me.txtKDPAYMENTTYPE.Properties.Appearance.BackColor = System.Drawing.Color.Black
        Me.txtKDPAYMENTTYPE.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 32.0!, System.Drawing.FontStyle.Bold)
        Me.txtKDPAYMENTTYPE.Properties.Appearance.ForeColor = System.Drawing.Color.Green
        Me.txtKDPAYMENTTYPE.Properties.Appearance.Options.UseBackColor = True
        Me.txtKDPAYMENTTYPE.Properties.Appearance.Options.UseFont = True
        Me.txtKDPAYMENTTYPE.Properties.Appearance.Options.UseForeColor = True
        Me.txtKDPAYMENTTYPE.Properties.Appearance.Options.UseTextOptions = True
        Me.txtKDPAYMENTTYPE.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtKDPAYMENTTYPE.Properties.Mask.EditMask = "n2"
        Me.txtKDPAYMENTTYPE.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtKDPAYMENTTYPE.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtKDPAYMENTTYPE.Properties.NullText = "0"
        Me.txtKDPAYMENTTYPE.Properties.ReadOnly = True
        Me.txtKDPAYMENTTYPE.Size = New System.Drawing.Size(323, 58)
        Me.txtKDPAYMENTTYPE.StyleController = Me.LayoutControl2
        Me.txtKDPAYMENTTYPE.TabIndex = 5
        Me.txtKDPAYMENTTYPE.TabStop = False
        '
        'txtTotal
        '
        Me.txtTotal.EditValue = "0.00"
        Me.txtTotal.Location = New System.Drawing.Point(117, 12)
        Me.txtTotal.Name = "txtTotal"
        Me.txtTotal.Properties.Appearance.BackColor = System.Drawing.Color.Black
        Me.txtTotal.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 60.0!)
        Me.txtTotal.Properties.Appearance.ForeColor = System.Drawing.Color.Green
        Me.txtTotal.Properties.Appearance.Options.UseBackColor = True
        Me.txtTotal.Properties.Appearance.Options.UseFont = True
        Me.txtTotal.Properties.Appearance.Options.UseForeColor = True
        Me.txtTotal.Properties.Appearance.Options.UseTextOptions = True
        Me.txtTotal.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtTotal.Properties.Mask.EditMask = "n2"
        Me.txtTotal.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtTotal.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtTotal.Properties.NullText = "0"
        Me.txtTotal.Properties.ReadOnly = True
        Me.txtTotal.Size = New System.Drawing.Size(754, 104)
        Me.txtTotal.StyleController = Me.LayoutControl2
        Me.txtTotal.TabIndex = 4
        Me.txtTotal.TabStop = False
        '
        'LayoutControlGroup2
        '
        Me.LayoutControlGroup2.CustomizationFormText = "LayoutControlGroup2"
        Me.LayoutControlGroup2.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.lGRANDTOTAL, Me.lCHANGE, Me.lPAY})
        Me.LayoutControlGroup2.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup2.Name = "LayoutControlGroup2"
        Me.LayoutControlGroup2.Size = New System.Drawing.Size(883, 191)
        Me.LayoutControlGroup2.Text = "LayoutControlGroup2"
        Me.LayoutControlGroup2.TextVisible = False
        '
        'lGRANDTOTAL
        '
        Me.lGRANDTOTAL.AppearanceItemCaption.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lGRANDTOTAL.AppearanceItemCaption.Options.UseFont = True
        Me.lGRANDTOTAL.AppearanceItemCaption.Options.UseTextOptions = True
        Me.lGRANDTOTAL.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.lGRANDTOTAL.Control = Me.txtTotal
        Me.lGRANDTOTAL.CustomizationFormText = "Total : "
        Me.lGRANDTOTAL.Location = New System.Drawing.Point(0, 0)
        Me.lGRANDTOTAL.Name = "lGRANDTOTAL"
        Me.lGRANDTOTAL.Size = New System.Drawing.Size(863, 108)
        Me.lGRANDTOTAL.Text = "Total : "
        Me.lGRANDTOTAL.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.lGRANDTOTAL.TextSize = New System.Drawing.Size(100, 20)
        Me.lGRANDTOTAL.TextToControlDistance = 5
        '
        'lCHANGE
        '
        Me.lCHANGE.AppearanceItemCaption.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lCHANGE.AppearanceItemCaption.Options.UseFont = True
        Me.lCHANGE.AppearanceItemCaption.Options.UseTextOptions = True
        Me.lCHANGE.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.lCHANGE.Control = Me.txtBalance
        Me.lCHANGE.CustomizationFormText = "Balance : "
        Me.lCHANGE.Location = New System.Drawing.Point(432, 108)
        Me.lCHANGE.Name = "lCHANGE"
        Me.lCHANGE.Size = New System.Drawing.Size(431, 63)
        Me.lCHANGE.Text = "Change : "
        Me.lCHANGE.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.lCHANGE.TextSize = New System.Drawing.Size(115, 20)
        Me.lCHANGE.TextToControlDistance = 5
        '
        'lPAY
        '
        Me.lPAY.AppearanceItemCaption.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lPAY.AppearanceItemCaption.Options.UseFont = True
        Me.lPAY.AppearanceItemCaption.Options.UseTextOptions = True
        Me.lPAY.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.lPAY.Control = Me.txtKDPAYMENTTYPE
        Me.lPAY.CustomizationFormText = "Payments : "
        Me.lPAY.Location = New System.Drawing.Point(0, 108)
        Me.lPAY.Name = "lPAY"
        Me.lPAY.Size = New System.Drawing.Size(432, 63)
        Me.lPAY.Text = "Payment : "
        Me.lPAY.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.lPAY.TextSize = New System.Drawing.Size(100, 20)
        Me.lPAY.TextToControlDistance = 5
        '
        'txtMEMO5
        '
        Me.txtMEMO5.EnterMoveNextControl = True
        Me.txtMEMO5.Location = New System.Drawing.Point(545, 327)
        Me.txtMEMO5.Name = "txtMEMO5"
        Me.txtMEMO5.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.txtMEMO5.Properties.Appearance.Options.UseFont = True
        Me.txtMEMO5.Properties.ReadOnly = True
        Me.txtMEMO5.Size = New System.Drawing.Size(350, 26)
        Me.txtMEMO5.StyleController = Me.LayoutControl1
        Me.txtMEMO5.TabIndex = 14
        '
        'txtMEMO4
        '
        Me.txtMEMO4.EnterMoveNextControl = True
        Me.txtMEMO4.Location = New System.Drawing.Point(545, 297)
        Me.txtMEMO4.Name = "txtMEMO4"
        Me.txtMEMO4.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.txtMEMO4.Properties.Appearance.Options.UseFont = True
        Me.txtMEMO4.Properties.ReadOnly = True
        Me.txtMEMO4.Size = New System.Drawing.Size(350, 26)
        Me.txtMEMO4.StyleController = Me.LayoutControl1
        Me.txtMEMO4.TabIndex = 11
        '
        'txtMEMO3
        '
        Me.txtMEMO3.EnterMoveNextControl = True
        Me.txtMEMO3.Location = New System.Drawing.Point(545, 267)
        Me.txtMEMO3.Name = "txtMEMO3"
        Me.txtMEMO3.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.txtMEMO3.Properties.Appearance.Options.UseFont = True
        Me.txtMEMO3.Properties.ReadOnly = True
        Me.txtMEMO3.Size = New System.Drawing.Size(350, 26)
        Me.txtMEMO3.StyleController = Me.LayoutControl1
        Me.txtMEMO3.TabIndex = 8
        '
        'txtMEMO2
        '
        Me.txtMEMO2.EnterMoveNextControl = True
        Me.txtMEMO2.Location = New System.Drawing.Point(545, 237)
        Me.txtMEMO2.Name = "txtMEMO2"
        Me.txtMEMO2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.txtMEMO2.Properties.Appearance.Options.UseFont = True
        Me.txtMEMO2.Properties.ReadOnly = True
        Me.txtMEMO2.Size = New System.Drawing.Size(350, 26)
        Me.txtMEMO2.StyleController = Me.LayoutControl1
        Me.txtMEMO2.TabIndex = 5
        '
        'txtGRANDTOTAL5
        '
        Me.txtGRANDTOTAL5.EditValue = "0.00"
        Me.txtGRANDTOTAL5.EnterMoveNextControl = True
        Me.txtGRANDTOTAL5.Location = New System.Drawing.Point(396, 327)
        Me.txtGRANDTOTAL5.Name = "txtGRANDTOTAL5"
        Me.txtGRANDTOTAL5.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.txtGRANDTOTAL5.Properties.Appearance.Options.UseFont = True
        Me.txtGRANDTOTAL5.Properties.Appearance.Options.UseTextOptions = True
        Me.txtGRANDTOTAL5.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtGRANDTOTAL5.Properties.Mask.EditMask = "n2"
        Me.txtGRANDTOTAL5.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtGRANDTOTAL5.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtGRANDTOTAL5.Properties.NullText = "0.00"
        Me.txtGRANDTOTAL5.Properties.ReadOnly = True
        Me.txtGRANDTOTAL5.Size = New System.Drawing.Size(145, 26)
        Me.txtGRANDTOTAL5.StyleController = Me.LayoutControl1
        Me.txtGRANDTOTAL5.TabIndex = 13
        '
        'txtGRANDTOTAL4
        '
        Me.txtGRANDTOTAL4.EditValue = "0.00"
        Me.txtGRANDTOTAL4.EnterMoveNextControl = True
        Me.txtGRANDTOTAL4.Location = New System.Drawing.Point(396, 297)
        Me.txtGRANDTOTAL4.Name = "txtGRANDTOTAL4"
        Me.txtGRANDTOTAL4.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.txtGRANDTOTAL4.Properties.Appearance.Options.UseFont = True
        Me.txtGRANDTOTAL4.Properties.Appearance.Options.UseTextOptions = True
        Me.txtGRANDTOTAL4.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtGRANDTOTAL4.Properties.Mask.EditMask = "n2"
        Me.txtGRANDTOTAL4.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtGRANDTOTAL4.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtGRANDTOTAL4.Properties.NullText = "0.00"
        Me.txtGRANDTOTAL4.Properties.ReadOnly = True
        Me.txtGRANDTOTAL4.Size = New System.Drawing.Size(145, 26)
        Me.txtGRANDTOTAL4.StyleController = Me.LayoutControl1
        Me.txtGRANDTOTAL4.TabIndex = 10
        '
        'txtGRANDTOTAL3
        '
        Me.txtGRANDTOTAL3.EditValue = "0.00"
        Me.txtGRANDTOTAL3.EnterMoveNextControl = True
        Me.txtGRANDTOTAL3.Location = New System.Drawing.Point(396, 267)
        Me.txtGRANDTOTAL3.Name = "txtGRANDTOTAL3"
        Me.txtGRANDTOTAL3.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.txtGRANDTOTAL3.Properties.Appearance.Options.UseFont = True
        Me.txtGRANDTOTAL3.Properties.Appearance.Options.UseTextOptions = True
        Me.txtGRANDTOTAL3.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtGRANDTOTAL3.Properties.Mask.EditMask = "n2"
        Me.txtGRANDTOTAL3.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtGRANDTOTAL3.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtGRANDTOTAL3.Properties.NullText = "0.00"
        Me.txtGRANDTOTAL3.Properties.ReadOnly = True
        Me.txtGRANDTOTAL3.Size = New System.Drawing.Size(145, 26)
        Me.txtGRANDTOTAL3.StyleController = Me.LayoutControl1
        Me.txtGRANDTOTAL3.TabIndex = 7
        '
        'txtGRANDTOTAL2
        '
        Me.txtGRANDTOTAL2.EditValue = "0.00"
        Me.txtGRANDTOTAL2.EnterMoveNextControl = True
        Me.txtGRANDTOTAL2.Location = New System.Drawing.Point(396, 237)
        Me.txtGRANDTOTAL2.Name = "txtGRANDTOTAL2"
        Me.txtGRANDTOTAL2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.txtGRANDTOTAL2.Properties.Appearance.Options.UseFont = True
        Me.txtGRANDTOTAL2.Properties.Appearance.Options.UseTextOptions = True
        Me.txtGRANDTOTAL2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtGRANDTOTAL2.Properties.Mask.EditMask = "n2"
        Me.txtGRANDTOTAL2.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtGRANDTOTAL2.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtGRANDTOTAL2.Properties.NullText = "0.00"
        Me.txtGRANDTOTAL2.Properties.ReadOnly = True
        Me.txtGRANDTOTAL2.Size = New System.Drawing.Size(145, 26)
        Me.txtGRANDTOTAL2.StyleController = Me.LayoutControl1
        Me.txtGRANDTOTAL2.TabIndex = 4
        '
        'txtKDPAYMENTTYPE5
        '
        Me.txtKDPAYMENTTYPE5.EnterMoveNextControl = True
        Me.txtKDPAYMENTTYPE5.Location = New System.Drawing.Point(67, 327)
        Me.txtKDPAYMENTTYPE5.Name = "txtKDPAYMENTTYPE5"
        Me.txtKDPAYMENTTYPE5.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.txtKDPAYMENTTYPE5.Properties.Appearance.Options.UseFont = True
        Me.txtKDPAYMENTTYPE5.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.txtKDPAYMENTTYPE5.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor
        Me.txtKDPAYMENTTYPE5.Size = New System.Drawing.Size(119, 18)
        Me.txtKDPAYMENTTYPE5.StyleController = Me.LayoutControl1
        Me.txtKDPAYMENTTYPE5.TabIndex = 12
        Me.txtKDPAYMENTTYPE5.TabStop = False
        '
        'txtKDPAYMENTTYPE4
        '
        Me.txtKDPAYMENTTYPE4.EnterMoveNextControl = True
        Me.txtKDPAYMENTTYPE4.Location = New System.Drawing.Point(67, 297)
        Me.txtKDPAYMENTTYPE4.Name = "txtKDPAYMENTTYPE4"
        Me.txtKDPAYMENTTYPE4.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.txtKDPAYMENTTYPE4.Properties.Appearance.Options.UseFont = True
        Me.txtKDPAYMENTTYPE4.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.txtKDPAYMENTTYPE4.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor
        Me.txtKDPAYMENTTYPE4.Size = New System.Drawing.Size(119, 18)
        Me.txtKDPAYMENTTYPE4.StyleController = Me.LayoutControl1
        Me.txtKDPAYMENTTYPE4.TabIndex = 9
        Me.txtKDPAYMENTTYPE4.TabStop = False
        '
        'txtKDPAYMENTTYPE3
        '
        Me.txtKDPAYMENTTYPE3.EnterMoveNextControl = True
        Me.txtKDPAYMENTTYPE3.Location = New System.Drawing.Point(67, 267)
        Me.txtKDPAYMENTTYPE3.Name = "txtKDPAYMENTTYPE3"
        Me.txtKDPAYMENTTYPE3.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.txtKDPAYMENTTYPE3.Properties.Appearance.Options.UseFont = True
        Me.txtKDPAYMENTTYPE3.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.txtKDPAYMENTTYPE3.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor
        Me.txtKDPAYMENTTYPE3.Size = New System.Drawing.Size(119, 18)
        Me.txtKDPAYMENTTYPE3.StyleController = Me.LayoutControl1
        Me.txtKDPAYMENTTYPE3.TabIndex = 6
        Me.txtKDPAYMENTTYPE3.TabStop = False
        '
        'txtKDPAYMENTTYPE2
        '
        Me.txtKDPAYMENTTYPE2.EnterMoveNextControl = True
        Me.txtKDPAYMENTTYPE2.Location = New System.Drawing.Point(67, 237)
        Me.txtKDPAYMENTTYPE2.Name = "txtKDPAYMENTTYPE2"
        Me.txtKDPAYMENTTYPE2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.txtKDPAYMENTTYPE2.Properties.Appearance.Options.UseFont = True
        Me.txtKDPAYMENTTYPE2.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.txtKDPAYMENTTYPE2.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor
        Me.txtKDPAYMENTTYPE2.Size = New System.Drawing.Size(119, 18)
        Me.txtKDPAYMENTTYPE2.StyleController = Me.LayoutControl1
        Me.txtKDPAYMENTTYPE2.TabIndex = 3
        Me.txtKDPAYMENTTYPE2.TabStop = False
        '
        'txtMEMO1
        '
        Me.txtMEMO1.EnterMoveNextControl = True
        Me.txtMEMO1.Location = New System.Drawing.Point(545, 207)
        Me.txtMEMO1.Name = "txtMEMO1"
        Me.txtMEMO1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.txtMEMO1.Properties.Appearance.Options.UseFont = True
        Me.txtMEMO1.Properties.ReadOnly = True
        Me.txtMEMO1.Size = New System.Drawing.Size(350, 26)
        Me.txtMEMO1.StyleController = Me.LayoutControl1
        Me.txtMEMO1.TabIndex = 2
        '
        'txtGRANDTOTAL1
        '
        Me.txtGRANDTOTAL1.EditValue = "0.00"
        Me.txtGRANDTOTAL1.EnterMoveNextControl = True
        Me.txtGRANDTOTAL1.Location = New System.Drawing.Point(396, 207)
        Me.txtGRANDTOTAL1.Name = "txtGRANDTOTAL1"
        Me.txtGRANDTOTAL1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.txtGRANDTOTAL1.Properties.Appearance.Options.UseFont = True
        Me.txtGRANDTOTAL1.Properties.Appearance.Options.UseTextOptions = True
        Me.txtGRANDTOTAL1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtGRANDTOTAL1.Properties.Mask.EditMask = "n2"
        Me.txtGRANDTOTAL1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtGRANDTOTAL1.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtGRANDTOTAL1.Properties.NullText = "0.00"
        Me.txtGRANDTOTAL1.Properties.ReadOnly = True
        Me.txtGRANDTOTAL1.Size = New System.Drawing.Size(145, 26)
        Me.txtGRANDTOTAL1.StyleController = Me.LayoutControl1
        Me.txtGRANDTOTAL1.TabIndex = 1
        '
        'txtKDPAYMENTTYPE1
        '
        Me.txtKDPAYMENTTYPE1.EditValue = ""
        Me.txtKDPAYMENTTYPE1.EnterMoveNextControl = True
        Me.txtKDPAYMENTTYPE1.Location = New System.Drawing.Point(67, 207)
        Me.txtKDPAYMENTTYPE1.Name = "txtKDPAYMENTTYPE1"
        Me.txtKDPAYMENTTYPE1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.txtKDPAYMENTTYPE1.Properties.Appearance.Options.UseFont = True
        Me.txtKDPAYMENTTYPE1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.txtKDPAYMENTTYPE1.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor
        Me.txtKDPAYMENTTYPE1.Size = New System.Drawing.Size(119, 18)
        Me.txtKDPAYMENTTYPE1.StyleController = Me.LayoutControl1
        Me.txtKDPAYMENTTYPE1.TabIndex = 0
        Me.txtKDPAYMENTTYPE1.TabStop = False
        '
        'LayoutControlGroup1
        '
        Me.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1"
        Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.lPayment1, Me.lGRANDTOTAL1, Me.lMEMO1, Me.lPayment2, Me.lPayment3, Me.lPayment4, Me.lPayment5, Me.lGRANDTOTAL2, Me.lGRANDTOTAL3, Me.lGRANDTOTAL4, Me.lGRANDTOTAL5, Me.lMEMO2, Me.lMEMO3, Me.lMEMO4, Me.lMEMO5, Me.LayoutControlItem1, Me.LayoutControlItem2, Me.LayoutControlItem3, Me.LayoutControlItem4, Me.LayoutControlItem5, Me.LayoutControlItem6})
        Me.LayoutControlGroup1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup1.Name = "LayoutControlGroup1"
        Me.LayoutControlGroup1.Size = New System.Drawing.Size(907, 365)
        Me.LayoutControlGroup1.Text = "LayoutControlGroup1"
        Me.LayoutControlGroup1.TextVisible = False
        '
        'lPayment1
        '
        Me.lPayment1.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.lPayment1.AppearanceItemCaption.Options.UseFont = True
        Me.lPayment1.AppearanceItemCaption.Options.UseTextOptions = True
        Me.lPayment1.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.lPayment1.Control = Me.txtKDPAYMENTTYPE1
        Me.lPayment1.CustomizationFormText = "F1"
        Me.lPayment1.Location = New System.Drawing.Point(0, 195)
        Me.lPayment1.Name = "lPayment1"
        Me.lPayment1.Size = New System.Drawing.Size(178, 30)
        Me.lPayment1.Text = "Ctrl + F1"
        Me.lPayment1.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.lPayment1.TextSize = New System.Drawing.Size(50, 0)
        Me.lPayment1.TextToControlDistance = 5
        '
        'lGRANDTOTAL1
        '
        Me.lGRANDTOTAL1.Control = Me.txtGRANDTOTAL1
        Me.lGRANDTOTAL1.CustomizationFormText = "lGRANDTOTAL1"
        Me.lGRANDTOTAL1.Location = New System.Drawing.Point(384, 195)
        Me.lGRANDTOTAL1.Name = "lGRANDTOTAL1"
        Me.lGRANDTOTAL1.Size = New System.Drawing.Size(149, 30)
        Me.lGRANDTOTAL1.Text = "lGRANDTOTAL1"
        Me.lGRANDTOTAL1.TextSize = New System.Drawing.Size(0, 0)
        Me.lGRANDTOTAL1.TextToControlDistance = 0
        Me.lGRANDTOTAL1.TextVisible = False
        '
        'lMEMO1
        '
        Me.lMEMO1.Control = Me.txtMEMO1
        Me.lMEMO1.CustomizationFormText = "lMEMO1"
        Me.lMEMO1.Location = New System.Drawing.Point(533, 195)
        Me.lMEMO1.Name = "lMEMO1"
        Me.lMEMO1.Size = New System.Drawing.Size(354, 30)
        Me.lMEMO1.Text = "lMEMO1"
        Me.lMEMO1.TextSize = New System.Drawing.Size(0, 0)
        Me.lMEMO1.TextToControlDistance = 0
        Me.lMEMO1.TextVisible = False
        '
        'lPayment2
        '
        Me.lPayment2.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.lPayment2.AppearanceItemCaption.Options.UseFont = True
        Me.lPayment2.AppearanceItemCaption.Options.UseTextOptions = True
        Me.lPayment2.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.lPayment2.Control = Me.txtKDPAYMENTTYPE2
        Me.lPayment2.CustomizationFormText = "LayoutControlItem1"
        Me.lPayment2.Location = New System.Drawing.Point(0, 225)
        Me.lPayment2.Name = "lPayment2"
        Me.lPayment2.Size = New System.Drawing.Size(178, 30)
        Me.lPayment2.Text = "Ctrl + F2"
        Me.lPayment2.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.lPayment2.TextSize = New System.Drawing.Size(50, 0)
        Me.lPayment2.TextToControlDistance = 5
        '
        'lPayment3
        '
        Me.lPayment3.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.lPayment3.AppearanceItemCaption.Options.UseFont = True
        Me.lPayment3.AppearanceItemCaption.Options.UseTextOptions = True
        Me.lPayment3.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.lPayment3.Control = Me.txtKDPAYMENTTYPE3
        Me.lPayment3.CustomizationFormText = "F3"
        Me.lPayment3.Location = New System.Drawing.Point(0, 255)
        Me.lPayment3.Name = "lPayment3"
        Me.lPayment3.Size = New System.Drawing.Size(178, 30)
        Me.lPayment3.Text = "Ctrl + F3"
        Me.lPayment3.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.lPayment3.TextSize = New System.Drawing.Size(50, 0)
        Me.lPayment3.TextToControlDistance = 5
        '
        'lPayment4
        '
        Me.lPayment4.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.lPayment4.AppearanceItemCaption.Options.UseFont = True
        Me.lPayment4.AppearanceItemCaption.Options.UseTextOptions = True
        Me.lPayment4.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.lPayment4.Control = Me.txtKDPAYMENTTYPE4
        Me.lPayment4.CustomizationFormText = "Ctrl + F4"
        Me.lPayment4.Location = New System.Drawing.Point(0, 285)
        Me.lPayment4.Name = "lPayment4"
        Me.lPayment4.Size = New System.Drawing.Size(178, 30)
        Me.lPayment4.Text = "Ctrl + F4"
        Me.lPayment4.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.lPayment4.TextSize = New System.Drawing.Size(50, 0)
        Me.lPayment4.TextToControlDistance = 5
        '
        'lPayment5
        '
        Me.lPayment5.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.lPayment5.AppearanceItemCaption.Options.UseFont = True
        Me.lPayment5.AppearanceItemCaption.Options.UseTextOptions = True
        Me.lPayment5.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.lPayment5.Control = Me.txtKDPAYMENTTYPE5
        Me.lPayment5.CustomizationFormText = "Ctrl + F5"
        Me.lPayment5.Location = New System.Drawing.Point(0, 315)
        Me.lPayment5.Name = "lPayment5"
        Me.lPayment5.Size = New System.Drawing.Size(178, 30)
        Me.lPayment5.Text = "Ctrl + F5"
        Me.lPayment5.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.lPayment5.TextSize = New System.Drawing.Size(50, 0)
        Me.lPayment5.TextToControlDistance = 5
        '
        'lGRANDTOTAL2
        '
        Me.lGRANDTOTAL2.Control = Me.txtGRANDTOTAL2
        Me.lGRANDTOTAL2.CustomizationFormText = "lGRANDTOTAL2"
        Me.lGRANDTOTAL2.Location = New System.Drawing.Point(384, 225)
        Me.lGRANDTOTAL2.Name = "lGRANDTOTAL2"
        Me.lGRANDTOTAL2.Size = New System.Drawing.Size(149, 30)
        Me.lGRANDTOTAL2.Text = "lGRANDTOTAL2"
        Me.lGRANDTOTAL2.TextSize = New System.Drawing.Size(0, 0)
        Me.lGRANDTOTAL2.TextToControlDistance = 0
        Me.lGRANDTOTAL2.TextVisible = False
        '
        'lGRANDTOTAL3
        '
        Me.lGRANDTOTAL3.Control = Me.txtGRANDTOTAL3
        Me.lGRANDTOTAL3.CustomizationFormText = "lGRANDTOTAL3"
        Me.lGRANDTOTAL3.Location = New System.Drawing.Point(384, 255)
        Me.lGRANDTOTAL3.Name = "lGRANDTOTAL3"
        Me.lGRANDTOTAL3.Size = New System.Drawing.Size(149, 30)
        Me.lGRANDTOTAL3.Text = "lGRANDTOTAL3"
        Me.lGRANDTOTAL3.TextSize = New System.Drawing.Size(0, 0)
        Me.lGRANDTOTAL3.TextToControlDistance = 0
        Me.lGRANDTOTAL3.TextVisible = False
        '
        'lGRANDTOTAL4
        '
        Me.lGRANDTOTAL4.Control = Me.txtGRANDTOTAL4
        Me.lGRANDTOTAL4.CustomizationFormText = "lGRANDTOTAL4"
        Me.lGRANDTOTAL4.Location = New System.Drawing.Point(384, 285)
        Me.lGRANDTOTAL4.Name = "lGRANDTOTAL4"
        Me.lGRANDTOTAL4.Size = New System.Drawing.Size(149, 30)
        Me.lGRANDTOTAL4.Text = "lGRANDTOTAL4"
        Me.lGRANDTOTAL4.TextSize = New System.Drawing.Size(0, 0)
        Me.lGRANDTOTAL4.TextToControlDistance = 0
        Me.lGRANDTOTAL4.TextVisible = False
        '
        'lGRANDTOTAL5
        '
        Me.lGRANDTOTAL5.Control = Me.txtGRANDTOTAL5
        Me.lGRANDTOTAL5.CustomizationFormText = "lGRANDTOTAL5"
        Me.lGRANDTOTAL5.Location = New System.Drawing.Point(384, 315)
        Me.lGRANDTOTAL5.Name = "lGRANDTOTAL5"
        Me.lGRANDTOTAL5.Size = New System.Drawing.Size(149, 30)
        Me.lGRANDTOTAL5.Text = "lGRANDTOTAL5"
        Me.lGRANDTOTAL5.TextSize = New System.Drawing.Size(0, 0)
        Me.lGRANDTOTAL5.TextToControlDistance = 0
        Me.lGRANDTOTAL5.TextVisible = False
        '
        'lMEMO2
        '
        Me.lMEMO2.Control = Me.txtMEMO2
        Me.lMEMO2.CustomizationFormText = "lMEMO2"
        Me.lMEMO2.Location = New System.Drawing.Point(533, 225)
        Me.lMEMO2.Name = "lMEMO2"
        Me.lMEMO2.Size = New System.Drawing.Size(354, 30)
        Me.lMEMO2.Text = "lMEMO2"
        Me.lMEMO2.TextSize = New System.Drawing.Size(0, 0)
        Me.lMEMO2.TextToControlDistance = 0
        Me.lMEMO2.TextVisible = False
        '
        'lMEMO3
        '
        Me.lMEMO3.Control = Me.txtMEMO3
        Me.lMEMO3.CustomizationFormText = "lMEMO3"
        Me.lMEMO3.Location = New System.Drawing.Point(533, 255)
        Me.lMEMO3.Name = "lMEMO3"
        Me.lMEMO3.Size = New System.Drawing.Size(354, 30)
        Me.lMEMO3.Text = "lMEMO3"
        Me.lMEMO3.TextSize = New System.Drawing.Size(0, 0)
        Me.lMEMO3.TextToControlDistance = 0
        Me.lMEMO3.TextVisible = False
        '
        'lMEMO4
        '
        Me.lMEMO4.Control = Me.txtMEMO4
        Me.lMEMO4.CustomizationFormText = "lMEMO4"
        Me.lMEMO4.Location = New System.Drawing.Point(533, 285)
        Me.lMEMO4.Name = "lMEMO4"
        Me.lMEMO4.Size = New System.Drawing.Size(354, 30)
        Me.lMEMO4.Text = "lMEMO4"
        Me.lMEMO4.TextSize = New System.Drawing.Size(0, 0)
        Me.lMEMO4.TextToControlDistance = 0
        Me.lMEMO4.TextVisible = False
        '
        'lMEMO5
        '
        Me.lMEMO5.Control = Me.txtMEMO5
        Me.lMEMO5.CustomizationFormText = "lMEMO5"
        Me.lMEMO5.Location = New System.Drawing.Point(533, 315)
        Me.lMEMO5.Name = "lMEMO5"
        Me.lMEMO5.Size = New System.Drawing.Size(354, 30)
        Me.lMEMO5.Text = "lMEMO5"
        Me.lMEMO5.TextSize = New System.Drawing.Size(0, 0)
        Me.lMEMO5.TextToControlDistance = 0
        Me.lMEMO5.TextVisible = False
        '
        'LayoutControlItem1
        '
        Me.LayoutControlItem1.Control = Me.LayoutControl2
        Me.LayoutControlItem1.CustomizationFormText = "LayoutControlItem1"
        Me.LayoutControlItem1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem1.Name = "LayoutControlItem1"
        Me.LayoutControlItem1.Size = New System.Drawing.Size(887, 195)
        Me.LayoutControlItem1.Text = "LayoutControlItem1"
        Me.LayoutControlItem1.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem1.TextToControlDistance = 0
        Me.LayoutControlItem1.TextVisible = False
        '
        'LayoutControlItem2
        '
        Me.LayoutControlItem2.Control = Me.txtNAME1
        Me.LayoutControlItem2.CustomizationFormText = "LayoutControlItem2"
        Me.LayoutControlItem2.Location = New System.Drawing.Point(178, 195)
        Me.LayoutControlItem2.Name = "LayoutControlItem2"
        Me.LayoutControlItem2.Size = New System.Drawing.Size(206, 30)
        Me.LayoutControlItem2.Text = "LayoutControlItem2"
        Me.LayoutControlItem2.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem2.TextToControlDistance = 0
        Me.LayoutControlItem2.TextVisible = False
        '
        'LayoutControlItem3
        '
        Me.LayoutControlItem3.Control = Me.txtNAME2
        Me.LayoutControlItem3.CustomizationFormText = "LayoutControlItem3"
        Me.LayoutControlItem3.Location = New System.Drawing.Point(178, 225)
        Me.LayoutControlItem3.Name = "LayoutControlItem3"
        Me.LayoutControlItem3.Size = New System.Drawing.Size(206, 30)
        Me.LayoutControlItem3.Text = "LayoutControlItem3"
        Me.LayoutControlItem3.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem3.TextToControlDistance = 0
        Me.LayoutControlItem3.TextVisible = False
        '
        'LayoutControlItem4
        '
        Me.LayoutControlItem4.Control = Me.txtNAME3
        Me.LayoutControlItem4.CustomizationFormText = "LayoutControlItem4"
        Me.LayoutControlItem4.Location = New System.Drawing.Point(178, 255)
        Me.LayoutControlItem4.Name = "LayoutControlItem4"
        Me.LayoutControlItem4.Size = New System.Drawing.Size(206, 30)
        Me.LayoutControlItem4.Text = "LayoutControlItem4"
        Me.LayoutControlItem4.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem4.TextToControlDistance = 0
        Me.LayoutControlItem4.TextVisible = False
        '
        'LayoutControlItem5
        '
        Me.LayoutControlItem5.Control = Me.txtNAME4
        Me.LayoutControlItem5.CustomizationFormText = "LayoutControlItem5"
        Me.LayoutControlItem5.Location = New System.Drawing.Point(178, 285)
        Me.LayoutControlItem5.Name = "LayoutControlItem5"
        Me.LayoutControlItem5.Size = New System.Drawing.Size(206, 30)
        Me.LayoutControlItem5.Text = "LayoutControlItem5"
        Me.LayoutControlItem5.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem5.TextToControlDistance = 0
        Me.LayoutControlItem5.TextVisible = False
        '
        'LayoutControlItem6
        '
        Me.LayoutControlItem6.Control = Me.txtNAME5
        Me.LayoutControlItem6.CustomizationFormText = "LayoutControlItem6"
        Me.LayoutControlItem6.Location = New System.Drawing.Point(178, 315)
        Me.LayoutControlItem6.Name = "LayoutControlItem6"
        Me.LayoutControlItem6.Size = New System.Drawing.Size(206, 30)
        Me.LayoutControlItem6.Text = "LayoutControlItem6"
        Me.LayoutControlItem6.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem6.TextToControlDistance = 0
        Me.LayoutControlItem6.TextVisible = False
        '
        'frmPayment
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(907, 388)
        Me.Controls.Add(Me.LayoutControl1)
        Me.Controls.Add(Me.barDockControlLeft)
        Me.Controls.Add(Me.barDockControlRight)
        Me.Controls.Add(Me.barDockControlBottom)
        Me.Controls.Add(Me.barDockControlTop)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.KeyPreview = True
        Me.Name = "frmPayment"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl1.ResumeLayout(False)
        CType(Me.txtNAME5.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.mnuManager, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNAME4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNAME3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNAME2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNAME1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl2.ResumeLayout(False)
        CType(Me.txtBalance.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtKDPAYMENTTYPE.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTotal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lGRANDTOTAL, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lCHANGE, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lPAY, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtMEMO5.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtMEMO4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtMEMO3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtMEMO2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtGRANDTOTAL5.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtGRANDTOTAL4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtGRANDTOTAL3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtGRANDTOTAL2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtKDPAYMENTTYPE5.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtKDPAYMENTTYPE4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtKDPAYMENTTYPE3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtKDPAYMENTTYPE2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtMEMO1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtGRANDTOTAL1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtKDPAYMENTTYPE1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lPayment1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lGRANDTOTAL1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lMEMO1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lPayment2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lPayment3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lPayment4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lPayment5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lGRANDTOTAL2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lGRANDTOTAL3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lGRANDTOTAL4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lGRANDTOTAL5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lMEMO2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lMEMO3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lMEMO4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lMEMO5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents LayoutControl1 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents txtKDPAYMENTTYPE1 As DevExpress.XtraEditors.ButtonEdit
    Friend WithEvents lPayment1 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents txtGRANDTOTAL1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lGRANDTOTAL1 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents txtMEMO1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lMEMO1 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents txtKDPAYMENTTYPE2 As DevExpress.XtraEditors.ButtonEdit
    Friend WithEvents lPayment2 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents txtKDPAYMENTTYPE3 As DevExpress.XtraEditors.ButtonEdit
    Friend WithEvents lPayment3 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents txtKDPAYMENTTYPE4 As DevExpress.XtraEditors.ButtonEdit
    Friend WithEvents lPayment4 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents txtKDPAYMENTTYPE5 As DevExpress.XtraEditors.ButtonEdit
    Friend WithEvents lPayment5 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents txtGRANDTOTAL2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lGRANDTOTAL2 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents txtGRANDTOTAL5 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtGRANDTOTAL4 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtGRANDTOTAL3 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lGRANDTOTAL3 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents lGRANDTOTAL4 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents lGRANDTOTAL5 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents txtMEMO5 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtMEMO4 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtMEMO3 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtMEMO2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lMEMO2 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents lMEMO3 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents lMEMO4 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents lMEMO5 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControl2 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents LayoutControlGroup2 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents txtTotal As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lGRANDTOTAL As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents txtKDPAYMENTTYPE As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lPAY As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents txtBalance As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lCHANGE As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents mnuManager As DevExpress.XtraBars.BarManager
    Friend WithEvents Bar3 As DevExpress.XtraBars.Bar
    Friend WithEvents barDockControlTop As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlBottom As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlLeft As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlRight As DevExpress.XtraBars.BarDockControl
    Friend WithEvents BarStaticItem1 As DevExpress.XtraBars.BarStaticItem
    Friend WithEvents lblNoTransaction As DevExpress.XtraBars.BarStaticItem
    Friend WithEvents btnSave As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnClose As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents txtNAME1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem2 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents txtNAME5 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtNAME4 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtNAME3 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtNAME2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem3 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem4 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem5 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem6 As DevExpress.XtraLayout.LayoutControlItem
End Class
