﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'     Runtime Version:4.0.30319.17929
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated.
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On

Imports System

Namespace My.Resources
    
    'This class was auto-generated by the StronglyTypedResourceBuilder
    'class via a tool like ResGen or Visual Studio.
    'To add or remove a member, edit your .ResX file then rerun ResGen
    'with the /str option, or rebuild your VS project.
    '''<summary>
    '''  A strongly-typed resource class, for looking up localized strings, etc.
    '''</summary>
    <Global.System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0"),  _
     Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
     Global.System.Runtime.CompilerServices.CompilerGeneratedAttribute()>  _
    Public Class CashIn
        
        Private Shared resourceMan As Global.System.Resources.ResourceManager
        
        Private Shared resourceCulture As Global.System.Globalization.CultureInfo
        
        <Global.System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")>  _
        Friend Sub New()
            MyBase.New
        End Sub
        
        '''<summary>
        '''  Returns the cached ResourceManager instance used by this class.
        '''</summary>
        <Global.System.ComponentModel.EditorBrowsableAttribute(Global.System.ComponentModel.EditorBrowsableState.Advanced)>  _
        Public Shared ReadOnly Property ResourceManager() As Global.System.Resources.ResourceManager
            Get
                If Object.ReferenceEquals(resourceMan, Nothing) Then
                    Dim temp As Global.System.Resources.ResourceManager = New Global.System.Resources.ResourceManager("UI.WIN.MAIN.CashIn", GetType(CashIn).Assembly)
                    resourceMan = temp
                End If
                Return resourceMan
            End Get
        End Property
        
        '''<summary>
        '''  Overrides the current thread's CurrentUICulture property for all
        '''  resource lookups using this strongly typed resource class.
        '''</summary>
        <Global.System.ComponentModel.EditorBrowsableAttribute(Global.System.ComponentModel.EditorBrowsableState.Advanced)>  _
        Public Shared Property Culture() As Global.System.Globalization.CultureInfo
            Get
                Return resourceCulture
            End Get
            Set
                resourceCulture = value
            End Set
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Admin.
        '''</summary>
        Public Shared ReadOnly Property ADMIN() As String
            Get
                Return ResourceManager.GetString("ADMIN", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Amount Due.
        '''</summary>
        Public Shared ReadOnly Property DETAIL_AMOUNTDUE() As String
            Get
                Return ResourceManager.GetString("DETAIL_AMOUNTDUE", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Amount Original.
        '''</summary>
        Public Shared ReadOnly Property DETAIL_AMOUNTORIGINAL() As String
            Get
                Return ResourceManager.GetString("DETAIL_AMOUNTORIGINAL", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Amount Payment.
        '''</summary>
        Public Shared ReadOnly Property DETAIL_AMOUNTPAYMENT() As String
            Get
                Return ResourceManager.GetString("DETAIL_AMOUNTPAYMENT", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Invoice Number.
        '''</summary>
        Public Shared ReadOnly Property DETAIL_NOINVOICE() As String
            Get
                Return ResourceManager.GetString("DETAIL_NOINVOICE", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Remarks.
        '''</summary>
        Public Shared ReadOnly Property DETAIL_REMARKS() As String
            Get
                Return ResourceManager.GetString("DETAIL_REMARKS", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Grand Total.
        '''</summary>
        Public Shared ReadOnly Property GRANDTOTAL() As String
            Get
                Return ResourceManager.GetString("GRANDTOTAL", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Number.
        '''</summary>
        Public Shared ReadOnly Property KDCASHIN() As String
            Get
                Return ResourceManager.GetString("KDCASHIN", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Customer.
        '''</summary>
        Public Shared ReadOnly Property KDCUSTOMER() As String
            Get
                Return ResourceManager.GetString("KDCUSTOMER", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Payment Type.
        '''</summary>
        Public Shared ReadOnly Property KDPAYMENTTYPE() As String
            Get
                Return ResourceManager.GetString("KDPAYMENTTYPE", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Memo.
        '''</summary>
        Public Shared ReadOnly Property MEMO() As String
            Get
                Return ResourceManager.GetString("MEMO", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Round.
        '''</summary>
        Public Shared ReadOnly Property ROUND() As String
            Get
                Return ResourceManager.GetString("ROUND", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Sub Total.
        '''</summary>
        Public Shared ReadOnly Property SUBTOTAL() As String
            Get
                Return ResourceManager.GetString("SUBTOTAL", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Detail Information.
        '''</summary>
        Public Shared ReadOnly Property TAB_DETAIL() As String
            Get
                Return ResourceManager.GetString("TAB_DETAIL", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Detail Return Information.
        '''</summary>
        Public Shared ReadOnly Property TAB_DETAILRETURN() As String
            Get
                Return ResourceManager.GetString("TAB_DETAILRETURN", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Memo Information.
        '''</summary>
        Public Shared ReadOnly Property TAB_MEMO() As String
            Get
                Return ResourceManager.GetString("TAB_MEMO", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Date.
        '''</summary>
        Public Shared ReadOnly Property TANGGAL() As String
            Get
                Return ResourceManager.GetString("TANGGAL", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Cash In.
        '''</summary>
        Public Shared ReadOnly Property TITLE() As String
            Get
                Return ResourceManager.GetString("TITLE", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Report Receiveables.
        '''</summary>
        Public Shared ReadOnly Property TITLE_RECEIVEABLES() As String
            Get
                Return ResourceManager.GetString("TITLE_RECEIVEABLES", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Report Cash In.
        '''</summary>
        Public Shared ReadOnly Property TITLE_REPORT() As String
            Get
                Return ResourceManager.GetString("TITLE_REPORT", resourceCulture)
            End Get
        End Property
    End Class
End Namespace
