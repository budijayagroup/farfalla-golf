﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCOAList
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmCOAList))
        Me.splitContainerControl = New DevExpress.XtraEditors.SplitContainerControl()
        Me.tree = New DevExpress.XtraTreeList.TreeList()
        Me.colKDCOA = New DevExpress.XtraTreeList.Columns.TreeListColumn()
        Me.colNMCOA = New DevExpress.XtraTreeList.Columns.TreeListColumn()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.picAdd = New DevExpress.XtraEditors.PictureEdit()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.picRefresh = New DevExpress.XtraEditors.PictureEdit()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.picUpdate = New DevExpress.XtraEditors.PictureEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.picDelete = New DevExpress.XtraEditors.PictureEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.picPrint = New DevExpress.XtraEditors.PictureEdit()
        Me.SplitterControl1 = New DevExpress.XtraEditors.SplitterControl()
        Me.groupControlInfo = New DevExpress.XtraEditors.GroupControl()
        Me.lc = New DevExpress.XtraLayout.LayoutControl()
        Me.btnView = New DevExpress.XtraEditors.SimpleButton()
        Me.txtDC = New DevExpress.XtraEditors.TextEdit()
        Me.txtGROUPS = New DevExpress.XtraEditors.TextEdit()
        Me.txtTYPE = New DevExpress.XtraEditors.TextEdit()
        Me.txtPARENTKDCOA = New DevExpress.XtraEditors.TextEdit()
        Me.txtNMCOA = New DevExpress.XtraEditors.TextEdit()
        Me.txtKDCOA = New DevExpress.XtraEditors.TextEdit()
        Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.lKDCOA = New DevExpress.XtraLayout.LayoutControlItem()
        Me.lNMCOA = New DevExpress.XtraLayout.LayoutControlItem()
        Me.lPARENTKDCOA = New DevExpress.XtraLayout.LayoutControlItem()
        Me.lTYPE = New DevExpress.XtraLayout.LayoutControlItem()
        Me.lGROUPS = New DevExpress.XtraLayout.LayoutControlItem()
        Me.lDC = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem11 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem2 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.BarButtonItem1 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem2 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem3 = New DevExpress.XtraBars.BarButtonItem()
        CType(Me.splitContainerControl, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.splitContainerControl.SuspendLayout()
        CType(Me.tree, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.picAdd.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picRefresh.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picUpdate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picDelete.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picPrint.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.groupControlInfo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.groupControlInfo.SuspendLayout()
        CType(Me.lc, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.lc.SuspendLayout()
        CType(Me.txtDC.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtGROUPS.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTYPE.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtPARENTKDCOA.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNMCOA.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtKDCOA.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lKDCOA, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lNMCOA, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lPARENTKDCOA, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lTYPE, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lGROUPS, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lDC, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'splitContainerControl
        '
        Me.splitContainerControl.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel1
        Me.splitContainerControl.Dock = System.Windows.Forms.DockStyle.Fill
        Me.splitContainerControl.Location = New System.Drawing.Point(0, 0)
        Me.splitContainerControl.Name = "splitContainerControl"
        Me.splitContainerControl.Panel1.Controls.Add(Me.tree)
        Me.splitContainerControl.Panel1.Controls.Add(Me.PanelControl1)
        Me.splitContainerControl.Panel1.Text = "Panel1"
        Me.splitContainerControl.Panel2.Controls.Add(Me.SplitterControl1)
        Me.splitContainerControl.Panel2.Controls.Add(Me.groupControlInfo)
        Me.splitContainerControl.Panel2.Text = "Panel2"
        Me.splitContainerControl.Size = New System.Drawing.Size(792, 573)
        Me.splitContainerControl.SplitterPosition = 487
        Me.splitContainerControl.TabIndex = 0
        '
        'tree
        '
        Me.tree.Columns.AddRange(New DevExpress.XtraTreeList.Columns.TreeListColumn() {Me.colKDCOA, Me.colNMCOA})
        Me.tree.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tree.KeyFieldName = "KDCOA"
        Me.tree.Location = New System.Drawing.Point(0, 82)
        Me.tree.Name = "tree"
        Me.tree.OptionsBehavior.Editable = False
        Me.tree.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.tree.ParentFieldName = "PARENTKDCOA"
        Me.tree.PreviewFieldName = "PARENTKDCOA"
        Me.tree.Size = New System.Drawing.Size(487, 491)
        Me.tree.TabIndex = 8
        '
        'colKDCOA
        '
        Me.colKDCOA.Caption = "Akun"
        Me.colKDCOA.FieldName = "KDCOA"
        Me.colKDCOA.Name = "colKDCOA"
        Me.colKDCOA.Visible = True
        Me.colKDCOA.VisibleIndex = 0
        '
        'colNMCOA
        '
        Me.colNMCOA.Caption = "Name"
        Me.colNMCOA.FieldName = "NMCOA"
        Me.colNMCOA.Name = "colNMCOA"
        Me.colNMCOA.Visible = True
        Me.colNMCOA.VisibleIndex = 1
        '
        'PanelControl1
        '
        Me.PanelControl1.Controls.Add(Me.LabelControl6)
        Me.PanelControl1.Controls.Add(Me.picAdd)
        Me.PanelControl1.Controls.Add(Me.LabelControl7)
        Me.PanelControl1.Controls.Add(Me.picRefresh)
        Me.PanelControl1.Controls.Add(Me.LabelControl4)
        Me.PanelControl1.Controls.Add(Me.picUpdate)
        Me.PanelControl1.Controls.Add(Me.LabelControl3)
        Me.PanelControl1.Controls.Add(Me.picDelete)
        Me.PanelControl1.Controls.Add(Me.LabelControl1)
        Me.PanelControl1.Controls.Add(Me.picPrint)
        Me.PanelControl1.Dock = System.Windows.Forms.DockStyle.Top
        Me.PanelControl1.Location = New System.Drawing.Point(0, 0)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(487, 82)
        Me.PanelControl1.TabIndex = 1
        '
        'LabelControl6
        '
        Me.LabelControl6.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.LabelControl6.Location = New System.Drawing.Point(236, 60)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(44, 13)
        Me.LabelControl6.TabIndex = 13
        Me.LabelControl6.Text = "&Refresh"
        '
        'picAdd
        '
        Me.picAdd.EditValue = CType(resources.GetObject("picAdd.EditValue"), Object)
        Me.picAdd.Location = New System.Drawing.Point(18, 10)
        Me.picAdd.Name = "picAdd"
        Me.picAdd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.picAdd.Properties.Appearance.Options.UseBackColor = True
        Me.picAdd.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.picAdd.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom
        Me.picAdd.Size = New System.Drawing.Size(48, 48)
        Me.picAdd.TabIndex = 8
        '
        'LabelControl7
        '
        Me.LabelControl7.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.LabelControl7.Location = New System.Drawing.Point(191, 60)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(27, 13)
        Me.LabelControl7.TabIndex = 10
        Me.LabelControl7.Text = "&Print"
        '
        'picRefresh
        '
        Me.picRefresh.EditValue = CType(resources.GetObject("picRefresh.EditValue"), Object)
        Me.picRefresh.Location = New System.Drawing.Point(234, 10)
        Me.picRefresh.Name = "picRefresh"
        Me.picRefresh.Properties.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.picRefresh.Properties.Appearance.Options.UseBackColor = True
        Me.picRefresh.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.picRefresh.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom
        Me.picRefresh.Size = New System.Drawing.Size(48, 48)
        Me.picRefresh.TabIndex = 12
        '
        'LabelControl4
        '
        Me.LabelControl4.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.LabelControl4.Location = New System.Drawing.Point(132, 60)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(37, 13)
        Me.LabelControl4.TabIndex = 11
        Me.LabelControl4.Text = "&Delete"
        '
        'picUpdate
        '
        Me.picUpdate.EditValue = CType(resources.GetObject("picUpdate.EditValue"), Object)
        Me.picUpdate.Location = New System.Drawing.Point(72, 10)
        Me.picUpdate.Name = "picUpdate"
        Me.picUpdate.Properties.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.picUpdate.Properties.Appearance.Options.UseBackColor = True
        Me.picUpdate.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.picUpdate.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom
        Me.picUpdate.Size = New System.Drawing.Size(48, 48)
        Me.picUpdate.TabIndex = 6
        '
        'LabelControl3
        '
        Me.LabelControl3.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.LabelControl3.Location = New System.Drawing.Point(86, 60)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(21, 13)
        Me.LabelControl3.TabIndex = 9
        Me.LabelControl3.Text = "&Edit"
        '
        'picDelete
        '
        Me.picDelete.EditValue = CType(resources.GetObject("picDelete.EditValue"), Object)
        Me.picDelete.Location = New System.Drawing.Point(126, 10)
        Me.picDelete.Name = "picDelete"
        Me.picDelete.Properties.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.picDelete.Properties.Appearance.Options.UseBackColor = True
        Me.picDelete.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.picDelete.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom
        Me.picDelete.Size = New System.Drawing.Size(48, 48)
        Me.picDelete.TabIndex = 7
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.LabelControl1.Location = New System.Drawing.Point(31, 60)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(22, 13)
        Me.LabelControl1.TabIndex = 5
        Me.LabelControl1.Text = "&Add"
        '
        'picPrint
        '
        Me.picPrint.EditValue = CType(resources.GetObject("picPrint.EditValue"), Object)
        Me.picPrint.Location = New System.Drawing.Point(180, 10)
        Me.picPrint.Name = "picPrint"
        Me.picPrint.Properties.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.picPrint.Properties.Appearance.Options.UseBackColor = True
        Me.picPrint.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.picPrint.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom
        Me.picPrint.Size = New System.Drawing.Size(48, 48)
        Me.picPrint.TabIndex = 4
        '
        'SplitterControl1
        '
        Me.SplitterControl1.Dock = System.Windows.Forms.DockStyle.Top
        Me.SplitterControl1.Location = New System.Drawing.Point(0, 360)
        Me.SplitterControl1.Name = "SplitterControl1"
        Me.SplitterControl1.Size = New System.Drawing.Size(300, 5)
        Me.SplitterControl1.TabIndex = 1
        Me.SplitterControl1.TabStop = False
        '
        'groupControlInfo
        '
        Me.groupControlInfo.Controls.Add(Me.lc)
        Me.groupControlInfo.Dock = System.Windows.Forms.DockStyle.Top
        Me.groupControlInfo.Location = New System.Drawing.Point(0, 0)
        Me.groupControlInfo.Name = "groupControlInfo"
        Me.groupControlInfo.Size = New System.Drawing.Size(300, 360)
        Me.groupControlInfo.TabIndex = 0
        Me.groupControlInfo.Text = "Akun Information"
        '
        'lc
        '
        Me.lc.Controls.Add(Me.btnView)
        Me.lc.Controls.Add(Me.txtDC)
        Me.lc.Controls.Add(Me.txtGROUPS)
        Me.lc.Controls.Add(Me.txtTYPE)
        Me.lc.Controls.Add(Me.txtPARENTKDCOA)
        Me.lc.Controls.Add(Me.txtNMCOA)
        Me.lc.Controls.Add(Me.txtKDCOA)
        Me.lc.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lc.Location = New System.Drawing.Point(2, 20)
        Me.lc.Name = "lc"
        Me.lc.Root = Me.LayoutControlGroup1
        Me.lc.Size = New System.Drawing.Size(296, 338)
        Me.lc.TabIndex = 0
        Me.lc.Text = "LayoutControl1"
        '
        'btnView
        '
        Me.btnView.Location = New System.Drawing.Point(12, 156)
        Me.btnView.Name = "btnView"
        Me.btnView.Size = New System.Drawing.Size(272, 22)
        Me.btnView.StyleController = Me.lc
        Me.btnView.TabIndex = 13
        Me.btnView.TabStop = False
        Me.btnView.Text = "View"
        '
        'txtDC
        '
        Me.txtDC.EnterMoveNextControl = True
        Me.txtDC.Location = New System.Drawing.Point(117, 132)
        Me.txtDC.Name = "txtDC"
        Me.txtDC.Properties.ReadOnly = True
        Me.txtDC.Size = New System.Drawing.Size(167, 20)
        Me.txtDC.StyleController = Me.lc
        Me.txtDC.TabIndex = 9
        Me.txtDC.TabStop = False
        '
        'txtGROUPS
        '
        Me.txtGROUPS.EnterMoveNextControl = True
        Me.txtGROUPS.Location = New System.Drawing.Point(117, 108)
        Me.txtGROUPS.Name = "txtGROUPS"
        Me.txtGROUPS.Properties.ReadOnly = True
        Me.txtGROUPS.Size = New System.Drawing.Size(167, 20)
        Me.txtGROUPS.StyleController = Me.lc
        Me.txtGROUPS.TabIndex = 8
        Me.txtGROUPS.TabStop = False
        '
        'txtTYPE
        '
        Me.txtTYPE.EnterMoveNextControl = True
        Me.txtTYPE.Location = New System.Drawing.Point(117, 84)
        Me.txtTYPE.Name = "txtTYPE"
        Me.txtTYPE.Properties.ReadOnly = True
        Me.txtTYPE.Size = New System.Drawing.Size(167, 20)
        Me.txtTYPE.StyleController = Me.lc
        Me.txtTYPE.TabIndex = 7
        Me.txtTYPE.TabStop = False
        '
        'txtPARENTKDCOA
        '
        Me.txtPARENTKDCOA.EnterMoveNextControl = True
        Me.txtPARENTKDCOA.Location = New System.Drawing.Point(117, 60)
        Me.txtPARENTKDCOA.Name = "txtPARENTKDCOA"
        Me.txtPARENTKDCOA.Properties.ReadOnly = True
        Me.txtPARENTKDCOA.Size = New System.Drawing.Size(167, 20)
        Me.txtPARENTKDCOA.StyleController = Me.lc
        Me.txtPARENTKDCOA.TabIndex = 6
        Me.txtPARENTKDCOA.TabStop = False
        '
        'txtNMCOA
        '
        Me.txtNMCOA.EnterMoveNextControl = True
        Me.txtNMCOA.Location = New System.Drawing.Point(117, 36)
        Me.txtNMCOA.Name = "txtNMCOA"
        Me.txtNMCOA.Properties.ReadOnly = True
        Me.txtNMCOA.Size = New System.Drawing.Size(167, 20)
        Me.txtNMCOA.StyleController = Me.lc
        Me.txtNMCOA.TabIndex = 5
        Me.txtNMCOA.TabStop = False
        '
        'txtKDCOA
        '
        Me.txtKDCOA.EnterMoveNextControl = True
        Me.txtKDCOA.Location = New System.Drawing.Point(117, 12)
        Me.txtKDCOA.Name = "txtKDCOA"
        Me.txtKDCOA.Properties.ReadOnly = True
        Me.txtKDCOA.Size = New System.Drawing.Size(167, 20)
        Me.txtKDCOA.StyleController = Me.lc
        Me.txtKDCOA.TabIndex = 4
        Me.txtKDCOA.TabStop = False
        '
        'LayoutControlGroup1
        '
        Me.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1"
        Me.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup1.GroupBordersVisible = False
        Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.lKDCOA, Me.lNMCOA, Me.lPARENTKDCOA, Me.lTYPE, Me.lGROUPS, Me.lDC, Me.LayoutControlItem11})
        Me.LayoutControlGroup1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup1.Name = "LayoutControlGroup1"
        Me.LayoutControlGroup1.Size = New System.Drawing.Size(296, 338)
        Me.LayoutControlGroup1.TextVisible = False
        '
        'lKDCOA
        '
        Me.lKDCOA.AppearanceItemCaption.Options.UseTextOptions = True
        Me.lKDCOA.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.lKDCOA.Control = Me.txtKDCOA
        Me.lKDCOA.CustomizationFormText = "Tampilan Name :"
        Me.lKDCOA.Location = New System.Drawing.Point(0, 0)
        Me.lKDCOA.Name = "lKDCOA"
        Me.lKDCOA.Size = New System.Drawing.Size(276, 24)
        Me.lKDCOA.Text = "Akun :"
        Me.lKDCOA.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.lKDCOA.TextSize = New System.Drawing.Size(100, 20)
        Me.lKDCOA.TextToControlDistance = 5
        '
        'lNMCOA
        '
        Me.lNMCOA.AppearanceItemCaption.Options.UseTextOptions = True
        Me.lNMCOA.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.lNMCOA.Control = Me.txtNMCOA
        Me.lNMCOA.CustomizationFormText = "Company :"
        Me.lNMCOA.Location = New System.Drawing.Point(0, 24)
        Me.lNMCOA.Name = "lNMCOA"
        Me.lNMCOA.Size = New System.Drawing.Size(276, 24)
        Me.lNMCOA.Text = "Name :"
        Me.lNMCOA.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.lNMCOA.TextSize = New System.Drawing.Size(100, 20)
        Me.lNMCOA.TextToControlDistance = 5
        '
        'lPARENTKDCOA
        '
        Me.lPARENTKDCOA.AppearanceItemCaption.Options.UseTextOptions = True
        Me.lPARENTKDCOA.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.lPARENTKDCOA.Control = Me.txtPARENTKDCOA
        Me.lPARENTKDCOA.CustomizationFormText = "Contact :"
        Me.lPARENTKDCOA.Location = New System.Drawing.Point(0, 48)
        Me.lPARENTKDCOA.Name = "lPARENTKDCOA"
        Me.lPARENTKDCOA.Size = New System.Drawing.Size(276, 24)
        Me.lPARENTKDCOA.Text = "Parent Account :"
        Me.lPARENTKDCOA.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.lPARENTKDCOA.TextSize = New System.Drawing.Size(100, 20)
        Me.lPARENTKDCOA.TextToControlDistance = 5
        '
        'lTYPE
        '
        Me.lTYPE.AppearanceItemCaption.Options.UseTextOptions = True
        Me.lTYPE.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.lTYPE.Control = Me.txtTYPE
        Me.lTYPE.CustomizationFormText = "Phone :"
        Me.lTYPE.Location = New System.Drawing.Point(0, 72)
        Me.lTYPE.Name = "lTYPE"
        Me.lTYPE.Size = New System.Drawing.Size(276, 24)
        Me.lTYPE.Text = "Type :"
        Me.lTYPE.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.lTYPE.TextSize = New System.Drawing.Size(100, 20)
        Me.lTYPE.TextToControlDistance = 5
        '
        'lGROUPS
        '
        Me.lGROUPS.AppearanceItemCaption.Options.UseTextOptions = True
        Me.lGROUPS.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.lGROUPS.Control = Me.txtGROUPS
        Me.lGROUPS.CustomizationFormText = "Fax :"
        Me.lGROUPS.Location = New System.Drawing.Point(0, 96)
        Me.lGROUPS.Name = "lGROUPS"
        Me.lGROUPS.Size = New System.Drawing.Size(276, 24)
        Me.lGROUPS.Text = "Group :"
        Me.lGROUPS.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.lGROUPS.TextSize = New System.Drawing.Size(100, 20)
        Me.lGROUPS.TextToControlDistance = 5
        '
        'lDC
        '
        Me.lDC.AppearanceItemCaption.Options.UseTextOptions = True
        Me.lDC.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.lDC.Control = Me.txtDC
        Me.lDC.CustomizationFormText = "Mobile :"
        Me.lDC.Location = New System.Drawing.Point(0, 120)
        Me.lDC.Name = "lDC"
        Me.lDC.Size = New System.Drawing.Size(276, 24)
        Me.lDC.Text = "Debit / Credit :"
        Me.lDC.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.lDC.TextSize = New System.Drawing.Size(100, 20)
        Me.lDC.TextToControlDistance = 5
        '
        'LayoutControlItem11
        '
        Me.LayoutControlItem11.Control = Me.btnView
        Me.LayoutControlItem11.CustomizationFormText = "LayoutControlItem11"
        Me.LayoutControlItem11.Location = New System.Drawing.Point(0, 144)
        Me.LayoutControlItem11.Name = "LayoutControlItem11"
        Me.LayoutControlItem11.Size = New System.Drawing.Size(276, 174)
        Me.LayoutControlItem11.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem11.TextVisible = False
        '
        'LayoutControlItem2
        '
        Me.LayoutControlItem2.AppearanceItemCaption.Options.UseTextOptions = True
        Me.LayoutControlItem2.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.LayoutControlItem2.Control = Me.txtKDCOA
        Me.LayoutControlItem2.CustomizationFormText = "Tampilan Name :"
        Me.LayoutControlItem2.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem2.Name = "LayoutControlItem1"
        Me.LayoutControlItem2.Size = New System.Drawing.Size(463, 208)
        Me.LayoutControlItem2.Text = "Tampilan Name :"
        Me.LayoutControlItem2.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem2.TextSize = New System.Drawing.Size(100, 20)
        Me.LayoutControlItem2.TextToControlDistance = 5
        '
        'LayoutControlItem1
        '
        Me.LayoutControlItem1.AppearanceItemCaption.Options.UseTextOptions = True
        Me.LayoutControlItem1.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.LayoutControlItem1.Control = Me.txtKDCOA
        Me.LayoutControlItem1.CustomizationFormText = "Tampilan Name :"
        Me.LayoutControlItem1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem1.Name = "lcName_Display"
        Me.LayoutControlItem1.Size = New System.Drawing.Size(463, 208)
        Me.LayoutControlItem1.Text = "Tampilan Name :"
        Me.LayoutControlItem1.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem1.TextSize = New System.Drawing.Size(100, 20)
        Me.LayoutControlItem1.TextToControlDistance = 5
        '
        'BarButtonItem1
        '
        Me.BarButtonItem1.Caption = "New"
        Me.BarButtonItem1.Id = 0
        Me.BarButtonItem1.Name = "BarButtonItem1"
        '
        'BarButtonItem2
        '
        Me.BarButtonItem2.Caption = "Edit"
        Me.BarButtonItem2.Id = 1
        Me.BarButtonItem2.Name = "BarButtonItem2"
        '
        'BarButtonItem3
        '
        Me.BarButtonItem3.Caption = "Delete"
        Me.BarButtonItem3.Id = 2
        Me.BarButtonItem3.Name = "BarButtonItem3"
        '
        'frmCOAList
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(792, 573)
        Me.Controls.Add(Me.splitContainerControl)
        Me.KeyPreview = True
        Me.Name = "frmCOAList"
        Me.ShowIcon = False
        Me.Text = "Akun - List Form"
        CType(Me.splitContainerControl, System.ComponentModel.ISupportInitialize).EndInit()
        Me.splitContainerControl.ResumeLayout(False)
        CType(Me.tree, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        Me.PanelControl1.PerformLayout()
        CType(Me.picAdd.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picRefresh.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picUpdate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picDelete.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picPrint.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.groupControlInfo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.groupControlInfo.ResumeLayout(False)
        CType(Me.lc, System.ComponentModel.ISupportInitialize).EndInit()
        Me.lc.ResumeLayout(False)
        CType(Me.txtDC.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtGROUPS.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTYPE.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtPARENTKDCOA.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNMCOA.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtKDCOA.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lKDCOA, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lNMCOA, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lPARENTKDCOA, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lTYPE, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lGROUPS, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lDC, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents splitContainerControl As DevExpress.XtraEditors.SplitContainerControl
    Friend WithEvents groupControlInfo As DevExpress.XtraEditors.GroupControl
    Friend WithEvents SplitterControl1 As DevExpress.XtraEditors.SplitterControl
    Friend WithEvents lc As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents txtKDCOA As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lKDCOA As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem2 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents txtDC As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtGROUPS As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtTYPE As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtPARENTKDCOA As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtNMCOA As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lNMCOA As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents lPARENTKDCOA As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents lTYPE As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents lGROUPS As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents lDC As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents BarButtonItem1 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem2 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem3 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents picRefresh As DevExpress.XtraEditors.PictureEdit
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents picPrint As DevExpress.XtraEditors.PictureEdit
    Friend WithEvents picDelete As DevExpress.XtraEditors.PictureEdit
    Friend WithEvents picUpdate As DevExpress.XtraEditors.PictureEdit
    Friend WithEvents picAdd As DevExpress.XtraEditors.PictureEdit
    Friend WithEvents btnView As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LayoutControlItem11 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents tree As DevExpress.XtraTreeList.TreeList
    Friend WithEvents colKDCOA As DevExpress.XtraTreeList.Columns.TreeListColumn
    Friend WithEvents colNMCOA As DevExpress.XtraTreeList.Columns.TreeListColumn
End Class
